﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;

public partial class Report : System.Web.UI.Page {
    PositionRequest positionRequest = new PositionRequest();
    String strErrorContact = "<p>To report this error, please contact the IT Support Center by <a href=\"mailto:ITSupportCenter@ccs.spokane.edu\">email</a> or phone: 533-HELP.<br />Include a screen shot of this page and as much information as possible.</p>";
    string[] dayWeek = Utility.GetDayWeek();
    protected void Page_Load(object sender, EventArgs e) {
        //check if the user is logged in
        HttpCookie cookie = Request.Cookies.Get("phatt2");
        if (cookie == null) {
            users user = new users(HttpContext.Current.User.Identity.Name);
        }

        panList.Visible = false;
        panView.Visible = false;
        cmdBack.Visible = false;
        cmdPrint.Visible = false;
        panError.Visible = false;
        panMsg.Visible = false;
        panEmployeeInfo.Visible = false;

        if (!IsPostBack) {
            hidOrderBy.Value = "dtSubmitted";
            hidDirection.Value = "DESC";
            String userType = Request.QueryString["type"];
            if (userType != null && userType != "") {

                //display search criteria
                panList.Visible = true;
                tblList.Visible = false;
                panMsg.Visible = true;

                String employeeID = Request.Cookies["phatt2"]["userctclinkid"];
                String userPermission = positionRequest.GetUserPermission(employeeID);
                if (userType == "ExecutiveAssistant" && userPermission == "Executive Assistant") {
                    //populate college/unit for executive assistants
                    String businessUnit = positionRequest.GetEmployeeBusinessUnit(employeeID);
                    if(businessUnit == "HR171") {
                        cboSearchCollegeUnit.Items.Add("SCC");
                    }else if(businessUnit == "HR172") {
                        cboSearchCollegeUnit.Items.Add("SFCC");
                    }else if(businessUnit == "HR170") {
                        cboSearchCollegeUnit.Items.Add("ALL");
                        cboSearchCollegeUnit.Items[0].Value = "";
                        cboSearchCollegeUnit.Items.Add("SCC");
                        cboSearchCollegeUnit.Items.Add("SFCC");
                        cboSearchCollegeUnit.Items.Add("District");
                    }
                } else {
                    //populate college/unit for all other user types
                    cboSearchCollegeUnit.Items.Add("ALL");
                    cboSearchCollegeUnit.Items[0].Value = "";
                    cboSearchCollegeUnit.Items.Add("SCC");
                    cboSearchCollegeUnit.Items.Add("SFCC");
                    cboSearchCollegeUnit.Items.Add("District");
                }

                //check user permissions
                if (userType == "HROAdmin" && userPermission == "HRO Admin") {
                    lblTitle.Text = userPermission;
                    hidUserType.Value = userType;
                } else if (userType == "HROCoordinator" && userPermission == "HRO Coordinator") {
                    lblTitle.Text = userPermission;
                    hidUserType.Value = userType;
                } else if (userType == "HRORecruiter" && userPermission == "HRO Recruiter") {
                    lblTitle.Text = userPermission;
                    hidUserType.Value = userType;
                } else if (userType == "HROAssistant" && userPermission == "HRO Assistant") {
                    cmdExport.Visible = false;
                    lblTitle.Text = userPermission;
                    hidUserType.Value = userType;
                } else if (userType == "BudgetAdmin" && userPermission == "Budget Admin") {
                    lblTitle.Text = userPermission;
                    hidUserType.Value = userType;
                } else if (userType == "ExecutiveAssistant" && userPermission == "Executive Assistant") {
                    cmdExport.Visible = false;
                    lblTitle.Text = userPermission;
                    hidUserType.Value = userType;
                } else if (userType == "Supervisor") {
                    cmdExport.Visible = false;
                    lblTitle.Text = userType;
                    hidUserType.Value = userType;
                } else {
                    //display error message
                    lblError.Text = "<strong>Error: Access Denied</strong><br />You do not have permission to view this page.";
                    panError.Visible = true;
                    panMsg.Visible = false;
                    panList.Visible = false;
                }
            } else {
                //display error message
                lblError.Text = "<strong>Error: Access Denied</strong><br />You do not have permission to view this page.";
                panError.Visible = true;
                panMsg.Visible = false;
                panList.Visible = false;
            } 
        }

        if (hidTodo.Value == "view") {
            Int32 positionRequestID = Convert.ToInt32(hidID.Value);

            DataSet dsPositionRequest = positionRequest.GetPositionRequest(positionRequestID);
            DataTable dtPr = dsPositionRequest.Tables[0];
            if (dtPr.Rows.Count > 0)
            {
                DataRow drPr = dtPr.Rows[0];
                Decimal salaryAmount = 0;
                try {
                    salaryAmount = Convert.ToDecimal(dsPositionRequest.Tables[0].Rows[0]["SalaryAmount"]);
                } catch {
                    //do nothing
                }
                lblOriginatorID.Text = dsPositionRequest.Tables[0].Rows[0]["OriginatorID"].ToString();
                lblOriginatorName.Text = dsPositionRequest.Tables[0].Rows[0]["OriginatorName"].ToString();
                lblTrackingNumber.Text = dsPositionRequest.Tables[0].Rows[0]["TrackingNumber"].ToString();
                lblStatus.Text = dsPositionRequest.Tables[0].Rows[0]["Status"].ToString();
                lblOfficialTitle.Text = dsPositionRequest.Tables[0].Rows[0]["OfficialTitle"].ToString();
                lblWorkingTitle.Text = dsPositionRequest.Tables[0].Rows[0]["WorkingTitle"].ToString();
                lblJobCode.Text = dsPositionRequest.Tables[0].Rows[0]["JobCode"].ToString();
                lblBudgetNumber.Text = dsPositionRequest.Tables[0].Rows[0]["BudgetNumber"].ToString();
                lblPositionNumber.Text = dsPositionRequest.Tables[0].Rows[0]["PositionNumber"].ToString();
                lblCollegeUnit.Text = dsPositionRequest.Tables[0].Rows[0]["CollegeUnit"].ToString();
                lblDepartmentName.Text = dsPositionRequest.Tables[0].Rows[0]["Department"].ToString();
                lblDepartmentID.Text = dsPositionRequest.Tables[0].Rows[0]["DepartmentID"].ToString();
                lblPhoneNumber.Text = dsPositionRequest.Tables[0].Rows[0]["PhoneNumber"].ToString();
                lblMailStop.Text = dsPositionRequest.Tables[0].Rows[0]["MailStop"].ToString();
                lblSupervisorName.Text = dsPositionRequest.Tables[0].Rows[0]["SupervisorName"].ToString();
                lblSupervisorID.Text = dsPositionRequest.Tables[0].Rows[0]["SupervisorID"].ToString();
                lblEmployeeName.Text = dsPositionRequest.Tables[0].Rows[0]["EmployeeName"].ToString();
                lblEmployeeID.Text = dsPositionRequest.Tables[0].Rows[0]["EmployeeID"].ToString();
                lblEmployeeEmailAddress.Text = dsPositionRequest.Tables[0].Rows[0]["EmployeeEmailAddress"].ToString();
                lblSalaryRangeStep.Text = dsPositionRequest.Tables[0].Rows[0]["SalaryRangeStep"].ToString();
                lblSalaryAmount.Text = string.Format("{0:C}", salaryAmount);
                lblEmployeeType.Text = dsPositionRequest.Tables[0].Rows[0]["EmployeeType"].ToString();
                if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["NewPosition"]) == 1) {
                    lblNewReplacement.Text = "New Position";
                    employeeReplaced.Visible = false;
                } else if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["ReplacementPosition"]) == 1) {
                    lblNewReplacement.Text = "Replacement Position";
                    lblEmployeeReplaced.Text = dsPositionRequest.Tables[0].Rows[0]["ReplacedEmployeeName"].ToString();
                    employeeReplaced.Visible = true;
                }
                lblPositionType.Text = dsPositionRequest.Tables[0].Rows[0]["PositionType"].ToString();
                if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["PermanentPosition"]) == 1) {
                    lblClassified.Text = "Permanent";
                    lblClassified.Visible = true;
                }
                if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["NonPermanentPosition"]) == 1) {
                    lblClassified.Text = "Non Permanent";
                    lblClassified.Visible = true;
                    try {
                        lblEndDate.Text = Convert.ToDateTime(dsPositionRequest.Tables[0].Rows[0]["PositionEndDate"]).ToShortDateString();
                        lblEndDate.Visible = true;
                    } catch {
                        //do nothing - no date exists
                    }
                } else {
                    endDate.Visible = false;
                }
                lblRecruitmentType.Text = dsPositionRequest.Tables[0].Rows[0]["RecruitmentType"].ToString();
                if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["PCard"]) == 1) {
                    lblOnboarding.Text += "<div style=\"padding-bottom:5px;\">P-Card</div>";
                }
                if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["CCSIssuedCellPhone"]) == 1) {
                    lblOnboarding.Text += "<div style=\"padding-bottom:5px;\">CCS-issued Cell Phone</div>";
                }
                if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["CTCLinkAccess"]) == 1) {
                    lblOnboarding.Text += "<div style=\"padding-bottom:5px;\">Additional ctcLink Security Access</div>";
                }
                if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["TimeSheetApprover"]) == 1) {
                    lblOnboarding.Text += "<div style=\"padding-bottom:5px;\">Manager/Timesheet Approver</div>";
                }
                if (lblPositionType.Text == "Classified" || lblPositionType.Text == "Part-Time Hourly") {
                    lblHoursPerDay.Text = dsPositionRequest.Tables[0].Rows[0]["HoursPerDay"].ToString();
                    lblHoursPerWeek.Text = dsPositionRequest.Tables[0].Rows[0]["HoursPerWeek"].ToString();
                    lblMonthsPerYear.Text = dsPositionRequest.Tables[0].Rows[0]["MonthsPerYear"].ToString();
                    lblCyclicCalendarCode.Text = dsPositionRequest.Tables[0].Rows[0]["CyclicCalendarCode"].ToString();
                    if (lblCyclicCalendarCode.Text != "") {
                        cyclicCalendarCode.Attributes["style"] = cyclicCalendarCode.Attributes["style"].Replace("display: none;", "");
                    }
                    /*
                    DataSet dsWorkSchedule = positionRequest.GetWorkSchedule(positionRequestID);
                    if (dsWorkSchedule.Tables[0].Rows.Count > 0) {
                        String mondayStart = dsWorkSchedule.Tables[0].Rows[0]["MondayStart"].ToString();
                        String mondayEnd = dsWorkSchedule.Tables[0].Rows[0]["MondayEnd"].ToString();
                        String tuesdayStart = dsWorkSchedule.Tables[0].Rows[0]["TuesdayStart"].ToString();
                        String tuesdayEnd = dsWorkSchedule.Tables[0].Rows[0]["TuesdayEnd"].ToString();
                        String wednesdayStart = dsWorkSchedule.Tables[0].Rows[0]["WednesdayStart"].ToString();
                        String wednesdayEnd = dsWorkSchedule.Tables[0].Rows[0]["WednesdayEnd"].ToString();
                        String thursdayStart = dsWorkSchedule.Tables[0].Rows[0]["ThursdayStart"].ToString();
                        String thursdayEnd = dsWorkSchedule.Tables[0].Rows[0]["ThursdayEnd"].ToString();
                        String fridayStart = dsWorkSchedule.Tables[0].Rows[0]["FridayStart"].ToString();
                        String fridayEnd = dsWorkSchedule.Tables[0].Rows[0]["FridayEnd"].ToString();
                        String saturdayStart = dsWorkSchedule.Tables[0].Rows[0]["SaturdayStart"].ToString();
                        String saturdayEnd = dsWorkSchedule.Tables[0].Rows[0]["SaturdayEnd"].ToString();
                        String sundayStart = dsWorkSchedule.Tables[0].Rows[0]["SundayStart"].ToString();
                        String sundayEnd = dsWorkSchedule.Tables[0].Rows[0]["SundayEnd"].ToString();
                        if (mondayStart != "" || mondayEnd != "") {
                            if (mondayStart != "") {
                                mondayStart = Convert.ToDateTime(mondayStart).ToString("%h:mm tt");
                            }
                            if (mondayEnd != "") {
                                mondayEnd = Convert.ToDateTime(mondayEnd).ToString("%h:mm tt");
                            }
                            lblMonday.Text = mondayStart + "<br />to<br />" + mondayEnd;
                        }
                        if (tuesdayStart != "" || tuesdayEnd != "") {
                            if (tuesdayStart != "") {
                                tuesdayStart = Convert.ToDateTime(tuesdayStart).ToString("%h:mm tt");
                            }
                            if (tuesdayEnd != "") {
                                tuesdayEnd = Convert.ToDateTime(tuesdayEnd).ToString("%h:mm tt");
                            }
                            lblTuesday.Text = tuesdayStart + "<br />to<br />" + tuesdayEnd;
                        }
                        if (wednesdayStart != "" || wednesdayEnd != "") {
                            if (wednesdayStart != "") {
                                wednesdayStart = Convert.ToDateTime(wednesdayStart).ToString("%h:mm tt");
                            }
                            if (wednesdayEnd != "") {
                                wednesdayEnd = Convert.ToDateTime(wednesdayEnd).ToString("%h:mm tt");
                            }
                            lblWednesday.Text = wednesdayStart + "<br />to<br />" + wednesdayEnd;
                        }
                        if (thursdayStart != "" || thursdayEnd != "") {
                            if (thursdayStart != "") {
                                thursdayStart = Convert.ToDateTime(thursdayStart).ToString("%h:mm tt");
                            }
                            if (thursdayEnd != "") {
                                thursdayEnd = Convert.ToDateTime(thursdayEnd).ToString("%h:mm tt");
                            }
                            lblThursday.Text = thursdayStart + "<br />to<br />" + thursdayEnd;
                        }
                        if (fridayStart != "" || fridayEnd != "") {
                            if (fridayStart != "") {
                                fridayStart = Convert.ToDateTime(fridayStart).ToString("%h:mm tt");
                            }
                            if (fridayEnd != "") {
                                fridayEnd = Convert.ToDateTime(fridayEnd).ToString("%h:mm tt");
                            }
                            lblFriday.Text = fridayStart + "<br />to<br />" + fridayEnd;
                        }
                        if (saturdayStart != "" || saturdayEnd != "") {
                            if (saturdayStart != "") {
                                saturdayStart = Convert.ToDateTime(saturdayStart).ToString("%h:mm tt");
                            }
                            if (saturdayEnd != "") {
                                saturdayEnd = Convert.ToDateTime(saturdayEnd).ToString("%h:mm tt");
                            }
                            lblSaturday.Text = saturdayStart + "<br />to<br />" + saturdayEnd;
                        }
                        if (sundayStart != "" || sundayEnd != "") {
                            if (sundayStart != "") {
                                sundayStart = Convert.ToDateTime(sundayStart).ToString("%h:mm tt");
                            }
                            if (sundayEnd != "") {
                                sundayEnd = Convert.ToDateTime(sundayEnd).ToString("%h:mm tt");
                            }
                            lblSunday.Text = sundayStart + "<br />to<br />" + sundayEnd;
                        }
                        schedule.Visible = true;
                    }
                    */
                    //Populate Work schedule for view/confirm
                    PopulateWorkScheduleForLabel(positionRequestID);
                } else {
                    schedule.Visible = false;
                }

                if ((lblStatus.Text == "HRO" || lblStatus.Text == "Recruiting" || lblStatus.Text == "Hired" || lblStatus.Text == "Complete") &&
                    (lblEmployeeName.Text != "" && lblEmployeeID.Text != "" && lblEmployeeEmailAddress.Text != "" && lblSalaryRangeStep.Text != "" && lblSalaryAmount.Text != "$0.00" && lblEmployeeType.Text != "")) {
                    panEmployeeInfo.Visible = true;
                }
                lblOrigComments.Text = drPr["Comments"].ToString();
                //Populate label for BuildingNumber and RoomNumber
                lblBuildingNumber.Text = drPr["BuildingNumber"].ToString();
                lblRoomNumber.Text = drPr["RoomNumber"].ToString();
            }

            //show uploaded file attachments
            DataSet dsAttachments = positionRequest.GetAttachments(positionRequestID);
            Int32 attachmentCount = dsAttachments.Tables[0].Rows.Count;
            if (attachmentCount > 0) {
                for (Int32 i = 0; i < attachmentCount; i++) {
                    lblAttachments.Text += "<div style=\"padding-bottom:5px;\"><a href=\"Attachments/" + dsAttachments.Tables[0].Rows[i]["FileName"].ToString() + "\" target=\"self\" class=\"smallText\" title=\"View File Attachment " + (i + 1) + "\">Attachment " + (i + 1) + "</a></div>";                }
                panFileAttachments.Visible = true;
            } else {
                panFileAttachments.Visible = false;
            }

            //get Routing/Approvals
            DataSet dsRouting = positionRequest.GetRouting(positionRequestID);
            Int32 routingCount = dsRouting.Tables[0].Rows.Count;
            if (routingCount > 0) {
                for (Int32 i = 0; i < routingCount; i++) {
                    String note = dsRouting.Tables[0].Rows[i]["Note"].ToString();
                    String description = dsRouting.Tables[0].Rows[i]["Description"].ToString();
                    TableRow tr = new TableRow();
                    TableCell td = new TableCell();
                    td.CssClass = "solid";
                    td.Attributes["style"] = "vertical-align:top;padding-left:10px;width:75px";
                    td.Controls.Add(new LiteralControl(dsRouting.Tables[0].Rows[i]["UserType"].ToString()));
                    tr.Cells.Add(td);
                    td = new TableCell();
                    td.CssClass = "solid";
                    td.Attributes["style"] = "vertical-align:top;width:190px";
                    td.Controls.Add(new LiteralControl(dsRouting.Tables[0].Rows[i]["Name"].ToString()));
                    tr.Cells.Add(td);
                    td = new TableCell();
                    td.CssClass = "solid";
                    td.Attributes["style"] = "vertical-align:top";
                    td.Controls.Add(new LiteralControl(description));
                    try {
                        if (description != "Pending Approval" && description != "Complete") {
                            td.Controls.Add(new LiteralControl("&nbsp;" + Convert.ToDateTime(dsRouting.Tables[0].Rows[i]["Date"]).ToString("MM/dd/yy %h:mm tt")));
                        } else if (description == "Complete") {
                            td.Controls.Add(new LiteralControl("&nbsp;" + Convert.ToDateTime(dsRouting.Tables[0].Rows[i]["Completed"]).ToString("MM/dd/yy")));
                        }
                    } catch {
                        //do nothing
                    }
                    if (note != "") {
                        td.Controls.Add(new LiteralControl("<br />" + note));
                    }
                    tr.Cells.Add(td);
                    tblApprovals.Rows.Add(tr);
                }
                panApprovals.Visible = true;
            } else {
                panApprovals.Visible = false;
            }

            if (lblStatus.Text == "HRO" || lblStatus.Text == "Recruiting" || lblStatus.Text == "Hired" || lblStatus.Text == "Complete") {
                panEmployeeInfo.Visible = true;
            }

            cmdBack.Visible = true;
            cmdPrint.Visible = true;
            panView.Visible = true;

        } 
    }

    protected void cmdSearch_Click(object sender, System.EventArgs e) {
        hidTodo.Value = "search";
        SearchPositionRequests(txtSearchOriginatorName.Text, cboSearchStatus.SelectedValue, txtSearchTrackingNumber.Text, txtSearchOfficialTitle.Text, txtSearchPositionNumber.Text, hidSearchDepartment.Value, cboSearchPositionType.SelectedValue, cboSearchRecruitmentType.SelectedValue, cboSearchCollegeUnit.SelectedValue, txtCreationStartDate.Value, txtCreationEndDate.Value, txtCompletionStartDate.Value, txtCompletionEndDate.Value);
    }

    protected void cmdExport_Click(object sender, System.EventArgs e) {
        DataSet dsReport = positionRequest.GetReport(txtSearchOriginatorName.Text, cboSearchStatus.SelectedValue, txtSearchTrackingNumber.Text, txtSearchOfficialTitle.Text, txtSearchPositionNumber.Text, hidSearchDepartment.Value, cboSearchPositionType.SelectedValue, cboSearchRecruitmentType.SelectedValue, cboSearchCollegeUnit.SelectedValue, txtCreationStartDate.Value, txtCreationEndDate.Value, txtCompletionStartDate.Value, txtCompletionEndDate.Value);

        StreamWriter objSW = File.CreateText(HttpContext.Current.Server.MapPath("ExcelReport.csv"));
        objSW.WriteLine("Tracking #, Originator's Name, Originator's ctcLink ID, Official Position Title, Position Type, Recruitment Type, Status, Creation Date, College/Unit, Department, Routing User Type, Routing User Name, Routing User ctcLink ID, Routing Description, Routing Date");

        for (Int32 i = 0; i < dsReport.Tables[0].Rows.Count; i++) {
            objSW.WriteLine("\"" + dsReport.Tables[0].Rows[i]["TrackingNumber"].ToString() + "\",\"" + dsReport.Tables[0].Rows[i]["OriginatorName"].ToString() + "\",\"" + dsReport.Tables[0].Rows[i]["OriginatorID"].ToString() + "\",\"" + dsReport.Tables[0].Rows[i]["OfficialTitle"].ToString() + "\",\"" + dsReport.Tables[0].Rows[i]["PositionType"].ToString() + "\",\"" + dsReport.Tables[0].Rows[i]["RecruitmentType"].ToString() + "\",\"" + dsReport.Tables[0].Rows[i]["Status"].ToString() + "\",\"" + Convert.ToDateTime(dsReport.Tables[0].Rows[i]["Created"]).ToString("MM/dd/yy %h:mm tt") + "\",\"" + dsReport.Tables[0].Rows[i]["CollegeUnit"].ToString() + "\",\"" + dsReport.Tables[0].Rows[i]["Department"].ToString() + "\",\"" + dsReport.Tables[0].Rows[i]["UserType"].ToString() + "\",\"" + dsReport.Tables[0].Rows[i]["Name"].ToString() + "\",\"" + dsReport.Tables[0].Rows[i]["EmployeeID"].ToString() + "\",\"" + dsReport.Tables[0].Rows[i]["Description"].ToString() + "\",\"" + Convert.ToDateTime(dsReport.Tables[0].Rows[i]["Date"]).ToString("MM/dd/yy %h:mm tt") + "\"");
        }

        objSW.Close();
        Response.Redirect("ExcelReport.csv");
    }

    private void SearchPositionRequests(String originatorName, String status, String trackingNumber, String officialTitle, String positionNumber, String departmentID, String positionType, String recruitmentType, String collegeUnit, String creationStartDate, String creationEndDate, String completionStartDate, String completionEndDate) {
        DataSet dsPositionRequests = new DataSet();
        if(hidUserType.Value == "Supervisor") {
            dsPositionRequests = positionRequest.GetSupervisorReport(Request.Cookies["phatt2"]["userctclinkid"], originatorName, status, trackingNumber, officialTitle, positionNumber, departmentID, positionType, recruitmentType, collegeUnit, creationStartDate, creationEndDate, completionStartDate, completionEndDate);
        } else if(hidUserType.Value == "ExecutiveAssistant") {
            dsPositionRequests = positionRequest.GetExecutiveAssistantReport(Request.Cookies["phatt2"]["userctclinkid"], originatorName, status, trackingNumber, officialTitle, positionNumber, departmentID, positionType, recruitmentType, collegeUnit, creationStartDate, creationEndDate, completionStartDate, completionEndDate);
        } else { 
            dsPositionRequests = positionRequest.GetPositionRequests("", originatorName, status, trackingNumber, officialTitle, positionNumber, departmentID, positionType, recruitmentType, collegeUnit, creationStartDate, creationEndDate, completionStartDate, completionEndDate);
        }
        Int32 rowCount = dsPositionRequests.Tables[0].Rows.Count;

        TableHeaderRow thr = new TableHeaderRow();
        TableHeaderCell thd = new TableHeaderCell();
        thd.Controls.Add(new LiteralControl("Status"));
        thd.CssClass = "sortHeader";
        thd.Attributes.Add("onclick", "sortTable(0, \"" + tblList.ClientID + "\")");
        thr.Cells.Add(thd);

        thd = new TableHeaderCell();
        thd.Controls.Add(new LiteralControl("Created"));
        thd.CssClass = "sortHeader";
        thd.Attributes.Add("onclick", "sortTable(1, \"" + tblList.ClientID + "\")");
        thr.Cells.Add(thd);

        thd = new TableHeaderCell();
        thd.Controls.Add(new LiteralControl("Official Title"));
        thd.CssClass = "sortHeader";
        thd.Attributes.Add("onclick", "sortTable(2, \"" + tblList.ClientID + "\")");
        thr.Cells.Add(thd);

        thd = new TableHeaderCell();
        thd.Controls.Add(new LiteralControl("Position Type"));
        thd.CssClass = "sortHeader";
        thd.Attributes.Add("onclick", "sortTable(3, \"" + tblList.ClientID + "\")");
        thr.Cells.Add(thd);

        thd = new TableHeaderCell();
        thr.Cells.Add(thd);
        tblList.Rows.Add(thr);

        if (rowCount > 0) {
            for (Int32 i = 0; i < rowCount; i++) {
                Int32 positionRequestID = Convert.ToInt32(dsPositionRequests.Tables[0].Rows[i]["PositionRequestID"]);
                String created = "";
                try { 
                    created = Convert.ToDateTime(dsPositionRequests.Tables[0].Rows[i]["Created"]).ToShortDateString();
                } catch {
                    //do nothing
                }
                String department = dsPositionRequests.Tables[0].Rows[i]["Department"].ToString();
                status = dsPositionRequests.Tables[0].Rows[i]["Status"].ToString();
                officialTitle = dsPositionRequests.Tables[0].Rows[i]["OfficialTitle"].ToString();
                positionType = dsPositionRequests.Tables[0].Rows[i]["PositionType"].ToString();

                TableRow tr = new TableRow();
                TableCell td = new TableCell();
                td.CssClass = "solid";
                td.Controls.Add(new LiteralControl(status));
                tr.Cells.Add(td);

                td = new TableCell();
                td.CssClass = "solid";
                td.Controls.Add(new LiteralControl(created));
                tr.Cells.Add(td);

                td = new TableCell();
                td.CssClass = "solid";
                td.Controls.Add(new LiteralControl(officialTitle));
                tr.Cells.Add(td);

                td = new TableCell();
                td.CssClass = "solid";
                td.Controls.Add(new LiteralControl(positionType));
                tr.Cells.Add(td);

                /*
                td = new TableCell();
                td.CssClass = "solid";
                td.Controls.Add(new LiteralControl(department));
                tr.Cells.Add(td);
                */

                td = new TableCell();
                td.CssClass = "action";
                td.Attributes["style"] = "vertical-align: top;width: 16px; background: #eee none";
                td.Controls.Add(new LiteralControl("<a href=\"#\" class=\"view\" title=\"View Form\" onclick=\"document.getElementById('" + hidTodo.ClientID + "').value='view';document.getElementById('" + hidStatus.ClientID + "').value='" + status + "';document.getElementById('" + hidID.ClientID + "').value='" + positionRequestID + "';document.getElementById('" + hidCreated.ClientID + "').value='" + created + "';document.getElementById('" + frmPR.ClientID + "').submit();\"></a>"));
                tr.Cells.Add(td);

                tblList.Rows.Add(tr);
            }
        } else {
            TableRow tr = new TableRow();
            TableCell td = new TableCell();
            td.ColumnSpan = 6;
            if (originatorName == "" && status == "" && trackingNumber == "" && officialTitle == "" && positionNumber == "" && departmentID == "" && positionType == "" && recruitmentType == "" && collegeUnit == "" && creationStartDate == "" && creationEndDate == "" && completionStartDate == "" && completionEndDate == "") {
                td.Controls.Add(new LiteralControl("No forms currently exist."));
            } else {
                td.Controls.Add(new LiteralControl("Your search returned 0 results."));
            }
            tr.Cells.Add(td);
            tblList.Rows.Add(tr);
        }

        tblList.Visible = true;
        panList.Visible = true;
    }

    private void PopulateWorkScheduleForLabel(int positionRequestID)
    {
        DataSet dsWorkSchedule = positionRequest.GetWorkSchedule(positionRequestID);
        DataTable dtWorkSchedule = dsWorkSchedule.Tables[0];
        if (dtWorkSchedule.Rows.Count > 0)
        {
            DataRow drWorkSchedule = dtWorkSchedule.Rows[0];
            foreach (var s in dayWeek)
            {
                Label lbl = (Label)this.Page.Master.FindControl("maintextHolder").FindControl("lbl" + s);
                if (lbl != null)
                {
                    string startTime = Utility.ConvertToDateStr(drWorkSchedule[s + "Start"].ToString());
                    string endTime = Utility.ConvertToDateStr(drWorkSchedule[s + "End"].ToString());
                    if (startTime != "" || endTime != "")
                    {
                        lbl.Text = String.Format("{0}<br />to<br />{1}", startTime, endTime);
                    }

                }
            }
            schedule.Visible = true;
        }
    }
}
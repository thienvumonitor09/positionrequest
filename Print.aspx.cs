﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Print : System.Web.UI.Page
{
    PositionRequest positionRequest = new PositionRequest();
    string[] dayWeek = Utility.GetDayWeek();
    protected void Page_Load(object sender, EventArgs e)
    {
        //check if the user is logged in
        HttpCookie cookie = Request.Cookies.Get("phatt2");
        if (cookie == null) {
            users user = new users(HttpContext.Current.User.Identity.Name);
        }

        //hide all panels
        panPrint.Visible = false;
        panError.Visible = false;
        panEmployeeInfo.Visible = false;

        //check if ID is in the right format - numerical only
        Int32 positionRequestID = 0;
        //try {
            positionRequestID = Convert.ToInt32(Request.QueryString["id"]);
            

            //check if user has access to selected Position Request
            String employeeID = Request.Cookies["phatt2"]["userctclinkid"];
            //if (positionRequest.GetUserPermission(employeeID) == "" && !positionRequest.isSuper(employeeID, positionRequestID) && !positionRequest.isOriginator(employeeID, positionRequestID)) {
            //    lblError.Text = "<strong>Error: Access Denied</strong><br />You do not have permission to view this page.";
            //    panError.Visible = true;
            //} else {
            //Get Position Request Form
        DataSet dsPositionRequest = positionRequest.GetPositionRequest(positionRequestID);
        DataTable dtPositionRequest = dsPositionRequest.Tables[0];
        if (dtPositionRequest.Rows.Count > 0)
        {
            DataRow drPr = dtPositionRequest.Rows[0];
            Decimal salaryAmount = 0;
            try {
                salaryAmount = Convert.ToDecimal(dsPositionRequest.Tables[0].Rows[0]["SalaryAmount"]);
            } catch {
                //do nothing
            }
            lblTrackingNumber.Text = dsPositionRequest.Tables[0].Rows[0]["TrackingNumber"].ToString();
            lblOriginatorID.Text = dsPositionRequest.Tables[0].Rows[0]["OriginatorID"].ToString();
            lblOriginatorName.Text = dsPositionRequest.Tables[0].Rows[0]["OriginatorName"].ToString();
            lblStatus.Text = dsPositionRequest.Tables[0].Rows[0]["Status"].ToString();
            lblOfficialTitle.Text = dsPositionRequest.Tables[0].Rows[0]["OfficialTitle"].ToString();
            lblWorkingTitle.Text = dsPositionRequest.Tables[0].Rows[0]["WorkingTitle"].ToString();
            lblJobCode.Text = dsPositionRequest.Tables[0].Rows[0]["JobCode"].ToString();
            lblBudgetNumber.Text = dsPositionRequest.Tables[0].Rows[0]["BudgetNumber"].ToString();
            lblPositionNumber.Text = dsPositionRequest.Tables[0].Rows[0]["PositionNumber"].ToString();
            lblCollegeUnit.Text = dsPositionRequest.Tables[0].Rows[0]["CollegeUnit"].ToString(); //may need to add conditional logic to show scc, sfcc, or dist
            lblDepartmentName.Text = dsPositionRequest.Tables[0].Rows[0]["Department"].ToString();
            lblDepartmentID2.Text = dsPositionRequest.Tables[0].Rows[0]["DepartmentID"].ToString();
            lblPhoneNumber.Text = dsPositionRequest.Tables[0].Rows[0]["PhoneNumber"].ToString();
            lblMailStop.Text = dsPositionRequest.Tables[0].Rows[0]["MailStop"].ToString();
            lblSupervisorName.Text = dsPositionRequest.Tables[0].Rows[0]["SupervisorName"].ToString();
            lblSupervisorID2.Text = dsPositionRequest.Tables[0].Rows[0]["SupervisorID"].ToString();
            if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["NewPosition"]) == 1) {
                lblNewReplacement.Text = "New Position";
                employeeReplaced2.Visible = false;
            } else if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["ReplacementPosition"]) == 1) {
                lblNewReplacement.Text = "Replacement Position";
                lblEmployeeReplaced.Text = dsPositionRequest.Tables[0].Rows[0]["ReplacedEmployeeName"].ToString();
                employeeReplaced2.Visible = true;
            }
            lblPositionType.Text = dsPositionRequest.Tables[0].Rows[0]["PositionType"].ToString();
            if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["PermanentPosition"]) == 1) {
                lblClassified.Text = "Permanent";
                lblClassified.Visible = true;
            }
            if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["NonPermanentPosition"]) == 1) {
                lblClassified.Text = "Non Permanent";
                lblClassified.Visible = true;
                try {
                    lblEndDate.Text = Convert.ToDateTime(dsPositionRequest.Tables[0].Rows[0]["PositionEndDate"]).ToShortDateString();
                    lblEndDate.Visible = true;
                } catch {
                    //do nothing - no date exists
                }
            } else {
                endDate2.Visible = false;
            }
            lblRecruitmentType.Text = dsPositionRequest.Tables[0].Rows[0]["RecruitmentType"].ToString();
            if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["PCard"]) == 1) {
                lblOnboarding.Text += "<div style=\"padding-bottom:5px;\">P-Card</div>";
            }
            if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["CCSIssuedCellPhone"]) == 1) {
                lblOnboarding.Text += "<div style=\"padding-bottom:5px;\">CCS-issued Cell Phone</div>";
            }
            if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["CTCLinkAccess"]) == 1) {
                lblOnboarding.Text += "<div style=\"padding-bottom:5px;\">Additional ctcLink Security Access</div>";
            }
            if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["TimeSheetApprover"]) == 1) {
                lblOnboarding.Text += "<div style=\"padding-bottom:5px;\">Manager/Timesheet Approver</div>";
            }
            if (lblPositionType.Text == "Classified" || lblPositionType.Text == "Part-Time Hourly") {
                lblHoursPerDay.Text = dsPositionRequest.Tables[0].Rows[0]["HoursPerDay"].ToString();
                lblHoursPerWeek.Text = dsPositionRequest.Tables[0].Rows[0]["HoursPerWeek"].ToString();
                lblMonthsPerYear.Text = dsPositionRequest.Tables[0].Rows[0]["MonthsPerYear"].ToString();
                lblCyclicCalendarCode.Text = dsPositionRequest.Tables[0].Rows[0]["CyclicCalendarCode"].ToString();
                if (lblCyclicCalendarCode.Text != "") {
                    cyclicCalendarCode2.Attributes["style"] = cyclicCalendarCode2.Attributes["style"].Replace("display: none;", "");
                }

                /*
                DataSet dsWorkSchedule = positionRequest.GetWorkSchedule(positionRequestID);
                if (dsWorkSchedule.Tables[0].Rows.Count > 0) {
                    String mondayStart = dsWorkSchedule.Tables[0].Rows[0]["MondayStart"].ToString();
                    String mondayEnd = dsWorkSchedule.Tables[0].Rows[0]["MondayEnd"].ToString();
                    String tuesdayStart = dsWorkSchedule.Tables[0].Rows[0]["TuesdayStart"].ToString();
                    String tuesdayEnd = dsWorkSchedule.Tables[0].Rows[0]["TuesdayEnd"].ToString();
                    String wednesdayStart = dsWorkSchedule.Tables[0].Rows[0]["WednesdayStart"].ToString();
                    String wednesdayEnd = dsWorkSchedule.Tables[0].Rows[0]["WednesdayEnd"].ToString();
                    String thursdayStart = dsWorkSchedule.Tables[0].Rows[0]["ThursdayStart"].ToString();
                    String thursdayEnd = dsWorkSchedule.Tables[0].Rows[0]["ThursdayEnd"].ToString();
                    String fridayStart = dsWorkSchedule.Tables[0].Rows[0]["FridayStart"].ToString();
                    String fridayEnd = dsWorkSchedule.Tables[0].Rows[0]["FridayEnd"].ToString();
                    String saturdayStart = dsWorkSchedule.Tables[0].Rows[0]["SaturdayStart"].ToString();
                    String saturdayEnd = dsWorkSchedule.Tables[0].Rows[0]["SaturdayEnd"].ToString();
                    String sundayStart = dsWorkSchedule.Tables[0].Rows[0]["SundayStart"].ToString();
                    String sundayEnd = dsWorkSchedule.Tables[0].Rows[0]["SundayEnd"].ToString();
                    if (mondayStart != "" || mondayEnd != "") {
                        if (mondayStart != "") {
                            mondayStart = Convert.ToDateTime(mondayStart).ToString("%h:mm tt");
                        }
                        if (mondayEnd != "") {
                            mondayEnd = Convert.ToDateTime(mondayEnd).ToString("%h:mm tt");
                        }
                        lblMonday.Text = mondayStart + "<br />to<br />" + mondayEnd;
                    }
                    if (tuesdayStart != "" || tuesdayEnd != "") {
                        if (tuesdayStart != "") {
                            tuesdayStart = Convert.ToDateTime(tuesdayStart).ToString("%h:mm tt");
                        }
                        if (tuesdayEnd != "") {
                            tuesdayEnd = Convert.ToDateTime(tuesdayEnd).ToString("%h:mm tt");
                        }
                        lblTuesday.Text = tuesdayStart + "<br />to<br />" + tuesdayEnd;
                    }
                    if (wednesdayStart != "" || wednesdayEnd != "") {
                        if (wednesdayStart != "") {
                            wednesdayStart = Convert.ToDateTime(wednesdayStart).ToString("%h:mm tt");
                        }
                        if (wednesdayEnd != "") {
                            wednesdayEnd = Convert.ToDateTime(wednesdayEnd).ToString("%h:mm tt");
                        }
                        lblWednesday.Text = wednesdayStart + "<br />to<br />" + wednesdayEnd;
                    }
                    if (thursdayStart != "" || thursdayEnd != "") {
                        if (thursdayStart != "") {
                            thursdayStart = Convert.ToDateTime(thursdayStart).ToString("%h:mm tt");
                        }
                        if (thursdayEnd != "") {
                            thursdayEnd = Convert.ToDateTime(thursdayEnd).ToString("%h:mm tt");
                        }
                        lblThursday.Text = thursdayStart + "<br />to<br />" + thursdayEnd;
                    }
                    if (fridayStart != "" || fridayEnd != "") {
                        if (fridayStart != "") {
                            fridayStart = Convert.ToDateTime(fridayStart).ToString("%h:mm tt");
                        }
                        if (fridayEnd != "") {
                            fridayEnd = Convert.ToDateTime(fridayEnd).ToString("%h:mm tt");
                        }
                        lblFriday.Text = fridayStart + "<br />to<br />" + fridayEnd;
                    }
                    if (saturdayStart != "" || saturdayEnd != "") {
                        if (saturdayStart != "") {
                            saturdayStart = Convert.ToDateTime(saturdayStart).ToString("%h:mm tt");
                        }
                        if (saturdayEnd != "") {
                            saturdayEnd = Convert.ToDateTime(saturdayEnd).ToString("%h:mm tt");
                        }
                        lblSaturday.Text = saturdayStart + "<br />to<br />" + saturdayEnd;
                    }
                    if (sundayStart != "" || sundayEnd != "") {
                        if (sundayStart != "") {
                            sundayStart = Convert.ToDateTime(sundayStart).ToString("%h:mm tt");
                        }
                        if (sundayEnd != "") {
                            sundayEnd = Convert.ToDateTime(sundayEnd).ToString("%h:mm tt");
                        }
                        lblSunday.Text = sundayStart + "<br />to<br />" + sundayEnd;
                    }
                    schedule2.Visible = true;
                }
                */
                //Populate Work schedule for view/confirm
                PopulateWorkScheduleForLabel(positionRequestID);
            } else {
                schedule2.Visible = false;
            }

            lblEmployeeName.Text = dsPositionRequest.Tables[0].Rows[0]["EmployeeName"].ToString();
            lblEmployeeID.Text = dsPositionRequest.Tables[0].Rows[0]["EmployeeID"].ToString();
            lblSalaryRangeStep.Text = dsPositionRequest.Tables[0].Rows[0]["SalaryRangeStep"].ToString();
            lblSalaryAmount.Text = string.Format("{0:C}", salaryAmount);
            lblEffectiveDate.Text = (String.IsNullOrEmpty(drPr["Effective"].ToString())) ? "<br/>" : Convert.ToDateTime(drPr["Effective"]).ToString("MM/dd/yy");
            lblEmployeeType.Text = dsPositionRequest.Tables[0].Rows[0]["EmployeeType"].ToString();
            lblEmployeeEmailAddress.Text = dsPositionRequest.Tables[0].Rows[0]["EmployeeEmailAddress"].ToString();

            if ((lblStatus.Text == "HRO" || lblStatus.Text == "Recruiting" || lblStatus.Text == "Hired" || lblStatus.Text == "Complete") &&
                (lblEmployeeName.Text != "" && lblEmployeeID.Text != "" && lblEmployeeEmailAddress.Text != "" && lblSalaryRangeStep.Text != "" && lblSalaryAmount.Text != "$0.00" && lblEmployeeType.Text != "")) {
                panEmployeeInfo.Visible = true;
            }

            //Show Originator Comments
            lblOrigComments.Text = drPr["Comments"].ToString();
            //Populate label for BuildingNumber and RoomNumber
            lblBuildingNumber.Text = drPr["BuildingNumber"].ToString();
            lblRoomNumber.Text = drPr["RoomNumber"].ToString();
        }

        //show uploaded file attachments
        DataSet dsAttachments = positionRequest.GetAttachments(positionRequestID);
        Int32 attachmentCount = dsAttachments.Tables[0].Rows.Count;
        if (attachmentCount > 0) {
            for (Int32 i = 0; i < attachmentCount; i++) {
                lblAttachments.Text += "<div style=\"padding-bottom:5px;\"><a href=\"Attachments/" + dsAttachments.Tables[0].Rows[i]["FileName"].ToString() + "\" target=\"self\" class=\"smallText\" title=\"View File Attachment " + (i + 1) + "\">Attachment " + (i + 1) + "</a></div>";
            }
            panAttachments.Visible = true;
        } else {
            panAttachments.Visible = false;
        }

        //get Routing/Approvals
        DataSet dsRouting = positionRequest.GetRouting(positionRequestID);
        DataTable dtRouting = dsRouting.Tables[0];        
        if (dtRouting.Rows.Count > 0) {
            foreach (DataRow drRouting in dtRouting.Rows) {
                String note = drRouting["Note"].ToString();
                String description = drRouting["Description"].ToString();
                TableRow tr = new TableRow();
                TableCell td = new TableCell();
                td.CssClass = "solid";
                td.Attributes["style"] = "vertical-align:top;padding-left:10px;width:75px";
                td.Controls.Add(new LiteralControl(drRouting["UserType"].ToString()));
                tr.Cells.Add(td);
                td = new TableCell();
                td.CssClass = "solid";
                td.Attributes["style"] = "vertical-align:top;width:190px";
                td.Controls.Add(new LiteralControl(drRouting["Name"].ToString()));
                tr.Cells.Add(td);
                td = new TableCell();
                td.CssClass = "solid";
                td.Attributes["style"] = "vertical-align:top";
                td.Controls.Add(new LiteralControl(description));
                if (description != "Pending Approval" && description != "Complete") {
                    td.Controls.Add(new LiteralControl("&nbsp;" + Convert.ToDateTime(drRouting["Date"]).ToString("MM/dd/yy %h:mm tt")));
                } else if (description == "Complete") {
                    td.Controls.Add(new LiteralControl("&nbsp;" 
                                                    + (String.IsNullOrEmpty(drRouting["Completed"].ToString()) ? "" : Convert.ToDateTime(drRouting["Completed"]).ToString("MM/dd/yy")))
                                                        );
                }
                if (note != "") {
                    td.Controls.Add(new LiteralControl("<br />" + note));
                }
                tr.Cells.Add(td);
                tblApprovals.Rows.Add(tr);
            }
            panApprovals.Visible = true;
        } else {
            panApprovals.Visible = false;
        }
        panPrint.Visible = true;
            //}
        //} catch {
            //display error message
        //    lblError.Text = "<strong>Error:</strong> The Position Request form could not be found.";
        //    panError.Visible = true;
        //}
    }
    private void PopulateWorkScheduleForLabel(int positionRequestID)
    {
        DataSet dsWorkSchedule = positionRequest.GetWorkSchedule(positionRequestID);
        DataTable dtWorkSchedule = dsWorkSchedule.Tables[0];
        if (dtWorkSchedule.Rows.Count > 0)
        {
            DataRow drWorkSchedule = dtWorkSchedule.Rows[0];
            foreach (var s in dayWeek)
            {
                Label lbl = (Label)this.Page.FindControl("lbl" + s);
                if (lbl != null)
                {
                    string startTime = Utility.ConvertToDateStr(drWorkSchedule[s + "Start"].ToString());
                    string endTime = Utility.ConvertToDateStr(drWorkSchedule[s + "End"].ToString());
                    if (startTime != "" || endTime != "")
                    {
                        lbl.Text = String.Format("{0}<br />to<br />{1}", startTime, endTime);
                    }

                }
            }
            schedule2.Visible = true;
        }
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/container/container.master" AutoEventWireup="true" CodeFile="Permissions.aspx.cs" Inherits="HRO_Permissions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="menuHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="maintextHolder" Runat="Server">
    <form id="frmPR" runat="server">
        <input type="hidden" id="hidTodo" runat="server" />
        <input type="hidden" id="hidID" runat="server" />
        <h3 class="pageTitle">Grant Permissions</h3>
        <asp:Panel ID="panList" runat="server">
            <asp:Table ID="tblList" runat="server">
                <asp:TableRow>
                    <asp:TableHeaderCell style="width:80px" CssClass="sortHeader" onclick="sortTable(0,'maintextHolder_tblList')">ctcLink ID</asp:TableHeaderCell>
                    <asp:TableHeaderCell style="width:225px" CssClass="sortHeader" onclick="sortTable(1,'maintextHolder_tblList')">Name</asp:TableHeaderCell>
                    <asp:TableHeaderCell style="width:145px" CssClass="sortHeader" onclick="sortTable(2,'maintextHolder_tblList')">Permissions</asp:TableHeaderCell>
                    <asp:TableHeaderCell nowrap="nowrap" ColumnSpan="2" style="background: url('../App_Themes/Moxie/images/defaults/blue.gif');width:70px"><a href="#" class="add" style="color:#fff" title="Add a New User" onclick="document.getElementById('<%= hidTodo.ClientID %>').value='add';document.getElementById('<%= frmPR.ClientID %>').submit();">ADD NEW</a></asp:TableHeaderCell>
                </asp:TableRow>
            </asp:Table>
        </asp:Panel>
        <asp:Panel ID="panAdd" runat="server">
            <table>
                <tr>
                    <th>Add Permissions</th>
                </tr>
                <tr>
                    <td class="large">
                        <div style="width:225px;text-align:right;float:left;padding-top:10px;padding-bottom:5px"><strong>Name:&nbsp;</strong></div>
                        <div style="width:320px;text-align:left;float:left;padding-top:10px;padding-bottom:5px"><asp:Label ID="lblName1" runat="Server"></asp:Label></div>
                        <div style="clear:both"></div>
                        <div style="width:225px;text-align:right;float:left;padding-top:10px"><strong>ctcLink ID:&nbsp;</strong></div>
                        <div style="width:320px;text-align:left;float:left;padding-top:10px"><asp:TextBox id="txtEmployeeID" runat="server" AutoPostBack="true"></asp:TextBox></div>
                        <div style="clear:both"></div>
                        <div style="width:225px;text-align:right;float:left;padding-top:10px;padding-bottom:10px"><strong>Permissions:&nbsp;</strong></div>
                        <div style="width:320px;text-align:left;float:left;padding-top:10px;padding-bottom:10px">
                            <asp:DropDownList id="cboPermissions1" runat="server">
                                <asp:ListItem Value="HRO Admin">HRO Admin</asp:ListItem>
                                <asp:ListItem Value="HRO Coordinator">HRO Coordinator</asp:ListItem>
                                <asp:ListItem Value="HRO Recruiter">HRO Recruiter</asp:ListItem>
                                <asp:ListItem Value="HRO Assistant">HRO Assistant</asp:ListItem>
                                <asp:ListItem Value="Budget Admin">Budget Admin</asp:ListItem>
                                <asp:ListItem Value="Executive Assistant">Executive Assistant</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </td>
                </tr>
            </table>
            <div style="clear:both;width:100%;text-align:center;padding-top:20px;">
                <asp:Button ID="cmdCancel1" runat="server" Text="Cancel" OnClick="cmdCancel_Click" />&nbsp;&nbsp;
                <asp:Button ID="cmdAdd" runat="server" Text="Submit" OnClick="cmdAdd_Click" />
            </div>
        </asp:Panel>
        <asp:Panel ID="panEdit" runat="server">
            <table>
                <tr>
                    <th>Edit Permissions</th>
                </tr>
                <tr>
                    <td class="large">
                        <div style="width:225px;text-align:right;float:left;padding-top:10px"><strong>Name:&nbsp;</strong></div>
                        <div style="width:320px;text-align:left;float:left;padding-top:10px"><asp:Label ID="lblName2" runat="server"></asp:Label></div>
                        <div style="clear:both"></div>
                        <div style="width:225px;text-align:right;float:left;padding-top:10px"><strong>ctcLink ID:&nbsp;</strong></div>
                        <div style="width:320px;text-align:left;float:left;padding-top:10px"><asp:Label ID="lblEmployeeID" runat="server"></asp:Label></div>
                        <div style="clear:both"></div>
                        <div style="width:225px;text-align:right;float:left;padding-top:10px;padding-bottom:10px"><strong>Permissions:&nbsp;</strong></div>
                        <div style="width:320px;text-align:left;float:left;padding-top:10px;padding-bottom:10px">
                            <asp:DropDownList id="cboPermissions2" runat="server">
                                <asp:ListItem Value="HRO Admin">HRO Admin</asp:ListItem>
                                <asp:ListItem Value="HRO Coordinator">HRO Coordinator</asp:ListItem>
                                <asp:ListItem Value="HRO Recruiter">HRO Recruiter</asp:ListItem>
                                <asp:ListItem Value="HRO Assistant">HRO Assistant</asp:ListItem>
                                <asp:ListItem Value="Budget Admin">Budget Admin</asp:ListItem>
                                <asp:ListItem Value="Executive Assistant">Executive Assistant</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </td>
                </tr>
            </table>
            <div style="clear:both;width:100%;text-align:center;padding-top:20px;">
                <asp:Button ID="cmdCancel2" runat="server" Text="Cancel" OnClick="cmdCancel_Click" />&nbsp;&nbsp;
                <asp:Button ID="cmdEdit" runat="server" Text="Submit" OnClick="cmdEdit_Click" />
            </div>
        </asp:Panel>
        <asp:Panel ID="panError" runat="server">
            <asp:Label style="color:red" ID="lblError" runat="server"></asp:Label>
        </asp:Panel>
    </form>
    <script type="text/javascript" src="../Scripts/SortTable.js"></script>
</asp:Content>


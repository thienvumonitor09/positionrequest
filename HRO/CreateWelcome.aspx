﻿<%@ Page Title="" Language="C#" MasterPageFile="~/container/container.master" AutoEventWireup="true" CodeFile="CreateWelcome.aspx.cs" Inherits="HRO_CreateWelcome" validateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="menuHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="maintextHolder" Runat="Server">
    <form id="frmPR" runat="server">
        <asp:Panel ID="panMain" runat="server">
            <h3 class="pageTitle">Create Welcome</h3>
            <textarea name="txtWelcome" id="txtWelcome" runat="server" class="defaultText" cols="87" rows="80"></textarea>
            <script type="text/javascript">
                CKEDITOR.replace('<%= txtWelcome.ClientID %>',
                {
                    customConfig: '/EAN/ckeditor_config.js',
                    toolbar:
                    [
                        { name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike', '-',
                        'NumberedList', 'BulletedList', '-',
				        'JustifyLeft', 'JustifyCenter', 'JustifyRight', '-',
				        'Outdent', 'Indent', '-',
				        'Copy', 'Cut', 'Paste', '-',
				        'Link', 'Unlink', '-',
				        'SpellChecker', 'Scayt']
                        }
                    ],
                    height: "500px"
                });
		    </script>     
		    <div style="width:100%;text-align:center;padding-top:20px">
		        <asp:button id="cmdPreview" runat="server" text="Preview" style="width:100px;" OnClick="cmdPreview_Click"></asp:button>
			    &nbsp;<asp:button id="cmdSubmit" runat="server" text="Submit" style="width:100px;" OnClick="cmdSubmit_Click"></asp:button>											                    
		    </div>
		</asp:Panel>
        <asp:Panel ID="panPreview" runat="server">
		    <asp:Label ID="lblWelcome" runat="server"></asp:Label>
		    <div id="welcomeText" style="width:100%" runat="server"></div>
		    <div style="padding-top:20px;width:100%;text-align:center">
		        <input type="button" id="cmdBack" runat="server" value="Back" style="width:100px" onclick="history.back(1);" />
		    </div>
		</asp:Panel>
		<asp:Panel ID="panConfirm" runat="server">
		    <asp:Label ID="lblConfirmWelcome" runat="server"></asp:Label>
		    <div id="confirmWelcomeText" style="width:100%" runat="server"></div>
		    <div style="padding-top:20px;width:100%;text-align:center">
		        <input type="button" id="cmdOK" runat="server" value="OK" style="width:100px" onclick="location.href = 'CreateWelcome.aspx';" />
		    </div>
		</asp:Panel>
		<asp:Panel ID="panError" runat="server">
            <asp:Label style="color:red" ID="lblError" runat="server"></asp:Label>
        </asp:Panel>
    </form>
</asp:Content>


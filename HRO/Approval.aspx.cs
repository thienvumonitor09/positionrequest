﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Collections;
using System.Data;
using System.Net.Mail;
using System.Web.Services;
using System.Configuration;

public partial class HRO_Approval : System.Web.UI.Page {
    PositionRequest positionRequest = new PositionRequest();
    String strErrorContact = "<p>To report this error, please contact the IT Support Center by <a href=\"mailto:ITSupportCenter@ccs.spokane.edu\">email</a> or phone: 533-HELP.<br />Include a screen shot of this page and as much information as possible.</p>";
    String strAdminEmailAddress = ConfigurationManager.AppSettings["AdminEmailAddress"].ToString();
    List<string> excutiveApprovalJC = Utility.GetExecutiveApprovalJobCode();
    string[] dayWeek = Utility.GetDayWeek();
    //string emailTest = "vu.nguyen@ccs.spokane.edu";
    //string appHost = HttpContext.Current.Request.Url.Host.ToLower();

    protected override void OnInit(EventArgs e) {
        //check if the user is logged in
        HttpCookie cookie = Request.Cookies.Get("phatt2");
        if (cookie == null) {
            users user = new users(HttpContext.Current.User.Identity.Name);
        }

        if (!IsPostBack) {
            //store the logged in user's permission level
            hidUserType.Value = positionRequest.GetUserPermission(Request.Cookies["phatt2"]["userctclinkid"]).Replace(" ", "");
        }

        String userType = Request.Form["ctl00$maintextHolder$hidUserType"];
        String status = Request.Form["ctl00$maintextHolder$hidStatus"];
        String strTodo = Request.Form["ctl00$maintextHolder$hidTodo"];
        
        if (strTodo == "view" || strTodo == "edit") {
            //get Routing/Approvals
            DataSet dsRouting = positionRequest.GetRouting(Convert.ToInt32(Request.Form["ctl00$maintextHolder$hidID"]));
            Int32 routingCount = dsRouting.Tables[0].Rows.Count;
            if (routingCount > 0) {
                for (Int32 i = 0; i < routingCount; i++) {
                    String note = dsRouting.Tables[0].Rows[i]["Note"].ToString();
                    String description = dsRouting.Tables[0].Rows[i]["Description"].ToString();
                    TableRow tr = new TableRow();
                    TableCell td = new TableCell();
                    td.CssClass = "solid";
                    td.Attributes["style"] = "vertical-align:top;padding-left:10px;width:75px";
                    td.Controls.Add(new LiteralControl(dsRouting.Tables[0].Rows[i]["UserType"].ToString()));
                    tr.Cells.Add(td);
                    td = new TableCell();
                    td.CssClass = "solid";
                    td.Attributes["style"] = "vertical-align:top;";
                    if (description != "Pending Approval" && description != "Recruiting" && description != "Hired") { //may need to change this logic - test
                        td.Attributes["style"] += "width:190px";
                        td.Controls.Add(new LiteralControl(dsRouting.Tables[0].Rows[i]["Name"].ToString()));
                        tr.Cells.Add(td);
                        td = new TableCell();
                        td.CssClass = "solid";
                        td.Attributes["style"] = "vertical-align:top";
                        td.Controls.Add(new LiteralControl(description));
                        try {
                            if (description != "Complete") {
                                td.Controls.Add(new LiteralControl("&nbsp;" + Convert.ToDateTime(dsRouting.Tables[0].Rows[i]["Date"]).ToString("MM/dd/yy %h:mm tt")));
                            } else if (description == "Complete") {
                                td.Controls.Add(new LiteralControl("&nbsp;" + Convert.ToDateTime(dsRouting.Tables[0].Rows[i]["Completed"]).ToString("MM/dd/yy")));
                            }
                        } catch {
                            //do nothing
                        }
                        if (note != "") {
                            td.Controls.Add(new LiteralControl("<br />" + note));
                        }
                    } else if(userType == "HROAdmin" && description == "Pending Approval" && status == "HRO"){
                        td.ColumnSpan = 2;
                        td.Attributes["style"] += "wite-space:nowrap";
                        HtmlInputRadioButton optApprove = new HtmlInputRadioButton();
                        optApprove.Attributes["runat"] = "server";
                        optApprove.ID = "optApprove";
                        optApprove.Name = "optApproval";
                        td.Controls.Add(new LiteralControl("<div style=\"padding-bottom:5px\">"));
                        td.Controls.Add(optApprove);
                        td.Controls.Add(new LiteralControl("<strong>Approve</strong>&nbsp;"));
                        td.Controls.Add(new LiteralControl("<div style=\"padding: 5px 0px 0px 35px\" id=\"divStatus\" runat=\"server\">"));
                        td.Controls.Add(new LiteralControl("<strong>Status</strong>&nbsp;"));
                        DropDownList cboStatus = new DropDownList();
                        cboStatus.ID = "cboStatus";
                        cboStatus.Items.Add(new ListItem("",""));
                        cboStatus.Items.Add(new ListItem("Recruiting", "Recruiting"));
                        cboStatus.Items.Add(new ListItem("Hired", "Hired"));
                        cboStatus.Items.Add(new ListItem("Complete", "Complete"));
                        if(description != "Pending Approval") {
                            cboStatus.SelectedValue = description;
                        }
                        td.Controls.Add(cboStatus);
                        td.Controls.Add(new LiteralControl("<span id=\"completionDate\" runat=\"server\">&nbsp;&nbsp;<strong>Date</strong> <span class='required'>*</span>&nbsp;"));
                        HtmlInputText txtCompletionDate = new HtmlInputText();
                        txtCompletionDate.ID = "txtCompletionDate";
                        txtCompletionDate.Attributes["class"] = "datepicker";
                        txtCompletionDate.MaxLength = 10;
                        td.Controls.Add(txtCompletionDate);
                        td.Controls.Add(new LiteralControl("</span></div></div>"));
                        if (strTodo == "view") {
                            td.Controls.Add(new LiteralControl("<div style=\"padding-bottom:5px\">"));
                            HtmlInputRadioButton optReject = new HtmlInputRadioButton();
                            optReject.Attributes["runat"] = "server";
                            optReject.ID = "optReject";
                            optReject.Name = "optApproval";
                            td.Controls.Add(optReject);
                            td.Controls.Add(new LiteralControl("<strong>Reject</strong>&nbsp;"));
                            TextBox txtReject = new TextBox();
                            txtReject.ID = "txtReject";
                            txtReject.Attributes["runat"] = "server";
                            txtReject.Attributes["style"] = "width:310px";
                            td.Controls.Add(txtReject);
                            td.Controls.Add(new LiteralControl("</div>"));
                            td.Controls.Add(new LiteralControl("<div style=\"padding-bottom:5px\">"));
                            HtmlInputRadioButton optVoid = new HtmlInputRadioButton();
                            optVoid.Attributes["runat"] = "server";
                            optVoid.ID = "optVoid";
                            optVoid.Name = "optApproval";
                            td.Controls.Add(optVoid);
                            td.Controls.Add(new LiteralControl("<strong>Void</strong>&nbsp;"));
                            TextBox txtVoid = new TextBox();
                            txtVoid.ID = "txtVoid";
                            txtVoid.Attributes["runat"] = "server";
                            txtVoid.Attributes["style"] = "width:310px";
                            td.Controls.Add(txtVoid);
                            td.Controls.Add(new LiteralControl("</div>"));
                        } else if (strTodo == "edit") {
                            HtmlInputRadioButton optRestart = new HtmlInputRadioButton();
                            optRestart.Attributes["runat"] = "server";
                            optRestart.ID = "optRestart";
                            optRestart.Name = "optApproval";
                            td.Controls.Add(optRestart);
                            td.Controls.Add(new LiteralControl("<strong>Restart the Approval Process</strong>&nbsp;"));
                        }
                    } else if (userType == "HROAdmin" && description == "Pending Approval" && status != "HRO") {
                        String chkModify = Request.Form["ctl00$maintextHolder$chkModify"];

                        //show pending approval
                        td.Attributes["style"] += "width:190px;";
                        td.Controls.Add(new LiteralControl(dsRouting.Tables[0].Rows[i]["Name"].ToString()));
                        tr.Cells.Add(td);
                        td = new TableCell();
                        td.CssClass = "solid";
                        td.Attributes["style"] = "vertical-align:top";
                        td.Controls.Add(new LiteralControl(description));
                        if (note != "") {
                            td.Controls.Add(new LiteralControl("<br />" + note));
                        }
                        tr.Cells.Add(td);
                        tblApprovals.Rows.Add(tr);

                        tr = new TableRow();
                        td = new TableCell();
                        td.CssClass = "solid";
                        td.ColumnSpan = 3;
                        td.Attributes["style"] = "white-space:nowrap;width:100%";

                        HtmlInputRadioButton optApprove = new HtmlInputRadioButton();
                        optApprove.Attributes["runat"] = "server";
                        optApprove.ID = "optApprove";
                        optApprove.Name = "optApproval";
                        td.Controls.Add(new LiteralControl("<div style=\"padding-bottom:5px\">"));
                        td.Controls.Add(new LiteralControl("<strong>Overwrite Approval:</strong>&nbsp;"));
                        td.Controls.Add(optApprove);
                        td.Controls.Add(new LiteralControl("<strong>Approve</strong>&nbsp;"));

                        if (chkModify == "on") {
                            HtmlInputRadioButton optRestart = new HtmlInputRadioButton();
                            optRestart.Attributes["runat"] = "server";
                            optRestart.ID = "optRestart";
                            optRestart.Name = "optApproval";
                            td.Controls.Add(optRestart);
                            td.Controls.Add(new LiteralControl("<strong>Restart Approval Process</strong>"));
                        } else {
                            HtmlInputRadioButton optReject = new HtmlInputRadioButton();
                            optReject.Attributes["runat"] = "server";
                            optReject.ID = "optReject";
                            optReject.Name = "optApproval";
                            td.Controls.Add(optReject);
                            td.Controls.Add(new LiteralControl("<strong>Reject</strong>&nbsp;"));
                            TextBox txtReject = new TextBox();
                            txtReject.ID = "txtReject";
                            txtReject.Attributes["runat"] = "server";
                            txtReject.Attributes["style"] = "width:280px";
                            td.Controls.Add(txtReject);
                        }
                    } else if(description == "Recruiting" || description == "Hired") {
                        if ((i + 1) == routingCount) {
                            td.ColumnSpan = 2;
                            td.Attributes["style"] += "wite-space:nowrap";
                            td.Controls.Add(new LiteralControl("<div id=\"divStatus\" runat=\"server\">"));
                            td.Controls.Add(new LiteralControl("&nbsp;<strong>Status</strong>&nbsp;"));
                            DropDownList cboStatus = new DropDownList();
                            cboStatus.ID = "cboStatus";
                            cboStatus.Items.Add(new ListItem("", ""));
                            cboStatus.Items.Add(new ListItem("Recruiting", "Recruiting"));
                            cboStatus.Items.Add(new ListItem("Hired", "Hired"));
                            cboStatus.Items.Add(new ListItem("Complete", "Complete"));
                            try {
                                cboStatus.SelectedValue = description;
                            } catch {
                                //do nothing
                            }
                            td.Controls.Add(cboStatus);
                            td.Controls.Add(new LiteralControl("<span id=\"completionDate\" runat=\"server\">&nbsp;&nbsp;<strong>Date</strong>&nbsp;"));
                            HtmlInputText txtCompletionDate = new HtmlInputText();
                            txtCompletionDate.ID = "txtCompletionDate";
                            txtCompletionDate.Attributes["class"] = "datepicker";
                            txtCompletionDate.MaxLength = 10;
                            td.Controls.Add(txtCompletionDate);
                            td.Controls.Add(new LiteralControl("</span></div>"));
                        } else {
                            td.Attributes["style"] += "width:190px";
                            td.Controls.Add(new LiteralControl(dsRouting.Tables[0].Rows[i]["Name"].ToString()));
                            tr.Cells.Add(td);
                            td = new TableCell();
                            td.CssClass = "solid";
                            td.Attributes["style"] = "vertical-align:top";
                            td.Controls.Add(new LiteralControl(description));
                            if (description != "Complete") {
                                td.Controls.Add(new LiteralControl("&nbsp;" + Convert.ToDateTime(dsRouting.Tables[0].Rows[i]["Date"]).ToString("MM/dd/yy %h:mm tt")));
                            }
                            if (note != "") {
                                td.Controls.Add(new LiteralControl("<br />" + note));
                            }
                        }
                    }
                    tr.Cells.Add(td);
                    tblApprovals.Rows.Add(tr);
                }
                panApprovals.Visible = true;
            } else {
                panApprovals.Visible = false;
            }
        }
        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e) {

        if (hidUserType.Value == "HROAdmin") {
            lblTitle.Text = "HRO Admin Approval";
        } else if (hidUserType.Value == "HROCoordinator") {
            lblTitle.Text = "HRO Coordinator Action";
        } else if (hidUserType.Value == "HRORecruiter") {
            lblTitle.Text = "HRO Recruiter Action";
        }

        panList.Visible = false;
        panForm.Visible = false;
        panInput.Visible = false;
        panOutput.Visible = false;
        panCheckbox.Visible = false;
        panModificationReason.Visible = false;
        panEditComments.Visible = false;
        panEditFileAttachments.Visible = false;
        panEmployeeInfo.Visible = false;
        panEditEmployee.Visible = false;
        panAddAttachments.Visible = false;
        lblError.Visible = false;
        lblConfirm.Visible = false;
        cmdOK.Visible = false;
        cmdPrint.Visible = false;
        cmdSubmit.Visible = false;
        cmdOK.Visible = false;

        //check if user has the right permissions
        if (hidUserType.Value != "HROAdmin" && hidUserType.Value != "HRORecruiter" && hidUserType.Value != "HROCoordinator") { 
            lblError.Text = "<p><strong>Error: Access Denied</strong><br />You do not have permission to view this page.</p>";
            lblError.Visible = true;

        } else {
            String todo = Request.Form["ctl00$maintextHolder$hidTodo"];
            if (todo == null || todo == "") {
                todo = Request.QueryString["todo"];
            }

            if (!IsPostBack) {
                if(todo == null || todo == "") {
                    cboSearchStatus.SelectedValue = "HRO";
                }
                hidPopulateFromDB.Value = "true";
            }

            if (todo == "view")
            {

                //uncheck approval selections
                if (hidChangeViews.Value == "true") {
                    HtmlInputRadioButton optApprove = (HtmlInputRadioButton)this.Master.FindControl("maintextHolder").FindControl("optApprove");
                    HtmlInputRadioButton optReject = (HtmlInputRadioButton)this.Master.FindControl("maintextHolder").FindControl("optReject");
                    optApprove.Checked = false;
                    optReject.Checked = false;
                    hidChangeViews.Value = "";
                }

                Int32 positionRequestID = Convert.ToInt32(hidID.Value);

                DataSet dsPositionRequest = positionRequest.GetPositionRequest(positionRequestID);
                DataTable dtPositionRequest = dsPositionRequest.Tables[0];
                if (dtPositionRequest.Rows.Count > 0)
                {
                    DataRow drPr = dtPositionRequest.Rows[0];
                    Decimal salaryAmount = 0;
                    try {
                        salaryAmount = Convert.ToDecimal(dsPositionRequest.Tables[0].Rows[0]["SalaryAmount"]);
                    } catch {
                        //do nothing
                    }
                    lblOriginatorName.Text = dsPositionRequest.Tables[0].Rows[0]["OriginatorName"].ToString();
                    lblOriginatorID.Text = dsPositionRequest.Tables[0].Rows[0]["OriginatorID"].ToString();
                    lblTrackingNumber.Text = dsPositionRequest.Tables[0].Rows[0]["TrackingNumber"].ToString();
                    lblStatus.Text = dsPositionRequest.Tables[0].Rows[0]["Status"].ToString();
                    lblOfficialTitle.Text = dsPositionRequest.Tables[0].Rows[0]["OfficialTitle"].ToString();
                    lblWorkingTitle.Text = dsPositionRequest.Tables[0].Rows[0]["WorkingTitle"].ToString();
                    lblJobCode.Text = dsPositionRequest.Tables[0].Rows[0]["JobCode"].ToString();
                    lblBudgetNumber.Text = dsPositionRequest.Tables[0].Rows[0]["BudgetNumber"].ToString();
                    lblPositionNumber.Text = dsPositionRequest.Tables[0].Rows[0]["PositionNumber"].ToString();
                    lblCollegeUnit.Text = dsPositionRequest.Tables[0].Rows[0]["CollegeUnit"].ToString();
                    lblDepartmentName.Text = dsPositionRequest.Tables[0].Rows[0]["Department"].ToString();
                    lblDepartmentID2.Text = dsPositionRequest.Tables[0].Rows[0]["DepartmentID"].ToString();
                    lblPhoneNumber.Text = dsPositionRequest.Tables[0].Rows[0]["PhoneNumber"].ToString();
                    lblMailStop.Text = dsPositionRequest.Tables[0].Rows[0]["MailStop"].ToString();
                    lblSupervisorName.Text = dsPositionRequest.Tables[0].Rows[0]["SupervisorName"].ToString();
                    lblSupervisorID2.Text = dsPositionRequest.Tables[0].Rows[0]["SupervisorID"].ToString();
                    txtEmployeeName.Text = dsPositionRequest.Tables[0].Rows[0]["EmployeeName"].ToString();
                    txtEmployeeID.Text = dsPositionRequest.Tables[0].Rows[0]["EmployeeID"].ToString();
                    txtEmployeeEmailAddress.Text = dsPositionRequest.Tables[0].Rows[0]["EmployeeEmailAddress"].ToString();
                    txtSalaryRangeStep.Text = dsPositionRequest.Tables[0].Rows[0]["SalaryRangeStep"].ToString();
                    txtSalaryAmount.Text = string.Format("{0:N}", salaryAmount);
                    cboEmployeeType.SelectedValue = dsPositionRequest.Tables[0].Rows[0]["EmployeeType"].ToString();
                    lblEmployeeName.Text = txtEmployeeName.Text;
                    lblEmployeeID.Text = txtEmployeeID.Text;
                    lblEmployeeEmailAddress.Text = txtEmployeeEmailAddress.Text;
                    lblSalaryRangeStep.Text = txtSalaryRangeStep.Text;
                    lblSalaryAmount.Text = string.Format("{0:C}", salaryAmount);
                    lblEffectiveDate.Text =  (String.IsNullOrEmpty(drPr["Effective"].ToString())) ? "<br/>" :  Convert.ToDateTime(drPr["Effective"]).ToString("MM/dd/yy");
                    lblEmployeeType.Text = cboEmployeeType.SelectedValue;
                    if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["NewPosition"]) == 1) {
                        lblNewReplacement.Text = "New Position";
                        employeeReplaced2.Visible = false;
                    } else if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["ReplacementPosition"]) == 1) {
                        lblNewReplacement.Text = "Replacement Position";
                        lblEmployeeReplaced.Text = dsPositionRequest.Tables[0].Rows[0]["ReplacedEmployeeName"].ToString();
                        employeeReplaced2.Visible = true;
                    }
                    lblPositionType.Text = dsPositionRequest.Tables[0].Rows[0]["PositionType"].ToString();
                    if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["PermanentPosition"]) == 1) {
                        lblClassified.Text = "Permanent";
                        lblClassified.Visible = true;
                    }
                    if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["NonPermanentPosition"]) == 1) {
                        lblClassified.Text = "Non Permanent";
                        lblClassified.Visible = true;
                        try {
                            lblEndDate.Text = Convert.ToDateTime(dsPositionRequest.Tables[0].Rows[0]["PositionEndDate"]).ToShortDateString();
                            lblEndDate.Visible = true;
                        } catch {
                            //do nothing - no date exists
                        }
                    } else {
                        endDate2.Visible = false;
                    }
                    lblRecruitmentType.Text = dsPositionRequest.Tables[0].Rows[0]["RecruitmentType"].ToString();
                    lblOnboarding.Text = "";
                    if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["PCard"]) == 1) {
                        lblOnboarding.Text += "<div style=\"padding-bottom:5px;\">P-Card</div>";
                    }
                    if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["CCSIssuedCellPhone"]) == 1) {
                        lblOnboarding.Text += "<div style=\"padding-bottom:5px;\">CCS-issued Cell Phone</div>";
                    }
                    if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["CTCLinkAccess"]) == 1) {
                        lblOnboarding.Text += "<div style=\"padding-bottom:5px;\">Additional ctcLink Security Access</div>";
                    }
                    if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["TimeSheetApprover"]) == 1) {
                        lblOnboarding.Text += "<div style=\"padding-bottom:5px;\">Manager/Timesheet Approver</div>";
                    }
                    if (lblPositionType.Text == "Classified" || lblPositionType.Text == "Part-Time Hourly") {
                        lblHoursPerDay.Text = dsPositionRequest.Tables[0].Rows[0]["HoursPerDay"].ToString();
                        lblHoursPerWeek.Text = dsPositionRequest.Tables[0].Rows[0]["HoursPerWeek"].ToString();
                        lblMonthsPerYear.Text = dsPositionRequest.Tables[0].Rows[0]["MonthsPerYear"].ToString();
                        lblCyclicCalendarCode.Text = dsPositionRequest.Tables[0].Rows[0]["CyclicCalendarCode"].ToString();
                        if (lblCyclicCalendarCode.Text != "") {
                            cyclicCalendarCode2.Attributes["style"] = cyclicCalendarCode2.Attributes["style"].Replace("display: none;", "");
                        }
                        /*
                        DataSet dsWorkSchedule = positionRequest.GetWorkSchedule(positionRequestID);
                        if (dsWorkSchedule.Tables[0].Rows.Count > 0)
                        {
                            String mondayStart = dsWorkSchedule.Tables[0].Rows[0]["MondayStart"].ToString();
                            String mondayEnd = dsWorkSchedule.Tables[0].Rows[0]["MondayEnd"].ToString();
                            String tuesdayStart = dsWorkSchedule.Tables[0].Rows[0]["TuesdayStart"].ToString();
                            String tuesdayEnd = dsWorkSchedule.Tables[0].Rows[0]["TuesdayEnd"].ToString();
                            String wednesdayStart = dsWorkSchedule.Tables[0].Rows[0]["WednesdayStart"].ToString();
                            String wednesdayEnd = dsWorkSchedule.Tables[0].Rows[0]["WednesdayEnd"].ToString();
                            String thursdayStart = dsWorkSchedule.Tables[0].Rows[0]["ThursdayStart"].ToString();
                            String thursdayEnd = dsWorkSchedule.Tables[0].Rows[0]["ThursdayEnd"].ToString();
                            String fridayStart = dsWorkSchedule.Tables[0].Rows[0]["FridayStart"].ToString();
                            String fridayEnd = dsWorkSchedule.Tables[0].Rows[0]["FridayEnd"].ToString();
                            String saturdayStart = dsWorkSchedule.Tables[0].Rows[0]["SaturdayStart"].ToString();
                            String saturdayEnd = dsWorkSchedule.Tables[0].Rows[0]["SaturdayEnd"].ToString();
                            String sundayStart = dsWorkSchedule.Tables[0].Rows[0]["SundayStart"].ToString();
                            String sundayEnd = dsWorkSchedule.Tables[0].Rows[0]["SundayEnd"].ToString();
                            if (mondayStart != "" || mondayEnd != "") {
                                if (mondayStart != "") {
                                    mondayStart = Convert.ToDateTime(mondayStart).ToString("%h:mm tt");
                                }
                                if (mondayEnd != "") {
                                    mondayEnd = Convert.ToDateTime(mondayEnd).ToString("%h:mm tt");
                                }
                                lblMonday.Text = mondayStart + "<br />to<br />" + mondayEnd;
                            }
                            if (tuesdayStart != "" || tuesdayEnd != "") {
                                if (tuesdayStart != "") {
                                    tuesdayStart = Convert.ToDateTime(tuesdayStart).ToString("%h:mm tt");
                                }
                                if (tuesdayEnd != "") {
                                    tuesdayEnd = Convert.ToDateTime(tuesdayEnd).ToString("%h:mm tt");
                                }
                                lblTuesday.Text = tuesdayStart + "<br />to<br />" + tuesdayEnd;
                            }
                            if (wednesdayStart != "" || wednesdayEnd != "") {
                                if (wednesdayStart != "") {
                                    wednesdayStart = Convert.ToDateTime(wednesdayStart).ToString("%h:mm tt");
                                }
                                if (wednesdayEnd != "") {
                                    wednesdayEnd = Convert.ToDateTime(wednesdayEnd).ToString("%h:mm tt");
                                }
                                lblWednesday.Text = wednesdayStart + "<br />to<br />" + wednesdayEnd;
                            }
                            if (thursdayStart != "" || thursdayEnd != "") {
                                if (thursdayStart != "") {
                                    thursdayStart = Convert.ToDateTime(thursdayStart).ToString("%h:mm tt");
                                }
                                if (thursdayEnd != "") {
                                    thursdayEnd = Convert.ToDateTime(thursdayEnd).ToString("%h:mm tt");
                                }
                                lblThursday.Text = thursdayStart + "<br />to<br />" + thursdayEnd;
                            }
                            if (fridayStart != "" || fridayEnd != "") {
                                if (fridayStart != "") {
                                    fridayStart = Convert.ToDateTime(fridayStart).ToString("%h:mm tt");
                                }
                                if (fridayEnd != "") {
                                    fridayEnd = Convert.ToDateTime(fridayEnd).ToString("%h:mm tt");
                                }
                                lblFriday.Text = fridayStart + "<br />to<br />" + fridayEnd;
                            }
                            if (saturdayStart != "" || saturdayEnd != "") {
                                if (saturdayStart != "") {
                                    saturdayStart = Convert.ToDateTime(saturdayStart).ToString("%h:mm tt");
                                }
                                if (saturdayEnd != "") {
                                    saturdayEnd = Convert.ToDateTime(saturdayEnd).ToString("%h:mm tt");
                                }
                                lblSaturday.Text = saturdayStart + "<br />to<br />" + saturdayEnd;
                            }
                            if (sundayStart != "" || sundayEnd != "") {
                                if (sundayStart != "") {
                                    sundayStart = Convert.ToDateTime(sundayStart).ToString("%h:mm tt");
                                }
                                if (sundayEnd != "") {
                                    sundayEnd = Convert.ToDateTime(sundayEnd).ToString("%h:mm tt");
                                }
                                lblSunday.Text = sundayStart + "<br />to<br />" + sundayEnd;
                            }
                            schedule2.Visible = true;
                        }
                        */
                        //Populate Work schedule for view/confirm
                        PopulateWorkScheduleForLabel(positionRequestID);
                    } else {
                        schedule2.Visible = false;
                    }
                    //Show Originator Comments
                    lblOrigComments.Text = drPr["Comments"].ToString();
                    //Populate label for BuildingNumber and RoomNumber
                    lblBuildingNumber.Text = drPr["BuildingNumber"].ToString();
                    lblRoomNumber.Text = drPr["RoomNumber"].ToString();
                    //Populate label for ComboCode
                    lblComboCode.Text = drPr["ComboCode"].ToString();
                }
                
                //show uploaded file attachments
                lblAttachments.Text = "";
                DataSet dsAttachments = positionRequest.GetAttachments(positionRequestID);
                Int32 attachmentCount = dsAttachments.Tables[0].Rows.Count;
                if (attachmentCount > 0) {
                    for (Int32 i = 0; i < attachmentCount; i++) {
                        lblAttachments.Text += "<div style=\"padding-bottom:5px;\"><a href=\"..\\Attachments/" + dsAttachments.Tables[0].Rows[i]["FileName"].ToString() + "\" target=\"self\" class=\"smallText\" title=\"View File Attachment " + (i + 1) + "\">Attachment " + (i + 1) + "</a></div>";
                    }
                    panViewFileAttachments.Visible = true;
                } else {
                    panViewFileAttachments.Visible = false;
                }

                if(lblStatus.Text == "HRO" || lblStatus.Text == "Executive" || lblStatus.Text == "Chancellor" || lblStatus.Text == "Budget" || lblStatus.Text.IndexOf("Approver") > -1) { 
                    panCheckbox.Visible = true;
                }
                if(lblStatus.Text == "Recruiting" || lblStatus.Text == "Hired" || lblStatus.Text == "HRO" || lblStatus.Text == "Executive" || lblStatus.Text == "Chancellor" || lblStatus.Text == "Budget" || lblStatus.Text.IndexOf("Approver") > -1)
                {
                    cmdSubmit.Visible = true;
                }
                if(lblStatus.Text == "HRO" || lblStatus.Text == "Recruiting" || lblStatus.Text == "Hired"){
                    panEditEmployee.Visible = true;
                    panAddAttachments.Visible = true;
                } else if(lblStatus.Text == "Complete") {
                    panEmployeeInfo.Visible = true;
                }

                panForm.Visible = true;
                panOutput.Visible = true;
                cmdBack.Visible = true;
                cmdPrint.Visible = true;

            } else if (todo == "edit") {
                panEditComments.Visible = true;
                panModificationReason.Visible = true;
                
                //uncheck approval selections
                if (hidChangeViews.Value == "true") {
                    HtmlInputRadioButton optApprove = (HtmlInputRadioButton)this.Master.FindControl("maintextHolder").FindControl("optApprove");
                    HtmlInputRadioButton optRestart = (HtmlInputRadioButton)this.Master.FindControl("maintextHolder").FindControl("optRestart");
                    optApprove.Checked = false;
                    optRestart.Checked = false;
                    hidChangeViews.Value = "";
                }

                Int32 positionRequestID = Convert.ToInt32(hidID.Value);

                if (hidPopulateFromDB.Value == "true")
                {

                    //show edit panel and populate position request form
                    DataSet dsPositionRequest = positionRequest.GetPositionRequest(positionRequestID);
                    DataTable dtPr = dsPositionRequest.Tables[0];
                    if (dtPr.Rows.Count > 0)
                    {
                        DataRow drPr = dtPr.Rows[0];
                        Decimal salaryAmount = 0;
                        try {
                            salaryAmount = Convert.ToDecimal(dsPositionRequest.Tables[0].Rows[0]["SalaryAmount"].ToString());
                        } catch {
                            //do nothing
                        }
                        lblOriginatorID.Text = dsPositionRequest.Tables[0].Rows[0]["OriginatorID"].ToString();
                        lblOriginatorName.Text = dsPositionRequest.Tables[0].Rows[0]["OriginatorName"].ToString();
                        txtOfficialTitle.Text = dsPositionRequest.Tables[0].Rows[0]["OfficialTitle"].ToString();
                        txtWorkingTitle.Text = dsPositionRequest.Tables[0].Rows[0]["WorkingTitle"].ToString();
                        txtJobCode.Text = dsPositionRequest.Tables[0].Rows[0]["JobCode"].ToString();
                        txtBudgetNumber.Text = dsPositionRequest.Tables[0].Rows[0]["BudgetNumber"].ToString();
                        txtPositionNumber.Text = dsPositionRequest.Tables[0].Rows[0]["PositionNumber"].ToString();
                        cboCollegeUnit.SelectedValue = dsPositionRequest.Tables[0].Rows[0]["CollegeUnit"].ToString();
                        hidDepartment.Value = dsPositionRequest.Tables[0].Rows[0]["Department"].ToString();
                        hidDepartmentID.Value = dsPositionRequest.Tables[0].Rows[0]["DepartmentID"].ToString();
                        txtPhoneNumber.Text = dsPositionRequest.Tables[0].Rows[0]["PhoneNumber"].ToString();
                        txtMailStop.Text = dsPositionRequest.Tables[0].Rows[0]["MailStop"].ToString();
                        txtSupervisorName.Text = dsPositionRequest.Tables[0].Rows[0]["SupervisorName"].ToString();
                        hidSupervisorID.Value = dsPositionRequest.Tables[0].Rows[0]["SupervisorID"].ToString();
                        lblStatus.Text = dsPositionRequest.Tables[0].Rows[0]["Status"].ToString();
                        lblTrackingNumber.Text = dsPositionRequest.Tables[0].Rows[0]["TrackingNumber"].ToString();
                        txtEmployeeName.Text = dsPositionRequest.Tables[0].Rows[0]["EmployeeName"].ToString();
                        txtEmployeeID.Text = dsPositionRequest.Tables[0].Rows[0]["EmployeeID"].ToString();
                        txtEmployeeEmailAddress.Text = dsPositionRequest.Tables[0].Rows[0]["EmployeeEmailAddress"].ToString();
                        txtSalaryRangeStep.Text = dsPositionRequest.Tables[0].Rows[0]["SalaryRangeStep"].ToString();
                        txtSalaryAmount.Text = string.Format("{0:N}", salaryAmount); 
                        cboEmployeeType.SelectedValue = dsPositionRequest.Tables[0].Rows[0]["EmployeeType"].ToString();
                        lblEmployeeName.Text = txtEmployeeName.Text;
                        lblEmployeeID.Text = txtEmployeeID.Text;
                        lblEmployeeEmailAddress.Text = txtEmployeeEmailAddress.Text;
                        lblSalaryRangeStep.Text = txtSalaryRangeStep.Text;
                        lblSalaryAmount.Text = string.Format("{0:C}", salaryAmount);
                        lblEffectiveDate.Text = (String.IsNullOrEmpty(drPr["Effective"].ToString())) ? "<br/>" : Convert.ToDateTime(drPr["Effective"]).ToString("MM/dd/yy");
                        lblEmployeeType.Text = cboEmployeeType.SelectedValue;
                        if (lblTrackingNumber.Text == "") {
                            panTrackingNumber.Visible = false;
                        } else {
                            panTrackingNumber.Visible = true;
                        }
                        if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["NewPosition"]) == 1) {
                            optNew.Checked = true;
                        } else if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["ReplacementPosition"]) == 1) {
                            optReplacement.Checked = true;
                            txtEmployeeReplaced.Text = dsPositionRequest.Tables[0].Rows[0]["ReplacedEmployeeName"].ToString();
                        }
                        cboPositionType.SelectedValue = dsPositionRequest.Tables[0].Rows[0]["PositionType"].ToString();
                        if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["PermanentPosition"]) == 1) {
                            optPermanent.Checked = true;
                        } else if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["NonPermanentPosition"]) == 1) {
                            optNonPermanent.Checked = true;
                            try {
                                txtEndDate.Value = Convert.ToDateTime(dsPositionRequest.Tables[0].Rows[0]["PositionEndDate"]).ToShortDateString();
                            } catch {
                                //do nothing - no date exists
                            }
                        }
                        cboRecruitmentType.SelectedValue = dsPositionRequest.Tables[0].Rows[0]["RecruitmentType"].ToString();
                        if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["PCard"]) == 1) {
                            chkPCard.Checked = true;
                        } else {
                            chkPCard.Checked = false;
                        }
                        if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["CCSIssuedCellPhone"]) == 1) {
                            chkCellPhone.Checked = true;
                        } else {
                            chkCellPhone.Checked = false;
                        }
                        if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["CTCLinkAccess"]) == 1) {
                            chkAdditionalAccess.Checked = true;
                        } else {
                            chkAdditionalAccess.Checked = false;
                        }
                        if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["TimeSheetApprover"]) == 1) {
                            chkTimeSheetApprover.Checked = true;
                        } else {
                            chkTimeSheetApprover.Checked = false;
                        }
                        txtHoursPerDay.Text = dsPositionRequest.Tables[0].Rows[0]["HoursPerDay"].ToString();
                        txtHoursPerWeek.Text = dsPositionRequest.Tables[0].Rows[0]["HoursPerWeek"].ToString();
                        txtMonthsPerYear.Text = dsPositionRequest.Tables[0].Rows[0]["MonthsPerYear"].ToString();
                        txtCyclicCalendarCode.Text = dsPositionRequest.Tables[0].Rows[0]["CyclicCalendarCode"].ToString();

                        txtOrigComments.Text = drPr["Comments"].ToString();
                        //Populate builing and room #
                        txtBuildingNumber.Text = drPr["BuildingNumber"].ToString();
                        txtRoomNumber.Text = drPr["RoomNumber"].ToString();
                        //Populate for text ComboCode
                        txtComboCode.Text = drPr["ComboCode"].ToString();
                    }

                    //Get Work Schedule for edit
                    DataSet dsWorkSchedule = positionRequest.GetWorkSchedule(positionRequestID);
                    DataTable dtWorkSchedule = dsWorkSchedule.Tables[0];
                    if (dtWorkSchedule.Rows.Count > 0)
                    {
                        DataRow drWorkSchedule = dtWorkSchedule.Rows[0];
                        foreach (var s in dayWeek)
                        {
                            foreach (var w in new string[] { "Start", "End" })
                            {
                                var txt = (HtmlInputText)this.Page.Master.FindControl("maintextHolder").FindControl("txt" + s + w);
                                if (txt != null)
                                {
                                    txt.Value = Utility.ConvertToDateStr(drWorkSchedule[s + w].ToString()); //for ex: s+w = ModayStart 
                                }
                            }
                        }
                    }
                    /*
                    DataSet dsWorkSchedule = positionRequest.GetWorkSchedule(positionRequestID);
                    if (dsWorkSchedule.Tables[0].Rows.Count > 0) {
                        String mondayStart = dsWorkSchedule.Tables[0].Rows[0]["MondayStart"].ToString();
                        String mondayEnd = dsWorkSchedule.Tables[0].Rows[0]["MondayEnd"].ToString();
                        String tuesdayStart = dsWorkSchedule.Tables[0].Rows[0]["TuesdayStart"].ToString();
                        String tuesdayEnd = dsWorkSchedule.Tables[0].Rows[0]["TuesdayEnd"].ToString();
                        String wednesdayStart = dsWorkSchedule.Tables[0].Rows[0]["WednesdayStart"].ToString();
                        String wednesdayEnd = dsWorkSchedule.Tables[0].Rows[0]["WednesdayEnd"].ToString();
                        String thursdayStart = dsWorkSchedule.Tables[0].Rows[0]["ThursdayStart"].ToString();
                        String thursdayEnd = dsWorkSchedule.Tables[0].Rows[0]["ThursdayEnd"].ToString();
                        String fridayStart = dsWorkSchedule.Tables[0].Rows[0]["FridayStart"].ToString();
                        String fridayEnd = dsWorkSchedule.Tables[0].Rows[0]["FridayEnd"].ToString();
                        String saturdayStart = dsWorkSchedule.Tables[0].Rows[0]["SaturdayStart"].ToString();
                        String saturdayEnd = dsWorkSchedule.Tables[0].Rows[0]["SaturdayEnd"].ToString();
                        String sundayStart = dsWorkSchedule.Tables[0].Rows[0]["SundayStart"].ToString();
                        String sundayEnd = dsWorkSchedule.Tables[0].Rows[0]["SundayEnd"].ToString();
                        if (mondayStart != "") {
                            txtMondayStart.Value = Convert.ToDateTime(mondayStart).ToString("%h:mm tt");
                        }
                        if (mondayEnd != "") {
                            txtMondayEnd.Value = Convert.ToDateTime(mondayEnd).ToString("%h:mm tt");
                        }
                        if (tuesdayStart != "") {
                            txtTuesdayStart.Value = Convert.ToDateTime(tuesdayStart).ToString("%h:mm tt");
                        }
                        if (tuesdayEnd != "") {
                            txtTuesdayEnd.Value = Convert.ToDateTime(tuesdayEnd).ToString("%h:mm tt");
                        }
                        if (wednesdayStart != "") {
                            txtWednesdayStart.Value = Convert.ToDateTime(wednesdayStart).ToString("%h:mm tt");
                        }
                        if (wednesdayEnd != "") {
                            txtWednesdayEnd.Value = Convert.ToDateTime(wednesdayEnd).ToString("%h:mm tt");
                        }
                        if (thursdayStart != "") {
                            txtThursdayStart.Value = Convert.ToDateTime(thursdayStart).ToString("%h:mm tt");
                        }
                        if (thursdayEnd != "") {
                            txtThursdayEnd.Value = Convert.ToDateTime(thursdayEnd).ToString("%h:mm tt");
                        }
                        if (fridayStart != "") {
                            txtFridayStart.Value = Convert.ToDateTime(fridayStart).ToString("%h:mm tt");
                        }
                        if (fridayEnd != "") {
                            txtFridayEnd.Value = Convert.ToDateTime(fridayEnd).ToString("%h:mm tt");
                        }
                        if (saturdayStart != "") {
                            txtSaturdayStart.Value = Convert.ToDateTime(saturdayStart).ToString("%h:mm tt");
                        }
                        if (saturdayEnd != "") {
                            txtSaturdayEnd.Value = Convert.ToDateTime(saturdayEnd).ToString("%h:mm tt");
                        }
                        if (sundayStart != "") {
                            txtSundayStart.Value = Convert.ToDateTime(sundayStart).ToString("%h:mm tt");
                        }
                        if (sundayEnd != "") {
                            txtSundayEnd.Value = Convert.ToDateTime(sundayEnd).ToString("%h:mm tt");
                        }
                    }
                    */

                    hidPopulateFromDB.Value = "false";
                }

                //get uploaded file attachments
                hidFileAttachments.Value = "";
                DataSet dsAttachments = positionRequest.GetAttachments(positionRequestID);
                Int32 attachmentCount = dsAttachments.Tables[0].Rows.Count;
                if (attachmentCount > 0) {
                    for (Int32 i = 0; i < attachmentCount; i++) {
                        hidFileAttachments.Value += "|" + dsAttachments.Tables[0].Rows[i]["FileName"].ToString();
                    }
                }
                populateFileAttachments();

                if (lblStatus.Text == "HRO" || lblStatus.Text == "Recruiting" || lblStatus.Text == "Hired") {
                    panEditEmployee.Visible = true;
                    panAddAttachments.Visible = true;
                } else if(lblStatus.Text == "Complete") {
                    panEmployeeInfo.Visible = true;
                }

                panCheckbox.Visible = true;
                panInput.Visible = true;
                panForm.Visible = true;
                cmdBack.Visible = true;
                cmdSubmit.Visible = true;
                cmdPrint.Visible = true;

            } else if (todo == "delete") {

                hidTodo.Value = "";
                Int32 positionRequestID = Convert.ToInt32(hidID.Value);
                //get file attachments
                DataSet dsAttachments = positionRequest.GetAttachments(positionRequestID);
                for (Int32 i = 0; i < dsAttachments.Tables[0].Rows.Count; i++) {
                    //delete file attachments from server
                    System.IO.File.Delete(Server.MapPath("..\\Attachments/") + dsAttachments.Tables[0].Rows[i]["FileName"].ToString());
                }
                //delete Position Request Form from DB
                positionRequest.DeletePositionRequest(positionRequestID);
                //display position request list
                SearchPositionRequests(txtSearchOriginatorName.Text, cboSearchStatus.SelectedValue, txtSearchTrackingNumber.Text, txtSearchOfficialTitle.Text, txtSearchPositionNumber.Text, hidSearchDepartment.Value, cboSearchPositionType.SelectedValue, cboSearchRecruitmentType.SelectedValue, cboSearchCollegeUnit.SelectedValue, txtCreationStartDate.Value, txtCreationEndDate.Value, txtCompletionStartDate.Value, txtCompletionEndDate.Value);
                panList.Visible = true;

            } else if (todo == "confirm")
            {

                Int32 positionRequestID = Convert.ToInt32(Request.QueryString["id"]);
                hidID.Value = positionRequestID.ToString();

                DataSet dsPositionRequest = positionRequest.GetPositionRequest(positionRequestID);
                DataTable dtPr = dsPositionRequest.Tables[0];
                if (dtPr.Rows.Count > 0) {
                    DataRow drPr = dtPr.Rows[0];
                    Decimal salaryAmount = 0;
                    try {
                        salaryAmount = Convert.ToDecimal(dsPositionRequest.Tables[0].Rows[0]["SalaryAmount"]);
                    } catch {
                        //do nothing
                    }
                    String task = Request.QueryString["task"];
                    String notified = Request.QueryString["notified"];
                    String notNotified = Request.QueryString["notNotified"];
                    lblConfirm.Text = "<p><strong>The following form has been " + task + ".</strong><br /></p>";
                    if (notified != "") {
                        lblConfirm.Text += "<p><strong>Notification has been sent to:</strong>" + notified.Replace("|", "<br />") + "</p>";
                    }
                    if (notNotified != "") {
                        lblConfirm.Text += "<p><strong>Notification could not be sent to:</strong>" + notNotified.Replace("|", "<br />") + "</p>";
                    }
                    lblConfirm.Text += "<p>To view previous approvals, click on the PR Report link in the menu to the left.<br /></p>"; lblOriginatorID.Text = dsPositionRequest.Tables[0].Rows[0]["OriginatorID"].ToString();
                    lblOriginatorName.Text = dsPositionRequest.Tables[0].Rows[0]["OriginatorName"].ToString();
                    lblTrackingNumber.Text = dsPositionRequest.Tables[0].Rows[0]["TrackingNumber"].ToString();
                    lblStatus.Text = dsPositionRequest.Tables[0].Rows[0]["Status"].ToString();
                    lblOfficialTitle.Text = dsPositionRequest.Tables[0].Rows[0]["OfficialTitle"].ToString();
                    lblWorkingTitle.Text = dsPositionRequest.Tables[0].Rows[0]["WorkingTitle"].ToString();
                    lblJobCode.Text = dsPositionRequest.Tables[0].Rows[0]["JobCode"].ToString();
                    lblBudgetNumber.Text = dsPositionRequest.Tables[0].Rows[0]["BudgetNumber"].ToString();
                    lblPositionNumber.Text = dsPositionRequest.Tables[0].Rows[0]["PositionNumber"].ToString();
                    lblCollegeUnit.Text = dsPositionRequest.Tables[0].Rows[0]["CollegeUnit"].ToString(); 
                    lblDepartmentName.Text = dsPositionRequest.Tables[0].Rows[0]["Department"].ToString();
                    lblDepartmentID2.Text = dsPositionRequest.Tables[0].Rows[0]["DepartmentID"].ToString();
                    lblPhoneNumber.Text = dsPositionRequest.Tables[0].Rows[0]["PhoneNumber"].ToString();
                    lblMailStop.Text = dsPositionRequest.Tables[0].Rows[0]["MailStop"].ToString();
                    lblSupervisorName.Text = dsPositionRequest.Tables[0].Rows[0]["SupervisorName"].ToString();
                    lblSupervisorID2.Text = dsPositionRequest.Tables[0].Rows[0]["SupervisorID"].ToString();
                    lblEmployeeName.Text = dsPositionRequest.Tables[0].Rows[0]["EmployeeName"].ToString();
                    lblEmployeeID.Text = dsPositionRequest.Tables[0].Rows[0]["EmployeeID"].ToString();
                    lblEmployeeEmailAddress.Text = dsPositionRequest.Tables[0].Rows[0]["EmployeeEmailAddress"].ToString();
                    lblSalaryRangeStep.Text = dsPositionRequest.Tables[0].Rows[0]["SalaryRangeStep"].ToString();
                    lblSalaryAmount.Text = string.Format("{0:C}", salaryAmount);
                    lblEffectiveDate.Text = (String.IsNullOrEmpty(drPr["Effective"].ToString())) ? "<br/>" : Convert.ToDateTime(drPr["Effective"]).ToString("MM/dd/yy");
                    lblEmployeeType.Text = dsPositionRequest.Tables[0].Rows[0]["EmployeeType"].ToString();
                    if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["NewPosition"]) == 1) {
                        lblNewReplacement.Text = "New Position";
                        employeeReplaced2.Visible = false;
                    } else if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["ReplacementPosition"]) == 1) {
                        lblNewReplacement.Text = "Replacement Position";
                        lblEmployeeReplaced.Text = dsPositionRequest.Tables[0].Rows[0]["ReplacedEmployeeName"].ToString();
                        employeeReplaced2.Visible = true;
                    }
                    lblPositionType.Text = dsPositionRequest.Tables[0].Rows[0]["PositionType"].ToString();
                    if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["PermanentPosition"]) == 1) {
                        lblClassified.Text = "Permanent";
                        lblClassified.Visible = true;
                    }
                    if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["NonPermanentPosition"]) == 1) {
                        lblClassified.Text = "Non Permanent";
                        lblClassified.Visible = true;
                        try {
                            lblEndDate.Text = Convert.ToDateTime(dsPositionRequest.Tables[0].Rows[0]["PositionEndDate"]).ToShortDateString();
                            lblEndDate.Visible = true;
                        } catch {
                            //do nothing - no date exists
                        }
                    } else {
                        endDate2.Visible = false;
                    }
                    lblRecruitmentType.Text = dsPositionRequest.Tables[0].Rows[0]["RecruitmentType"].ToString();
                    if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["PCard"]) == 1) {
                        lblOnboarding.Text += "<div style=\"padding-bottom:5px;\">P-Card</div>";
                    }
                    if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["CCSIssuedCellPhone"]) == 1) {
                        lblOnboarding.Text += "<div style=\"padding-bottom:5px;\">CCS-issued Cell Phone</div>";
                    }
                    if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["CTCLinkAccess"]) == 1) {
                        lblOnboarding.Text += "<div style=\"padding-bottom:5px;\">Additional ctcLink Security Access</div>";
                    }
                    if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["TimeSheetApprover"]) == 1) {
                        lblOnboarding.Text += "<div style=\"padding-bottom:5px;\">Manager/Timesheet Approver</div>";
                    }
                    if (lblPositionType.Text == "Classified" || lblPositionType.Text == "Part-Time Hourly") {
                        lblHoursPerDay.Text = dsPositionRequest.Tables[0].Rows[0]["HoursPerDay"].ToString();
                        lblHoursPerWeek.Text = dsPositionRequest.Tables[0].Rows[0]["HoursPerWeek"].ToString();
                        lblMonthsPerYear.Text = dsPositionRequest.Tables[0].Rows[0]["MonthsPerYear"].ToString();
                        lblCyclicCalendarCode.Text = dsPositionRequest.Tables[0].Rows[0]["CyclicCalendarCode"].ToString();
                        if (lblCyclicCalendarCode.Text != "") {
                            cyclicCalendarCode2.Attributes["style"] = cyclicCalendarCode2.Attributes["style"].Replace("display: none;", "");
                        }

                        /*
                        DataSet dsWorkSchedule = positionRequest.GetWorkSchedule(positionRequestID);
                        if (dsWorkSchedule.Tables[0].Rows.Count > 0) {
                            String mondayStart = dsWorkSchedule.Tables[0].Rows[0]["MondayStart"].ToString();
                            String mondayEnd = dsWorkSchedule.Tables[0].Rows[0]["MondayEnd"].ToString();
                            String tuesdayStart = dsWorkSchedule.Tables[0].Rows[0]["TuesdayStart"].ToString();
                            String tuesdayEnd = dsWorkSchedule.Tables[0].Rows[0]["TuesdayEnd"].ToString();
                            String wednesdayStart = dsWorkSchedule.Tables[0].Rows[0]["WednesdayStart"].ToString();
                            String wednesdayEnd = dsWorkSchedule.Tables[0].Rows[0]["WednesdayEnd"].ToString();
                            String thursdayStart = dsWorkSchedule.Tables[0].Rows[0]["ThursdayStart"].ToString();
                            String thursdayEnd = dsWorkSchedule.Tables[0].Rows[0]["ThursdayEnd"].ToString();
                            String fridayStart = dsWorkSchedule.Tables[0].Rows[0]["FridayStart"].ToString();
                            String fridayEnd = dsWorkSchedule.Tables[0].Rows[0]["FridayEnd"].ToString();
                            String saturdayStart = dsWorkSchedule.Tables[0].Rows[0]["SaturdayStart"].ToString();
                            String saturdayEnd = dsWorkSchedule.Tables[0].Rows[0]["SaturdayEnd"].ToString();
                            String sundayStart = dsWorkSchedule.Tables[0].Rows[0]["SundayStart"].ToString();
                            String sundayEnd = dsWorkSchedule.Tables[0].Rows[0]["SundayEnd"].ToString();
                            if (mondayStart != "" || mondayEnd != "") {
                                if (mondayStart != "") {
                                    mondayStart = Convert.ToDateTime(mondayStart).ToString("%h:mm tt");
                                }
                                if (mondayEnd != "") {
                                    mondayEnd = Convert.ToDateTime(mondayEnd).ToString("%h:mm tt");
                                }
                                lblMonday.Text = mondayStart + "<br />to<br />" + mondayEnd;
                            }
                            if (tuesdayStart != "" || tuesdayEnd != "") {
                                if (tuesdayStart != "") {
                                    tuesdayStart = Convert.ToDateTime(tuesdayStart).ToString("%h:mm tt");
                                }
                                if (tuesdayEnd != "") {
                                    tuesdayEnd = Convert.ToDateTime(tuesdayEnd).ToString("%h:mm tt");
                                }
                                lblTuesday.Text = tuesdayStart + "<br />to<br />" + tuesdayEnd;
                            }
                            if (wednesdayStart != "" || wednesdayEnd != "") {
                                if (wednesdayStart != "") {
                                    wednesdayStart = Convert.ToDateTime(wednesdayStart).ToString("%h:mm tt");
                                }
                                if (wednesdayEnd != "") {
                                    wednesdayEnd = Convert.ToDateTime(wednesdayEnd).ToString("%h:mm tt");
                                }
                                lblWednesday.Text = wednesdayStart + "<br />to<br />" + wednesdayEnd;
                            }
                            if (thursdayStart != "" || thursdayEnd != "") {
                                if (thursdayStart != "") {
                                    thursdayStart = Convert.ToDateTime(thursdayStart).ToString("%h:mm tt");
                                }
                                if (thursdayEnd != "") {
                                    thursdayEnd = Convert.ToDateTime(thursdayEnd).ToString("%h:mm tt");
                                }
                                lblThursday.Text = thursdayStart + "<br />to<br />" + thursdayEnd;
                            }
                            if (fridayStart != "" || fridayEnd != "") {
                                if (fridayStart != "") {
                                    fridayStart = Convert.ToDateTime(fridayStart).ToString("%h:mm tt");
                                }
                                if (fridayEnd != "") {
                                    fridayEnd = Convert.ToDateTime(fridayEnd).ToString("%h:mm tt");
                                }
                                lblFriday.Text = fridayStart + "<br />to<br />" + fridayEnd;
                            }
                            if (saturdayStart != "" || saturdayEnd != "") {
                                if (saturdayStart != "") {
                                    saturdayStart = Convert.ToDateTime(saturdayStart).ToString("%h:mm tt");
                                }
                                if (saturdayEnd != "") {
                                    saturdayEnd = Convert.ToDateTime(saturdayEnd).ToString("%h:mm tt");
                                }
                                lblSaturday.Text = saturdayStart + "<br />to<br />" + saturdayEnd;
                            }
                            if (sundayStart != "" || sundayEnd != "") {
                                if (sundayStart != "") {
                                    sundayStart = Convert.ToDateTime(sundayStart).ToString("%h:mm tt");
                                }
                                if (sundayEnd != "") {
                                    sundayEnd = Convert.ToDateTime(sundayEnd).ToString("%h:mm tt");
                                }
                                lblSunday.Text = sundayStart + "<br />to<br />" + sundayEnd;
                            }
                            schedule2.Visible = true;
                        }
                        */
                        //Populate Work schedule for view/confirm
                        PopulateWorkScheduleForLabel(positionRequestID);
                    } else {
                        schedule2.Visible = false;
                    }
                    //Show Originator Comments
                    lblOrigComments.Text = drPr["Comments"].ToString();
                    //Populate Building number and room number
                    lblBuildingNumber.Text = drPr["BuildingNumber"].ToString();
                    lblRoomNumber.Text = drPr["RoomNumber"].ToString();
                    //Populate label for ComboCode
                    lblComboCode.Text = drPr["ComboCode"].ToString();
                }

                //show uploaded file attachments
                DataSet dsAttachments = positionRequest.GetAttachments(positionRequestID);
                Int32 attachmentCount = dsAttachments.Tables[0].Rows.Count;
                if (attachmentCount > 0) {
                    for (Int32 i = 0; i < attachmentCount; i++) {
                        lblAttachments.Text += "<div style=\"padding-bottom:5px;\"><a href=\"..\\Attachments/" + dsAttachments.Tables[0].Rows[i]["FileName"].ToString() + "\" target=\"self\" class=\"smallText\" title=\"View File Attachment " + (i + 1) + "\">Attachment " + (i + 1) + "</a></div>";
                    }
                    panViewFileAttachments.Visible = true;
                } else {
                    panViewFileAttachments.Visible = false;
                }

                //get Routing/Approvals
                DataSet dsRouting = positionRequest.GetRouting(positionRequestID);
                Int32 routingCount = dsRouting.Tables[0].Rows.Count;
                if (routingCount > 0) {
                    for (Int32 i = 0; i < routingCount; i++) {
                        String note = dsRouting.Tables[0].Rows[i]["Note"].ToString();
                        String description = dsRouting.Tables[0].Rows[i]["Description"].ToString();
                        TableRow tr = new TableRow();
                        TableCell td = new TableCell();
                        td.CssClass = "solid";
                        td.Attributes["style"] = "vertical-align:top;padding-left:10px;width:75px";
                        td.Controls.Add(new LiteralControl(dsRouting.Tables[0].Rows[i]["UserType"].ToString()));
                        tr.Cells.Add(td);
                        td = new TableCell();
                        td.CssClass = "solid";
                        td.Attributes["style"] = "vertical-align:top;width:190px";
                        td.Controls.Add(new LiteralControl(dsRouting.Tables[0].Rows[i]["Name"].ToString()));
                        tr.Cells.Add(td);
                        td = new TableCell();
                        td.CssClass = "solid";
                        td.Attributes["style"] = "vertical-align:top";
                        td.Controls.Add(new LiteralControl(description));
                        try {
                            if (description != "Pending Approval" && description != "Complete") {
                                td.Controls.Add(new LiteralControl("&nbsp;" + Convert.ToDateTime(dsRouting.Tables[0].Rows[i]["Date"]).ToString("MM/dd/yy %h:mm tt")));
                            } else if (description == "Complete") {
                                td.Controls.Add(new LiteralControl("&nbsp;" + Convert.ToDateTime(dsRouting.Tables[0].Rows[i]["Completed"]).ToString("MM/dd/yy")));
                            }
                        } catch {
                            //do nothing
                        }
                        if (note != "") {
                            td.Controls.Add(new LiteralControl("<br />" + note));
                        }
                        tr.Cells.Add(td);
                        tblApprovals.Rows.Add(tr);
                    }
                    panApprovals.Visible = true;
                } else {
                    panApprovals.Visible = false;
                }

                if ((lblStatus.Text == "HRO" || lblStatus.Text == "Recruiting" || lblStatus.Text == "Hired" || lblStatus.Text == "Complete") &&
                    (lblEmployeeName.Text != "" && lblEmployeeID.Text != "" && lblEmployeeEmailAddress.Text != "" && lblSalaryRangeStep.Text != "" && lblSalaryAmount.Text != "$0.00" && lblEmployeeType.Text != "")) {
                    panEmployeeInfo.Visible = true;
                }

                lblConfirm.Visible = true;
                cmdBack.Visible = false;
                cmdSubmit.Visible = false;
                panForm.Visible = true;
                panOutput.Visible = true;
                cmdPrint.Visible = true;
                cmdOK.Visible = true;

            } else {
                if (hidUserType.Value == "HROAdmin") {
                    SearchPositionRequests(txtSearchOriginatorName.Text, cboSearchStatus.SelectedValue, txtSearchTrackingNumber.Text, txtSearchOfficialTitle.Text, txtSearchPositionNumber.Text, hidSearchDepartment.Value, cboSearchPositionType.SelectedValue, cboSearchRecruitmentType.SelectedValue, cboSearchCollegeUnit.SelectedValue, txtCreationStartDate.Value, txtCreationEndDate.Value, txtCompletionStartDate.Value, txtCompletionEndDate.Value);
                } else {
                    GetPositionRequestsForHROAction();
                }
            }
        }
    }

    private void GetPositionRequestsForHROAction() {
        panSearch.Visible = false;
        DataSet dsPositionRequests = positionRequest.GetPositionRequestsForHROAction();
        Int32 rowCount = dsPositionRequests.Tables[0].Rows.Count;

        TableHeaderRow thr = new TableHeaderRow();
        TableHeaderCell thd = new TableHeaderCell();
        thd.Controls.Add(new LiteralControl("Status"));
        thd.CssClass = "sortHeader";
        thd.Attributes.Add("onclick", "sortTable(0, \"" + tblList.ClientID + "\")");
        thr.Cells.Add(thd);

        thd = new TableHeaderCell();
        thd.Controls.Add(new LiteralControl("Created"));
        thd.CssClass = "sortHeader";
        thd.Attributes.Add("onclick", "sortTable(1, \"" + tblList.ClientID + "\")");
        thr.Cells.Add(thd);

        thd = new TableHeaderCell();
        thd.Controls.Add(new LiteralControl("Official Title"));
        thd.CssClass = "sortHeader";
        thd.Attributes.Add("onclick", "sortTable(2, \"" + tblList.ClientID + "\")");
        thr.Cells.Add(thd);

        thd = new TableHeaderCell();
        thd.Controls.Add(new LiteralControl("Position Type"));
        thd.CssClass = "sortHeader";
        thd.Attributes.Add("onclick", "sortTable(3, \"" + tblList.ClientID + "\")");
        thr.Cells.Add(thd);

        thd = new TableHeaderCell();
        thr.Cells.Add(thd);
        tblList.Rows.Add(thr);

        if (rowCount > 0) {
            for (Int32 i = 0; i < rowCount; i++) {
                Int32 positionRequestID = Convert.ToInt32(dsPositionRequests.Tables[0].Rows[i]["PositionRequestID"]);
                String created = "";
                try {
                    created = Convert.ToDateTime(dsPositionRequests.Tables[0].Rows[i]["Created"]).ToShortDateString();
                } catch {
                    //do nothing
                }
                String department = dsPositionRequests.Tables[0].Rows[i]["Department"].ToString();
                String status = dsPositionRequests.Tables[0].Rows[i]["Status"].ToString();
                String officialTitle = dsPositionRequests.Tables[0].Rows[i]["OfficialTitle"].ToString();
                String positionType = dsPositionRequests.Tables[0].Rows[i]["PositionType"].ToString();

                TableRow tr = new TableRow();
                TableCell td = new TableCell();
                td.CssClass = "solid";
                td.Controls.Add(new LiteralControl(status));
                tr.Cells.Add(td);

                td = new TableCell();
                td.CssClass = "solid";
                td.Controls.Add(new LiteralControl(created));
                tr.Cells.Add(td);

                td = new TableCell();
                td.CssClass = "solid";
                td.Controls.Add(new LiteralControl(officialTitle));
                tr.Cells.Add(td);

                td = new TableCell();
                td.CssClass = "solid";
                td.Controls.Add(new LiteralControl(positionType));
                tr.Cells.Add(td);

                td = new TableCell();
                td.CssClass = "action";
                td.Attributes["style"] = "vertical-align: top;width: 16px; background: #eee none";
                td.Controls.Add(new LiteralControl("<a href=\"#\" class=\"view\" title=\"View Form for HRO Action\" onclick=\"document.getElementById('" + hidTodo.ClientID + "').value='view';document.getElementById('" + hidID.ClientID + "').value='" + positionRequestID + "';document.getElementById('" + hidStatus.ClientID + "').value='" + status + "';document.getElementById('" + hidCreated.ClientID + "').value='" + created + "';document.getElementById('" + frmPR.ClientID + "').submit();\"></a>"));
                tr.Cells.Add(td);

                tr.Cells.Add(td);
                tblList.Rows.Add(tr);
            }
        } else {
            TableRow tr = new TableRow();
            TableCell td = new TableCell();
            td.ColumnSpan = 5;
            td.Controls.Add(new LiteralControl("No forms awaiting HRO action currently exist."));
            tr.Cells.Add(td);
            tblList.Rows.Add(tr);
        }

        panList.Visible = true;
    }

    private void SearchPositionRequests(String originatorName, String status, String trackingNumber, String officialTitle, String positionNumber, String departmentID, String positionType, String recruitmentType, String collegeUnit, String creationStartDate, String creationEndDate, String completionStartDate, String completionEndDate) { 

        DataSet dsPositionRequests = positionRequest.GetPositionRequests("", originatorName, status, trackingNumber, officialTitle, positionNumber, departmentID, positionType, recruitmentType, collegeUnit, creationStartDate, creationEndDate, completionStartDate, completionEndDate);
        Int32 rowCount = dsPositionRequests.Tables[0].Rows.Count;

        TableHeaderRow thr = new TableHeaderRow();
        TableHeaderCell thd = new TableHeaderCell();
        thd.Controls.Add(new LiteralControl("Status"));
        thd.CssClass = "sortHeader";
        thd.Attributes.Add("onclick", "sortTable(0, \"" + tblList.ClientID + "\")");
        thr.Cells.Add(thd);

        thd = new TableHeaderCell();
        thd.Controls.Add(new LiteralControl("Created"));
        thd.CssClass = "sortHeader";
        thd.Attributes.Add("onclick", "sortTable(1, \"" + tblList.ClientID + "\")");
        thr.Cells.Add(thd);

        thd = new TableHeaderCell();
        thd.Controls.Add(new LiteralControl("Official Title"));
        thd.CssClass = "sortHeader";
        thd.Attributes.Add("onclick", "sortTable(2, \"" + tblList.ClientID + "\")");
        thr.Cells.Add(thd);

        thd = new TableHeaderCell();
        thd.Controls.Add(new LiteralControl("Position Type"));
        thd.CssClass = "sortHeader";
        thd.Attributes.Add("onclick", "sortTable(3, \"" + tblList.ClientID + "\")");
        thr.Cells.Add(thd);

        thd = new TableHeaderCell();
        thd.ColumnSpan = 2;
        thr.Cells.Add(thd);
        tblList.Rows.Add(thr);

        if (rowCount > 0) {
            for (Int32 i = 0; i < rowCount; i++) {
                Int32 positionRequestID = Convert.ToInt32(dsPositionRequests.Tables[0].Rows[i]["PositionRequestID"]);
                String created = "";
                try {
                    created = Convert.ToDateTime(dsPositionRequests.Tables[0].Rows[i]["Created"]).ToShortDateString();
                } catch {
                    //do nothing
                }
                String department = dsPositionRequests.Tables[0].Rows[i]["Department"].ToString();
                status = dsPositionRequests.Tables[0].Rows[i]["Status"].ToString();
                officialTitle = dsPositionRequests.Tables[0].Rows[i]["OfficialTitle"].ToString();
                positionType = dsPositionRequests.Tables[0].Rows[i]["PositionType"].ToString();

                TableRow tr = new TableRow();
                TableCell td = new TableCell();
                td.CssClass = "solid";
                td.Controls.Add(new LiteralControl(status));
                tr.Cells.Add(td);

                td = new TableCell();
                td.CssClass = "solid";
                td.Controls.Add(new LiteralControl(created));
                tr.Cells.Add(td);

                td = new TableCell();
                td.CssClass = "solid";
                td.Controls.Add(new LiteralControl(officialTitle));
                tr.Cells.Add(td);

                td = new TableCell();
                td.CssClass = "solid";
                td.Controls.Add(new LiteralControl(positionType));
                tr.Cells.Add(td);

                /*
                td = new TableCell();
                td.CssClass = "solid";
                td.Controls.Add(new LiteralControl(department));
                tr.Cells.Add(td);
                */

                td = new TableCell();
                td.CssClass = "action";
                td.Attributes["style"] = "vertical-align: top;width: 16px; background: #eee none";
                if (status == "HRO") {
                    td.Controls.Add(new LiteralControl("<a href=\"#\" class=\"view\" title=\"View Form for HRO Approval\" onclick=\"document.getElementById('" + hidTodo.ClientID + "').value='view';document.getElementById('" + hidStatus.ClientID + "').value='" + status + "';document.getElementById('" + hidID.ClientID + "').value='" + positionRequestID + "';document.getElementById('" + hidCreated.ClientID + "').value='" + created + "';document.getElementById('" + frmPR.ClientID + "').submit();\"></a>"));
                } else if (status == "Recruiting" || status == "Hired") {
                    td.Controls.Add(new LiteralControl("<a href=\"#\" class=\"view\" title=\"View Form for HRO Action\" onclick=\"document.getElementById('" + hidTodo.ClientID + "').value='view';document.getElementById('" + hidStatus.ClientID + "').value='" + status + "';document.getElementById('" + hidID.ClientID + "').value='" + positionRequestID + "';document.getElementById('" + hidCreated.ClientID + "').value='" + created + "';document.getElementById('" + frmPR.ClientID + "').submit();\"></a>"));
                } else {
                    td.Controls.Add(new LiteralControl("<a href=\"#\" class=\"view\" title=\"View Form\" onclick=\"document.getElementById('" + hidTodo.ClientID + "').value='view';document.getElementById('" + hidStatus.ClientID + "').value='" + status + "';document.getElementById('" + hidID.ClientID + "').value='" + positionRequestID + "';document.getElementById('" + hidCreated.ClientID + "').value='" + created + "';document.getElementById('" + frmPR.ClientID + "').submit();\"></a>"));
                }
                tr.Cells.Add(td);

                td = new TableCell();
                td.CssClass = "action";
                td.Attributes["style"] = "vertical-align: top;width: 16px; background: #eee none";
                if (status == "Cancelled" || status == "Complete" || status == "Incomplete" || status == "Rejected" || status == "Void") {
                    td.Controls.Add(new LiteralControl("<a href=\"#\" class=\"delete\" title=\"Delete Form\" onclick=\"if(confirm('Are you sure you want to delete the following position request form?\\n\\nStatus: " + status + "\\nCreated: " + created + "\\nOfficial Title: " + officialTitle + "\\nPosition Type: " + positionType + "\\nDepartment: " + department.Replace("'", "\\'") + "')){document.getElementById('" + hidTodo.ClientID + "').value='delete';document.getElementById('" + hidID.ClientID + "').value='" + positionRequestID + "';document.getElementById('" + frmPR.ClientID + "').submit();}\"></a>"));
                }
                tr.Cells.Add(td);
                tblList.Rows.Add(tr);
            }
        } else {
            TableRow tr = new TableRow();
            TableCell td = new TableCell();
            td.ColumnSpan = 6;
            if (originatorName == "" && status == "" && trackingNumber == "" && officialTitle == "" && positionNumber == "" && departmentID == "" && positionType == "" && recruitmentType == "" && collegeUnit == "" && creationStartDate == "" && creationEndDate == "" && completionStartDate == "" && completionEndDate == "") {
                td.Controls.Add(new LiteralControl("No forms currently exist."));
            } else {
                td.Controls.Add(new LiteralControl("Your search returned 0 results."));
            }
            tr.Cells.Add(td);
            tblList.Rows.Add(tr);
        }

        panList.Visible = true;

    }

    private void populateFileAttachments() {
        tblAttachments.Rows.Clear();
        if (hidFileAttachments.Value != "") {
            String[] strFileName = hidFileAttachments.Value.Substring(1).Split('|');
            for (Int32 i = 0; i < strFileName.Length; i++) {
                TableRow tr = new TableRow();
                TableCell td = new TableCell();
                tr = new TableRow();
                td = new TableCell();
                td.Attributes["style"] = "background:none;border:none;padding-left:10px;padding-top:10px;";
                if (i < (strFileName.Length - 1)) {
                    td.Attributes["style"] += "border-bottom:solid 1px #aaa";
                }
                td.Controls.Add(new LiteralControl("<a href=\"..\\Attachments/" + strFileName[i] + "\" target=\"self\" class=\"smallText\" title=\"View File Attachment " + (i + 1) + "\">Attachment " + (i + 1) + "</a>"));
                tr.Cells.Add(td);
                td = new TableCell();
                td.CssClass = "action";
                td.Attributes["style"] = "background:none;border:none;vertical-align:top;padding-top:10px;width:16px;";
                if (i < (strFileName.Length - 1)) {
                    td.Attributes["style"] += "border-bottom:solid 1px #aaa";
                }
                LinkButton lnkDeleteAttachment = new LinkButton();
                lnkDeleteAttachment.ID = "lnkDeleteAttachment" + i;
                lnkDeleteAttachment.CssClass = "delete";
                lnkDeleteAttachment.Text = "Delete File Attachment " + (i + 1);
                lnkDeleteAttachment.OnClientClick = "return confirm('Are you sure you want to delete file attachment " + (i + 1) + "?\\nNote: Any existing file attachments will be renumbered.');";
                lnkDeleteAttachment.CommandArgument = strFileName[i];
                lnkDeleteAttachment.Click += new EventHandler(lnkDeleteAttachment_Click);
                td.Controls.Add(lnkDeleteAttachment);
                tr.Cells.Add(td);
                tblAttachments.Rows.Add(tr);
            }
            panEditFileAttachments.Visible = true;
        } else {
            panEditFileAttachments.Visible = false;
        }
    }

    protected void lnkDeleteAttachment_Click(object sender, System.EventArgs e) {
        //get clicked linkbutton
        LinkButton lnkDelete = (LinkButton)sender;
        //get the file name
        String strFileName = lnkDelete.CommandArgument.ToString();
        //delete the file from the server
        System.IO.File.Delete(Server.MapPath("..\\Attachments\\") + strFileName);
        //delete the file from the database
        positionRequest.DeleteAttachment(strFileName);
        //delete the file name from the hidden form element
        hidFileAttachments.Value = hidFileAttachments.Value.Replace("|" + strFileName, "");
        //populate file attachments
        populateFileAttachments();
    }

    protected void cmdSearch_Click(object sender, System.EventArgs e) {
        hidTodo.Value = "";
    }

    protected void cmdBack_Click(object sender, System.EventArgs e) {
        hidTodo.Value = "";
        hidID.Value = "";
        hidDepartment.Value = "";
        hidDepartmentID.Value = "";
        hidSupervisorID.Value = "";
        hidStatus.Value = "";
        hidCreated.Value = "";
        hidPopulateFromDB.Value = "true";
        hidFileAttachments.Value = "";
        hidChangeViews.Value = "";
        chkModify.Checked = false;
        panCheckbox.Visible = false;
        panForm.Visible = false;
        if (hidUserType.Value == "HROAdmin") {
            SearchPositionRequests(txtSearchOriginatorName.Text, cboSearchStatus.SelectedValue, txtSearchTrackingNumber.Text, txtSearchOfficialTitle.Text, txtSearchPositionNumber.Text, hidSearchDepartment.Value, cboSearchPositionType.SelectedValue, cboSearchRecruitmentType.SelectedValue, cboSearchCollegeUnit.SelectedValue, txtCreationStartDate.Value, txtCreationEndDate.Value, txtCompletionStartDate.Value, txtCompletionEndDate.Value);
        } else {
            GetPositionRequestsForHROAction();
        }
    }

    protected void cmdOK_Click(object sender, System.EventArgs e) {
        hidTodo.Value = "";
        hidID.Value = "";
        hidDepartment.Value = "";
        hidDepartmentID.Value = "";
        hidSupervisorID.Value = "";
        hidStatus.Value = "";
        hidCreated.Value = "";
        hidPopulateFromDB.Value = "true";
        hidFileAttachments.Value = "";
        hidChangeViews.Value = "";
        chkModify.Checked = false;
        panCheckbox.Visible = false;
        panForm.Visible = false;
        if (hidUserType.Value == "HROAdmin") {
            SearchPositionRequests(txtSearchOriginatorName.Text, cboSearchStatus.SelectedValue, txtSearchTrackingNumber.Text, txtSearchOfficialTitle.Text, txtSearchPositionNumber.Text, hidSearchDepartment.Value, cboSearchPositionType.SelectedValue, cboSearchRecruitmentType.SelectedValue, cboSearchCollegeUnit.SelectedValue, txtCreationStartDate.Value, txtCreationEndDate.Value, txtCompletionStartDate.Value, txtCompletionEndDate.Value);
        } else {
            GetPositionRequestsForHROAction();
        }
    }

    protected void cmdSubmit_Click(object sender, System.EventArgs e) {
        String employeeID = Request.Cookies["phatt2"]["userctclinkid"], selectedOption = Request.Form["ctl00$maintextHolder$optApproval"], rejectReason = Request.Form["ctl00$maintextHolder$txtReject"], voidReason = Request.Form["ctl00$maintextHolder$txtVoid"];
        String newEmployeeName = Request.Form["ctl00$maintextHolder$txtEmployeeName"], newEmployeeID = Request.Form["ctl00$maintextHolder$txtEmployeeID"], salaryRangeStep = Request.Form["ctl00$maintextHolder$txtSalaryRangeStep"];
        String newEmployeeType = Request.Form["ctl00$maintextHolder$cboEmployeeType"], newEmployeeEmailAddress = Request.Form["ctl00$maintextHolder$txtEmployeeEmailAddress"], completionDate = Request.Form["ctl00$maintextHolder$txtCompletionDate"];
        String name = positionRequest.GetEmployeeName(employeeID), notified = "", notNotified = "", task = "", status = lblStatus.Text;
        string effectiveDate = Request.Form["ctl00$maintextHolder$txtEffectiveDate"];
        String selectedStatus = Request.Form["ctl00$maintextHolder$cboStatus"];
        Int16 pCard = 0, CCSIssuedCellPhone = 0, ctcLinkAccess = 0, timeSheetApprover = 0;
        Int32 positionRequestID = Convert.ToInt32(hidID.Value);
        bool blnSuccess = true;

        Decimal salaryAmount = 0; 
        try {
            salaryAmount = Convert.ToDecimal(Request.Form["ctl00$maintextHolder$txtSalaryAmount"]);
        } catch {
            //do nothing
        }

        //upload file attachments
        HttpFileCollection uploads = HttpContext.Current.Request.Files;
        for (int i = 0; i < uploads.Count; i++) {
            HttpPostedFile upload = uploads[i];
            if (upload.ContentLength == 0) {
                continue;
            }
            String c = System.IO.Path.GetFileName(upload.FileName);
            try {
                String strFileName = positionRequestID + "_" + i + System.IO.Path.GetExtension(c);
                upload.SaveAs(Server.MapPath("..\\Attachments\\") + strFileName); 
                positionRequest.AddAttachment(positionRequestID, strFileName);
            } catch {
                blnSuccess = false;
            }
        }

        if (blnSuccess) {

            //get pending approval record
            String pendingEmployeeID = "", pendingName = "", pendingUserType = "", pendingDescription = "";
            DataSet dsPendingApproval = positionRequest.GetPendingApproval(positionRequestID);
            if(dsPendingApproval.Tables[0].Rows.Count > 0) {
                pendingEmployeeID = dsPendingApproval.Tables[0].Rows[0]["EmployeeID"].ToString();
                pendingName = dsPendingApproval.Tables[0].Rows[0]["Name"].ToString();
                pendingUserType = dsPendingApproval.Tables[0].Rows[0]["UserType"].ToString();
                pendingDescription = dsPendingApproval.Tables[0].Rows[0]["Description"].ToString();
            } else {
                dsPendingApproval = positionRequest.GetCurrentRoutingRecord(positionRequestID, status);
                if(dsPendingApproval.Tables[0].Rows.Count > 0) {
                    pendingEmployeeID = dsPendingApproval.Tables[0].Rows[0]["EmployeeID"].ToString();
                    pendingName = dsPendingApproval.Tables[0].Rows[0]["Name"].ToString();
                    pendingUserType = dsPendingApproval.Tables[0].Rows[0]["UserType"].ToString();
                    pendingDescription = dsPendingApproval.Tables[0].Rows[0]["Description"].ToString();
                }
            }

            if (chkModify.Checked) {
                //store bit field values
                Int16 permanentPosition = 0, nonPermanentPosition = 0, newPosition = 0, replacementPosition = 0;

                if (optPermanent.Checked) {
                    permanentPosition = 1;
                }
                if (optNonPermanent.Checked) {
                    nonPermanentPosition = 1;
                }
                if (optNew.Checked) {
                    newPosition = 1;
                }
                if (optReplacement.Checked) {
                    replacementPosition = 1;
                }
                if (chkPCard.Checked) {
                    pCard = 1;
                }
                if (chkCellPhone.Checked) {
                    CCSIssuedCellPhone = 1;
                }
                if (chkAdditionalAccess.Checked) {
                    ctcLinkAccess = 1;
                }
                if (chkTimeSheetApprover.Checked) {
                    timeSheetApprover = 1;
                }

                positionRequestID = Convert.ToInt32(hidID.Value);
                blnSuccess = positionRequest.EditPositionRequest(positionRequestID, lblOriginatorName.Text, txtOfficialTitle.Text, txtWorkingTitle.Text, txtJobCode.Text
                                                                , cboCollegeUnit.SelectedValue, txtBudgetNumber.Text, txtPositionNumber.Text, hidDepartmentID.Value, hidDepartment.Value
                                                                , txtPhoneNumber.Text, txtMailStop.Text, txtSupervisorName.Text, hidSupervisorID.Value, cboPositionType.Text, permanentPosition
                                                                , nonPermanentPosition, txtEndDate.Value, cboRecruitmentType.SelectedValue, newPosition, replacementPosition, txtEmployeeReplaced.Text
                                                                , pCard, CCSIssuedCellPhone, ctcLinkAccess, timeSheetApprover, txtHoursPerDay.Text, txtHoursPerWeek.Text, txtMonthsPerYear.Text, txtCyclicCalendarCode.Text
                                                                , status
                                                                , txtOrigComments.Text
                                                                 , txtBuildingNumber.Text, txtRoomNumber.Text);
                //Update ComboCode
                blnSuccess = positionRequest.UpdateComboCode(positionRequestID, txtComboCode.Text);
                if (blnSuccess) {
                    task = "modified";

                    //add work schedule
                    if (cboPositionType.SelectedValue == "Classified" || cboPositionType.SelectedValue == "Part-Time Hourly") {
                        blnSuccess = positionRequest.EditWorkSchedule(positionRequestID, txtMondayStart.Value, txtMondayEnd.Value, txtTuesdayStart.Value, txtTuesdayEnd.Value, txtWednesdayStart.Value, txtWednesdayEnd.Value, txtThursdayStart.Value, txtThursdayEnd.Value, txtFridayStart.Value, txtFridayEnd.Value, txtSaturdayStart.Value, txtSaturdayEnd.Value, txtSundayStart.Value, txtSundayEnd.Value);
                        if (blnSuccess == false) {
                            lblError.Text = "<p><strong>Error: Submitting the work schedule failed.</strong></p>" + strErrorContact + "<br />";
                        }
                    } else {
                        //delete the work schedule
                        positionRequest.DeleteWorkSchedule(positionRequestID);
                    }

                    //store when HRO modifed the form
                    positionRequest.AddRouting(positionRequestID, "HRO", "Modified", txtModReason.Text, name, employeeID, DateTime.Now.ToString("MM/dd/yy HH:mm:ss.fff"));

                    //modify pending approval date
                    positionRequest.EditRoutingDate(positionRequestID, pendingUserType, "Pending Approval", DateTime.Now.AddMilliseconds(100).ToString("MM/dd/yy HH:mm:ss.fff"));

                    SmtpClient client = new SmtpClient();
                    MailMessage objEmail = new MailMessage();
                    String emailBody = "";

                    if (selectedOption == "optRestart") {//restart the approval process
                        //get the originator's supervisor
                        DataSet dsSupervisor = positionRequest.GetSupervisor(lblOriginatorID.Text);
                        String supervisorJobCode = "", supervisorEmail = "", supervisorID = "", supervisorName = "", menu = "Supervisor";
                        if (dsSupervisor.Tables[0].Rows.Count > 0) {
                            supervisorJobCode = dsSupervisor.Tables[0].Rows[0]["JOBCODE"].ToString();
                            supervisorEmail = dsSupervisor.Tables[0].Rows[0]["EMAIL_ADDR"].ToString();
                            supervisorID = dsSupervisor.Tables[0].Rows[0]["EMPLID"].ToString();
                            supervisorName = dsSupervisor.Tables[0].Rows[0]["NAME"].ToString();
                            status = "Approver 1";

                            if (cboPositionType.SelectedValue != "Adjunct Faculty" && cboPositionType.SelectedValue != "Part-Time Hourly" && (String.Compare(hidDepartmentID.Value, "99001") < 0 || String.Compare(hidDepartmentID.Value, "99021") > 0)) { //(Convert.ToInt32(hidDepartmentID.Value) < 99001 && Convert.ToInt32(hidDepartmentID.Value) > 99021)) {
                                if (supervisorJobCode == "111113" || supervisorJobCode == "111133") {
                                    status = "Executive";
                                }
                            } else if (cboPositionType.SelectedValue == "Adjunct Faculty")
                            {
                                //if (supervisorJobCode == "111274" || supervisorJobCode == "111577" || supervisorJobCode == "111465" || supervisorJobCode == "111439" || supervisorJobCode == "111120")
                                if(Utility.GetAdjunctApprovalJobCode().Contains(supervisorJobCode))
                                {
                                    status = "Budget";
                                    //supervisorEmail = "vu.nguyen@ccs.spokane.edu"; //for testing
                                    supervisorEmail = "BudgetPR@ccs.spokane.edu"; //setup this email account
                                    supervisorName = "";
                                    supervisorID = "";
                                    menu = "Budget Admin";
                                }
                            } else if (cboPositionType.SelectedValue == "Part-Time Hourly" || (String.Compare(hidDepartmentID.Value, "99001") >= 0 && String.Compare(hidDepartmentID.Value, "99021") <= 0)) {
                                //var supervisorJobCodeList = new List<string> { "111272", "111165", "111499", "111319", "111433", "111274", "111577", "111465" , "111439", "111120" };
                                //if (supervisorJobCode == "111272" || supervisorJobCode == "111165" || supervisorJobCode == "111319" || supervisorJobCode == "111433" || supervisorJobCode == "111274" || supervisorJobCode == "111577" || supervisorJobCode == "111465" || supervisorJobCode == "111439" || supervisorJobCode == "111120") {
                                if(excutiveApprovalJC.Contains(supervisorJobCode))
                                { 
                                    status = "Executive";
                                    if (supervisorJobCode == "111433") { //if Greg Stevens is the Executive approver
                                        //change the Executive approver to Melody Matthews
                                        supervisorJobCode = "111490";
                                        dsSupervisor = positionRequest.GetEmployeeByJobCode(supervisorJobCode);
                                        if (dsSupervisor.Tables[0].Rows.Count > 0) {
                                            supervisorEmail = dsSupervisor.Tables[0].Rows[0]["EMAIL_ADDR"].ToString();
                                            supervisorID = dsSupervisor.Tables[0].Rows[0]["EMPLID"].ToString();
                                            supervisorName = dsSupervisor.Tables[0].Rows[0]["NAME"].ToString();
                                        } else {
                                            lblError.Text = "<p><strong>Error: The approval process could not be restarted. The originator's supervisor data could not be found.</strong></p>" + strErrorContact + "<br />";
                                            blnSuccess = false;
                                        }
                                    }
                                }
                            }

                            //stop the current pending approval
                            blnSuccess = positionRequest.EditApproval(positionRequestID, pendingEmployeeID, pendingName, pendingUserType, "Pending Approval", "HRO Restarted Approval Process", "", DateTime.Now.AddMilliseconds(100).ToString("MM/dd/yy HH:mm:ss.fff"));
                            if (blnSuccess) {
                                //restart approval process
                                blnSuccess = positionRequest.AddRouting(positionRequestID, status, "Pending Approval", "", supervisorName, supervisorID, DateTime.Now.AddMilliseconds(200).ToString("MM/dd/yy HH:mm:ss.fff"));
                                if (blnSuccess) {
                                    //change position request status
                                    blnSuccess = positionRequest.EditStatus(positionRequestID, status);

                                    if (blnSuccess) {
                                        if (status == "Budget") {
                                            supervisorName = "the Budget Admin";
                                        }

                                        //send email for approval
                                        client = new SmtpClient();
                                        objEmail = new MailMessage();
                                        emailBody = "<p><strong>Tracking #:</strong> " + lblTrackingNumber.Text +
                                                    "<br /><strong>Originator:</strong> " + lblOriginatorName.Text +
                                                    "<br /><strong>Created:</strong> " + hidCreated.Value +
                                                    "<br /><strong>College/Unit:</strong> " + cboCollegeUnit.Text +
                                                    "<br /><strong>Department:</strong> " + hidDepartment.Value +
                                                    "<br /><strong>Official Title:</strong> " + txtOfficialTitle.Text +
                                                    "<br /><strong>Supervisor Name:</strong> " + txtSupervisorName.Text +
                                                    "<br /><strong>Position Type:</strong> " + cboPositionType.Text +
                                                    "<br /><strong>Status:</strong> " + status + "</p>";

                                        if (supervisorEmail != "") {
                                            //objEmail.To.Add(supervisorEmail); 
                                            //objEmail.To.Add("vu.nguyen@ccs.spokane.edu");
                                            /*
                                            if (appHost.Contains("internal"))
                                            {
                                                objEmail.To.Add(supervisorEmail);
                                            }
                                            else
                                            {
                                                objEmail.To.Add(emailTest);
                                            }
                                            */
                                            objEmail.To.Add(Utility.GetSendEmail(supervisorEmail));
                                            objEmail.From = new MailAddress(strAdminEmailAddress);
                                            objEmail.IsBodyHtml = true;
                                            objEmail.Priority = MailPriority.High;
                                            objEmail.Subject = "Position Request Approval Notification";
                                            objEmail.Body = "<p>Dear " + supervisorName + ",</p>";
                                            objEmail.Body += "<p>Please review the Position Request below for approval.</p>";
                                            objEmail.Body += "<p><a href=\"https://internal.spokane.edu/PositionRequest/\">https://internal.spokane.edu/PositionRequest/</a>";
                                            objEmail.Body += "<br />Log in with your Active Directory account and click on the PR Approval link in the " + menu + " menu.</p>";
                                            objEmail.Body += emailBody;

                                            try {
                                                client.Send(objEmail);
                                                notified += "|" + status + ": " + supervisorName;
                                            } catch {
                                                objEmail = new MailMessage();
                                                objEmail.To.Add(strAdminEmailAddress);
                                                objEmail.From = new MailAddress(strAdminEmailAddress);
                                                objEmail.IsBodyHtml = true;
                                                objEmail.Priority = MailPriority.High;
                                                objEmail.Subject = "Position Request Approval Notification Could Not Be Sent";
                                                objEmail.Body = "<p>The following approval notification email could not be sent to " + supervisorName + " at the email address " + supervisorEmail + ".</p>";
                                                objEmail.Body += "<hr style=\"width:100%\" />";
                                                objEmail.Body += emailBody;

                                                try {
                                                    client.Send(objEmail);
                                                    notNotified += "|" + status + ": " + supervisorName + ".|HRO has been notified.";
                                                } catch {
                                                    notNotified += "|" + status + ": " + supervisorName + " or HRO.|Please contact HRO at " + strAdminEmailAddress + ".";
                                                }
                                            }
                                        } else {
                                            objEmail = new MailMessage();
                                            objEmail.To.Add(strAdminEmailAddress);
                                            objEmail.From = new MailAddress(strAdminEmailAddress);
                                            objEmail.IsBodyHtml = true;
                                            objEmail.Priority = MailPriority.High;
                                            objEmail.Subject = "Position Request Approval Notification Could Not Be Sent";
                                            objEmail.Body = "<p>The following approval notification email could not be sent to " + supervisorName + " because their email address does not exist.</p>";
                                            objEmail.Body += "<hr style=\"width:100%\" />";
                                            objEmail.Body += emailBody;

                                            try {
                                                client.Send(objEmail);
                                                notNotified += "|" + status + ": " + supervisorName + ".|HRO has been notified.";
                                            } catch {
                                                notNotified += "|" + status + ": " + supervisorName + " or HRO.|Please contact HRO at " + strAdminEmailAddress + ".";
                                            }
                                        }

                                        task += " and the approval process restarted";

                                    } else {
                                        lblError.Text = "<p><strong>Error: The Position Request status could not be updated to " + status + ".";
                                        //set routing record back to Pending the previous approval 
                                        blnSuccess = positionRequest.EditApproval(positionRequestID, pendingEmployeeID, pendingName, pendingUserType, "HRO Restarted Approval Process", "Pending Approval", "", DateTime.Now.AddMilliseconds(100).ToString("MM/dd/yy HH:mm:ss.fff"));
                                        if (blnSuccess) {
                                            lblError.Text += " The position request has been set back to pending " + pendingUserType + " approval.";
                                        } else {
                                            lblError.Text += " The position request could not be set back to pending " + pendingUserType + " approval.";
                                        }
                                        lblError.Text += "</strong></p>" + strErrorContact + "<br />";
                                        blnSuccess = false;
                                    }
                                } else {
                                    lblError.Text = "<p><strong>Error: The approval process could not be restarted. Unable to send to " + status + ".";
                                    //set routing record back to Pending the previous approval 
                                        blnSuccess = positionRequest.EditApproval(positionRequestID, pendingEmployeeID, pendingName, pendingUserType, "HRO Restarted Approval Process", "Pending Approval", "", DateTime.Now.AddMilliseconds(100).ToString("MM/dd/yy HH:mm:ss.fff"));
                                        if (blnSuccess) {
                                            lblError.Text += " The position request has been set back to pending " + pendingUserType + " approval.";
                                        } else {
                                            lblError.Text += " The position request could not be set back to pending " + pendingUserType + " approval.";
                                        }
                                    lblError.Text += "</strong></p>" + strErrorContact + "<br />";
                                    blnSuccess = false;
                                }
                            } else {
                                lblError.Text = "<p><strong>Error: The approval process could not be restarted. Unable to record HRO Restarted Approval record.</strong></p>" + strErrorContact + "<br />";
                            }
                        } else {
                            lblError.Text = "<p><strong>Error: The approval process could not be restarted. The originator's supervisor data could not be found.</strong></p>" + strErrorContact + "<br />";
                            blnSuccess = false;
                        }
                    }

                    //notify originator of modification
                    client = new SmtpClient();
                    objEmail = new MailMessage();
                    String originatorEmail = positionRequest.GetEmployeeEmail(lblOriginatorID.Text);
                    emailBody = "<p><strong>Modified by:</strong> HRO: " + name +
                                "<br /><strong>Reason:</strong> " + txtModReason.Text +
                                "<br /><br /><strong>Tracking #:</strong> " + lblTrackingNumber.Text +
                                "<br /><strong>Originator:</strong> " + lblOriginatorName.Text +
                                "<br /><strong>Created:</strong> " + hidCreated.Value +
                                "<br /><strong>College/Unit:</strong> " + cboCollegeUnit.Text +
                                "<br /><strong>Department:</strong> " + hidDepartment.Value +
                                "<br /><strong>Official Title:</strong> " + txtOfficialTitle.Text +
                                "<br /><strong>Supervisor Name:</strong> " + txtSupervisorName.Text +
                                "<br /><strong>Position Type:</strong> " + cboPositionType.Text +
                                "<br /><strong>Status:</strong> " + status + "</p>";

                    //objEmail.To.Add(originatorEmail); 
                    //objEmail.To.Add("vu.nguyen@ccs.spokane.edu");
                    /*
                    if (appHost.Contains("internal"))
                    {
                        objEmail.To.Add(originatorEmail);
                    }
                    else
                    {
                        objEmail.To.Add(emailTest);
                    }
                    */
                    objEmail.To.Add(Utility.GetSendEmail(originatorEmail));
                    objEmail.From = new MailAddress(strAdminEmailAddress);
                    objEmail.IsBodyHtml = true;
                    objEmail.Priority = MailPriority.High;
                    objEmail.Subject = "Position Request has been Modified by HRO";
                    objEmail.Body = "<p>Dear originator " + lblOriginatorName.Text + ",</p>";
                    objEmail.Body += "<p>Please review the Position Request below to view the modifications.";
                    if (selectedOption == "optRestart") {
                        objEmail.Body += "<br />The approval process has been restarted.";
                    }
                    objEmail.Body += "</p>";
                    objEmail.Body += "<p><a href=\"https://internal.spokane.edu/PositionRequest/\">https://internal.spokane.edu/PositionRequest/</a>";
                    objEmail.Body += "<br />Log in with your Active Directory account and click on the Employee Action Notice link in the Main Menu.</p>";
                    objEmail.Body += emailBody;

                    try {
                        client.Send(objEmail);
                        notified += "|Originator: " + lblOriginatorName.Text;
                    } catch {
                        objEmail = new MailMessage();
                        objEmail.To.Add(strAdminEmailAddress);
                        objEmail.From = new MailAddress(strAdminEmailAddress);
                        objEmail.IsBodyHtml = true;
                        objEmail.Priority = MailPriority.High;
                        objEmail.Subject = "Position Request Modification Notification Could Not Be Sent";
                        objEmail.Body = "<p>The following modification email could not be sent to the originator: " + lblOriginatorName.Text + " at the email address " + originatorEmail + ".</p>";
                        objEmail.Body += "<hr style=\"width:100%\" />";
                        objEmail.Body += emailBody;

                        try {
                            client.Send(objEmail);
                            notNotified += "|Originator: " + lblOriginatorName.Text + ". HRO has been notified.";
                        } catch {
                            notNotified += "|Originator: " + lblOriginatorName.Text + ". HRO could not be notified. Please contact HRO at " + strAdminEmailAddress + ".";
                        }
                    }
                } else {
                    lblError.Text = "<p><strong>Error: Submitting the modifications failed.</strong></p>" + strErrorContact + "<br />";
                }
            } else {
                if(lblOnboarding.Text.IndexOf("P-Card") > -1) {
                    pCard = 1;
                }
                if(lblOnboarding.Text.IndexOf("CCS-issued Cell Phone") > -1) {
                    CCSIssuedCellPhone = 1;
                }
                if(lblOnboarding.Text.IndexOf("Additional ctcLink Security Access") > -1) {
                    ctcLinkAccess = 1;
                }
                if(lblOnboarding.Text.IndexOf("Manager/Timesheet Approver") > -1) {
                    timeSheetApprover = 1;
                }
            }

            if(status == "HRO" || status == "Recruiting" || status == "Hired") {
                blnSuccess = positionRequest.EditPositionEmployee(positionRequestID, newEmployeeName, newEmployeeID, salaryRangeStep, salaryAmount
                                                                    , newEmployeeType, newEmployeeEmailAddress, completionDate
                                                                    , effectiveDate);
                if (blnSuccess == false) {
                    lblError.Text = "<p><strong>Error: The employee information could not be updated.</strong></p>" + strErrorContact + "<br />";
                    //task = "not modified blnSuccess = false"; //WHY IS THIS HERE?
                }
            } //else {
                //task = "not modified status = " + status; //WHY IS THIS HERE?
            //}

            if (blnSuccess && ((selectedOption == "optApprove") || (selectedStatus == "Recruiting" || selectedStatus == "Hired" || selectedStatus == "Complete"))) {
                String approverEmail = "", approverName = "", approverID = "", menu = "Supervisor", error = "", approval = "Approved";

                if(pendingUserType != "HRO") {
                    approval = "HRO Approved";
                }else{
                    pendingUserType = "HRO";
                    pendingName = name;
                    pendingEmployeeID = employeeID;
                }

                if (selectedOption == "optApprove") {
                    //update Approval Record
                    blnSuccess = positionRequest.EditApproval(positionRequestID, pendingEmployeeID, pendingName, pendingUserType, "Pending Approval", approval, "", DateTime.Now.ToString("MM/dd/yy HH:mm:ss.fff"));
                    if (blnSuccess) { task = "approved"; }
                }

                if (blnSuccess) {
                    String positionType = lblPositionType.Text, collegeUnit = lblCollegeUnit.Text,  officialTitle = lblOfficialTitle.Text, supervisorName = lblSupervisorName.Text;
                    if (chkModify.Checked) {
                        positionType = cboPositionType.SelectedValue;
                        collegeUnit = cboCollegeUnit.SelectedValue;
                        officialTitle = txtOfficialTitle.Text;
                        supervisorName = txtSupervisorName.Text;
                    } else {
                        hidDepartment.Value = lblDepartmentName.Text;
                        hidDepartmentID.Value = lblDepartmentID2.Text;
                    }
                    if (pendingUserType == "HRO") {
                        //update status and routing to cboStatus.selectedValue
                        //add pending approval routing record
                        blnSuccess = positionRequest.AddRouting(positionRequestID, "HRO", selectedStatus, "", name, employeeID, DateTime.Now.AddMilliseconds(100).ToString("MM/dd/yy HH:mm:ss.fff"));
                        if (blnSuccess) {
                            //update position request status
                            blnSuccess = positionRequest.EditStatus(positionRequestID, selectedStatus);
                            if (blnSuccess) {
                                if(task != "") {
                                    task += " and the status set to " + selectedStatus;
                                } else {
                                    task = "set to " + selectedStatus;
                                }

                                //check if selected status is set to complete to send email notification for onboarding selections
                                if(selectedStatus == "Complete") {
                                    //check onboarding options and send out email notifications
                                    SmtpClient client = new SmtpClient();
                                    MailMessage objEmail = new MailMessage();
                                    String emailBody = "<p><strong>Employee Name:</strong> " + newEmployeeName +
                                                           "<br /><strong>Employee ID:</strong> " + newEmployeeID +
                                                           "<br /><strong>Employee Email Address:</strong> " + newEmployeeEmailAddress +
                                                           "<br /><strong>Employee Type:</strong> " + newEmployeeType +
                                                           "<br /><strong>Official Title:</strong> " + officialTitle +
                                                           "<br /><strong>Supervisor Name: </strong> " + supervisorName +
                                                           "<br /><strong>Position Type:</strong> " + positionType +
                                                           "<br /><strong>College/Unit:</strong> " + collegeUnit +
                                                           "<br /><strong>Department:</strong> " + hidDepartment.Value + "</p>";

                                    if (pCard == 1) {
                                        String pCardAddress = "PCardPR@ccs.spokane.edu"; 
                                        //String pCardAddress = "vu.nguyen@ccs.spokane.edu";
                                        objEmail = new MailMessage();
                                        //objEmail.To.Add(pCardAddress); 
                                        /*
                                        if (appHost.Contains("internal"))
                                        {
                                            objEmail.To.Add(pCardAddress);
                                        }
                                        else
                                        {
                                            objEmail.To.Add(emailTest);
                                        }
                                        */
                                        objEmail.To.Add(Utility.GetSendEmail(pCardAddress));
                                        objEmail.From = new MailAddress(strAdminEmailAddress);
                                        objEmail.IsBodyHtml = true;
                                        objEmail.Priority = MailPriority.High;
                                        objEmail.Subject = "PCard Needed for Employee";
                                        objEmail.Body = "<p>Dear " + "P-Card, " + ",</p>";
                                        objEmail.Body += "<p>The following employee is in need of a pcard.</p>" + emailBody;

                                        try {
                                            client.Send(objEmail);
                                            notified += "|PCard Administrator";
                                        } catch {
                                            objEmail = new MailMessage();
                                            objEmail.To.Add(strAdminEmailAddress);
                                            objEmail.From = new MailAddress(strAdminEmailAddress);
                                            objEmail.IsBodyHtml = true;
                                            objEmail.Priority = MailPriority.High;
                                            objEmail.Subject = "PCard Notification Could Not Be Sent";
                                            objEmail.Body = "<p>The following pcard notification email could not be sent to: " + pCardAddress + ".</p>";
                                            objEmail.Body += "<hr style=\"width:100%\" />";
                                            objEmail.Body += emailBody;

                                            try {
                                                client.Send(objEmail);
                                                notNotified += "|PCard Administrator. HRO has been notified.";
                                            } catch {
                                                notNotified += "|PCard Administrator. HRO could not be notified. Please contact HRO at " + strAdminEmailAddress + ".";
                                            }
                                        }
                                    }

                                    if(CCSIssuedCellPhone == 1) {
                                        String CCSCellPhoneAddress = "ITSupportCenter@ccs.spokane.edu"; 
                                        //String CCSCellPhoneAddress = "vu.nguyen@ccs.spokane.edu";
                                        objEmail = new MailMessage();
                                        //objEmail.To.Add(CCSCellPhoneAddress);
                                        /*
                                        if (appHost.Contains("internal"))
                                        {
                                            objEmail.To.Add(CCSCellPhoneAddress);
                                        }
                                        else
                                        {
                                            objEmail.To.Add(emailTest);
                                        }
                                        */
                                        objEmail.To.Add(Utility.GetSendEmail(CCSCellPhoneAddress));
                                        objEmail.From = new MailAddress(strAdminEmailAddress);
                                        objEmail.IsBodyHtml = true;
                                        objEmail.Priority = MailPriority.High;
                                        objEmail.Subject = "CCS-issued Cell Phone Needed for Employee";
                                        objEmail.Body = "<p>Dear " + "CCS-issued cell phone, " + ",</p>";
                                        objEmail.Body += "<p>The following employee is in need of a CCS-issued cell phone.</p>" + emailBody;

                                        try {
                                            client.Send(objEmail);
                                            notified += "|CCS-issued Cell Phone Administrator";
                                        } catch {
                                            objEmail = new MailMessage();
                                            objEmail.To.Add(strAdminEmailAddress);
                                            objEmail.From = new MailAddress(strAdminEmailAddress);
                                            objEmail.IsBodyHtml = true;
                                            objEmail.Priority = MailPriority.High;
                                            objEmail.Subject = "CCS-issued Cell Phone Notification Could Not Be Sent";
                                            objEmail.Body = "<p>The following CCS-issued cell phone notification email could not be sent to: " + CCSCellPhoneAddress + ".</p>";
                                            objEmail.Body += "<hr style=\"width:100%\" />";
                                            objEmail.Body += emailBody;

                                            try {
                                                client.Send(objEmail);
                                                notNotified += "|CCS-issued Cell Phone Administrator. HRO has been notified.";
                                            } catch {
                                                notNotified += "|CCS-issued Cell Phone Administrator. HRO could not be notified. Please contact HRO at " + strAdminEmailAddress + ".";
                                            }
                                        }
                                    }

                                    if(ctcLinkAccess == 1) {
                                        String ctcLinkAccessEmail = "ITSupportCenter@ccs.spokane.edu"; 
                                        //String ctcLinkAccessEmail = "vu.nguyen@ccs.spokane.edu";
                                        objEmail = new MailMessage();
                                        //objEmail.To.Add(ctcLinkAccessEmail);
                                        /*
                                        if (appHost.Contains("internal"))
                                        {
                                            objEmail.To.Add(ctcLinkAccessEmail);
                                        }
                                        else
                                        {
                                            objEmail.To.Add(emailTest);
                                        }
                                        */
                                        objEmail.To.Add(Utility.GetSendEmail(ctcLinkAccessEmail));
                                        objEmail.From = new MailAddress(strAdminEmailAddress);
                                        objEmail.IsBodyHtml = true;
                                        objEmail.Priority = MailPriority.High;
                                        objEmail.Subject = "Additional ctcLink Security Access Needed for Employee";
                                        objEmail.Body = "<p>The following employee is in need of additional ctcLink security access.</p>" + emailBody;

                                        try {
                                            client.Send(objEmail);
                                            notified += "|ctcLink Security Access Administrator";
                                        } catch {
                                            objEmail = new MailMessage();
                                            objEmail.To.Add(strAdminEmailAddress);
                                            objEmail.From = new MailAddress(strAdminEmailAddress);
                                            objEmail.IsBodyHtml = true;
                                            objEmail.Priority = MailPriority.High;
                                            objEmail.Subject = "Additioanl ctcLink Security Access Notification Could Not Be Sent";
                                            objEmail.Body = "<p>The following additional ctcLink security access notification email could not be sent to: " + ctcLinkAccessEmail + ".</p>";
                                            objEmail.Body += "<hr style=\"width:100%\" />";
                                            objEmail.Body += emailBody;

                                            try {
                                                client.Send(objEmail);
                                                notNotified += "|ctcLink Security Access Administrator. HRO has been notified.";
                                            } catch {
                                                notNotified += "|ctcLink Security Access Administrator. HRO could not be notified. Please contact HRO at " + strAdminEmailAddress + ".";
                                            }
                                        }
                                    }

                                    //Notify Originator that the Position Request is complete
                                    String originatorEmail = positionRequest.GetEmployeeEmail(lblOriginatorID.Text); 
                                    //String originatorEmail = "vu.nguyen@ccs.spokane.edu";
                                    objEmail = new MailMessage();
                                    //objEmail.To.Add(originatorEmail);
                                    /*
                                    if (appHost.Contains("internal"))
                                    {
                                        objEmail.To.Add(originatorEmail);
                                    }
                                    else
                                    {
                                        objEmail.To.Add(emailTest);
                                    }
                                    */
                                    objEmail.To.Add(Utility.GetSendEmail(originatorEmail));
                                    objEmail.From = new MailAddress(strAdminEmailAddress);
                                    objEmail.IsBodyHtml = true;
                                    objEmail.Priority = MailPriority.High;
                                    objEmail.Subject = "Position Request Complete";
                                    objEmail.Body = "<p>Dear " + lblOriginatorName.Text + ",</p>";
                                    objEmail.Body += "<p>The following position request is complete.</p>" + emailBody;

                                    try {
                                        client.Send(objEmail);
                                        notified += "|Originator: " + lblOriginatorName.Text;
                                    } catch {
                                        objEmail = new MailMessage();
                                        objEmail.To.Add(strAdminEmailAddress);
                                        objEmail.From = new MailAddress(strAdminEmailAddress);
                                        objEmail.IsBodyHtml = true;
                                        objEmail.Priority = MailPriority.High;
                                        objEmail.Subject = "Position Request Complete Notification Could Not Be Sent";
                                        objEmail.Body = "<p>The following position request complete notification could not be sent to the originator: " + lblOriginatorName.Text + " at the email address " + originatorEmail + ".</p>";
                                        objEmail.Body += "<hr style=\"width:100%\" />";
                                        objEmail.Body += emailBody; //check that the correct information is being sent in the notification

                                        try {
                                            client.Send(objEmail);
                                            notNotified += "|Originator: " + lblOriginatorName.Text + ". HRO has been notified.";
                                        } catch {
                                            notNotified += "|Originator: " + lblOriginatorName.Text + ". HRO could not be notified. Please contact HRO at " + strAdminEmailAddress + ".";
                                        }
                                    }

                                    //Notify Budget that the Position Request is complete
                                    String budgetEmail = "BudgetPR@ccs.spokane.edu"; 
                                    //String budgetEmail = "vu.nguyen@ccs.spokane.edu";
                                    objEmail = new MailMessage();
                                    //objEmail.To.Add(budgetEmail);
                                    /*
                                    if (appHost.Contains("internal"))
                                    {
                                        objEmail.To.Add(budgetEmail);
                                    }
                                    else
                                    {
                                        objEmail.To.Add(emailTest);
                                    }
                                    */
                                    objEmail.To.Add(Utility.GetSendEmail(budgetEmail));
                                    objEmail.From = new MailAddress(strAdminEmailAddress);
                                    objEmail.IsBodyHtml = true;
                                    objEmail.Priority = MailPriority.High;
                                    objEmail.Subject = "Position Request Complete";
                                    objEmail.Body = "<p>Dear " + "Budget Admin" + ",</p>";
                                    objEmail.Body += "<p>The following position request is complete.</p>" + emailBody;

                                    try {
                                        client.Send(objEmail);
                                        notified += "|The Budget Admin";
                                    } catch {
                                        objEmail = new MailMessage();
                                        objEmail.To.Add(strAdminEmailAddress);
                                        objEmail.From = new MailAddress(strAdminEmailAddress);
                                        objEmail.IsBodyHtml = true;
                                        objEmail.Priority = MailPriority.High;
                                        objEmail.Subject = "Position Request Complete Notification Could Not Be Sent";
                                        objEmail.Body = "<p>The following position request complete notification could not be sent to the budget admin email address: " + budgetEmail + ".</p>";
                                        objEmail.Body += "<hr style=\"width:100%\" />";
                                        objEmail.Body += emailBody; //check that the correct information is being sent in the notification

                                        try {
                                            client.Send(objEmail);
                                            notNotified += "|The Budget Admin. HRO has been notified.";
                                        } catch {
                                            notNotified += "|The Budget Admin. HRO could not be notified. Please contact HRO at " + strAdminEmailAddress + ".";
                                        }
                                    }

                                    //Notify IT that the Position Request is complete
                                    String ITEmail = "ITSupportCenter@ccs.spokane.edu"; 
                                    //String ITEmail = "vu.nguyen@ccs.spokane.edu";
                                    objEmail = new MailMessage();
                                    //objEmail.To.Add(ITEmail);
                                    /*
                                    if (appHost.Contains("internal"))
                                    {
                                        objEmail.To.Add(ITEmail);
                                    }
                                    else
                                    {
                                        objEmail.To.Add(emailTest);
                                    }
                                    */
                                    objEmail.To.Add(Utility.GetSendEmail(ITEmail));
                                    objEmail.From = new MailAddress(strAdminEmailAddress);
                                    objEmail.IsBodyHtml = true;
                                    objEmail.Priority = MailPriority.High;
                                    objEmail.Subject = "Position Request Complete";
                                    objEmail.Body = "<p>Dear " + "IT" + ",</p>";
                                    objEmail.Body += "<p>The following position request is complete.</p>" + emailBody;

                                    try {
                                        client.Send(objEmail);
                                        notified += "|Information Technology";
                                    } catch {
                                        objEmail = new MailMessage();
                                        objEmail.To.Add(strAdminEmailAddress);
                                        objEmail.From = new MailAddress(strAdminEmailAddress);
                                        objEmail.IsBodyHtml = true;
                                        objEmail.Priority = MailPriority.High;
                                        objEmail.Subject = "Position Request Complete Notification Could Not Be Sent";
                                        objEmail.Body = "<p>The following position request complete notification could not be sent to the Information Technology email address: " + ITEmail + ".</p>";
                                        objEmail.Body += "<hr style=\"width:100%\" />";
                                        objEmail.Body += emailBody; //check that the correct information is being sent in the notification

                                        try {
                                            client.Send(objEmail);
                                            notNotified += "|Information Technology. HRO has been notified.";
                                        } catch {
                                            notNotified += "|Information Technology. HRO could not be notified. Please contact HRO at " + strAdminEmailAddress + ".";
                                        }
                                    }

                                    /* Currently an HR function and they don't need email notification, may switch to IT function with a notification email of ITSupportCenter@ccs.spokane.edu
                                    if (timeSheetApprover == 1) {
                                        String timeSheetApproverEmail = "ITSupportCenter@ccs.spokane.edu"; //"vu.nguyen@ccs.spokane.edu";
                                        objEmail = new MailMessage();
                                        objEmail.To.Add(timeSheetApproverEmail);
                                        objEmail.From = new MailAddress(strAdminEmailAddress);
                                        objEmail.IsBodyHtml = true;
                                        objEmail.Priority = MailPriority.High;
                                        objEmail.Subject = "Timesheet Approval Access Needed for Employee";
                                        objEmail.Body = "<p>The following employee is in need of timesheet approval access.</p>" + emailBody;

                                        try {
                                            client.Send(objEmail);
                                            notified += "|Timesheet Administrator";
                                        } catch {
                                            objEmail = new MailMessage();
                                            objEmail.To.Add(strAdminEmailAddress);
                                            objEmail.From = new MailAddress(strAdminEmailAddress);
                                            objEmail.IsBodyHtml = true;
                                            objEmail.Priority = MailPriority.High;
                                            objEmail.Subject = "Time Sheet Approval Access Notification Could Not Be Sent";
                                            objEmail.Body = "<p>The following timesheet approval access notification email could not be sent to: " + timeSheetApproverEmail + ".</p>";
                                            objEmail.Body += "<hr style=\"width:100%\" />";
                                            objEmail.Body += emailBody;

                                            try {
                                                client.Send(objEmail);
                                                notNotified += "|Timesheet Administrator. HRO has been notified.";
                                            } catch {
                                                notNotified += "|Timesheet Administrator. HRO could not be notified. Please contact HRO at " + strAdminEmailAddress + ".";
                                            }
                                        }
                                    }
                                    */
                                }

                            } else {
                                lblError.Text = "<p><strong>Error: The " + status + " status could not be updated to " + selectedStatus + ".";

                                //delete pending action record
                                blnSuccess = positionRequest.DeleteRouting(positionRequestID, "HRO", selectedStatus, "", "");
                                if (blnSuccess) {
                                    
                                    //set routing record back to Pending Previous Approver
                                    blnSuccess = positionRequest.EditApproval(positionRequestID, pendingEmployeeID, pendingName, pendingUserType, selectedStatus, pendingDescription, "", DateTime.Now.AddMilliseconds(100).ToString("MM/dd/yy HH:mm:ss.fff"));
                                    if (blnSuccess) {
                                        lblError.Text += " The position request has been set back to " + pendingDescription + "."; 
                                    } else {
                                        lblError.Text += " The position request could not be set back to " + pendingDescription + "."; 
                                    }
                                    lblError.Text += "</strong></p>" + strErrorContact + "<br />";
                                    blnSuccess = false;
                                } else {
                                    lblError.Text += " The position request could not be set back to " + pendingDescription + ". The " + selectedStatus + " record could not be deleted.</strong></p>" + strErrorContact + "<br />";
                                }
                            }
                        } else {
                            lblError.Text = "<p><strong>Error: The HRO " + selectedStatus + " routing record could not be added.</strong></p>" + strErrorContact + "<br />";
                            blnSuccess = false;
                        }
                    } else {
                        if (pendingUserType == "Budget") {
                            if (positionType != "Adjunct Faculty" && positionType != "Part-Time Hourly" && (String.Compare(hidDepartmentID.Value, "99001") < 0 || String.Compare(hidDepartmentID.Value, "99021") > 0)) { //(Convert.ToInt32(lblDepartmentID.Text) < 99001 && Convert.ToInt32(lblDepartmentID.Text) > 99021)) {
                                DataSet dsApprover = positionRequest.GetEmployeeByJobCode("111133");
                                if (dsApprover.Tables[0].Rows.Count > 0) {
                                    status = "Chancellor";
                                    //approverEmail = "vu.nguyen@ccs.spokane.edu"; //for testing
                                    approverEmail = dsApprover.Tables[0].Rows[0]["EMAIL_ADDR"].ToString();
                                    approverName = dsApprover.Tables[0].Rows[0]["Name"].ToString();
                                    approverID = dsApprover.Tables[0].Rows[0]["EMPLID"].ToString();
                                } else {
                                    error = "Error: The Chancellor's contact information could not be found in the database.";
                                }
                            } else if (positionType == "Adjunct Faculty") {
                                status = "HRO";
                                //approverEmail = "vu.nguyen@ccs.spokane.edu"; //for testing
                                approverEmail = "HROPR@ccs.spokane.edu"; //setup this email account
                                approverName = "";
                                approverID = "";
                                menu = "HRO Admin";
                            } else if (positionType == "Part-Time Hourly" || (String.Compare(hidDepartmentID.Value, "99001") >= 0 && String.Compare(hidDepartmentID.Value, "99021") <= 0)) {
                                //Set Executive approver as Melody Matthews 
                                DataSet dsApprover = positionRequest.GetEmployeeByJobCode("111490"); //positionRequest.GetEmployeeByJobCode("111433"); //Greg Stevens job code
                                if (dsApprover.Tables[0].Rows.Count > 0) {
                                    status = "Executive";
                                    //approverEmail = "vu.nguyen@ccs.spokane.edu"; //for testing
                                    approverEmail = dsApprover.Tables[0].Rows[0]["EMAIL_ADDR"].ToString();
                                    approverName = dsApprover.Tables[0].Rows[0]["Name"].ToString();
                                    approverID = dsApprover.Tables[0].Rows[0]["EMPLID"].ToString();
                                } else {
                                    error = "Error: The Executive Approver's contact information could not be found in the database.";
                                }
                            }
                        } else {
                            //get previous approver jobcode
                            String jobCode = positionRequest.GetEmployeeJobCode(pendingEmployeeID);

                            //get next supervisor in routing process
                            DataSet dsSupervisor = positionRequest.GetSupervisor(pendingEmployeeID);
                            menu = "Supervisor";
                            status = "";
                            if (lblStatus.Text.IndexOf("Approver") > -1) {
                                status = "Approver " + (Convert.ToInt16(lblStatus.Text.Replace("Approver ", "")) + 1).ToString();
                            }

                            if (dsSupervisor.Tables[0].Rows.Count > 0) {
                                String approverJobCode = dsSupervisor.Tables[0].Rows[0]["JOBCODE"].ToString();
                                approverEmail = dsSupervisor.Tables[0].Rows[0]["EMAIL_ADDR"].ToString();
                                approverID = dsSupervisor.Tables[0].Rows[0]["EMPLID"].ToString();
                                approverName = dsSupervisor.Tables[0].Rows[0]["NAME"].ToString();

                                if (lblPositionType.Text != "Adjunct Faculty" && lblPositionType.Text != "Part-Time Hourly" && (String.Compare(hidDepartmentID.Value, "99001") < 0 || String.Compare(hidDepartmentID.Value, "99021") > 0)) { 
                                    if (lblStatus.Text == "Executive") {
                                        status = "Budget";
                                        //approverEmail = "vu.nguyen@ccs.spokane.edu";
                                        approverEmail = "BudgetPR@ccs.spokane.edu"; //setup this email account
                                        approverName = "";
                                        approverID = "";
                                        menu = "Budget Admin";
                                    } else if (lblStatus.Text == "Chancellor") {
                                        status = "HRO";
                                        //approverEmail = "vu.nguyen@ccs.spokane.edu";
                                        approverEmail = "HROPR@ccs.spokane.edu"; //setup this email account
                                        approverName = "";
                                        approverID = "";
                                        menu = "HRO Admin";
                                    } else if (approverJobCode == "111113" || approverJobCode == "111133") {
                                        status = "Executive";
                                    }
                                } else if (lblPositionType.Text == "Adjunct Faculty") {
                                    if (approverJobCode == "111274" || approverJobCode == "111577" || approverJobCode == "111465" || approverJobCode == "111439" || approverJobCode == "111120") {
                                        status = "Budget";
                                        //approverEmail = "vu.nguyen@ccs.spokane.edu";
                                        approverEmail = "BudgetPR@ccs.spokane.edu"; //setup this email account
                                        approverName = "";
                                        approverID = "";
                                        menu = "Budget Admin";
                                    }
                                } else if (lblPositionType.Text == "Part-Time Hourly" || (String.Compare(hidDepartmentID.Value, "99001") >= 0 && String.Compare(hidDepartmentID.Value, "99021") <= 0)) {
                                    if (lblStatus.Text == "Executive") {
                                        if (jobCode == "111490") { //Melody Matthews job code
                                            if (positionRequest.GetBudgetApprovalCount(positionRequestID) > 0) {
                                                status = "HRO";
                                                //approverEmail = "vu.nguyen@ccs.spokane.edu"; //for testing
                                                approverEmail = "HROPR@ccs.spokane.edu"; 
                                                approverName = "";
                                                approverID = "";
                                                menu = "HRO Admin";
                                            } else {
                                                status = "Budget";
                                                //approverEmail = "vu.nguyen@ccs.spokane.edu"; //for testing
                                                approverEmail = "BudgetPR@ccs.spokane.edu"; 
                                                approverName = "";
                                                approverID = "";
                                                menu = "Budget Admin";
                                            }
                                        } else { //all other executive approvals
                                            status = "Budget";
                                            //approverEmail = "vu.nguyen@ccs.spokane.edu"; //for testing
                                            approverEmail = "BudgetPR@ccs.spokane.edu"; //setup this email account
                                            approverName = "";
                                            approverID = "";
                                            menu = "Budget Admin";
                                        }
                                    } else if(excutiveApprovalJC.Contains(approverJobCode))
                                    { 
                                        //(approverJobCode == "111272" || approverJobCode == "111165" || approverJobCode == "111499" || approverJobCode == "111319" || approverJobCode == "111433" || approverJobCode == "111274" || approverJobCode == "111577" || approverJobCode == "111465" || approverJobCode == "111439" || approverJobCode == "111120") {
                                        status = "Executive";
                                        if (approverJobCode == "111433") { //if Greg Stevens is the Executive approver
                                            //change the Executive approver to Melody Matthews
                                            approverJobCode = "111490";
                                            dsSupervisor = positionRequest.GetEmployeeByJobCode(approverJobCode);
                                            if (dsSupervisor.Tables[0].Rows.Count > 0) {
                                                approverEmail = dsSupervisor.Tables[0].Rows[0]["EMAIL_ADDR"].ToString();
                                                approverID = dsSupervisor.Tables[0].Rows[0]["EMPLID"].ToString();
                                                approverName = dsSupervisor.Tables[0].Rows[0]["NAME"].ToString();
                                            } else {
                                                error = "Error: The next supervisor for approval could not be found.";
                                            }
                                        }
                                    }
                                }
                            } else {
                                error = "Error: The next supervisor for approval could not be found.";
                            }
                        }

                        if (error == "") {
                            try {
                                //add pending approval routing record
                                blnSuccess = positionRequest.AddRouting(positionRequestID, status, "Pending Approval", "", approverName, approverID, DateTime.Now.AddMilliseconds(100).ToString("MM/dd/yy HH:mm:ss.fff"));

                                if (blnSuccess) {
                                    //update position request status
                                    blnSuccess = positionRequest.EditStatus(positionRequestID, status); //may need to check boolean return value to make sure status is updated and notify HR if not

                                    if (blnSuccess) {
                                        if (status == "HRO") {
                                            approverName = "the HRO Admin";
                                        }else if(status == "Budget") {
                                            approverName = "the Budget Admin";
                                        }

                                        //send email for approval
                                        SmtpClient client = new SmtpClient();
                                        MailMessage objEmail = new MailMessage();
                                        String emailBody = "<p><strong>Tracking #:</strong> " + lblTrackingNumber.Text +
                                                              "<br /><strong>Originator:</strong> " + lblOriginatorName.Text +
                                                              "<br /><strong>Created:</strong> " + hidCreated.Value +
                                                              "<br /><strong>College/Unit:</strong> " + collegeUnit +
                                                              "<br /><strong>Department:</strong> " + hidDepartment.Value +
                                                              "<br /><strong>Official Title:</strong> " + officialTitle +
                                                              "<br /><strong>Supervisor Name:</strong> " + supervisorName +
                                                              "<br /><strong>Position Type:</strong> " + positionType +
                                                              "<br /><strong>Status:</strong> " + status + "</p>";

                                        if (approverEmail != "") {
                                            //objEmail.To.Add(approverEmail); 
                                            //objEmail.To.Add("vu.nguyen@ccs.spokane.edu");
                                            /*
                                            if (appHost.Contains("internal"))
                                            {
                                                objEmail.To.Add(approverEmail);
                                            }
                                            else
                                            {
                                                objEmail.To.Add(emailTest);
                                            }
                                            */
                                            objEmail.To.Add(Utility.GetSendEmail(approverEmail));
                                            objEmail.From = new MailAddress(strAdminEmailAddress);
                                            objEmail.IsBodyHtml = true;
                                            objEmail.Priority = MailPriority.High;
                                            objEmail.Subject = "Position Request Approval Notification";
                                            objEmail.Body = "<p>Dear " + approverName + ",</p>";
                                            objEmail.Body += "<p>Please review the Position Request below for approval.</p>";
                                            objEmail.Body += "<p><a href=\"https://internal.spokane.edu/PositionRequest/\">https://internal.spokane.edu/PositionRequest/</a>";
                                            objEmail.Body += "<br />Log in with your Active Directory account and click on the PR Approval link in the " + menu + " menu.</p>";
                                            objEmail.Body += emailBody;

                                            try {
                                                client.Send(objEmail);
                                                notified += "|" + status + ": " + approverName;
                                            } catch {
                                                objEmail = new MailMessage();
                                                objEmail.To.Add(strAdminEmailAddress);
                                                objEmail.From = new MailAddress(strAdminEmailAddress);
                                                objEmail.IsBodyHtml = true;
                                                objEmail.Priority = MailPriority.High;
                                                objEmail.Subject = "Position Request Approval Notification Could Not Be Sent";
                                                objEmail.Body = "<p>The following approval notification email could not be sent to " + approverName + " at the email address " + approverEmail + ".</p>";
                                                objEmail.Body += "<hr style=\"width:100%\" />";
                                                objEmail.Body += emailBody;

                                                try {
                                                    client.Send(objEmail);
                                                    notNotified += "|" + status + ": " + approverName + ".|HRO has been notified.";
                                                } catch {
                                                    notNotified += "|" + status + ": " + approverName + " or HRO.|Please contact HRO at " + strAdminEmailAddress + ".";
                                                }
                                            }
                                        } else {
                                            objEmail = new MailMessage();
                                            objEmail.To.Add(strAdminEmailAddress);
                                            objEmail.From = new MailAddress(strAdminEmailAddress);
                                            objEmail.IsBodyHtml = true;
                                            objEmail.Priority = MailPriority.High;
                                            objEmail.Subject = "Position Request Approval Notification Could Not Be Sent";
                                            objEmail.Body = "<p>The following approval notification email could not be sent to " + approverName + " because their email address does not exist.</p>";
                                            objEmail.Body += "<hr style=\"width:100%\" />";
                                            objEmail.Body += emailBody;

                                            try {
                                                client.Send(objEmail);
                                                notNotified += "|" + status + ": " + approverName + ".|HRO has been notified.";
                                            } catch {
                                                notNotified += "|" + status + ": " + approverName + " or HRO.|Please contact HRO at " + strAdminEmailAddress + ".";
                                            }
                                        }

                                        if (task == "modified") {
                                            task += " and approved";
                                        } else {
                                            task = "approved";
                                        }
                                    } else {
                                        lblError.Text = "<p><strong>Error: The Position Request status could not be updated to " + status + ".";

                                        //delete pending approval record
                                        blnSuccess = positionRequest.DeleteRouting(positionRequestID, status, "Pending Approval", approverName, approverID);
                                        if (blnSuccess) { 
                                            //set routing record back to Pending Previous Approver
                                            blnSuccess = positionRequest.EditApproval(positionRequestID, pendingEmployeeID, pendingName, pendingUserType, "Approved", "Pending Approval", "", DateTime.Now.AddMilliseconds(100).ToString("MM/dd/yy HH:mm:ss.fff"));
                                            if (blnSuccess) {
                                                lblError.Text += " The position request has been set back to pending " + pendingUserType + " approval.";
                                            } else {
                                                lblError.Text += " The position request could not be set back to pending " + pendingUserType + " approval.";
                                            }
                                            lblError.Text += "</strong></p>" + strErrorContact + "<br />";
                                            blnSuccess = false;
                                        } else {
                                            lblError.Text += " The position request could not be set back to pending " + pendingUserType + " approval. The " + status + " pending approval routing record could not be deleted.</strong></p>" + strErrorContact + "<br />";
                                        }
                                    }
                                } else {
                                    lblError.Text = "<p><strong>Error: The " + status + " pending approval record could not be added.";
                                    //set routing record back to Pending Previous Approver
                                    blnSuccess = positionRequest.EditApproval(positionRequestID, pendingEmployeeID, pendingName, pendingUserType, "Approved", "Pending Approval", "", DateTime.Now.AddMilliseconds(100).ToString("MM/dd/yy HH:mm:ss.fff"));
                                    if (blnSuccess) {
                                        lblError.Text += " The position request has been set back to pending " + pendingUserType + " approval.";
                                    } else {
                                        lblError.Text += " The position request could not be set back to pending " + pendingUserType + " approval.";
                                    }
                                    lblError.Text += "</strong></p>" + strErrorContact + "<br />";
                                    blnSuccess = false;
                                }

                            } catch {
                                lblError.Text = "<p><strong>Error: An error occurred while submitting the " + status + " pending approval record.";
                                //set routing record back to Pending Previous Approver
                                blnSuccess = positionRequest.EditApproval(positionRequestID, pendingEmployeeID, pendingName, pendingUserType, "Approved", "Pending Approval", "", DateTime.Now.AddMilliseconds(100).ToString("MM/dd/yy HH:mm:ss.fff"));
                                if (blnSuccess) {
                                    lblError.Text += " The position request has been set back to pending " + pendingUserType + " approval.";
                                } else {
                                    lblError.Text += " The position request could not be set back to pending " + pendingUserType + " approval.";
                                }
                                lblError.Text += "</strong></p>" + strErrorContact + "<br />";
                                blnSuccess = false;
                            }
                        } else {
                            lblError.Text = "<p><strong>" + error;
                            //set routing record back to Pending Previous Approval
                            blnSuccess = positionRequest.EditApproval(positionRequestID, pendingEmployeeID, pendingName, pendingUserType, "Approved", "Pending Approval", "", DateTime.Now.AddMilliseconds(100).ToString("MM/dd/yy HH:mm:ss.fff"));
                            if (blnSuccess) {
                                lblError.Text += " The position request has been set back to pending " + pendingUserType + " approval.";
                            } else {
                                lblError.Text += " The position request could not be set back to pending " + pendingUserType + " approval.";
                            }
                            lblError.Text += "</strong></p>" + strErrorContact + "<br />";
                            blnSuccess = false;
                        }
                    } 
                } else {
                    //display error message
                    lblError.Text = "<p><strong>Error: Your approval could not be recorded.</strong></p>" + strErrorContact + "<br />";
                }
                
            } else if (blnSuccess && selectedOption == "optReject") { //FIGURE OUT WHY REJECT PROCESS IS SO SLOW!!!
                String rejection = "Rejected", rejectEmployeeID = pendingEmployeeID, rejectName = pendingName;
                if(hidUserType.Value == "HROAdmin") {
                    rejection = "HRO Rejected";
                    rejectEmployeeID = employeeID;
                    rejectName = name;
                }

                blnSuccess = positionRequest.EditApproval(positionRequestID, rejectEmployeeID, rejectName, pendingUserType, "Pending Approval", rejection, rejectReason, DateTime.Now.ToString("MM/dd/yy HH:mm:ss.fff"));

                if (blnSuccess) {
                    //update the status
                    blnSuccess = positionRequest.EditStatus(positionRequestID, "Rejected");

                    if (blnSuccess) {
                        SmtpClient client = new SmtpClient();
                        MailMessage objEmail = new MailMessage();
                        String strEmailBody = "<p><strong>Rejected by:</strong> " + lblStatus.Text + ": " + name +
                                              "<br /><strong>Reason:</strong> " + rejectReason +
                                              "<br /><br /><strong>Tracking #:</strong> " + lblTrackingNumber.Text +
                                              "<br /><strong>Originator:</strong> " + lblOriginatorName.Text +
                                              "<br /><strong>Created:</strong> " + hidCreated.Value +
                                              "<br /><strong>College/Unit:</strong> " + lblCollegeUnit.Text +
                                              "<br /><strong>Department:</strong> " + lblDepartmentName.Text +
                                              "<br /><strong>Official Title:</strong> " + lblOfficialTitle.Text +
                                              "<br /><strong>Supervisor Name:</strong> " + lblSupervisorName.Text +
                                              "<br /><strong>Position Type:</strong> " + lblPositionType.Text + "</p>";

                        //notify the originator
                        String originatorEmail = positionRequest.GetEmployeeEmail(lblOriginatorID.Text);
                        String originatorName = lblOriginatorName.Text;
                        objEmail.From = new MailAddress(strAdminEmailAddress);
                        //objEmail.To.Add(originatorEmail); 
                        //objEmail.To.Add("vu.nguyen@ccs.spokane.edu");
                        /*
                        if (appHost.Contains("internal"))
                        {
                            objEmail.To.Add(originatorEmail);
                        }
                        else
                        {
                            objEmail.To.Add(emailTest);
                        }
                        */
                        objEmail.To.Add(Utility.GetSendEmail(originatorEmail));
                        objEmail.IsBodyHtml = true;
                        objEmail.Priority = MailPriority.High;
                        objEmail.Subject = "Position Request Rejected Notification";
                        objEmail.Body = "<p>The following Position Request has been rejected.<p>";
                        objEmail.Body += "<p><a href=\"https://internal.spokane.edu/PositionRequest/\">https://internal.spokane.edu/PositionRequest/</a>";
                        objEmail.Body += "<br />To modify and resubmit this form, log in with your Active Directory account and click on the Position Request link in the Main Menu.</p>";
                        objEmail.Body += strEmailBody;

                        try {
                            client.Send(objEmail);
                            notified += "|Originator: " + originatorName;
                        } catch {
                            objEmail = new MailMessage();
                            objEmail.From = new MailAddress(strAdminEmailAddress);
                            objEmail.To.Add(strAdminEmailAddress);
                            objEmail.IsBodyHtml = true;
                            objEmail.Priority = MailPriority.High;
                            objEmail.Subject = "Position Request Rejection Notification Could Not Be Sent";
                            objEmail.Body = "<p>The following rejection notification email could not be sent to:<br />Originator: " + originatorName + "</p>";
                            objEmail.Body += "<hr style=\"width:100%\" />";
                            objEmail.Body += strEmailBody;
                            try {
                                client.Send(objEmail);
                                notNotified += "|Originator: " + originatorName + ". HRO has been notified.";
                            } catch {
                                notNotified += "|Originator: " + originatorName + ". HRO could not be notified. Please contact HRO at " + strAdminEmailAddress + ".";
                            }
                        }

                        //notify past approvers
                        String previousApproverList = "", notificationList = "";
                        DataSet dsPreviousApprovers = positionRequest.GetPreviousApprovers(positionRequestID);
                        DataTable dtPreviousApprovers = dsPreviousApprovers.Tables[0];
                        objEmail = new MailMessage();
                        if (dtPreviousApprovers.Rows.Count > 0) {
                            foreach (DataRow drPreviousApprover in dtPreviousApprovers.Rows) {
                                String userType = drPreviousApprover["UserType"].ToString();
                                String approverName = drPreviousApprover["Name"].ToString();
                                if (!previousApproverList.Contains(approverName)) {
                                    previousApproverList += "<br />" + userType + ": " + approverName;
                                    notificationList += "|" + userType + ": " + approverName;
                                    //objEmail.To.Add(dsPreviousApprovers.Tables[0].Rows[i]["EMAIL_ADDR"].ToString());
                                    //objEmail.To.Add("vu.nguyen@ccs.spokane.edu");
                                    /*
                                    if (appHost.Contains("internal"))
                                    {
                                        objEmail.To.Add(drPreviousApprover["EMAIL_ADDR"].ToString());
                                    }
                                    else
                                    {
                                        objEmail.To.Add(emailTest);
                                    }
                                    */
                                    objEmail.To.Add(Utility.GetSendEmail(drPreviousApprover["EMAIL_ADDR"].ToString()));
                                }
                            }
                            //objEmail.CC.Add(positionRequest.GetEmployeeEmail(employeeID)); 
                            //objEmail.CC.Add("vu.nguyen@ccs.spokane.edu");
                            /*
                            if (appHost.Contains("internal"))
                            {
                                objEmail.CC.Add(positionRequest.GetEmployeeEmail(employeeID));
                            }
                            else
                            {
                                objEmail.CC.Add(emailTest);
                            }
                            */
                            objEmail.CC.Add(Utility.GetSendEmail(positionRequest.GetEmployeeEmail(employeeID)));
                            objEmail.From = new MailAddress(strAdminEmailAddress);
                            objEmail.IsBodyHtml = true;
                            objEmail.Priority = MailPriority.High;
                            objEmail.Subject = "Position Request Rejected Notification";
                            objEmail.Body = "<p>The following Position Request has been rejected.</p>";
                            objEmail.Body += strEmailBody;

                            try {
                                client.Send(objEmail);
                                notified += notificationList;
                            } catch {
                                objEmail = new MailMessage();
                                objEmail.From = new MailAddress(strAdminEmailAddress);
                                objEmail.To.Add(strAdminEmailAddress);
                                objEmail.IsBodyHtml = true;
                                objEmail.Priority = MailPriority.High;
                                objEmail.Subject = "Position Request Rejection Notification Could Not Be Sent";
                                objEmail.Body = "<p>The following rejection notification email could not be sent to:" + previousApproverList + ".</p>";
                                objEmail.Body += "<hr style=\"width:100%\" />";
                                objEmail.Body += strEmailBody;
                                try {
                                    client.Send(objEmail);
                                    notNotified += notificationList + "|HRO has been notified.";
                                } catch {
                                    notNotified += notificationList + "|HRO could not be notified. Please contact HRO at " + strAdminEmailAddress + ".";
                                }
                            }
                        }

                        task = "rejected";

                    } else {
                        lblError.Text = "<p><strong>Error: The Position Request status could not be updated to Rejected.";
                        //set routing record back to Pending Budget Approval
                        blnSuccess = positionRequest.EditApproval(positionRequestID, pendingEmployeeID, pendingName, pendingUserType, "Rejected", "Pending Approval", "", DateTime.Now.AddMilliseconds(100).ToString("MM/dd/yy HH:mm:ss.fff"));
                        if (blnSuccess) {
                            lblError.Text += " The position request has been set back to pending Budget approval.";
                        } else {
                            lblError.Text += " The position request could not be set back to pending Budget approval.";
                        }
                        lblError.Text += "</strong></p>" + strErrorContact + "<br />";
                        blnSuccess = false;
                    }
                } else {
                    //display error message
                    lblError.Text = "<p><strong>Error: Your rejection could not be recorded.</strong></p>" + strErrorContact + "<br />";
                }

            }else if(blnSuccess && selectedOption == "optVoid") {
                blnSuccess = positionRequest.EditApproval(positionRequestID, employeeID, name, "HRO", "Pending Approval", "Void", voidReason, DateTime.Now.ToString("MM/dd/yy HH:mm:ss.fff"));

                if (blnSuccess) {
                    //update the status
                    blnSuccess = positionRequest.EditStatus(positionRequestID, "Void");

                    if (blnSuccess) {
                        SmtpClient client = new SmtpClient();
                        MailMessage objEmail = new MailMessage();
                        String strEmailBody = "<p><strong>Voided by:</strong> " + lblStatus.Text + ": " + name +
                                              "<br /><strong>Reason:</strong> " + voidReason +
                                              "<br /><br /><strong>Tracking #:</strong> " + lblTrackingNumber.Text +
                                              "<br /><strong>Originator:</strong> " + lblOriginatorName.Text +
                                              "<br /><strong>Created:</strong> " + hidCreated.Value +
                                              "<br /><strong>College/Unit:</strong> " + lblCollegeUnit.Text +
                                              "<br /><strong>Department:</strong> " + lblDepartmentName.Text +
                                              "<br /><strong>Official Title:</strong> " + lblOfficialTitle.Text +
                                              "<br /><strong>Supervisor Name:</strong> " + lblSupervisorName.Text +
                                              "<br /><strong>Position Type:</strong> " + lblPositionType.Text + "</p>";

                        //notify the originator
                        String originatorEmail = positionRequest.GetEmployeeEmail(lblOriginatorID.Text);
                        String originatorName = lblOriginatorName.Text;
                        objEmail.From = new MailAddress(strAdminEmailAddress);
                        //objEmail.To.Add(originatorEmail); 
                        //objEmail.To.Add("vu.nguyen@ccs.spokane.edu");
                        /*
                        if (appHost.Contains("internal"))
                        {
                            objEmail.To.Add(originatorEmail);
                        }
                        else
                        {
                            objEmail.To.Add(emailTest);
                        }
                        */
                        objEmail.To.Add(Utility.GetSendEmail(originatorEmail));
                        objEmail.IsBodyHtml = true;
                        objEmail.Priority = MailPriority.High;
                        objEmail.Subject = "Position Request Void Notification";
                        objEmail.Body = "<p>Dear " + originatorName + ",< p>";
                        objEmail.Body += "<p>The following Position Request has been marked as Void by HRO.<p>";
                        objEmail.Body += "<p><a href=\"https://internal.spokane.edu/PositionRequest/\">https://internal.spokane.edu/PositionRequest/</a></p>";
                        objEmail.Body += strEmailBody;

                        try {
                            client.Send(objEmail);
                            notified += "|Originator: " + originatorName;
                        } catch {
                            objEmail = new MailMessage();
                            objEmail.From = new MailAddress(strAdminEmailAddress);
                            objEmail.To.Add(strAdminEmailAddress);
                            objEmail.IsBodyHtml = true;
                            objEmail.Priority = MailPriority.High;
                            objEmail.Subject = "Position Request Void Notification Could Not Be Sent";
                            objEmail.Body = "<p>The following void notification email could not be sent to:<br />Originator: " + originatorName + "</p>";
                            objEmail.Body += "<hr style=\"width:100%\" />";
                            objEmail.Body += strEmailBody;
                            try {
                                client.Send(objEmail);
                                notNotified += "|Originator: " + originatorName + ". HRO has been notified.";
                            } catch {
                                notNotified += "|Originator: " + originatorName + ". HRO could not be notified. Please contact HRO at " + strAdminEmailAddress + ".";
                            }
                        }

                        //notify past approvers
                        /* //Should previous approvers be notified if a position request is marked as void? If so, uncomment below
                        String previousApproverList = "", notificationList = "";
                        DataSet dsPreviousApprovers = positionRequest.GetPreviousApprovers(positionRequestID);
                        Int32 approverCount = dsPreviousApprovers.Tables[0].Rows.Count;
                        objEmail = new MailMessage();
                        if (approverCount > 0) {
                            for (Int32 i = 0; i < approverCount; i++) {
                                String userType = dsPreviousApprovers.Tables[0].Rows[i]["UserType"].ToString();
                                String approverName = dsPreviousApprovers.Tables[0].Rows[i]["Name"].ToString();
                                if (!previousApproverList.Contains(approverName)) {
                                    previousApproverList += "<br />" + userType + ": " + approverName;
                                    notificationList += "|" + userType + ": " + approverName;
                                    objEmail.To.Add(dsPreviousApprovers.Tables[0].Rows[i]["EMAIL_ADDR"].ToString());
                                    //objEmail.To.Add("vu.nguyen@ccs.spokane.edu");
                                }
                            }
                            objEmail.CC.Add(positionRequest.GetEmployeeEmail(employeeID)); //"vu.nguyen@ccs.spokane.edu"
                            objEmail.From = new MailAddress(strAdminEmailAddress);
                            objEmail.IsBodyHtml = true;
                            objEmail.Priority = MailPriority.High;
                            objEmail.Subject = "Position Request Voided Notification";
                            objEmail.Body = "<p>The following Position Request has been marked as Void by HRO.</p>";
                            objEmail.Body += strEmailBody;

                            try {
                                client.Send(objEmail);
                                notified += notificationList;
                            } catch {
                                objEmail = new MailMessage();
                                objEmail.From = new MailAddress(strAdminEmailAddress);
                                objEmail.To.Add(strAdminEmailAddress);
                                objEmail.IsBodyHtml = true;
                                objEmail.Priority = MailPriority.High;
                                objEmail.Subject = "Position Request Void Notification Could Not Be Sent";
                                objEmail.Body = "<p>The following void notification email could not be sent to:" + previousApproverList + ".</p>";
                                objEmail.Body += "<hr style=\"width:100%\" />";
                                objEmail.Body += strEmailBody;
                                try {
                                    client.Send(objEmail);
                                    notNotified += notificationList + "|HRO has been notified.";
                                } catch {
                                    notNotified += notificationList + "|HRO could not be notified. Please contact HRO at " + strAdminEmailAddress + ".";
                                }
                            }
                        }
                        */

                        task = "marked as void";

                    } else {
                        lblError.Text = "<p><strong>Error: The Position Request status could not be updated to Void.";
                        //set routing record back to Pending Budget Approval
                        blnSuccess = positionRequest.EditApproval(positionRequestID, employeeID, name, "HRO", "Void", "Pending Approval", "", DateTime.Now.AddMilliseconds(100).ToString("MM/dd/yy HH:mm:ss.fff"));
                        if (blnSuccess) {
                            lblError.Text += " The position request has been set back to pending HRO approval.";
                        } else {
                            lblError.Text += " The position request could not be set back to pending HRO approval.";
                        }
                        lblError.Text += "</strong></p>" + strErrorContact + "<br />";
                        blnSuccess = false;
                    }
                } else {
                    //display error message
                    lblError.Text = "<p><strong>Error: The void routing record could not be recorded.</strong></p>" + strErrorContact + "<br />";
                }
            }

        } else {
            lblError.Text = "<p><strong>Error: The file attachment(s) could not be uploaded.</strong></p>" + strErrorContact + "<br />";
        }

        if (blnSuccess) {
            Response.Redirect("Approval.aspx?todo=confirm&id=" + positionRequestID + "&task=" + task + "&notified=" + notified + "&notNotified=" + notNotified);
        } else {
            lblError.Visible = true;
            cmdBack.Visible = false;
            cmdPrint.Visible = false;
            cmdSubmit.Visible = false;
            cmdOK.Visible = true;
        }
    }

    private void PopulateWorkScheduleForLabel(int positionRequestID)
    {
        DataSet dsWorkSchedule = positionRequest.GetWorkSchedule(positionRequestID);
        DataTable dtWorkSchedule = dsWorkSchedule.Tables[0];
        if (dtWorkSchedule.Rows.Count > 0)
        {
            DataRow drWorkSchedule = dtWorkSchedule.Rows[0];
            foreach (var s in dayWeek)
            {
                Label lbl = (Label)this.Page.Master.FindControl("maintextHolder").FindControl("lbl" + s);
                if (lbl != null)
                {
                    string startTime = Utility.ConvertToDateStr(drWorkSchedule[s + "Start"].ToString());
                    string endTime = Utility.ConvertToDateStr(drWorkSchedule[s + "End"].ToString());
                    if (startTime != "" || endTime != "")
                    {
                        lbl.Text = String.Format("{0}<br />to<br />{1}", startTime, endTime);
                    }

                }
            }
            schedule2.Visible = true;
        }
    }
}
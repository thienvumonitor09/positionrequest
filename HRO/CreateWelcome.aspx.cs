﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Globalization;

public partial class HRO_CreateWelcome : System.Web.UI.Page {

    PositionRequest positionRequest = new PositionRequest();

    protected void Page_Load(object sender, EventArgs e) {
        //check if the user is logged in
        HttpCookie cookie = Request.Cookies.Get("phatt2");
        if (cookie == null) {
            users user = new users(HttpContext.Current.User.Identity.Name);
        }

        //hide all panels
        panMain.Visible = false;
        panPreview.Visible = false;
        panConfirm.Visible = false;
        panError.Visible = false;

        //check if user has the right permissions
        /*
        if (positionRequest.GetUserPermission(Request.Cookies["phatt2"]["userctclinkid"]) != "HRO Admin") {
            lblError.Text = "<strong>Error: Access Denied</strong><br />You do not have permission to view this page.";
            panError.Visible = true;
        } else {
        */
        if (!IsPostBack) {
                txtWelcome.InnerHtml = positionRequest.GetWelcomeText();
                panMain.Visible = true;
            }
        /*
        }
        */
        
    }

    protected void cmdPreview_Click(object sender, EventArgs e) {
        lblWelcome.Text = "<h3 class=\"pageTitle\">Position Request</h3>";
        CultureInfo cultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture;
        TextInfo textInfo = cultureInfo.TextInfo;
        DataSet dsEmployeeName = positionRequest.GetEmployeeNames(Request.Cookies["phatt2"]["userctclinkid"]);
        try {
            lblWelcome.Text += "<h5 style=\"padding-bottom:0px;margin-bottom:0px;\">Welcome " + textInfo.ToTitleCase(dsEmployeeName.Tables[0].Rows[0]["FIRST_NAME"].ToString().ToLower() + " " + dsEmployeeName.Tables[0].Rows[0]["LAST_NAME"].ToString().ToLower()) + ".</h5>";
        } catch {
            lblWelcome.Text += "<h5 style=\"color:red\">Your employee record could not be found.<br />Please contact the IT Support Center at 533-HELP (4357). id=" + Request.Cookies["phatt2"]["userctclinkid"] + "</h5>";
        }
        welcomeText.InnerHtml = txtWelcome.InnerHtml.Replace("\r\n", "").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&amp;", "&").Replace("&quot;", "\"");
        panPreview.Visible = true;

    }

    protected void cmdSubmit_Click(object sender, EventArgs e) {
        bool success = positionRequest.EditWelcomeText(txtWelcome.InnerHtml.Replace("\r\n", "").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&amp;", "&").Replace("&quot;", "\""));
        CultureInfo cultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture;
        TextInfo textInfo = cultureInfo.TextInfo;
        lblConfirmWelcome.Text = "<h3 class=\"pageTitle\">Create Welcome</h3>";
        lblConfirmWelcome.Text += "<p><strong>The following welcome text has been saved.</strong></p>";
        confirmWelcomeText.InnerHtml = txtWelcome.InnerHtml.Replace("\r\n", "").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&amp;", "&").Replace("&quot;", "\"");
        panConfirm.Visible = true;
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class HRO_Permissions : System.Web.UI.Page {

    PositionRequest positionRequest = new PositionRequest();
    protected void Page_Load(object sender, EventArgs e) {

        //check if the user is logged in
        HttpCookie cookie = Request.Cookies.Get("phatt2");
        if (cookie == null) {
            users user = new users(HttpContext.Current.User.Identity.Name);
        }

        //hide all the panels
        panList.Visible = false;
        panAdd.Visible = false;
        panEdit.Visible = false;
        panError.Visible = false;

        //check if user has the right permissions
        /*
        if (positionRequest.GetUserPermission(Request.Cookies["phatt2"]["userctclinkid"]) != "HRO Admin") {
            lblError.Text = "<strong>Error: Access Denied</strong><br />You do not have permission to view this page.";
            panError.Visible = true;
        } else {
        */
            if (hidTodo.Value == "add") {
                //get the user's name by the EMPLID entered
                if (txtEmployeeID.Text != "") {
                    String name = positionRequest.GetEmployeeName(txtEmployeeID.Text);
                    if (name != null && name != "") {
                        lblName1.Text = name;
                    } else {
                        lblName1.Text = "An invalid ctcLink ID has been entered.";
                    }
                }
                panAdd.Visible = true;
            } else if (hidTodo.Value == "edit") {
                DataSet dsPermission = positionRequest.GetPermission(Convert.ToInt32(hidID.Value));
                if (dsPermission.Tables[0].Rows.Count > 0) {
                    lblName2.Text = dsPermission.Tables[0].Rows[0]["Name"].ToString();
                    lblEmployeeID.Text = dsPermission.Tables[0].Rows[0]["EmployeeID"].ToString();
                    cboPermissions2.SelectedValue = dsPermission.Tables[0].Rows[0]["Permission"].ToString();
                }
                hidTodo.Value = "";
                panEdit.Visible = true;
            } else if (hidTodo.Value == "delete") {
                bool success = positionRequest.DeletePermission(Convert.ToInt32(hidID.Value));
                if (success == true) {
                    Response.Redirect("Permissions.aspx");
                } else {
                    //show error
                }
            } else {
                DataSet dsPermissions = positionRequest.GetPermissions();
                Int32 intRowCount = dsPermissions.Tables[0].Rows.Count;
                if (intRowCount > 0) {
                    for (Int32 intDSRow = 0; intDSRow < intRowCount; intDSRow++) {
                        String permissionID = dsPermissions.Tables[0].Rows[intDSRow]["PermissionID"].ToString();
                        String name = dsPermissions.Tables[0].Rows[intDSRow]["Name"].ToString();
                        String permission = dsPermissions.Tables[0].Rows[intDSRow]["Permission"].ToString();

                        TableRow tr = new TableRow();
                        TableCell td = new TableCell();
                        td.Controls.Add(new LiteralControl(dsPermissions.Tables[0].Rows[intDSRow]["EmployeeID"].ToString()));
                        tr.Cells.Add(td);

                        td = new TableCell();
                        td.Controls.Add(new LiteralControl(name));
                        tr.Cells.Add(td);

                        td = new TableCell();
                        td.Controls.Add(new LiteralControl(permission));
                        tr.Cells.Add(td);

                        td = new TableCell();
                        td.Attributes["class"] = "action";
                        td.Controls.Add(new LiteralControl("<a href=\"#\" class=\"edit\" style=\"margin-left:5px\" title=\"Edit\" onclick=\"document.getElementById('" + hidTodo.ClientID + "').value='edit';document.getElementById('" + hidID.ClientID + "').value='" + permissionID + "';document.getElementById('" + frmPR.ClientID + "').submit();\"></a>"));
                        tr.Cells.Add(td);

                        td = new TableCell();
                        td.Attributes["class"] = "action";
                        td.Controls.Add(new LiteralControl("<a href=\"#\" class=\"delete\" style=\"margin-left:5px\" title=\"Delete\" onclick=\"if(confirm('Are you sure you want to delete permissions for " + name.Replace("'", "\\'") + "?')){document.getElementById('" + hidTodo.ClientID + "').value='delete';document.getElementById('" + hidID.ClientID + "').value='" + permissionID + "';document.getElementById('" + frmPR.ClientID + "').submit();}\"></a>"));
                        tr.Cells.Add(td);
                        tblList.Rows.Add(tr);
                    }
                } else {
                    TableRow tr = new TableRow();
                    TableCell td = new TableCell();
                    td.ColumnSpan = 5;
                    td.Controls.Add(new LiteralControl("No permissions currently exist"));
                    tr.Cells.Add(td);
                    tblList.Rows.Add(tr);
                }
                panList.Visible = true;
            }
        /*}*/
    }

    protected void cmdCancel_Click(object sender, EventArgs e) {
        Response.Redirect("Permissions.aspx");
    }

    protected void cmdAdd_Click(object sender, EventArgs e) {
        bool success = positionRequest.AddPermission(txtEmployeeID.Text, lblName1.Text, cboPermissions1.SelectedValue);
        if (success == true) {
            Response.Redirect("Permissions.aspx");
        } else {
            //show error message
        }
    }

    protected void cmdEdit_Click(object sender, EventArgs e) {
        bool success = positionRequest.EditPermission(Convert.ToInt32(hidID.Value), cboPermissions2.SelectedValue);
        if (success == true) {
            Response.Redirect("Permissions.aspx?test=true");
        } else {
            //show error message
        }

    }
}
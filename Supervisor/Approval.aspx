﻿<%@ Page Title="" Language="C#" MasterPageFile="~/container/container.master" AutoEventWireup="true" CodeFile="Approval.aspx.cs" Inherits="Supervisor_Approval" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="menuHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="maintextHolder" runat="Server">
    <form id="frmPR" runat="server">
        <input type="hidden" id="hidTodo" runat="server" />
        <input type="hidden" id="hidID" runat="server" />
        <input type="hidden" id="hidCreated" runat="server" />
        <h3 class="pageTitle">Position Request &ndash; Supervisor Approval</h3>
        <asp:Label style="color:red" ID="lblError" runat="server"></asp:Label>
        <asp:Panel ID="panList" runat="Server">
            <asp:Table ID="tblList" runat="server">
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell CssClass="sortHeader" onclick="sortTable(0,'maintextHolder_tblList')">Originator</asp:TableHeaderCell>
                    <asp:TableHeaderCell CssClass="sortHeader" onclick="sortTable(1,'maintextHolder_tblList')">Created</asp:TableHeaderCell>
                    <asp:TableHeaderCell CssClass="sortHeader" onclick="sortTable(2,'maintextHolder_tblList')">Official Title</asp:TableHeaderCell>
                    <asp:TableHeaderCell CssClass="sortHeader" onclick="sortTable(3,'maintextHolder_tblList')">Position Type</asp:TableHeaderCell>
                    <asp:TableHeaderCell></asp:TableHeaderCell>
                </asp:TableHeaderRow>
            </asp:Table>
        </asp:Panel>
        <asp:Panel ID="panView" runat="server">
            <asp:Label ID="lblConfirm" runat="server"></asp:Label>
            <div class="title">
                <div style="float: left; width: 68%;">
                    ORIGINATOR:&nbsp;<span class="normal"><asp:Label ID="lblOriginatorName" runat="Server"></asp:Label></span>
                    &nbsp;ctcLink ID:&nbsp;<span class="normal"><asp:Label ID="lblOriginatorID" runat="Server"></asp:Label></span>
                </div>
                <div style="float: right; text-align: right; width: 31%; margin-right: 5px;">
                    <asp:Panel ID="panTrackingNumber" runat="server">TRACKING #:&nbsp;<span class="normal"><asp:Label ID="lblTrackingNumber" runat="server"></asp:Label></span></asp:Panel>
                </div>
                <div class="clearer"></div>
            </div>
            <div class="section" style="border-bottom: none;">
                <div class="row">
                    <div class="col" style="width: 46%">
                        <strong>Official Position Title</strong><br />
                        <asp:Label ID="lblOfficialTitle" runat="Server" Style="width: 100%;"></asp:Label>
                    </div>
                    <div class="col" style="width: 46%">
                        <strong>Working Title</strong><br />
                        <asp:Label ID="lblWorkingTitle" runat="Server" Style="width: 100%;"></asp:Label>
                    </div>
                </div>
                <div class="row">
                    <div class="col" style="width: 46%;">
                        <strong>Job Code</strong><br />
                        <asp:Label ID="lblJobCode" runat="Server" Style="width: 100%;"></asp:Label>
                    </div>
                    <div class="col" style="width: 46%;">
                        <strong>Budget #</strong><br />
                        <asp:Label ID="lblBudgetNumber" runat="Server" Style="width: 100%;"></asp:Label>
                    </div>
                </div>
                <div class="row">
                    <div class="col" style="width: 46%;">
                        <strong>Position Control #</strong><br />
                        <asp:Label ID="lblPositionNumber" runat="Server" Style="width: 100%;"></asp:Label>
                    </div>
                    <div class="col" style="width: 46%;">
                        <strong>College/Unit</strong><br />
                        <asp:Label ID="lblCollegeUnit" runat="server" Style="width: 100%;"></asp:Label>
                    </div>
                </div>
                <div class="row">
                    <div class="col" style="width: 46%;">
                        <strong>Department Name</strong><br />
                        <asp:Label ID="lblDepartmentName" runat="server" Style="width: 100%;"></asp:Label>
                    </div>
                    <div class="col">
                        <strong>Department #</strong><br />
                        <asp:Label ID="lblDepartmentID" runat="server" Style="width: 100%;"></asp:Label>
                    </div>
                </div>
                <div class="row">
                    <div class="col" style="width: 46%;">
                        <strong>Building #</strong><br />
                        <asp:Label ID="lblBuildingNumber" runat="Server" Style="width: 100%"></asp:Label>
                    </div>
                    <div class="col" style="width: 46%">
                        <strong>Room #</strong><br />
                        <asp:Label ID="lblRoomNumber" runat="server" Style="width: 100%"></asp:Label>
                    </div>
                </div>
                <div class="row">
                    <div class="col" style="width: 46%;">
                        <strong>Position Phone #</strong><br />
                        <asp:Label ID="lblPhoneNumber" runat="Server" Style="width: 100%"></asp:Label>
                    </div>
                    <div class="col" style="width: 46%">
                        <strong>Mail Stop</strong><br />
                        <asp:Label ID="lblMailStop" runat="server" Style="width: 100%"></asp:Label>
                    </div>
                </div>
                <div class="row">
                    <div class="col" style="width: 46%">
                        <strong>Immediate Supervisor</strong><br />
                        <asp:Label ID="lblSupervisorName" runat="Server" Style="width: 100%"></asp:Label>
                    </div>
                    <div class="col" style="width: 46%">
                        <strong>Supervisor's ctcLink ID</strong><br />
                        <asp:Label ID="lblSupervisorID" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div style="display: flex; overflow: hidden; border-bottom: 1px solid #aaa;">
                <div style="width: 50%;">
                    <div class="title" style="width: 100%">TYPE OF POSITION</div>
                    <div class="section" style="width: 100%; height: 100%;">
                        <div class="row">
                            <div id="newReplacement" class="col" style="padding-bottom: 5px;">
                                <asp:Label ID="lblNewReplacement" runat="server"><strong></strong></asp:Label>
                            </div>
                            <div id="employeeReplaced" runat="server" style="margin-left: 15px; padding-top: 10px;">
                                <strong>Name of Employee Replaced</strong><br />
                                <asp:Label ID="lblEmployeeReplaced" runat="server" Style="width: 70%"></asp:Label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col" style="width: 100%;">
                                <div class="col" style="text-align: left;">
                                    <strong>Type of Position</strong><br />
                                    <asp:Label ID="lblClassified" runat="server"></asp:Label>
                                    <asp:Label ID="lblPositionType" runat="server"></asp:Label>
                                </div>
                                <div class="col" id="endDate" runat="server" style="margin-left: 10px; padding-bottom: 10px;">
                                    <strong>End Date</strong><br />
                                    <asp:Label ID="lblEndDate" runat="server"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col" style="width: 100%;" id="recruitmentType">
                                <strong>Type of Recruitment</strong>
                                <div>
                                    <asp:Label ID="lblRecruitmentType" runat="server"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="padding-right: 5px; width: 48%">
                    <div class="title" style="width: 100%; padding-right: 5px;">ONBOARDING</div>
                    <div class="section" style="width: 100%; padding-right: 0; padding-bottom: 15px; height: 100%;">
                        <div class="row">
                            <div class="col">
                                <strong>This position will require:</strong>
                                <div style="padding-top: 10px; margin-left: 10px;">
                                    <asp:Label ID="lblOnboarding" runat="server"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="schedule" runat="server">
                <div class="title" style="border-top:none;">POSITION WORK SCHEDULE</div>
                <div class="section">
                    <div class="row">
                        <div class="col" style="width: 20%">
                            <strong>Hours Per Day</strong><br />
                            <asp:Label ID="lblHoursPerDay" runat="server" Style="width: 100%"></asp:Label>
                        </div>
                        <div class="col" style="width: 19%">
                            <strong>Hours Per Week</strong><br />
                            <asp:Label ID="lblHoursPerWeek" runat="server" Style="width: 100%"></asp:Label>
                        </div>
                        <div class="col" style="width: 19%">
                            <strong>Months Per Year</strong><br />
                            <asp:Label ID="lblMonthsPerYear" runat="server" Style="width: 100%"></asp:Label>
                        </div>
                        <div id="cyclicCalendarCode2" runat="server" class="col" style="width: 25%; vertical-align: top; display: none;">
                            <strong>Cyclic Calendar Code</strong><br />
                            <asp:Label ID="lblCyclicCalendarCode" runat="server" Style="width: 100%"></asp:Label>
                        </div>
                    </div>
                    <div class="row" style="white-space: nowrap; padding-left: 0; padding-top: 10px;">
                        <div class="timeCol" style="width: 12%; text-align: center; padding-left: 0;">
                            <div style="padding-bottom: 3px;"><strong>Monday</strong></div>
                            <asp:Label ID="lblMonday" runat="server"></asp:Label>
                        </div>
                        <div class="timeCol" style="width: 13%; text-align: center;">
                            <div style="padding-bottom: 3px;"><strong>Tuesday</strong></div>
                            <asp:Label ID="lblTuesday" runat="server"></asp:Label>
                        </div>
                        <div class="timeCol" style="width: 14%; text-align: center;">
                            <div style="padding-bottom: 3px;"><strong>Wednesday</strong></div>
                            <asp:Label ID="lblWednesday" runat="server"></asp:Label>
                        </div>
                        <div class="timeCol" style="width: 13%; text-align: center;">
                            <div style="padding-bottom: 3px;"><strong>Thursday</strong></div>
                            <asp:Label ID="lblThursday" runat="server"></asp:Label>
                        </div>
                        <div class="timeCol" style="width: 13%; text-align: center;">
                            <div style="padding-bottom: 3px;"><strong>Friday</strong></div>
                            <asp:Label ID="lblFriday" runat="server"></asp:Label>
                        </div>
                        <div class="timeCol" style="width: 12%; text-align: center;">
                            <div style="padding-bottom: 3px;"><strong>Saturday</strong></div>
                            <asp:Label ID="lblSaturday" runat="server"></asp:Label>
                        </div>
                        <div class="timeCol" style="width: 12%; text-align: center;">
                            <div style="padding-bottom: 3px;"><strong>Sunday</strong></div>
                            <asp:Label ID="lblSunday" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
            <asp:Panel ID="panComments" runat="server">
                    <div class="title" style="border-top:none;">ORIGINATOR COMMENTS</div>
                    <div class="section">
                        <div style="padding: 0px 5px 5px 5px; text-align: left">
                            <asp:Label ID="lblOrigComments" runat="server"></asp:Label>
                        </div>
                    </div>
            </asp:Panel>
            <asp:Panel ID="panFileAttachments" runat="server">
                <div class="title" style="border-top:none;">FILE ATTACHMENTS</div>
                <div class="section">
                    <div style="padding: 0px 5px 5px 5px; text-align: left">
                        <asp:Label ID="lblAttachments" runat="server"></asp:Label>
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="panApprovals" runat="server">
                <div style="padding: 10px"></div>
                <div class="title" style="padding-right: 5px; border-bottom: none">
                    <div style="float: left; width: 50%">ROUTING/APPROVALS</div>
                    <div style="float: right; text-align: right; width: 50%;">STATUS:&nbsp;<span class="normal"><asp:Label ID="lblStatus" runat="server"></asp:Label></span></div>
                    <div class="clearer"></div>
                </div>
                <asp:Table ID="tblApprovals" runat="Server" Style="width: 100%" CellPadding="0"></asp:Table>
            </asp:Panel>
            <div style="width: 100%; text-align: center; padding-top: 20px">
                <input type="button" runat="server" value="Back" id="cmdBack" style="width: 70px; margin-right: 10px;" onclick="history.back(1)" />
                <input type="button" runat="server" id="cmdPrint" value="Print" style="width: 70px; margin-right: 10px;" onclick="openPage('../print.aspx?id=' + document.getElementById('maintextHolder_hidID').value, 'PositionRequest', '1000', '800');" />
                <asp:Button ID="cmdSubmit" runat="server" Text="Submit" Style="width: 70px" OnClientClick="return validate('Supervisor');" OnClick="cmdSubmit_Click" />
                <input type="button" runat="server" value="OK" id="cmdOK" style="width: 70px;" onclick="location.href='Approval.aspx'" />
            </div>
        </asp:Panel>
    </form>
    <script type="text/javascript" src="../Scripts/Form.js"></script>
    <script type="text/javascript" src="../Scripts/SortTable.js"></script>
</asp:Content>


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Net.Mail;
using System.Web.UI.HtmlControls;

public partial class Supervisor_Approval : System.Web.UI.Page {
    PositionRequest positionRequest = new PositionRequest();
    String strErrorContact = "<p>To report this error, please contact the IT Support Center by <a href=\"mailto:ITSupportCenter@ccs.spokane.edu\">email</a> or phone: 533-HELP.<br />Include a screen shot of this page and as much information as possible.</p>";
    String strAdminEmailAddress = ConfigurationManager.AppSettings["AdminEmailAddress"].ToString();
    List<string> excutiveApprovalJC = Utility.GetExecutiveApprovalJobCode();
    string[] dayWeek = Utility.GetDayWeek();
    //string emailTest = "vu.nguyen@ccs.spokane.edu";
    //string appHost = HttpContext.Current.Request.Url.Host.ToLower();
    protected void Page_Load(object sender, EventArgs e) {
        //check if the user is logged in
        HttpCookie cookie = Request.Cookies.Get("phatt2");
        if (cookie == null) {
            users user = new users(HttpContext.Current.User.Identity.Name);
        }

        panList.Visible = false;
        panView.Visible = false;
        lblError.Visible = false;

        String todo = Request.Form["ctl00$maintextHolder$hidTodo"];
        if (todo == null || todo == "") {
            todo = Request.QueryString["todo"];
        }

        if (todo == "view") {
            Int32 positionRequestID = Convert.ToInt32(hidID.Value);

            DataSet dsPositionRequest = positionRequest.GetPositionRequest(positionRequestID);
            DataTable dtPr = dsPositionRequest.Tables[0];
            if (dtPr.Rows.Count > 0)
            {
                DataRow drPr = dtPr.Rows[0];
                lblOriginatorName.Text = dsPositionRequest.Tables[0].Rows[0]["OriginatorName"].ToString();
                lblOriginatorID.Text = dsPositionRequest.Tables[0].Rows[0]["OriginatorID"].ToString();
                lblTrackingNumber.Text = dsPositionRequest.Tables[0].Rows[0]["TrackingNumber"].ToString();
                lblStatus.Text = dsPositionRequest.Tables[0].Rows[0]["Status"].ToString();
                lblOfficialTitle.Text = dsPositionRequest.Tables[0].Rows[0]["OfficialTitle"].ToString();
                lblWorkingTitle.Text = dsPositionRequest.Tables[0].Rows[0]["WorkingTitle"].ToString();
                lblJobCode.Text = dsPositionRequest.Tables[0].Rows[0]["JobCode"].ToString();
                lblBudgetNumber.Text = dsPositionRequest.Tables[0].Rows[0]["BudgetNumber"].ToString();
                lblPositionNumber.Text = dsPositionRequest.Tables[0].Rows[0]["PositionNumber"].ToString();
                lblCollegeUnit.Text = dsPositionRequest.Tables[0].Rows[0]["CollegeUnit"].ToString();
                lblDepartmentName.Text = dsPositionRequest.Tables[0].Rows[0]["Department"].ToString();
                lblDepartmentID.Text = dsPositionRequest.Tables[0].Rows[0]["DepartmentID"].ToString();
                lblPhoneNumber.Text = dsPositionRequest.Tables[0].Rows[0]["PhoneNumber"].ToString();
                lblMailStop.Text = dsPositionRequest.Tables[0].Rows[0]["MailStop"].ToString();
                lblSupervisorName.Text = dsPositionRequest.Tables[0].Rows[0]["SupervisorName"].ToString();
                lblSupervisorID.Text = dsPositionRequest.Tables[0].Rows[0]["SupervisorID"].ToString();
                if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["NewPosition"]) == 1) {
                    lblNewReplacement.Text = "New Position";
                    employeeReplaced.Visible = false;
                } else if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["ReplacementPosition"]) == 1) {
                    lblNewReplacement.Text = "Replacement Position";
                    lblEmployeeReplaced.Text = dsPositionRequest.Tables[0].Rows[0]["ReplacedEmployeeName"].ToString();
                    employeeReplaced.Visible = true;
                }
                lblPositionType.Text = dsPositionRequest.Tables[0].Rows[0]["PositionType"].ToString();
                if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["PermanentPosition"]) == 1) {
                    lblClassified.Text = "Permanent";
                    lblClassified.Visible = true;
                }
                if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["NonPermanentPosition"]) == 1) {
                    lblClassified.Text = "Non Permanent";
                    lblClassified.Visible = true;
                    try {
                        lblEndDate.Text = Convert.ToDateTime(dsPositionRequest.Tables[0].Rows[0]["PositionEndDate"]).ToShortDateString();
                        lblEndDate.Visible = true;
                    } catch {
                        //do nothing - no date exists
                    }
                } else {
                    endDate.Visible = false;
                }
                lblRecruitmentType.Text = dsPositionRequest.Tables[0].Rows[0]["RecruitmentType"].ToString();
                if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["PCard"]) == 1) {
                    lblOnboarding.Text += "<div style=\"padding-bottom:5px;\">P-Card</div>";
                }
                if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["CCSIssuedCellPhone"]) == 1) {
                    lblOnboarding.Text += "<div style=\"padding-bottom:5px;\">CCS-issued Cell Phone</div>";
                }
                if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["CTCLinkAccess"]) == 1) {
                    lblOnboarding.Text += "<div style=\"padding-bottom:5px;\">Additional ctcLink Security Access</div>";
                }
                if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["TimeSheetApprover"]) == 1) {
                    lblOnboarding.Text += "<div style=\"padding-bottom:5px;\">Manager/Timesheet Approver</div>";
                }
                if (lblPositionType.Text == "Classified" || lblPositionType.Text == "Part-Time Hourly")
                {
                    lblHoursPerDay.Text = dsPositionRequest.Tables[0].Rows[0]["HoursPerDay"].ToString();
                    lblHoursPerWeek.Text = dsPositionRequest.Tables[0].Rows[0]["HoursPerWeek"].ToString();
                    lblMonthsPerYear.Text = dsPositionRequest.Tables[0].Rows[0]["MonthsPerYear"].ToString();
                    lblCyclicCalendarCode.Text = dsPositionRequest.Tables[0].Rows[0]["CyclicCalendarCode"].ToString();
                    if (lblCyclicCalendarCode.Text != "") {
                        cyclicCalendarCode2.Attributes["style"] = cyclicCalendarCode2.Attributes["style"].Replace("display: none;", "");
                    }
                    /*
                    DataSet dsWorkSchedule = positionRequest.GetWorkSchedule(positionRequestID);
                    DataTable dtWorkSchedule = dsWorkSchedule.Tables[0];
                    if (dsWorkSchedule.Tables[0].Rows.Count > 0)
                    {
                        String mondayStart = dsWorkSchedule.Tables[0].Rows[0]["MondayStart"].ToString();
                        String mondayEnd = dsWorkSchedule.Tables[0].Rows[0]["MondayEnd"].ToString();
                        String tuesdayStart = dsWorkSchedule.Tables[0].Rows[0]["TuesdayStart"].ToString();
                        String tuesdayEnd = dsWorkSchedule.Tables[0].Rows[0]["TuesdayEnd"].ToString();
                        String wednesdayStart = dsWorkSchedule.Tables[0].Rows[0]["WednesdayStart"].ToString();
                        String wednesdayEnd = dsWorkSchedule.Tables[0].Rows[0]["WednesdayEnd"].ToString();
                        String thursdayStart = dsWorkSchedule.Tables[0].Rows[0]["ThursdayStart"].ToString();
                        String thursdayEnd = dsWorkSchedule.Tables[0].Rows[0]["ThursdayEnd"].ToString();
                        String fridayStart = dsWorkSchedule.Tables[0].Rows[0]["FridayStart"].ToString();
                        String fridayEnd = dsWorkSchedule.Tables[0].Rows[0]["FridayEnd"].ToString();
                        String saturdayStart = dsWorkSchedule.Tables[0].Rows[0]["SaturdayStart"].ToString();
                        String saturdayEnd = dsWorkSchedule.Tables[0].Rows[0]["SaturdayEnd"].ToString();
                        String sundayStart = dsWorkSchedule.Tables[0].Rows[0]["SundayStart"].ToString();
                        String sundayEnd = dsWorkSchedule.Tables[0].Rows[0]["SundayEnd"].ToString();
                        if (mondayStart != "" || mondayEnd != "") {
                            if (mondayStart != "") {
                                mondayStart = Convert.ToDateTime(mondayStart).ToString("%h:mm tt");
                            }
                            if (mondayEnd != "") {
                                mondayEnd = Convert.ToDateTime(mondayEnd).ToString("%h:mm tt");
                            }
                            lblMonday.Text = mondayStart + "<br />to<br />" + mondayEnd;
                        }
                        if (tuesdayStart != "" || tuesdayEnd != "") {
                            if (tuesdayStart != "") {
                                tuesdayStart = Convert.ToDateTime(tuesdayStart).ToString("%h:mm tt");
                            }
                            if (tuesdayEnd != "") {
                                tuesdayEnd = Convert.ToDateTime(tuesdayEnd).ToString("%h:mm tt");
                            }
                            lblTuesday.Text = tuesdayStart + "<br />to<br />" + tuesdayEnd;
                        }
                        if (wednesdayStart != "" || wednesdayEnd != "") {
                            if (wednesdayStart != "") {
                                wednesdayStart = Convert.ToDateTime(wednesdayStart).ToString("%h:mm tt");
                            }
                            if (wednesdayEnd != "") {
                                wednesdayEnd = Convert.ToDateTime(wednesdayEnd).ToString("%h:mm tt");
                            }
                            lblWednesday.Text = wednesdayStart + "<br />to<br />" + wednesdayEnd;
                        }
                        if (thursdayStart != "" || thursdayEnd != "") {
                            if (thursdayStart != "") {
                                thursdayStart = Convert.ToDateTime(thursdayStart).ToString("%h:mm tt");
                            }
                            if (thursdayEnd != "") {
                                thursdayEnd = Convert.ToDateTime(thursdayEnd).ToString("%h:mm tt");
                            }
                            lblThursday.Text = thursdayStart + "<br />to<br />" + thursdayEnd;
                        }
                        if (fridayStart != "" || fridayEnd != "") {
                            if (fridayStart != "") {
                                fridayStart = Convert.ToDateTime(fridayStart).ToString("%h:mm tt");
                            }
                            if (fridayEnd != "") {
                                fridayEnd = Convert.ToDateTime(fridayEnd).ToString("%h:mm tt");
                            }
                            lblFriday.Text = fridayStart + "<br />to<br />" + fridayEnd;
                        }
                        if (saturdayStart != "" || saturdayEnd != "") {
                            if (saturdayStart != "") {
                                saturdayStart = Convert.ToDateTime(saturdayStart).ToString("%h:mm tt");
                            }
                            if (saturdayEnd != "") {
                                saturdayEnd = Convert.ToDateTime(saturdayEnd).ToString("%h:mm tt");
                            }
                            lblSaturday.Text = saturdayStart + "<br />to<br />" + saturdayEnd;
                        }
                        if (sundayStart != "" || sundayEnd != "") {
                            if (sundayStart != "") {
                                sundayStart = Convert.ToDateTime(sundayStart).ToString("%h:mm tt");
                            }
                            if (sundayEnd != "") {
                                sundayEnd = Convert.ToDateTime(sundayEnd).ToString("%h:mm tt");
                            }
                            lblSunday.Text = sundayStart + "<br />to<br />" + sundayEnd;
                        }
                        schedule.Visible = true;
                    }
                    */
                    //Populate Work schedule for view/confirm
                    PopulateWorkScheduleForLabel(positionRequestID);
                }
                else
                {
                    schedule.Visible = false;
                }
                lblOrigComments.Text = drPr["Comments"].ToString();
                //Populate label for BuildingNumber and RoomNumber
                lblBuildingNumber.Text = drPr["BuildingNumber"].ToString();
                lblRoomNumber.Text = drPr["RoomNumber"].ToString();
            }

            //show uploaded file attachments
            DataSet dsAttachments = positionRequest.GetAttachments(positionRequestID);
            Int32 attachmentCount = dsAttachments.Tables[0].Rows.Count;
            if (attachmentCount > 0) {
                for (Int32 i = 0; i < attachmentCount; i++) {
                    lblAttachments.Text += "<div style=\"padding-bottom:5px;\"><a href=\"..\\Attachments/" + dsAttachments.Tables[0].Rows[i]["FileName"].ToString() + "\" target=\"self\" class=\"smallText\" title=\"View File Attachment " + (i + 1) + "\">Attachment " + (i + 1) + "</a></div>";
                }
                panFileAttachments.Visible = true;
            } else {
                panFileAttachments.Visible = false;
            }

            //get Routing/Approvals
            DataSet dsRouting = positionRequest.GetRouting(positionRequestID);
            Int32 routingCount = dsRouting.Tables[0].Rows.Count;
            if (routingCount > 0) {
                for (Int32 i = 0; i < routingCount; i++) {
                    String note = dsRouting.Tables[0].Rows[i]["Note"].ToString();
                    String description = dsRouting.Tables[0].Rows[i]["Description"].ToString();
                    TableRow tr = new TableRow();
                    TableCell td = new TableCell();
                    td.CssClass = "solid";
                    td.Attributes["style"] = "vertical-align:top;padding-left:10px;width:75px";
                    td.Controls.Add(new LiteralControl(dsRouting.Tables[0].Rows[i]["UserType"].ToString()));
                    tr.Cells.Add(td);
                    td = new TableCell();
                    td.CssClass = "solid";
                    td.Attributes["style"] = "vertical-align:top;";
                    if (description != "Pending Approval") {
                        td.Attributes["style"] += "width:190px;";
                        td.Controls.Add(new LiteralControl(dsRouting.Tables[0].Rows[i]["Name"].ToString()));
                        tr.Cells.Add(td);
                        td = new TableCell();
                        td.CssClass = "solid";
                        td.Attributes["style"] = "vertical-align:top";
                        td.Controls.Add(new LiteralControl(description + "&nbsp;" + Convert.ToDateTime(dsRouting.Tables[0].Rows[i]["Date"]).ToString("MM/dd/yy %h:mm tt")));
                        if (note != "") {
                            td.Controls.Add(new LiteralControl("<br />" + note));
                        }
                    } else {
                        td.ColumnSpan = 2;
                        td.Attributes["style"] += "wite-space:nowrap;";
                        HtmlInputRadioButton optApprove = new HtmlInputRadioButton();
                        optApprove.Attributes["runat"] = "server";
                        optApprove.ID = "optApprove";
                        optApprove.Name = "optApproval";
                        td.Controls.Add(optApprove);
                        td.Controls.Add(new LiteralControl("<strong>Approve</strong>&nbsp;"));
                        HtmlInputRadioButton optReject = new HtmlInputRadioButton();
                        optReject.Attributes["runat"] = "server";
                        optReject.ID = "optReject";
                        optReject.Name = "optApproval";
                        td.Controls.Add(optReject);
                        td.Controls.Add(new LiteralControl("<strong>Reject</strong>&nbsp;"));
                        TextBox txtReject = new TextBox();
                        txtReject.ID = "txtReject";
                        txtReject.Attributes["runat"] = "server";
                        txtReject.Attributes["style"] = "width:310px";
                        td.Controls.Add(txtReject);
                    }
                    tr.Cells.Add(td);
                    tblApprovals.Rows.Add(tr);
                }
                panApprovals.Visible = true;
            } else {
                panApprovals.Visible = false;
            }

            cmdOK.Visible = false;
            panView.Visible = true;

        } else if (todo == "confirm") {

            Int32 positionRequestID = Convert.ToInt32(Request.QueryString["id"]);
            hidID.Value = positionRequestID.ToString();

            DataSet dsPositionRequest = positionRequest.GetPositionRequest(positionRequestID);
            DataTable dtPositionRequest = dsPositionRequest.Tables[0];
            if (dtPositionRequest.Rows.Count > 0) {
                DataRow drPositionRequest = dtPositionRequest.Rows[0];
                String task = Request.QueryString["task"];
                String notified = Request.QueryString["notified"];
                String notNotified = Request.QueryString["notNotified"];
                lblConfirm.Text = "<p><strong>The following form has been " + task + ".</strong><br /></p>";
                if(notified != "") {
                    lblConfirm.Text += "<p><strong>Notification has been sent to:</strong>" + notified.Replace("|", "<br />") + "</p>";
                }
                if (notNotified != "") {
                    lblConfirm.Text += "<p><strong>Notification could not be sent to:</strong>" + notNotified.Replace("|", "<br />") + "</p>";
                }
                lblConfirm.Text += "<p>To view previous approvals, click on the PR Report link in the menu to the left.<br /></p>";
                lblOriginatorID.Text = drPositionRequest["OriginatorID"].ToString();
                lblOriginatorName.Text = drPositionRequest["OriginatorName"].ToString();
                lblTrackingNumber.Text = drPositionRequest["TrackingNumber"].ToString();
                lblStatus.Text = drPositionRequest["Status"].ToString();
                lblOfficialTitle.Text = drPositionRequest["OfficialTitle"].ToString();
                lblWorkingTitle.Text = drPositionRequest["WorkingTitle"].ToString();
                lblJobCode.Text = drPositionRequest["JobCode"].ToString();
                lblBudgetNumber.Text = drPositionRequest["BudgetNumber"].ToString();
                lblPositionNumber.Text = drPositionRequest["PositionNumber"].ToString();
                lblCollegeUnit.Text = drPositionRequest["CollegeUnit"].ToString(); //may need to add conditional logic to show scc, sfcc, or dist
                lblDepartmentName.Text = drPositionRequest["Department"].ToString();
                lblDepartmentID.Text = drPositionRequest["DepartmentID"].ToString();
                lblPhoneNumber.Text = drPositionRequest["PhoneNumber"].ToString();
                lblMailStop.Text = drPositionRequest["MailStop"].ToString();
                lblSupervisorName.Text = drPositionRequest["SupervisorName"].ToString();
                lblSupervisorID.Text = drPositionRequest["SupervisorID"].ToString();
                if (Convert.ToByte(drPositionRequest["NewPosition"]) == 1) {
                    lblNewReplacement.Text = "New Position";
                    employeeReplaced.Visible = false;
                } else if (Convert.ToByte(drPositionRequest["ReplacementPosition"]) == 1) {
                    lblNewReplacement.Text = "Replacement Position";
                    lblEmployeeReplaced.Text = drPositionRequest["ReplacedEmployeeName"].ToString();
                    employeeReplaced.Visible = true;
                }
                lblPositionType.Text = drPositionRequest["PositionType"].ToString();
                if (Convert.ToByte(drPositionRequest["PermanentPosition"]) == 1) {
                    lblClassified.Text = "Permanent";
                    lblClassified.Visible = true;
                }
                if (Convert.ToByte(drPositionRequest["NonPermanentPosition"]) == 1) {
                    lblClassified.Text = "Non Permanent";
                    lblClassified.Visible = true;
                    try {
                        lblEndDate.Text = Convert.ToDateTime(drPositionRequest["PositionEndDate"]).ToShortDateString();
                        lblEndDate.Visible = true;
                    } catch {
                        //do nothing - no date exists
                    }
                } else {
                    endDate.Visible = false;
                }
                lblRecruitmentType.Text = drPositionRequest["RecruitmentType"].ToString();
                if (Convert.ToByte(drPositionRequest["PCard"]) == 1) {
                    lblOnboarding.Text += "<div style=\"padding-bottom:5px;\">P-Card</div>";
                }
                if (Convert.ToByte(drPositionRequest["CCSIssuedCellPhone"]) == 1) {
                    lblOnboarding.Text += "<div style=\"padding-bottom:5px;\">CCS-issued Cell Phone</div>";
                }
                if (Convert.ToByte(drPositionRequest["CTCLinkAccess"]) == 1) {
                    lblOnboarding.Text += "<div style=\"padding-bottom:5px;\">Additional ctcLink Security Access</div>";
                }
                if (Convert.ToByte(drPositionRequest["TimeSheetApprover"]) == 1) {
                    lblOnboarding.Text += "<div style=\"padding-bottom:5px;\">Manager/Timesheet Approver</div>";
                }
                if (lblPositionType.Text == "Classified" || lblPositionType.Text == "Part-Time Hourly") {
                    lblHoursPerDay.Text = drPositionRequest["HoursPerDay"].ToString();
                    lblHoursPerWeek.Text = drPositionRequest["HoursPerWeek"].ToString();
                    lblMonthsPerYear.Text = drPositionRequest["MonthsPerYear"].ToString();
                    lblCyclicCalendarCode.Text = drPositionRequest["CyclicCalendarCode"].ToString();
                    if (lblCyclicCalendarCode.Text != "") {
                        cyclicCalendarCode2.Attributes["style"] = cyclicCalendarCode2.Attributes["style"].Replace("display: none;", "");
                    }

                    /*
                    DataSet dsWorkSchedule = positionRequest.GetWorkSchedule(positionRequestID);
                    if (dsWorkSchedule.Tables[0].Rows.Count > 0) {
                        String mondayStart = dsWorkSchedule.Tables[0].Rows[0]["MondayStart"].ToString();
                        String mondayEnd = dsWorkSchedule.Tables[0].Rows[0]["MondayEnd"].ToString();
                        String tuesdayStart = dsWorkSchedule.Tables[0].Rows[0]["TuesdayStart"].ToString();
                        String tuesdayEnd = dsWorkSchedule.Tables[0].Rows[0]["TuesdayEnd"].ToString();
                        String wednesdayStart = dsWorkSchedule.Tables[0].Rows[0]["WednesdayStart"].ToString();
                        String wednesdayEnd = dsWorkSchedule.Tables[0].Rows[0]["WednesdayEnd"].ToString();
                        String thursdayStart = dsWorkSchedule.Tables[0].Rows[0]["ThursdayStart"].ToString();
                        String thursdayEnd = dsWorkSchedule.Tables[0].Rows[0]["ThursdayEnd"].ToString();
                        String fridayStart = dsWorkSchedule.Tables[0].Rows[0]["FridayStart"].ToString();
                        String fridayEnd = dsWorkSchedule.Tables[0].Rows[0]["FridayEnd"].ToString();
                        String saturdayStart = dsWorkSchedule.Tables[0].Rows[0]["SaturdayStart"].ToString();
                        String saturdayEnd = dsWorkSchedule.Tables[0].Rows[0]["SaturdayEnd"].ToString();
                        String sundayStart = dsWorkSchedule.Tables[0].Rows[0]["SundayStart"].ToString();
                        String sundayEnd = dsWorkSchedule.Tables[0].Rows[0]["SundayEnd"].ToString();
                        if (mondayStart != "" || mondayEnd != "") {
                            if (mondayStart != "") {
                                mondayStart = Convert.ToDateTime(mondayStart).ToString("%h:mm tt");
                            }
                            if (mondayEnd != "") {
                                mondayEnd = Convert.ToDateTime(mondayEnd).ToString("%h:mm tt");
                            }
                            lblMonday.Text = mondayStart + "<br />to<br />" + mondayEnd;
                        }
                        if (tuesdayStart != "" || tuesdayEnd != "") {
                            if (tuesdayStart != "") {
                                tuesdayStart = Convert.ToDateTime(tuesdayStart).ToString("%h:mm tt");
                            }
                            if (tuesdayEnd != "") {
                                tuesdayEnd = Convert.ToDateTime(tuesdayEnd).ToString("%h:mm tt");
                            }
                            lblTuesday.Text = tuesdayStart + "<br />to<br />" + tuesdayEnd;
                        }
                        if (wednesdayStart != "" || wednesdayEnd != "") {
                            if (wednesdayStart != "") {
                                wednesdayStart = Convert.ToDateTime(wednesdayStart).ToString("%h:mm tt");
                            }
                            if (wednesdayEnd != "") {
                                wednesdayEnd = Convert.ToDateTime(wednesdayEnd).ToString("%h:mm tt");
                            }
                            lblWednesday.Text = wednesdayStart + "<br />to<br />" + wednesdayEnd;
                        }
                        if (thursdayStart != "" || thursdayEnd != "") {
                            if (thursdayStart != "") {
                                thursdayStart = Convert.ToDateTime(thursdayStart).ToString("%h:mm tt");
                            }
                            if (thursdayEnd != "") {
                                thursdayEnd = Convert.ToDateTime(thursdayEnd).ToString("%h:mm tt");
                            }
                            lblThursday.Text = thursdayStart + "<br />to<br />" + thursdayEnd;
                        }
                        if (fridayStart != "" || fridayEnd != "") {
                            if (fridayStart != "") {
                                fridayStart = Convert.ToDateTime(fridayStart).ToString("%h:mm tt");
                            }
                            if (fridayEnd != "") {
                                fridayEnd = Convert.ToDateTime(fridayEnd).ToString("%h:mm tt");
                            }
                            lblFriday.Text = fridayStart + "<br />to<br />" + fridayEnd;
                        }
                        if (saturdayStart != "" || saturdayEnd != "") {
                            if (saturdayStart != "") {
                                saturdayStart = Convert.ToDateTime(saturdayStart).ToString("%h:mm tt");
                            }
                            if (saturdayEnd != "") {
                                saturdayEnd = Convert.ToDateTime(saturdayEnd).ToString("%h:mm tt");
                            }
                            lblSaturday.Text = saturdayStart + "<br />to<br />" + saturdayEnd;
                        }
                        if (sundayStart != "" || sundayEnd != "") {
                            if (sundayStart != "") {
                                sundayStart = Convert.ToDateTime(sundayStart).ToString("%h:mm tt");
                            }
                            if (sundayEnd != "") {
                                sundayEnd = Convert.ToDateTime(sundayEnd).ToString("%h:mm tt");
                            }
                            lblSunday.Text = sundayStart + "<br />to<br />" + sundayEnd;
                        }
                        schedule.Visible = true;
                    }
                    */
                    //Populate Work schedule for view/confirm
                    PopulateWorkScheduleForLabel(positionRequestID);
                } else {
                    schedule.Visible = false;
                }
                lblOrigComments.Text = drPositionRequest["Comments"].ToString();
                //Populate label for BuildingNumber and RoomNumber
                lblBuildingNumber.Text = drPositionRequest["BuildingNumber"].ToString();
                lblRoomNumber.Text = drPositionRequest["RoomNumber"].ToString();
            }

            //show uploaded file attachments
            DataSet dsAttachments = positionRequest.GetAttachments(positionRequestID);
            Int32 attachmentCount = dsAttachments.Tables[0].Rows.Count;
            if (attachmentCount > 0) {
                for (Int32 i = 0; i < attachmentCount; i++) {
                    lblAttachments.Text += "<div style=\"padding-bottom:5px;\"><a href=\"..\\Attachments/" + dsAttachments.Tables[0].Rows[i]["FileName"].ToString() + "\" target=\"self\" class=\"smallText\" title=\"View File Attachment " + (i + 1) + "\">Attachment " + (i + 1) + "</a></div>";
                }
                panFileAttachments.Visible = true;
            } else {
                panFileAttachments.Visible = false;
            }

            //get Routing/Approvals
            DataSet dsRouting = positionRequest.GetRouting(positionRequestID);
            Int32 routingCount = dsRouting.Tables[0].Rows.Count;
            if (routingCount > 0) {
                for (Int32 i = 0; i < routingCount; i++) {
                    String note = dsRouting.Tables[0].Rows[i]["Note"].ToString();
                    String description = dsRouting.Tables[0].Rows[i]["Description"].ToString();
                    TableRow tr = new TableRow();
                    TableCell td = new TableCell();
                    td.CssClass = "solid";
                    td.Attributes["style"] = "vertical-align:top;padding-left:10px;width:75px";
                    td.Controls.Add(new LiteralControl(dsRouting.Tables[0].Rows[i]["UserType"].ToString()));
                    tr.Cells.Add(td);
                    td = new TableCell();
                    td.CssClass = "solid";
                    td.Attributes["style"] = "vertical-align:top;width:190px";
                    td.Controls.Add(new LiteralControl(dsRouting.Tables[0].Rows[i]["Name"].ToString()));
                    tr.Cells.Add(td);
                    td = new TableCell();
                    td.CssClass = "solid";
                    td.Attributes["style"] = "vertical-align:top";
                    td.Controls.Add(new LiteralControl(description));
                    if (description != "Pending Approval") {
                        td.Controls.Add(new LiteralControl("&nbsp;" + Convert.ToDateTime(dsRouting.Tables[0].Rows[i]["Date"]).ToString("MM/dd/yy %h:mm tt")));
                    }
                    if (note != "") {
                        td.Controls.Add(new LiteralControl("<br />" + note));
                    }
                    tr.Cells.Add(td);
                    tblApprovals.Rows.Add(tr);
                }
                panApprovals.Visible = true;
            } else {
                panApprovals.Visible = false;
            }

            cmdBack.Visible = false;
            cmdSubmit.Visible = false;
            panView.Visible = true;

        } else {

            DataSet dsPositionRequests = positionRequest.GetPositionRequestsForSupervisorApproval(Request.Cookies["phatt2"]["userctclinkid"]);
            Int32 rowCount = dsPositionRequests.Tables[0].Rows.Count;
            if (rowCount > 0) {
                for (Int32 i = 0; i < rowCount; i++) {
                    Int32 positionRequestID = Convert.ToInt32(dsPositionRequests.Tables[0].Rows[i]["PositionRequestID"]);
                    String created = "";
                    try { //temporary until created date is required and added to the db when form is saved and updated
                        created = Convert.ToDateTime(dsPositionRequests.Tables[0].Rows[i]["Created"]).ToShortDateString();
                    } catch {
                        //do nothing
                    }

                    TableRow tr = new TableRow();
                    TableCell td = new TableCell();
                    td.CssClass = "solid";
                    td.Controls.Add(new LiteralControl(dsPositionRequests.Tables[0].Rows[i]["OriginatorName"].ToString()));
                    tr.Cells.Add(td);

                    td = new TableCell();
                    td.CssClass = "solid";
                    td.Controls.Add(new LiteralControl(created));
                    tr.Cells.Add(td);

                    td = new TableCell();
                    td.CssClass = "solid";
                    td.Controls.Add(new LiteralControl(dsPositionRequests.Tables[0].Rows[i]["OfficialTitle"].ToString()));
                    tr.Cells.Add(td);

                    td = new TableCell();
                    td.CssClass = "solid";
                    td.Controls.Add(new LiteralControl(dsPositionRequests.Tables[0].Rows[i]["PositionType"].ToString()));
                    tr.Cells.Add(td);

                    td = new TableCell();
                    td.CssClass = "action";
                    td.Attributes["style"] = "vertical-align:top;width:16px;background:#eee none";
                    td.Controls.Add(new LiteralControl("<a href=\"#\" class=\"view\" title=\"View Form for Approval\" onclick=\"document.getElementById('" + hidTodo.ClientID + "').value='view';document.getElementById('" + hidID.ClientID + "').value='" + positionRequestID + "';document.getElementById('" + hidCreated.ClientID + "').value='" + created + "';document.getElementById('" + frmPR.ClientID + "').submit();\"></a>"));
                    tr.Cells.Add(td);
                    tblList.Rows.Add(tr);
                }
            } else {
                TableRow tr = new TableRow();
                TableCell td = new TableCell();
                td.ColumnSpan = 5;
                td.Controls.Add(new LiteralControl("No forms currently require your approval."));
                tr.Cells.Add(td);
                tblList.Rows.Add(tr);
            }
            panList.Visible = true;
        }
    }

    protected void cmdSubmit_Click(object sender, System.EventArgs e) {
        String employeeID = Request.Cookies["phatt2"]["userctclinkid"], selectedOption = Request.Form["ctl00$maintextHolder$optApproval"], rejectReason = Request.Form["ctl00$maintextHolder$txtReject"];
        String name = positionRequest.GetEmployeeName(employeeID), notified = "", notNotified = "", task = "";
        String jobCode = positionRequest.GetEmployeeJobCode(employeeID);
        Int32 positionRequestID = Convert.ToInt32(hidID.Value);
        bool blnSuccess = true;

        if (selectedOption == "optApprove") {

            //update Approval Record
            blnSuccess = positionRequest.EditApproval(positionRequestID, employeeID, name, lblStatus.Text, "Pending Approval", "Approved", "", DateTime.Now.ToString("MM/dd/yy HH:mm:ss.fff"));
            
            if (blnSuccess) {
                //get next supervisor in routing process
                DataSet dsSupervisor = positionRequest.GetSupervisor(employeeID);
                String supervisorJobCode = "", supervisorEmail = "", supervisorID = "", supervisorName = "", menu = "Supervisor";
                String status = "";
                if (lblStatus.Text.IndexOf("Approver") > -1) {
                    status = "Approver " + (Convert.ToInt16(lblStatus.Text.Replace("Approver ", "")) + 1).ToString();
                }
 
                if (dsSupervisor.Tables[0].Rows.Count > 0) {
                    supervisorJobCode = dsSupervisor.Tables[0].Rows[0]["JOBCODE"].ToString();
                    supervisorEmail = dsSupervisor.Tables[0].Rows[0]["EMAIL_ADDR"].ToString();
                    supervisorID = dsSupervisor.Tables[0].Rows[0]["EMPLID"].ToString();
                    supervisorName = dsSupervisor.Tables[0].Rows[0]["NAME"].ToString();
                    if (lblPositionType.Text != "Adjunct Faculty" && lblPositionType.Text != "Part-Time Hourly" && (String.Compare(lblDepartmentID.Text, "99001") < 0 || String.Compare(lblDepartmentID.Text, "99021") > 0)) { //(Convert.ToInt32(lblDepartmentID.Text) < 99001 && Convert.ToInt32(lblDepartmentID.Text) > 99021)) {
                        if (lblStatus.Text == "Executive") {
                            status = "Budget";
                            //supervisorEmail = "vu.nguyen@ccs.spokane.edu";
                            supervisorEmail = "BudgetPR@ccs.spokane.edu"; 
                            supervisorName = "";
                            supervisorID = "";
                            menu = "Budget Admin";
                        } else if (lblStatus.Text == "Chancellor") {
                            status = "HRO";
                            //supervisorEmail = "vu.nguyen@ccs.spokane.edu";
                            supervisorEmail = "HROPR@ccs.spokane.edu"; 
                            supervisorName = "";
                            supervisorID = "";
                            menu = "HRO Admin";
                        } else if (supervisorJobCode == "111113" || supervisorJobCode == "111133") {
                            status = "Executive";
                        }
                    } else if (lblPositionType.Text == "Adjunct Faculty") {
                        //Stop prior to reaching job code 111274, 111577, 111465, 111439, 111120
                        if (supervisorJobCode == "111274" || supervisorJobCode == "111577" || supervisorJobCode == "111465" || supervisorJobCode == "111439" || supervisorJobCode == "111120") {
                            status = "Budget";
                            //supervisorEmail = "vu.nguyen@ccs.spokane.edu";
                            supervisorEmail = "BudgetPR@ccs.spokane.edu"; 
                            supervisorName = "";
                            supervisorID = "";
                            menu = "Budget Admin";
                        }
                    } else if (lblPositionType.Text == "Part-Time Hourly" || (String.Compare(lblDepartmentID.Text, "99001") >= 0 && String.Compare(lblDepartmentID.Text, "99021") <= 0)) {
                        if (lblStatus.Text == "Executive") {
                            if (jobCode == "111490") { //Melody Matthews job code
                                if (positionRequest.GetBudgetApprovalCount(positionRequestID) > 0) { 
                                    status = "HRO";
                                    //supervisorEmail = "vu.nguyen@ccs.spokane.edu"; //for testing
                                    supervisorEmail = "HROPR@ccs.spokane.edu"; 
                                    supervisorName = "";
                                    supervisorID = "";
                                    menu = "HRO Admin";
                                } else {
                                    status = "Budget";
                                    //supervisorEmail = "vu.nguyen@ccs.spokane.edu"; //for testing
                                    supervisorEmail = "BudgetPR@ccs.spokane.edu"; 
                                    supervisorName = "";
                                    supervisorID = "";
                                    menu = "Budget Admin";
                                }
                            } else { //all other executive approvals
                                status = "Budget";
                                //supervisorEmail = "vu.nguyen@ccs.spokane.edu"; //for testing
                                supervisorEmail = "BudgetPR@ccs.spokane.edu"; 
                                supervisorName = "";
                                supervisorID = "";
                                menu = "Budget Admin";
                            }
                        }
                        else if(excutiveApprovalJC.Contains(supervisorJobCode))
                        { 
                            //(supervisorJobCode == "111272" || supervisorJobCode == "111165" || supervisorJobCode == "111319" || supervisorJobCode == "111433" || supervisorJobCode == "111274" || supervisorJobCode == "111577" || supervisorJobCode == "111465" || supervisorJobCode == "111439" || supervisorJobCode == "111120" || supervisorJobCode == "111499") {
                            status = "Executive";
                            if (supervisorJobCode == "111433") { //if Greg Stevens is the Executive approver
                                //change the Executive approver to Melody Matthews
                                supervisorJobCode = "111490";
                                dsSupervisor = positionRequest.GetEmployeeByJobCode(supervisorJobCode);
                                if (dsSupervisor.Tables[0].Rows.Count > 0) {
                                    supervisorEmail = dsSupervisor.Tables[0].Rows[0]["EMAIL_ADDR"].ToString();
                                    supervisorID = dsSupervisor.Tables[0].Rows[0]["EMPLID"].ToString();
                                    supervisorName = dsSupervisor.Tables[0].Rows[0]["NAME"].ToString();
                                } else {
                                    lblError.Text = "<p><strong>Error: The next supervisor for approval could not be found.";
                                    //set routing record back to current supervisor approval
                                    blnSuccess = positionRequest.EditApproval(positionRequestID, employeeID, name, lblStatus.Text, "Approved", "Pending Approval", "", DateTime.Now.AddMilliseconds(100).ToString("MM/dd/yy HH:mm:ss.fff"));
                                    if (blnSuccess) {
                                        lblError.Text += " The position request has been set back to pending your approval.";
                                    } else {
                                        lblError.Text += " The position request could not be set back to pending your approval.";
                                    }
                                    lblError.Text += "</strong></p>" + strErrorContact + "<br />";
                                    blnSuccess = false;
                                }
                            }
                        }
                    }

                    if (blnSuccess) {
                        try {

                            //add pending approval routing record
                            blnSuccess = positionRequest.AddRouting(positionRequestID, status, "Pending Approval", "", supervisorName, supervisorID, DateTime.Now.AddMilliseconds(100).ToString("MM/dd/yy HH:mm:ss.fff"));

                            if (blnSuccess) {
                                //update position request status
                                blnSuccess = positionRequest.EditStatus(positionRequestID, status); //may need to check boolean return value to make sure status is update and notify HR if not

                                if (blnSuccess) {
                                    if (status == "Budget") {
                                        supervisorName = "the Budget Admin";
                                    } else if (status == "HRO") {
                                        supervisorName = "the HRO Admin";
                                    }

                                    //send email for approval
                                    SmtpClient client = new SmtpClient();
                                    MailMessage objEmail = new MailMessage();
                                    String emailBody = "<p><strong>Tracking #:</strong> " + lblTrackingNumber.Text +
                                                          "<br /><strong>Originator:</strong> " + lblOriginatorName.Text +
                                                          "<br /><strong>Created:</strong> " + hidCreated.Value +
                                                          "<br /><strong>College/Unit:</strong> " + lblCollegeUnit.Text +
                                                          "<br /><strong>Department:</strong> " + lblDepartmentName.Text +
                                                          "<br /><strong>Official Title:</strong> " + lblOfficialTitle.Text +
                                                          "<br /><strong>Supervisor Name:</strong> " + lblSupervisorName.Text +
                                                          "<br /><strong>Position Type:</strong> " + lblPositionType.Text +
                                                          "<br /><strong>Status:</strong> " + status + "</p>";

                                    if (supervisorEmail != "") {
                                        //objEmail.To.Add(supervisorEmail); 
                                        //objEmail.To.Add("vu.nguyen@ccs.spokane.edu");
                                        /*
                                        if (appHost.Contains("internal"))
                                        {
                                            objEmail.To.Add(supervisorEmail);
                                        }
                                        else
                                        {
                                            objEmail.To.Add(emailTest);
                                        }
                                        */
                                        objEmail.To.Add(Utility.GetSendEmail(supervisorEmail));
                                        objEmail.From = new MailAddress(strAdminEmailAddress);
                                        objEmail.IsBodyHtml = true;
                                        objEmail.Priority = MailPriority.High;
                                        objEmail.Subject = "Position Request Approval Notification";
                                       
                                        objEmail.Body = "<p>Dear " + supervisorName + ",</p>";
                                        objEmail.Body += "<p>Please review the Position Request below for approval.</p>";
                                        objEmail.Body += "<p><a href=\"https://internal.spokane.edu/PositionRequest/\">https://internal.spokane.edu/PositionRequest/</a>";
                                        objEmail.Body += "<br />Log in with your Active Directory account and click on the PR Approval link in the " + menu + " menu.</p>";
                                        objEmail.Body += emailBody;

                                        try {
                                            client.Send(objEmail);
                                            notified += "|" + status + ": " + supervisorName;
                                        } catch {
                                            objEmail = new MailMessage();
                                            objEmail.To.Add(strAdminEmailAddress);
                                            objEmail.From = new MailAddress(strAdminEmailAddress);
                                            objEmail.IsBodyHtml = true;
                                            objEmail.Priority = MailPriority.High;
                                            objEmail.Subject = "Position Request Approval Notification Could Not Be Sent";
                                            objEmail.Body = "<p>The following approval notification email could not be sent to " + supervisorName + " at the email address " + supervisorEmail + ".</p>";
                                            objEmail.Body += "<hr style=\"width:100%\" />";
                                            objEmail.Body += emailBody;

                                            try {
                                                client.Send(objEmail);
                                                notNotified += "|" + status + ": " + supervisorName + ".|HRO has been notified.";
                                            } catch {
                                                notNotified += "|" + status + ": " + supervisorName + " or HRO.|Please contact HRO at " + strAdminEmailAddress + ".";
                                            }
                                        }
                                    } else {
                                        objEmail = new MailMessage();
                                        objEmail.To.Add(strAdminEmailAddress);
                                        objEmail.From = new MailAddress(strAdminEmailAddress);
                                        objEmail.IsBodyHtml = true;
                                        objEmail.Priority = MailPriority.High;
                                        objEmail.Subject = "Position Request Approval Notification Could Not Be Sent";
                                        objEmail.Body = "<p>The following approval notification email could not be sent to " + supervisorName + " because their email address does not exist.</p>";
                                        objEmail.Body += "<hr style=\"width:100%\" />";
                                        objEmail.Body += emailBody;

                                        try {
                                            client.Send(objEmail);
                                            notNotified += "|" + status + ": " + supervisorName + ".|HRO has been notified.";
                                        } catch {
                                            notNotified += "|" + status + ": " + supervisorName + " or HRO.|Please contact HRO at " + strAdminEmailAddress + ".";
                                        }
                                    }

                                    task = "approved";

                                } else {
                                    lblError.Text = "<p><strong>Error: The Position Request status could not be updated to " + status + ".";
                                    //set routing record back to current supervisor approval
                                    blnSuccess = positionRequest.EditApproval(positionRequestID, employeeID, name, lblStatus.Text, "Approved", "Pending Approval", "", DateTime.Now.AddMilliseconds(100).ToString("MM/dd/yy HH:mm:ss.fff"));
                                    if (blnSuccess) {
                                        lblError.Text += " The position request has been set back to pending your approval.";
                                    } else {
                                        lblError.Text += " The position request could not be set back to pending your approval.";
                                    }
                                    lblError.Text += "</strong></p>" + strErrorContact + "<br />";
                                    blnSuccess = false;
                                }

                            } else {
                                lblError.Text = "<p><strong>Error: The " + status + " pending approval record could not be added.";
                                //set routing record back to current supervisor approval
                                blnSuccess = positionRequest.EditApproval(positionRequestID, employeeID, name, lblStatus.Text, "Approved", "Pending Approval", "", DateTime.Now.AddMilliseconds(100).ToString("MM/dd/yy HH:mm:ss.fff"));
                                if (blnSuccess) {
                                    lblError.Text += " The position request has been set back to pending your approval.";
                                } else {
                                    lblError.Text += " The position request could not be set back to pending your approval.";
                                }
                                lblError.Text += "</strong></p>" + strErrorContact + "<br />";
                                blnSuccess = false;
                            }

                        } catch {
                            lblError.Text = "<p><strong>Error: An error occurred while submitting the " + status + " pending approval record.";
                            //set routing record back to current supervisor approval
                            blnSuccess = positionRequest.EditApproval(positionRequestID, employeeID, name, lblStatus.Text, "Approved", "Pending Approval", "", DateTime.Now.AddMilliseconds(100).ToString("MM/dd/yy HH:mm:ss.fff"));
                            if (blnSuccess) {
                                lblError.Text += " The position request has been set back to pending your approval.";
                            } else {
                                lblError.Text += " The position request could not be set back to pending your approval.";
                            }
                            lblError.Text += "</strong></p>" + strErrorContact + "<br />";
                            blnSuccess = false;
                        }
                    }

                } else {
                    lblError.Text = "<p><strong>Error: The next supervisor for approval could not be found.";
                    //set routing record back to current supervisor approval
                    blnSuccess = positionRequest.EditApproval(positionRequestID, employeeID, name, lblStatus.Text, "Approved", "Pending Approval", "", DateTime.Now.AddMilliseconds(100).ToString("MM/dd/yy HH:mm:ss.fff"));
                    if (blnSuccess) {
                        lblError.Text += " The position request has been set back to pending your approval.";
                    } else {
                        lblError.Text += " The position request could not be set back to pending your approval.";
                    }
                    lblError.Text += "</strong></p>" + strErrorContact + "<br />";
                    blnSuccess = false;
                }

            } else {
                lblError.Text = "<p><strong>Error: Your approval could not be recorded.</strong></p>" + strErrorContact + "<br />";
            }

        } else if (selectedOption == "optReject") { //FIGURE OUT WHY REJECT PROCESS IS SO SLOW!!!

            blnSuccess = positionRequest.EditApproval(positionRequestID, employeeID, name, lblStatus.Text, "Pending Approval", "Rejected", rejectReason, DateTime.Now.ToString("MM/dd/yy HH:mm:ss.fff"));
            
            if (blnSuccess) {
                //update the status
                blnSuccess = positionRequest.EditStatus(positionRequestID, "Rejected");

                if (blnSuccess) {
                    SmtpClient client = new SmtpClient();
                    MailMessage objEmail = new MailMessage();
                    String strEmailBody = "<p><strong>Rejected by:</strong> " + lblStatus.Text + ": " + name +
                                          "<br /><strong>Reason:</strong> " + rejectReason +
                                          "<br /><br /><strong>Tracking #:</strong> " + lblTrackingNumber.Text + 
                                          "<br /><strong>Originator:</strong> " + lblOriginatorName.Text +
                                          "<br /><strong>Created:</strong> " + hidCreated.Value + 
                                          "<br /><strong>College/Unit:</strong> " + lblCollegeUnit.Text +
                                          "<br /><strong>Department:</strong> " + lblDepartmentName.Text +
                                          "<br /><strong>Official Title:</strong> " + lblOfficialTitle.Text +
                                          "<br /><strong>Supervisor Name:</strong> " + lblSupervisorName.Text +
                                          "<br /><strong>Position Type:</strong> " + lblPositionType.Text + "</p>";

                    //notify the originator
                    String originatorEmail = positionRequest.GetEmployeeEmail(lblOriginatorID.Text);
                    String originatorName = lblOriginatorName.Text;
                    objEmail.From = new MailAddress(strAdminEmailAddress);
                    //objEmail.To.Add(originatorEmail); 
                    //objEmail.To.Add("vu.nguyen@ccs.spokane.edu");
                    /*
                    if (appHost.Contains("internal"))
                    {
                        objEmail.To.Add(originatorEmail);
                    }
                    else
                    {
                        objEmail.To.Add(emailTest);
                    }
                    */
                    objEmail.To.Add(Utility.GetSendEmail(originatorEmail));
                    objEmail.IsBodyHtml = true;
                    objEmail.Priority = MailPriority.High;
                    objEmail.Subject = "Position Request Rejected Notification";
                    objEmail.Body = "<p>Dear " + originatorName + ",</p>";
                    objEmail.Body += "<p>The following Position Request has been rejected.<p>";
                    objEmail.Body += "<p><a href=\"https://internal.spokane.edu/PositionRequest/\">https://internal.spokane.edu/PositionRequest/</a>";
                    objEmail.Body += "<br />To modify and resubmit this form, log in with your Active Directory account and click on the Position Request link in the Main Menu.</p>";
                    objEmail.Body += strEmailBody;

                    try {
                        client.Send(objEmail);
                        notified += "|Originator: " + originatorName;
                    } catch {
                        objEmail = new MailMessage();
                        objEmail.From = new MailAddress(strAdminEmailAddress);
                        objEmail.To.Add(strAdminEmailAddress);
                        objEmail.IsBodyHtml = true;
                        objEmail.Priority = MailPriority.High;
                        objEmail.Subject = "Position Request Rejection Notification Could Not Be Sent";
                        objEmail.Body = "<p>The following rejection notification email could not be sent to:<br />Originator: " + originatorName + "</p>";
                        objEmail.Body += "<hr style=\"width:100%\" />";
                        objEmail.Body += strEmailBody;
                        try {
                            client.Send(objEmail);
                            notNotified += "|Originator: " + originatorName + ". HRO has been notified.";
                        } catch {
                            notNotified += "|Originator: " + originatorName + ". HRO could not be notified. Please contact HRO at " + strAdminEmailAddress + ".";
                        }
                    }

                    //notify previous approvers
                    String previousApproverList = "", notificationList = "";
                    DataSet dsPreviousApprovers = positionRequest.GetPreviousApprovers(positionRequestID);
                    DataTable dtPreviousApprovers = dsPreviousApprovers.Tables[0];
                    objEmail = new MailMessage();
                    if (dtPreviousApprovers.Rows.Count > 0) {
                        foreach (DataRow drPreviousApprover in dtPreviousApprovers.Rows) {
                            String userType = drPreviousApprover["UserType"].ToString();
                            String approverName = drPreviousApprover["Name"].ToString();
                            if (!previousApproverList.Contains(approverName)) {
                                previousApproverList += "<br />" + userType + ": " + approverName;
                                notificationList += "|" + userType + ": " + approverName;
                                //objEmail.To.Add(dsPreviousApprovers.Tables[0].Rows[i]["EMAIL_ADDR"].ToString());
                                //objEmail.To.Add("vu.nguyen@ccs.spokane.edu");
                                /*
                                if (appHost.Contains("internal"))
                                {
                                    objEmail.To.Add(drPreviousApprover["EMAIL_ADDR"].ToString());
                                }
                                else
                                {
                                    objEmail.To.Add(emailTest);
                                }
                                */
                                objEmail.To.Add(Utility.GetSendEmail(drPreviousApprover["EMAIL_ADDR"].ToString()));
                            }
                        }
                        //objEmail.CC.Add(positionRequest.GetEmployeeEmail(employeeID)); 
                        //objEmail.CC.Add("vu.nguyen@ccs.spokane.edu");
                        /*
                        if (appHost.Contains("internal"))
                        {
                            objEmail.CC.Add(positionRequest.GetEmployeeEmail(employeeID));
                        }
                        else
                        {
                            objEmail.CC.Add(emailTest);
                        }
                        */
                        objEmail.CC.Add(Utility.GetSendEmail(positionRequest.GetEmployeeEmail(employeeID))); //CC login user's email
                        objEmail.From = new MailAddress(strAdminEmailAddress);
                        objEmail.IsBodyHtml = true;
                        objEmail.Priority = MailPriority.High;
                        objEmail.Subject = "Position Request Rejected Notification";
                        objEmail.Body = "<p>The following Position Request has been rejected.</p>";
                        objEmail.Body += strEmailBody;

                        try {
                            client.Send(objEmail);
                            notified += notificationList;
                        } catch {
                            objEmail = new MailMessage();
                            objEmail.From = new MailAddress(strAdminEmailAddress);
                            objEmail.To.Add(strAdminEmailAddress);
                            objEmail.IsBodyHtml = true;
                            objEmail.Priority = MailPriority.High;
                            objEmail.Subject = "Position Request Rejection Notification Could Not Be Sent";
                            objEmail.Body = "<p>The following rejection notification email could not be sent to:" + previousApproverList + ".</p>";
                            objEmail.Body += "<hr style=\"width:100%\" />";
                            objEmail.Body += strEmailBody;
                            try {
                                client.Send(objEmail);
                                notNotified += notificationList + "|HRO has been notified.";
                            } catch {
                                notNotified += notificationList + "|HRO could not be notified. Please contact HRO at " + strAdminEmailAddress + ".";
                            }
                        }
                    }

                    task = "rejected";

                } else {
                    lblError.Text = "<p><strong>Error: The Position Request status could not be updated to Rejected.";
                    //set routing record back to Pending Budget Approval
                    blnSuccess = positionRequest.EditApproval(positionRequestID, employeeID, name, lblStatus.Text, "Rejected", "Pending Approval", "", DateTime.Now.AddMilliseconds(100).ToString("MM/dd/yy HH:mm:ss.fff"));
                    if (blnSuccess) {
                        lblError.Text += " The position request has been set back to pending your approval.";
                    } else {
                        lblError.Text += " The position request could not be set back to pending your approval.";
                    }
                    lblError.Text += "</strong></p>" + strErrorContact + "<br />";
                    blnSuccess = false;
                }
            } else {
                lblError.Text = "<p><strong>Error: Your rejection could not be recorded.</strong></p>" + strErrorContact + "<br />";
            }
        }

        if (blnSuccess) {
            Response.Redirect("Approval.aspx?todo=confirm&id=" + positionRequestID + "&task=" + task + "&notified=" + notified + "&notNotified=" + notNotified);
        } else {
            lblError.Visible = true;
            cmdBack.Visible = false;
            cmdPrint.Visible = false;
            cmdSubmit.Visible = false;
            cmdOK.Visible = true;
        }
    }

    private void PopulateWorkScheduleForLabel(int positionRequestID)
    {
        DataSet dsWorkSchedule = positionRequest.GetWorkSchedule(positionRequestID);
        DataTable dtWorkSchedule = dsWorkSchedule.Tables[0];
        if (dtWorkSchedule.Rows.Count > 0)
        {
            DataRow drWorkSchedule = dtWorkSchedule.Rows[0];
            foreach (var s in dayWeek)
            {
                Label lbl = (Label)this.Page.Master.FindControl("maintextHolder").FindControl("lbl" + s);
                if (lbl != null)
                {
                    string startTime = Utility.ConvertToDateStr(drWorkSchedule[s + "Start"].ToString());
                    string endTime = Utility.ConvertToDateStr(drWorkSchedule[s + "End"].ToString());
                    if (startTime != "" || endTime != "")
                    {
                        lbl.Text = String.Format("{0}<br />to<br />{1}", startTime, endTime);
                    }

                }
            }
            schedule.Visible = true;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Data;
using System.Net.Mail;
using System.Web.Services;
using System.Configuration;

public partial class Form : System.Web.UI.Page {

    String strErrorContact = "<p>To report this error, please contact the IT Support Center by <a href=\"mailto:ITSupportCenter@ccs.spokane.edu\">email</a> or phone: 533-HELP.<br />Include a screen shot of this page and as much information as possible.</p>";
    String strAdminEmailAddress = ConfigurationManager.AppSettings["AdminEmailAddress"].ToString();
    PositionRequest positionRequest = new PositionRequest();

    protected void Page_Load(object sender, EventArgs e) {
        //check if the user is logged in
        HttpCookie cookie = Request.Cookies.Get("phatt2");
        if (cookie == null) {
            users user = new users(HttpContext.Current.User.Identity.Name);
        }

        if (!IsPostBack) {
            hidPopulateFromDB.Value = "true";

            //display originator name and id
            lblOriginatorName.Text = System.Web.HttpContext.Current.Server.UrlDecode(Request.Cookies["phatt2"]["userName"]);
            lblOriginatorID.Text = Request.Cookies["phatt2"]["userctclinkid"];
        }

        txtOfficialTitle.Attributes.Add("placeholder", "Required");

        //hide all the form panels
        panOutput.Visible = false;
        panInput.Visible = false;
        panForm.Visible = false;
        panList.Visible = false;
        panApprovals.Visible = false;
        panEmployeeInfo.Visible = false;
        divAttachment1.Visible = false;
        divAttachment2.Visible = false;
        cmdOK.Visible = false;
        cmdPrint.Visible = false;
        cmdSubmit.Visible = false;
        cmdSave.Visible = false;
        cmdBack.Visible = false;

        //show panel based on function passed through querystring
        String todo = Request.Form["ctl00$maintextHolder$hidTodo"];
        if (todo == null || todo == "") {
            todo = Request.QueryString["todo"];
        }

        if (todo == "view") { //could reduce code if populate output panel is in a function used for confirm and view
            //populate output controls and show confirmation message
            Int32 positionRequestID = Convert.ToInt32(hidID.Value);

            DataSet dsPositionRequest = positionRequest.GetPositionRequest(positionRequestID);
            if (dsPositionRequest.Tables[0].Rows.Count > 0) {
                Decimal salaryAmount = 0;
                try {
                    salaryAmount = Convert.ToDecimal(dsPositionRequest.Tables[0].Rows[0]["SalaryAmount"]);
                } catch {
                    //do nothing
                }
                lblTrackingNumber.Text = dsPositionRequest.Tables[0].Rows[0]["TrackingNumber"].ToString();
                lblStatus2.Text = dsPositionRequest.Tables[0].Rows[0]["Status"].ToString();
                lblOfficialTitle.Text = dsPositionRequest.Tables[0].Rows[0]["OfficialTitle"].ToString();
                lblWorkingTitle.Text = dsPositionRequest.Tables[0].Rows[0]["WorkingTitle"].ToString();
                lblJobCode.Text = dsPositionRequest.Tables[0].Rows[0]["JobCode"].ToString();
                lblBudgetNumber.Text = dsPositionRequest.Tables[0].Rows[0]["BudgetNumber"].ToString();
                lblPositionNumber.Text = dsPositionRequest.Tables[0].Rows[0]["PositionNumber"].ToString();
                lblCollegeUnit.Text = dsPositionRequest.Tables[0].Rows[0]["CollegeUnit"].ToString();
                lblDepartmentName.Text = dsPositionRequest.Tables[0].Rows[0]["Department"].ToString();
                lblDepartmentID2.Text = dsPositionRequest.Tables[0].Rows[0]["DepartmentID"].ToString();
                lblPhoneNumber.Text = dsPositionRequest.Tables[0].Rows[0]["PhoneNumber"].ToString();
                lblMailStop.Text = dsPositionRequest.Tables[0].Rows[0]["MailStop"].ToString();
                lblSupervisorName.Text = dsPositionRequest.Tables[0].Rows[0]["SupervisorName"].ToString();
                lblSupervisorID2.Text = dsPositionRequest.Tables[0].Rows[0]["SupervisorID"].ToString();
                if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["NewPosition"]) == 1) {
                    lblNewReplacement.Text = "New Position";
                    employeeReplaced2.Visible = false;
                } else if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["ReplacementPosition"]) == 1) {
                    lblNewReplacement.Text = "Replacement Position";
                    lblEmployeeReplaced.Text = dsPositionRequest.Tables[0].Rows[0]["ReplacedEmployeeName"].ToString();
                    employeeReplaced2.Visible = true;
                }
                lblPositionType.Text = dsPositionRequest.Tables[0].Rows[0]["PositionType"].ToString();
                if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["PermanentPosition"]) == 1) {
                    lblClassified.Text = "Permanent";
                    lblClassified.Visible = true;
                }
                if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["NonPermanentPosition"]) == 1) {
                    lblClassified.Text = "Non Permanent";
                    lblClassified.Visible = true;
                    try {
                        lblEndDate.Text = Convert.ToDateTime(dsPositionRequest.Tables[0].Rows[0]["PositionEndDate"]).ToShortDateString();
                        lblEndDate.Visible = true;
                    } catch {
                        //do nothing - no date exists
                    }
                } else {
                    endDate2.Visible = false;
                }
                lblRecruitmentType.Text = dsPositionRequest.Tables[0].Rows[0]["RecruitmentType"].ToString();
                if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["PCard"]) == 1) {
                    lblOnboarding.Text += "<div style=\"padding-bottom:5px;\">P-Card</div>";
                }
                if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["CCSIssuedCellPhone"]) == 1) {
                    lblOnboarding.Text += "<div style=\"padding-bottom:5px;\">CCS-issued Cell Phone</div>";
                }
                if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["CTCLinkAccess"]) == 1) {
                    lblOnboarding.Text += "<div style=\"padding-bottom:5px;\">Additional ctcLink Security Access</div>";
                }
                if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["TimeSheetApprover"]) == 1) {
                    lblOnboarding.Text += "<div style=\"padding-bottom:5px;\">Manager/Timesheet Approver</div>";
                }
                if (lblPositionType.Text == "Classified" || lblPositionType.Text == "Part-Time Hourly") {
                    lblHoursPerDay.Text = dsPositionRequest.Tables[0].Rows[0]["HoursPerDay"].ToString();
                    lblHoursPerWeek.Text = dsPositionRequest.Tables[0].Rows[0]["HoursPerWeek"].ToString();
                    lblMonthsPerYear.Text = dsPositionRequest.Tables[0].Rows[0]["MonthsPerYear"].ToString();
                    lblCyclicCalendarCode.Text = dsPositionRequest.Tables[0].Rows[0]["CyclicCalendarCode"].ToString();
                    if (lblCyclicCalendarCode.Text != "") {
                        cyclicCalendarCode2.Attributes["style"] = cyclicCalendarCode2.Attributes["style"].Replace("display: none;", "");
                    }

                    DataSet dsWorkSchedule = positionRequest.GetWorkSchedule(positionRequestID);
                    if (dsWorkSchedule.Tables[0].Rows.Count > 0) {
                        String mondayStart = dsWorkSchedule.Tables[0].Rows[0]["MondayStart"].ToString();
                        String mondayEnd = dsWorkSchedule.Tables[0].Rows[0]["MondayEnd"].ToString();
                        String tuesdayStart = dsWorkSchedule.Tables[0].Rows[0]["TuesdayStart"].ToString();
                        String tuesdayEnd = dsWorkSchedule.Tables[0].Rows[0]["TuesdayEnd"].ToString();
                        String wednesdayStart = dsWorkSchedule.Tables[0].Rows[0]["WednesdayStart"].ToString();
                        String wednesdayEnd = dsWorkSchedule.Tables[0].Rows[0]["WednesdayEnd"].ToString();
                        String thursdayStart = dsWorkSchedule.Tables[0].Rows[0]["ThursdayStart"].ToString();
                        String thursdayEnd = dsWorkSchedule.Tables[0].Rows[0]["ThursdayEnd"].ToString();
                        String fridayStart = dsWorkSchedule.Tables[0].Rows[0]["FridayStart"].ToString();
                        String fridayEnd = dsWorkSchedule.Tables[0].Rows[0]["FridayEnd"].ToString();
                        String saturdayStart = dsWorkSchedule.Tables[0].Rows[0]["SaturdayStart"].ToString();
                        String saturdayEnd = dsWorkSchedule.Tables[0].Rows[0]["SaturdayEnd"].ToString();
                        String sundayStart = dsWorkSchedule.Tables[0].Rows[0]["SundayStart"].ToString();
                        String sundayEnd = dsWorkSchedule.Tables[0].Rows[0]["SundayEnd"].ToString();
                        if (mondayStart != "" || mondayEnd != "") {
                            if (mondayStart != "") {
                                mondayStart = Convert.ToDateTime(mondayStart).ToString("%h:mm tt");
                            }
                            if (mondayEnd != "") {
                                mondayEnd = Convert.ToDateTime(mondayEnd).ToString("%h:mm tt");
                            }
                            lblMonday.Text = mondayStart + "<br />to<br />" + mondayEnd;
                        }
                        if (tuesdayStart != "" || tuesdayEnd != "") {
                            if (tuesdayStart != "") {
                                tuesdayStart = Convert.ToDateTime(tuesdayStart).ToString("%h:mm tt");
                            }
                            if (tuesdayEnd != "") {
                                tuesdayEnd = Convert.ToDateTime(tuesdayEnd).ToString("%h:mm tt");
                            }
                            lblTuesday.Text = tuesdayStart + "<br />to<br />" + tuesdayEnd;
                        }
                        if (wednesdayStart != "" || wednesdayEnd != "") {
                            if (wednesdayStart != "") {
                                wednesdayStart = Convert.ToDateTime(wednesdayStart).ToString("%h:mm tt");
                            }
                            if (wednesdayEnd != "") {
                                wednesdayEnd = Convert.ToDateTime(wednesdayEnd).ToString("%h:mm tt");
                            }
                            lblWednesday.Text = wednesdayStart + "<br />to<br />" + wednesdayEnd;
                        }
                        if (thursdayStart != "" || thursdayEnd != "") {
                            if (thursdayStart != "") {
                                thursdayStart = Convert.ToDateTime(thursdayStart).ToString("%h:mm tt");
                            }
                            if (thursdayEnd != "") {
                                thursdayEnd = Convert.ToDateTime(thursdayEnd).ToString("%h:mm tt");
                            }
                            lblThursday.Text = thursdayStart + "<br />to<br />" + thursdayEnd;
                        }
                        if (fridayStart != "" || fridayEnd != "") {
                            if (fridayStart != "") {
                                fridayStart = Convert.ToDateTime(fridayStart).ToString("%h:mm tt");
                            }
                            if (fridayEnd != "") {
                                fridayEnd = Convert.ToDateTime(fridayEnd).ToString("%h:mm tt");
                            }
                            lblFriday.Text = fridayStart + "<br />to<br />" + fridayEnd;
                        }
                        if (saturdayStart != "" || saturdayEnd != "") {
                            if (saturdayStart != "") {
                                saturdayStart = Convert.ToDateTime(saturdayStart).ToString("%h:mm tt");
                            }
                            if (saturdayEnd != "") {
                                saturdayEnd = Convert.ToDateTime(saturdayEnd).ToString("%h:mm tt");
                            }
                            lblSaturday.Text = saturdayStart + "<br />to<br />" + saturdayEnd;
                        }
                        if (sundayStart != "" || sundayEnd != "") {
                            if (sundayStart != "") {
                                sundayStart = Convert.ToDateTime(sundayStart).ToString("%h:mm tt");
                            }
                            if (sundayEnd != "") {
                                sundayEnd = Convert.ToDateTime(sundayEnd).ToString("%h:mm tt");
                            }
                            lblSunday.Text = sundayStart + "<br />to<br />" + sundayEnd;
                        }
                        schedule2.Visible = true;
                    }
                } else {
                    schedule2.Visible = false;
                }

                lblEmployeeName.Text = dsPositionRequest.Tables[0].Rows[0]["EmployeeName"].ToString();
                lblEmployeeID.Text = dsPositionRequest.Tables[0].Rows[0]["EmployeeID"].ToString();
                lblSalaryRangeStep.Text = dsPositionRequest.Tables[0].Rows[0]["SalaryRangeStep"].ToString();
                lblSalaryAmount.Text = string.Format("{0:C}", salaryAmount);
                lblEmployeeType.Text = dsPositionRequest.Tables[0].Rows[0]["EmployeeType"].ToString();
                lblEmployeeEmailAddress.Text = dsPositionRequest.Tables[0].Rows[0]["EmployeeEmailAddress"].ToString();

                if ((lblStatus2.Text == "HRO" || lblStatus2.Text == "Recruiting" || lblStatus2.Text == "Hired" || lblStatus2.Text == "Complete") &&
                    (lblEmployeeName.Text != "" && lblEmployeeID.Text != "" && lblEmployeeEmailAddress.Text != "" && lblSalaryRangeStep.Text != "" && lblSalaryAmount.Text != "$0.00" && lblEmployeeType.Text != "")) {
                    panEmployeeInfo.Visible = true;
                }
            }

            //show uploaded file attachments
            DataSet dsAttachments = positionRequest.GetAttachments(positionRequestID);
            Int32 attachmentCount = dsAttachments.Tables[0].Rows.Count;
            if (attachmentCount > 0) {
                for (Int32 i = 0; i < attachmentCount; i++) {
                    lblAttachments.Text += "<div style=\"padding-bottom:5px;\"><a href=\"Attachments/" + dsAttachments.Tables[0].Rows[i]["FileName"].ToString() + "\" target=\"self\" class=\"smallText\" title=\"View File Attachment " + (i + 1) + "\">Attachment " + (i + 1) + "</a></div>";
                }
                panFileAttachments.Visible = true;
            } else {
                panFileAttachments.Visible = false;
            }

            //get Routing/Approvals
            DataSet dsRouting = positionRequest.GetRouting(positionRequestID);
            Int32 routingCount = dsRouting.Tables[0].Rows.Count;
            if (routingCount > 0) {
                for (Int32 i = 0; i < routingCount; i++) {
                    String note = dsRouting.Tables[0].Rows[i]["Note"].ToString();
                    String description = dsRouting.Tables[0].Rows[i]["Description"].ToString();
                    TableRow tr = new TableRow();
                    TableCell td = new TableCell();
                    td.CssClass = "solid";
                    td.Attributes["style"] = "vertical-align:top;padding-left:10px;width:75px";
                    td.Controls.Add(new LiteralControl(dsRouting.Tables[0].Rows[i]["UserType"].ToString()));
                    tr.Cells.Add(td);
                    td = new TableCell();
                    td.CssClass = "solid";
                    td.Attributes["style"] = "vertical-align:top;width:190px";
                    td.Controls.Add(new LiteralControl(dsRouting.Tables[0].Rows[i]["Name"].ToString()));
                    tr.Cells.Add(td);
                    td = new TableCell();
                    td.CssClass = "solid";
                    td.Attributes["style"] = "vertical-align:top";
                    td.Controls.Add(new LiteralControl(description));
                    if (description != "Pending Approval") {
                        td.Controls.Add(new LiteralControl("&nbsp;" + Convert.ToDateTime(dsRouting.Tables[0].Rows[i]["Date"]).ToString("MM/dd/yy %h:mm tt")));
                    }
                    if (note != "") {
                        td.Controls.Add(new LiteralControl("<br />" + note));
                    }
                    tr.Cells.Add(td);
                    tblApprovals2.Rows.Add(tr);
                }
                panApprovals2.Visible = true;
            } else {
                panApprovals2.Visible = false;
            }

            panOutput.Visible = true;
            panForm.Visible = true;
            cmdBack.Visible = true;
            cmdPrint.Visible = true;

        } else if (todo == "add") {

            panTrackingNumber.Visible = false;
            panInput.Visible = true;
            panForm.Visible = true;
            cmdBack.Visible = true;
            cmdSave.Visible = true;
            cmdSubmit.Visible = true;

        } else if (todo == "edit") {

            Int32 positionRequestID = Convert.ToInt32(hidID.Value);

            if (hidPopulateFromDB.Value == "true") {
                //show edit panel and populate position request form

                DataSet dsPositionRequest = positionRequest.GetPositionRequest(positionRequestID);
                if (dsPositionRequest.Tables[0].Rows.Count > 0) {
                    lblOriginatorID.Text = dsPositionRequest.Tables[0].Rows[0]["OriginatorID"].ToString();
                    lblOriginatorName.Text = dsPositionRequest.Tables[0].Rows[0]["OriginatorName"].ToString();
                    txtOfficialTitle.Text = dsPositionRequest.Tables[0].Rows[0]["OfficialTitle"].ToString();
                    txtWorkingTitle.Text = dsPositionRequest.Tables[0].Rows[0]["WorkingTitle"].ToString();
                    txtJobCode.Text = dsPositionRequest.Tables[0].Rows[0]["JobCode"].ToString();
                    txtBudgetNumber.Text = dsPositionRequest.Tables[0].Rows[0]["BudgetNumber"].ToString();
                    txtPositionNumber.Text = dsPositionRequest.Tables[0].Rows[0]["PositionNumber"].ToString();
                    cboCollegeUnit.SelectedValue = dsPositionRequest.Tables[0].Rows[0]["CollegeUnit"].ToString();
                    hidDepartment.Value = dsPositionRequest.Tables[0].Rows[0]["Department"].ToString();
                    hidDepartmentID.Value = dsPositionRequest.Tables[0].Rows[0]["DepartmentID"].ToString();
                    txtPhoneNumber.Text = dsPositionRequest.Tables[0].Rows[0]["PhoneNumber"].ToString();
                    txtMailStop.Text = dsPositionRequest.Tables[0].Rows[0]["MailStop"].ToString();
                    txtSupervisorName.Text = dsPositionRequest.Tables[0].Rows[0]["SupervisorName"].ToString();
                    hidSupervisorID.Value = dsPositionRequest.Tables[0].Rows[0]["SupervisorID"].ToString();
                    lblStatus.Text = dsPositionRequest.Tables[0].Rows[0]["Status"].ToString();
                    lblTrackingNumber.Text = dsPositionRequest.Tables[0].Rows[0]["TrackingNumber"].ToString();
                    if (lblTrackingNumber.Text == "") {
                        panTrackingNumber.Visible = false;
                    } else {
                        panTrackingNumber.Visible = true;
                    }
                    if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["NewPosition"]) == 1) {
                        optNew.Checked = true;
                    } else if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["ReplacementPosition"]) == 1) {
                        optReplacement.Checked = true;
                        txtEmployeeReplaced.Text = dsPositionRequest.Tables[0].Rows[0]["ReplacedEmployeeName"].ToString();
                    }
                    cboPositionType.SelectedValue = dsPositionRequest.Tables[0].Rows[0]["PositionType"].ToString();
                    if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["PermanentPosition"]) == 1) {
                        optPermanent.Checked = true;
                    } else if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["NonPermanentPosition"]) == 1) {
                        optNonPermanent.Checked = true;
                        try {
                            txtEndDate.Value = Convert.ToDateTime(dsPositionRequest.Tables[0].Rows[0]["PositionEndDate"]).ToShortDateString();
                        } catch {
                            //do nothing - no date exists
                        }
                    }
                    cboRecruitmentType.SelectedValue = dsPositionRequest.Tables[0].Rows[0]["RecruitmentType"].ToString();
                    if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["PCard"]) == 1) {
                        chkPCard.Checked = true;
                    }
                    if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["CCSIssuedCellPhone"]) == 1) {
                        chkCellPhone.Checked = true;
                    }
                    if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["CTCLinkAccess"]) == 1) {
                        chkAdditionalAccess.Checked = true;
                    }
                    if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["TimeSheetApprover"]) == 1) {
                        chkTimeSheetApprover.Checked = true;
                    }
                    txtHoursPerDay.Text = dsPositionRequest.Tables[0].Rows[0]["HoursPerDay"].ToString();
                    txtHoursPerWeek.Text = dsPositionRequest.Tables[0].Rows[0]["HoursPerWeek"].ToString();
                    txtMonthsPerYear.Text = dsPositionRequest.Tables[0].Rows[0]["MonthsPerYear"].ToString();
                    txtCyclicCalendarCode.Text = dsPositionRequest.Tables[0].Rows[0]["CyclicCalendarCode"].ToString();
                }

                DataSet dsWorkSchedule = positionRequest.GetWorkSchedule(positionRequestID);
                if (dsWorkSchedule.Tables[0].Rows.Count > 0) {
                    String mondayStart = dsWorkSchedule.Tables[0].Rows[0]["MondayStart"].ToString();
                    String mondayEnd = dsWorkSchedule.Tables[0].Rows[0]["MondayEnd"].ToString();
                    String tuesdayStart = dsWorkSchedule.Tables[0].Rows[0]["TuesdayStart"].ToString();
                    String tuesdayEnd = dsWorkSchedule.Tables[0].Rows[0]["TuesdayEnd"].ToString();
                    String wednesdayStart = dsWorkSchedule.Tables[0].Rows[0]["WednesdayStart"].ToString();
                    String wednesdayEnd = dsWorkSchedule.Tables[0].Rows[0]["WednesdayEnd"].ToString();
                    String thursdayStart = dsWorkSchedule.Tables[0].Rows[0]["ThursdayStart"].ToString();
                    String thursdayEnd = dsWorkSchedule.Tables[0].Rows[0]["ThursdayEnd"].ToString();
                    String fridayStart = dsWorkSchedule.Tables[0].Rows[0]["FridayStart"].ToString();
                    String fridayEnd = dsWorkSchedule.Tables[0].Rows[0]["FridayEnd"].ToString();
                    String saturdayStart = dsWorkSchedule.Tables[0].Rows[0]["SaturdayStart"].ToString();
                    String saturdayEnd = dsWorkSchedule.Tables[0].Rows[0]["SaturdayEnd"].ToString();
                    String sundayStart = dsWorkSchedule.Tables[0].Rows[0]["SundayStart"].ToString();
                    String sundayEnd = dsWorkSchedule.Tables[0].Rows[0]["SundayEnd"].ToString();
                    if (mondayStart != "") {
                        txtMondayStart.Value = Convert.ToDateTime(mondayStart).ToString("%h:mm tt");
                    }
                    if (mondayEnd != "") {
                        txtMondayEnd.Value = Convert.ToDateTime(mondayEnd).ToString("%h:mm tt");
                    }
                    if (tuesdayStart != "") {
                        txtTuesdayStart.Value = Convert.ToDateTime(tuesdayStart).ToString("%h:mm tt");
                    }
                    if (tuesdayEnd != "") {
                        txtTuesdayEnd.Value = Convert.ToDateTime(tuesdayEnd).ToString("%h:mm tt");
                    }
                    if (wednesdayStart != "") {
                        txtWednesdayStart.Value = Convert.ToDateTime(wednesdayStart).ToString("%h:mm tt");
                    }
                    if (wednesdayEnd != "") {
                        txtWednesdayEnd.Value = Convert.ToDateTime(wednesdayEnd).ToString("%h:mm tt");
                    }
                    if (thursdayStart != "") {
                        txtThursdayStart.Value = Convert.ToDateTime(thursdayStart).ToString("%h:mm tt");
                    }
                    if (thursdayEnd != "") {
                        txtThursdayEnd.Value = Convert.ToDateTime(thursdayEnd).ToString("%h:mm tt");
                    }
                    if (fridayStart != "") {
                        txtFridayStart.Value = Convert.ToDateTime(fridayStart).ToString("%h:mm tt");
                    }
                    if (fridayEnd != "") {
                        txtFridayEnd.Value = Convert.ToDateTime(fridayEnd).ToString("%h:mm tt");
                    }
                    if (saturdayStart != "") {
                        txtSaturdayStart.Value = Convert.ToDateTime(saturdayStart).ToString("%h:mm tt");
                    }
                    if (saturdayEnd != "") {
                        txtSaturdayEnd.Value = Convert.ToDateTime(saturdayEnd).ToString("%h:mm tt");
                    }
                    if (sundayStart != "") {
                        txtSundayStart.Value = Convert.ToDateTime(sundayStart).ToString("%h:mm tt");
                    }
                    if (sundayEnd != "") {
                        txtSundayEnd.Value = Convert.ToDateTime(sundayEnd).ToString("%h:mm tt");
                    }
                }

                hidPopulateFromDB.Value = "false";
            }

            //get uploaded file attachments
            hidFileAttachments.Value = "";
            DataSet dsAttachments = positionRequest.GetAttachments(positionRequestID);
            Int32 attachmentCount = dsAttachments.Tables[0].Rows.Count;
            if (attachmentCount > 0) {
                for (Int32 i = 0; i < attachmentCount; i++) {
                    hidFileAttachments.Value += "|" + dsAttachments.Tables[0].Rows[i]["FileName"].ToString();
                }
            }
            populateFileAttachments();

            //get Routing/Approvals
            DataSet dsRouting = positionRequest.GetRouting(positionRequestID);
            Int32 routingCount = dsRouting.Tables[0].Rows.Count;
            if (routingCount > 0) {
                for (Int32 i = 0; i < routingCount; i++) {
                    String note = dsRouting.Tables[0].Rows[i]["Note"].ToString();
                    String description = dsRouting.Tables[0].Rows[i]["Description"].ToString();
                    TableRow tr = new TableRow();
                    TableCell td = new TableCell();
                    td.CssClass = "solid";
                    td.Attributes["style"] = "vertical-align:top;padding-left:10px;width:75px";
                    td.Controls.Add(new LiteralControl(dsRouting.Tables[0].Rows[i]["UserType"].ToString()));
                    tr.Cells.Add(td);
                    td = new TableCell();
                    td.CssClass = "solid";
                    td.Attributes["style"] = "vertical-align:top;width:190px";
                    td.Controls.Add(new LiteralControl(dsRouting.Tables[0].Rows[i]["Name"].ToString()));
                    tr.Cells.Add(td);
                    td = new TableCell();
                    td.CssClass = "solid";
                    td.Attributes["style"] = "vertical-align:top";
                    td.Controls.Add(new LiteralControl(description));
                    if (description != "Pending Approval") {
                        td.Controls.Add(new LiteralControl("&nbsp;" + Convert.ToDateTime(dsRouting.Tables[0].Rows[i]["Date"]).ToString("MM/dd/yy %h:mm tt")));
                    }
                    if (note != "") {
                        td.Controls.Add(new LiteralControl("<br />" + note));
                    }
                    tr.Cells.Add(td);
                    tblApprovals.Rows.Add(tr);
                }
                panApprovals.Visible = true;
            } else {
                panApprovals.Visible = false;
            }

            panInput.Visible = true;
            panForm.Visible = true;
            cmdBack.Visible = true;
            cmdSave.Visible = true;
            cmdSubmit.Visible = true;

        } else if (todo == "delete") {

            hidTodo.Value = "";
            Int32 positionRequestID = Convert.ToInt32(hidID.Value);
            //get file attachments
            DataSet dsAttachments = positionRequest.GetAttachments(positionRequestID);
            for (Int32 i = 0; i < dsAttachments.Tables[0].Rows.Count; i++) {
                //delete file attachments from server
                System.IO.File.Delete(Server.MapPath("Attachments\\") + dsAttachments.Tables[0].Rows[i]["FileName"].ToString());
            }
            //delete Position Request Form from DB
            positionRequest.DeletePositionRequest(positionRequestID);
            //display position request list
            SearchPositionRequests(cboSearchStatus.SelectedValue, txtSearchOfficialTitle.Text, cboSearchPositionType.SelectedValue);
            panList.Visible = true;

        } else if (todo == "cancel") {

            hidTodo.Value = ""; //not sure if this is needed
            Int32 positionRequestID = Convert.ToInt32(hidID.Value);
            String trackingNumber = "", originatorName = "", created = "", collegeUnit = "", department = "", officialTitle = "", supervisorName = "", positionType = "", status = "";

            //cancel the position request form
            positionRequest.EditStatus(positionRequestID, "Cancelled");

            //add a cancelled routing record
            positionRequest.AddRouting(positionRequestID, "Originator", "Cancelled", "", System.Web.HttpContext.Current.Server.UrlDecode(Request.Cookies["phatt2"]["userName"]), Request.Cookies["phatt2"]["userctclinkid"], DateTime.Now.ToString("MM/dd/yy HH:mm:ss.fff"));

            //get position request form data
            DataSet dsPositionRequest = positionRequest.GetPositionRequest(positionRequestID);
            if(dsPositionRequest.Tables[0].Rows.Count > 0) {
                trackingNumber = dsPositionRequest.Tables[0].Rows[0]["TrackingNumber"].ToString();
                originatorName = dsPositionRequest.Tables[0].Rows[0]["OriginatorName"].ToString();
                created = Convert.ToDateTime(dsPositionRequest.Tables[0].Rows[0]["Created"]).ToString("MM/dd/yy %h:mm tt");
                collegeUnit = dsPositionRequest.Tables[0].Rows[0]["CollegeUnit"].ToString();
                department = dsPositionRequest.Tables[0].Rows[0]["Department"].ToString();
                officialTitle = dsPositionRequest.Tables[0].Rows[0]["OfficialTitle"].ToString();
                supervisorName = dsPositionRequest.Tables[0].Rows[0]["SupervisorName"].ToString();
                positionType = dsPositionRequest.Tables[0].Rows[0]["PositionType"].ToString();
                status = dsPositionRequest.Tables[0].Rows[0]["Status"].ToString();
            }

            //get pending approval record
            DataSet dsPendingApproval = positionRequest.GetPendingApproval(positionRequestID);
            if(dsPendingApproval.Tables[0].Rows.Count > 0) {
                //send cancel notification to pending approver
                String approverName = dsPendingApproval.Tables[0].Rows[0]["Name"].ToString();
                String approverEmail = positionRequest.GetEmployeeEmail(dsPendingApproval.Tables[0].Rows[0]["EmployeeID"].ToString());

                SmtpClient client = new SmtpClient();
                MailMessage objEmail = new MailMessage();
                if (approverEmail != "") {
                    objEmail.To.Add(approverEmail); 
                    objEmail.To.Add("vu.nguyen@ccs.spokane.edu");
                    //objEmail.From = new MailAddress(strAdminEmailAddress);
                    objEmail.IsBodyHtml = true;
                    objEmail.Priority = MailPriority.High;
                    objEmail.Subject = "Position Request was Cancelled by the Originator";
                    objEmail.Body = "<p>The following Position Request was cancelled by the originator and no longer needs your approval.</p>" +
                                    "<p><strong>Tracking #:</strong> " + trackingNumber +
                                    "<br /><strong>Originator:</strong> " + originatorName +
                                    "<br /><strong>Created:</strong> " + created +
                                    "<br /><strong>College/Unit:</strong> " + collegeUnit +
                                    "<br /><strong>Department:</strong> " + department +
                                    "<br /><strong>Official Title:</strong> " + officialTitle +
                                    "<br /><strong>Supervisor Name:</strong> " + supervisorName +
                                    "<br /><strong>Position Type:</strong> " + positionType +
                                    "<br /><strong>Status:</strong> " + status + "</p>";
                    try {
                        client.Send(objEmail);
                    } catch {
                        //do nothing
                    }
                }
            }

            SearchPositionRequests(cboSearchStatus.SelectedValue, txtSearchOfficialTitle.Text, cboSearchPositionType.SelectedValue);
            panList.Visible = true;

        } else if (todo == "confirm") {

            //populate output controls and show confirmation message
            Int32 positionRequestID = Convert.ToInt32(Request.QueryString["id"]);
            String message = Request.QueryString["msg"];
            if(message != null && message != "") {
                message = message.Replace("|", "<br />");
            }

            DataSet dsPositionRequest = positionRequest.GetPositionRequest(positionRequestID);
            if (dsPositionRequest.Tables[0].Rows.Count > 0) {
                lblConfirm.Text = "<p style=\"padding-bottom:10px;\"><strong>The following form has been " + Request.QueryString["task"] + ".</strong><br />" + message + "</p>";
                lblOriginatorID.Text = dsPositionRequest.Tables[0].Rows[0]["OriginatorID"].ToString();
                lblOriginatorName.Text = dsPositionRequest.Tables[0].Rows[0]["OriginatorName"].ToString();
                lblStatus2.Text = dsPositionRequest.Tables[0].Rows[0]["Status"].ToString();
                lblOfficialTitle.Text = dsPositionRequest.Tables[0].Rows[0]["OfficialTitle"].ToString();
                lblWorkingTitle.Text = dsPositionRequest.Tables[0].Rows[0]["WorkingTitle"].ToString();
                lblJobCode.Text = dsPositionRequest.Tables[0].Rows[0]["JobCode"].ToString();
                lblBudgetNumber.Text = dsPositionRequest.Tables[0].Rows[0]["BudgetNumber"].ToString();
                lblPositionNumber.Text = dsPositionRequest.Tables[0].Rows[0]["PositionNumber"].ToString();
                lblCollegeUnit.Text = dsPositionRequest.Tables[0].Rows[0]["CollegeUnit"].ToString(); 
                lblDepartmentName.Text = dsPositionRequest.Tables[0].Rows[0]["Department"].ToString();
                lblDepartmentID2.Text = dsPositionRequest.Tables[0].Rows[0]["DepartmentID"].ToString();
                lblPhoneNumber.Text = dsPositionRequest.Tables[0].Rows[0]["PhoneNumber"].ToString();
                lblMailStop.Text = dsPositionRequest.Tables[0].Rows[0]["MailStop"].ToString();
                lblSupervisorName.Text = dsPositionRequest.Tables[0].Rows[0]["SupervisorName"].ToString();
                lblSupervisorID2.Text = dsPositionRequest.Tables[0].Rows[0]["SupervisorID"].ToString();
                lblTrackingNumber.Text = dsPositionRequest.Tables[0].Rows[0]["TrackingNumber"].ToString();
                if(lblTrackingNumber.Text == "") {
                    panTrackingNumber.Visible = false;
                } else {
                    panTrackingNumber.Visible = true;
                }
                if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["NewPosition"]) == 1) {
                    lblNewReplacement.Text = "New Position";
                    employeeReplaced2.Visible = false;
                } else if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["ReplacementPosition"]) == 1) {
                    lblNewReplacement.Text = "Replacement Position";
                    lblEmployeeReplaced.Text = dsPositionRequest.Tables[0].Rows[0]["ReplacedEmployeeName"].ToString();
                    employeeReplaced2.Visible = true;
                }
                lblPositionType.Text = dsPositionRequest.Tables[0].Rows[0]["PositionType"].ToString();
                if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["PermanentPosition"]) == 1) {
                    lblClassified.Text = "Permanent";
                    lblClassified.Visible = true;
                }
                if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["NonPermanentPosition"]) == 1) {
                    lblClassified.Text = "Non Permanent";
                    lblClassified.Visible = true;
                    try {
                        lblEndDate.Text = Convert.ToDateTime(dsPositionRequest.Tables[0].Rows[0]["PositionEndDate"]).ToShortDateString();
                        lblEndDate.Visible = true;
                    } catch {
                        //do nothing - no date exists
                    }
                } else {
                    endDate2.Visible = false;
                }
                lblRecruitmentType.Text = dsPositionRequest.Tables[0].Rows[0]["RecruitmentType"].ToString();
                if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["PCard"]) == 1) {
                    lblOnboarding.Text += "<div style=\"padding-bottom:5px;\">P-Card</div>";
                }
                if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["CCSIssuedCellPhone"]) == 1) {
                    lblOnboarding.Text += "<div style=\"padding-bottom:5px;\">CCS-issued Cell Phone</div>";
                }
                if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["CTCLinkAccess"]) == 1) {
                    lblOnboarding.Text += "<div style=\"padding-bottom:5px;\">Additional ctcLink Security Access</div>";
                }
                if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["TimeSheetApprover"]) == 1) {
                    lblOnboarding.Text += "<div style=\"padding-bottom:5px;\">Manager/Timesheet Approver</div>";
                }
                if (lblPositionType.Text == "Classified" || lblPositionType.Text == "Part-Time Hourly") {
                    lblHoursPerDay.Text = dsPositionRequest.Tables[0].Rows[0]["HoursPerDay"].ToString();
                    lblHoursPerWeek.Text = dsPositionRequest.Tables[0].Rows[0]["HoursPerWeek"].ToString();
                    lblMonthsPerYear.Text = dsPositionRequest.Tables[0].Rows[0]["MonthsPerYear"].ToString();
                    lblCyclicCalendarCode.Text = dsPositionRequest.Tables[0].Rows[0]["CyclicCalendarCode"].ToString();
                    if (lblCyclicCalendarCode.Text != "") {
                        cyclicCalendarCode2.Attributes["style"] = cyclicCalendarCode2.Attributes["style"].Replace("display: none;", "");
                    }

                    DataSet dsWorkSchedule = positionRequest.GetWorkSchedule(positionRequestID);
                    if (dsWorkSchedule.Tables[0].Rows.Count > 0) {
                        String mondayStart = dsWorkSchedule.Tables[0].Rows[0]["MondayStart"].ToString();
                        String mondayEnd = dsWorkSchedule.Tables[0].Rows[0]["MondayEnd"].ToString();
                        String tuesdayStart = dsWorkSchedule.Tables[0].Rows[0]["TuesdayStart"].ToString();
                        String tuesdayEnd = dsWorkSchedule.Tables[0].Rows[0]["TuesdayEnd"].ToString();
                        String wednesdayStart = dsWorkSchedule.Tables[0].Rows[0]["WednesdayStart"].ToString();
                        String wednesdayEnd = dsWorkSchedule.Tables[0].Rows[0]["WednesdayEnd"].ToString();
                        String thursdayStart = dsWorkSchedule.Tables[0].Rows[0]["ThursdayStart"].ToString();
                        String thursdayEnd = dsWorkSchedule.Tables[0].Rows[0]["ThursdayEnd"].ToString();
                        String fridayStart = dsWorkSchedule.Tables[0].Rows[0]["FridayStart"].ToString();
                        String fridayEnd = dsWorkSchedule.Tables[0].Rows[0]["FridayEnd"].ToString();
                        String saturdayStart = dsWorkSchedule.Tables[0].Rows[0]["SaturdayStart"].ToString();
                        String saturdayEnd = dsWorkSchedule.Tables[0].Rows[0]["SaturdayEnd"].ToString();
                        String sundayStart = dsWorkSchedule.Tables[0].Rows[0]["SundayStart"].ToString();
                        String sundayEnd = dsWorkSchedule.Tables[0].Rows[0]["SundayEnd"].ToString();
                        if (mondayStart != "" || mondayEnd != "") {
                            if (mondayStart != "") {
                                mondayStart = Convert.ToDateTime(mondayStart).ToString("%h:mm tt");
                            }
                            if (mondayEnd != "") {
                                mondayEnd = Convert.ToDateTime(mondayEnd).ToString("%h:mm tt");
                            }
                            lblMonday.Text = mondayStart + "<br />to<br />" + mondayEnd;
                        }
                        if (tuesdayStart != "" || tuesdayEnd != "") {
                            if (tuesdayStart != "") {
                                tuesdayStart = Convert.ToDateTime(tuesdayStart).ToString("%h:mm tt");
                            }
                            if (tuesdayEnd != "") {
                                tuesdayEnd = Convert.ToDateTime(tuesdayEnd).ToString("%h:mm tt");
                            }
                            lblTuesday.Text = tuesdayStart + "<br />to<br />" + tuesdayEnd;
                        }
                        if (wednesdayStart != "" || wednesdayEnd != "") {
                            if (wednesdayStart != "") {
                                wednesdayStart = Convert.ToDateTime(wednesdayStart).ToString("%h:mm tt");
                            }
                            if (wednesdayEnd != "") {
                                wednesdayEnd = Convert.ToDateTime(wednesdayEnd).ToString("%h:mm tt");
                            }
                            lblWednesday.Text = wednesdayStart + "<br />to<br />" + wednesdayEnd;
                        }
                        if (thursdayStart != "" || thursdayEnd != "") {
                            if (thursdayStart != "") {
                                thursdayStart = Convert.ToDateTime(thursdayStart).ToString("%h:mm tt");
                            }
                            if (thursdayEnd != "") {
                                thursdayEnd = Convert.ToDateTime(thursdayEnd).ToString("%h:mm tt");
                            }
                            lblThursday.Text = thursdayStart + "<br />to<br />" + thursdayEnd;
                        }
                        if (fridayStart != "" || fridayEnd != "") {
                            if (fridayStart != "") {
                                fridayStart = Convert.ToDateTime(fridayStart).ToString("%h:mm tt");
                            }
                            if (fridayEnd != "") {
                                fridayEnd = Convert.ToDateTime(fridayEnd).ToString("%h:mm tt");
                            }
                            lblFriday.Text = fridayStart + "<br />to<br />" + fridayEnd;
                        }
                        if (saturdayStart != "" || saturdayEnd != "") {
                            if (saturdayStart != "") {
                                saturdayStart = Convert.ToDateTime(saturdayStart).ToString("%h:mm tt");
                            }
                            if (saturdayEnd != "") {
                                saturdayEnd = Convert.ToDateTime(saturdayEnd).ToString("%h:mm tt");
                            }
                            lblSaturday.Text = saturdayStart + "<br />to<br />" + saturdayEnd;
                        }
                        if (sundayStart != "" || sundayEnd != "") {
                            if (sundayStart != "") {
                                sundayStart = Convert.ToDateTime(sundayStart).ToString("%h:mm tt");
                            }
                            if (sundayEnd != "") {
                                sundayEnd = Convert.ToDateTime(sundayEnd).ToString("%h:mm tt");
                            }
                            lblSunday.Text = sundayStart + "<br />to<br />" + sundayEnd;
                        }
                        schedule2.Visible = true;
                    }
                } else {
                    schedule2.Visible = false;
                }
            }

            //show uploaded file attachments
            DataSet dsAttachments = positionRequest.GetAttachments(positionRequestID);
            Int32 attachmentCount = dsAttachments.Tables[0].Rows.Count;
            if (attachmentCount > 0) {
                for (Int32 i = 0; i < attachmentCount; i++) {
                    lblAttachments.Text += "<div style=\"padding-bottom:5px;\"><a href=\"Attachments/" + dsAttachments.Tables[0].Rows[i]["FileName"].ToString() + "\" target=\"self\" class=\"smallText\" title=\"View File Attachment " + (i + 1) + "\">Attachment " + (i + 1) + "</a></div>";
                }
                panFileAttachments.Visible = true;
            } else {
                panFileAttachments.Visible = false;
            }

            //get Routing/Approvals
            DataSet dsRouting = positionRequest.GetRouting(positionRequestID);
            Int32 routingCount = dsRouting.Tables[0].Rows.Count;
            if (routingCount > 0) {
                for (Int32 i = 0; i < routingCount; i++) {
                    String note = dsRouting.Tables[0].Rows[i]["Note"].ToString();
                    String description = dsRouting.Tables[0].Rows[i]["Description"].ToString();
                    TableRow tr = new TableRow();
                    TableCell td = new TableCell();
                    td.CssClass = "solid";
                    td.Attributes["style"] = "vertical-align:top;padding-left:10px;width:75px";
                    td.Controls.Add(new LiteralControl(dsRouting.Tables[0].Rows[i]["UserType"].ToString()));
                    tr.Cells.Add(td);
                    td = new TableCell();
                    td.CssClass = "solid";
                    td.Attributes["style"] = "vertical-align:top;width:190px";
                    td.Controls.Add(new LiteralControl(dsRouting.Tables[0].Rows[i]["Name"].ToString()));
                    tr.Cells.Add(td);
                    td = new TableCell();
                    td.CssClass = "solid";
                    td.Attributes["style"] = "vertical-align:top";
                    td.Controls.Add(new LiteralControl(description));
                    if (description != "Pending Approval") {
                        td.Controls.Add(new LiteralControl("&nbsp;" + Convert.ToDateTime(dsRouting.Tables[0].Rows[i]["Date"]).ToString("MM/dd/yy %h:mm tt")));
                    }
                    if (note != "") {
                        td.Controls.Add(new LiteralControl("<br />" + note));
                    }
                    tr.Cells.Add(td);
                    tblApprovals2.Rows.Add(tr);
                }
                panApprovals2.Visible = true;
            } else {
                panApprovals2.Visible = false;
            }

            lblConfirm.Visible = true;
            hidTodo.Value = "";
            cmdOK.Visible = true;
            cmdPrint.Visible = false;
            cmdBack.Visible = false;
            cmdSave.Visible = false;
            cmdSubmit.Visible = false;
            panForm.Visible = true;
            panOutput.Visible = true;
            panInput.Visible = false;

        } else {
            SearchPositionRequests(cboSearchStatus.SelectedValue, txtSearchOfficialTitle.Text, cboSearchPositionType.SelectedValue);
            panList.Visible = true;
        }
    }

    protected void cmdSearch_Click(object sender, System.EventArgs e) {
        hidTodo.Value = "";
    }

    protected void cmdSave_Click(object sender, System.EventArgs e) {
        Int32 positionRequestID = 0;
        bool blnSuccess = false;

        //store bit field values
        Int16 permanentPosition = 0, nonPermanentPosition = 0, newPosition = 0, replacementPosition = 0, pCard = 0, CCSIssuedCellPhone = 0, ctcLinkAccess = 0, timeSheetApprover = 0;

        if (optPermanent.Checked) {
            permanentPosition = 1;
        }
        if (optNonPermanent.Checked) {
            nonPermanentPosition = 1;
        }
        if (optNew.Checked) {
            newPosition = 1;
        }
        if (optReplacement.Checked) {
            replacementPosition = 1;
        }
        if (chkPCard.Checked) {
            pCard = 1;
        }
        if (chkCellPhone.Checked) {
            CCSIssuedCellPhone = 1;
        }
        if (chkAdditionalAccess.Checked) {
            ctcLinkAccess = 1;
        }
        if (chkTimeSheetApprover.Checked) {
            timeSheetApprover = 1;
        }

        if (hidTodo.Value == "add") {
            positionRequestID = positionRequest.AddPositionRequest(lblOriginatorID.Text, lblOriginatorName.Text, txtOfficialTitle.Text, txtWorkingTitle.Text, txtJobCode.Text, cboCollegeUnit.SelectedValue, txtBudgetNumber.Text, txtPositionNumber.Text, hidDepartmentID.Value, hidDepartment.Value, txtPhoneNumber.Text, txtMailStop.Text, txtSupervisorName.Text, hidSupervisorID.Value, cboPositionType.Text, permanentPosition, nonPermanentPosition, txtEndDate.Value, cboRecruitmentType.SelectedValue, newPosition, replacementPosition, txtEmployeeReplaced.Text, pCard, CCSIssuedCellPhone, ctcLinkAccess, timeSheetApprover, txtHoursPerDay.Text, txtHoursPerWeek.Text, txtMonthsPerYear.Text, txtCyclicCalendarCode.Text, "Incomplete", DateTime.Now);
            if (positionRequestID > 0) {
                blnSuccess = true;
            }
        } else if (hidTodo.Value == "edit") {
            positionRequestID = Convert.ToInt32(hidID.Value);
            blnSuccess = positionRequest.EditPositionRequest(positionRequestID, lblOriginatorName.Text, txtOfficialTitle.Text, txtWorkingTitle.Text, txtJobCode.Text, cboCollegeUnit.SelectedValue, txtBudgetNumber.Text, txtPositionNumber.Text, hidDepartmentID.Value, hidDepartment.Value, txtPhoneNumber.Text, txtMailStop.Text, txtSupervisorName.Text, hidSupervisorID.Value, cboPositionType.Text, permanentPosition, nonPermanentPosition, txtEndDate.Value, cboRecruitmentType.SelectedValue, newPosition, replacementPosition, txtEmployeeReplaced.Text, pCard, CCSIssuedCellPhone, ctcLinkAccess, timeSheetApprover, txtHoursPerDay.Text, txtHoursPerWeek.Text, txtMonthsPerYear.Text, txtCyclicCalendarCode.Text, "Incomplete");
        }
        
        if (blnSuccess) {
            //add work schedule
            if (cboPositionType.SelectedValue == "Classified" || cboPositionType.SelectedValue == "Part-Time Hourly") {
                if (hidTodo.Value == "add") {
                    blnSuccess = positionRequest.AddWorkSchedule(positionRequestID, txtMondayStart.Value, txtMondayEnd.Value, txtTuesdayStart.Value, txtTuesdayEnd.Value, txtWednesdayStart.Value, txtWednesdayEnd.Value, txtThursdayStart.Value, txtThursdayEnd.Value, txtFridayStart.Value, txtFridayEnd.Value, txtSaturdayStart.Value, txtSaturdayEnd.Value, txtSundayStart.Value, txtSundayEnd.Value);
                } else if (hidTodo.Value == "edit") {
                    blnSuccess = positionRequest.EditWorkSchedule(positionRequestID, txtMondayStart.Value, txtMondayEnd.Value, txtTuesdayStart.Value, txtTuesdayEnd.Value, txtWednesdayStart.Value, txtWednesdayEnd.Value, txtThursdayStart.Value, txtThursdayEnd.Value, txtFridayStart.Value, txtFridayEnd.Value, txtSaturdayStart.Value, txtSaturdayEnd.Value, txtSundayStart.Value, txtSundayEnd.Value);
                }
            } else {
                //delete the work schedule
                positionRequest.DeleteWorkSchedule(positionRequestID);
            }

            if (blnSuccess) {
                //upload file attachments
                blnSuccess = true;
                HttpFileCollection uploads = HttpContext.Current.Request.Files;
                for (int i = 0; i < uploads.Count; i++) {
                    HttpPostedFile upload = uploads[i];
                    if (upload.ContentLength == 0) {
                        continue;
                    }
                    String c = System.IO.Path.GetFileName(upload.FileName);
                    try {
                        String strFileName = positionRequestID + "_" + i + System.IO.Path.GetExtension(c);
                        upload.SaveAs(Server.MapPath("Attachments\\") + strFileName);
                        positionRequest.AddAttachment(positionRequestID, strFileName);
                    } catch {
                        blnSuccess = false;
                    }
                }
                
                if (blnSuccess) {
                    //add routing record
                    blnSuccess = positionRequest.AddRouting(positionRequestID, "Originator", "Saved", "", lblOriginatorName.Text, lblOriginatorID.Text, DateTime.Now.ToString("MM/dd/yy HH:mm:ss.fff"));
                    if (blnSuccess == false) {
                        lblError.Text = "<p><strong>Error: The routing process could not be recorded.</strong></p>" + strErrorContact + "<br />";
                    }
                } else {
                    lblError.Text = "<p><strong>Error: The file attachment(s) could not be uploaded.</strong></p>" + strErrorContact + "<br />";
                }
            } else {
                lblError.Text = "<p><strong>Error: The work schedule failed to save.</strong></p>" + strErrorContact + "<br />";
            }
        } else {
            lblError.Text = "<p><strong>Error: The form failed to save.</strong></p>" + strErrorContact + "<br />";
        }
        
        if (blnSuccess) {
            hidTodo.Value = "confirm";
            Response.Redirect("form.aspx?todo=confirm&id=" + positionRequestID + "&task=saved");
        } else {
            lblError.Visible = true;
            cmdBack.Visible = false;
            cmdSave.Visible = false;
            cmdSubmit.Visible = false;
            cmdOK.Visible = true;
        }

        panList.Visible = false;
    }

    protected void cmdSubmit_Click(object sender, System.EventArgs e) {
        Int32 positionRequestID = 0;
        bool blnSuccess = false;
        String confirmMessage = "";

        //get supervisor info
        DataSet dsSupervisor = positionRequest.GetSupervisor(lblOriginatorID.Text); //create this method and write sp
        String status = "", supervisorJobCode = "", supervisorEmail = "", supervisorID = "", supervisorName = "", menu = "Supervisor";
        if (dsSupervisor.Tables[0].Rows.Count > 0) {
            supervisorJobCode = dsSupervisor.Tables[0].Rows[0]["JOBCODE"].ToString();
            supervisorEmail = dsSupervisor.Tables[0].Rows[0]["EMAIL_ADDR"].ToString();
            supervisorID = dsSupervisor.Tables[0].Rows[0]["EMPLID"].ToString();
            supervisorName = dsSupervisor.Tables[0].Rows[0]["NAME"].ToString();
            status = "Approver 1";

            if (cboPositionType.SelectedValue != "Adjunct Faculty" && cboPositionType.SelectedValue != "Part-Time Hourly" && ((String.Compare(hidDepartmentID.Value, "99001") <= 0) || (String.Compare(hidDepartmentID.Value, "99021") >= 0))) { //(Convert.ToInt32(hidDepartmentID.Value) < 99001 && Convert.ToInt32(hidDepartmentID.Value) > 99021)) {
                if (supervisorJobCode == "111113" || supervisorJobCode == "111133") {
                    status = "Executive";
                }
            } else if (cboPositionType.SelectedValue == "Adjunct Faculty") {
                if (supervisorJobCode == "111274" || supervisorJobCode == "111577" || supervisorJobCode == "111465" || supervisorJobCode == "111439" || supervisorJobCode == "111120") {
                    status = "Budget";
                    supervisorEmail = "vu.nguyen@ccs.spokane.edu"; //for testing
                    //supervisorEmail = "BudgetPR@ccs.spokane.edu"; 
                    supervisorName = "";
                    supervisorID = "";
                    menu = "Budget Admin";
                }
            } else if (cboPositionType.SelectedValue == "Part-Time Hourly" || (String.Compare(hidDepartmentID.Value, "99001") >= 0 && String.Compare(hidDepartmentID.Value, "99021") <= 0)) {
                if (supervisorJobCode == "111272" || supervisorJobCode == "111165" || supervisorJobCode == "111319" || supervisorJobCode == "111433" || supervisorJobCode == "111274" || supervisorJobCode == "111577" || supervisorJobCode == "111465" || supervisorJobCode == "111439" || supervisorJobCode == "111120") {
                    status = "Executive";
                    if(supervisorJobCode == "111433") { //if Greg Stevens is the Executive approver
                        //change the Executive approver to Melody Matthews
                        supervisorJobCode = "111490"; 
                        dsSupervisor = positionRequest.GetEmployeeByJobCode(supervisorJobCode);
                        if(dsSupervisor.Tables[0].Rows.Count > 0) {
                            supervisorEmail = dsSupervisor.Tables[0].Rows[0]["EMAIL_ADDR"].ToString();
                            supervisorID = dsSupervisor.Tables[0].Rows[0]["EMPLID"].ToString();
                            supervisorName = dsSupervisor.Tables[0].Rows[0]["NAME"].ToString();
                        } else {
                            lblError.Text = "<p><strong>Error: The supervisor for approval could not be found.</strong></p>" + strErrorContact + "<br />";
                        }
                    }
                }
            }

            //store bit field values
            Int16 permanentPosition = 0, nonPermanentPosition = 0, newPosition = 0, replacementPosition = 0, pCard = 0, CCSIssuedCellPhone = 0, ctcLinkAccess = 0, timeSheetApprover = 0;

            if (optPermanent.Checked) {
                permanentPosition = 1;
            }
            if (optNonPermanent.Checked) {
                nonPermanentPosition = 1;
            }
            if (optNew.Checked) {
                newPosition = 1;
            }
            if (optReplacement.Checked) {
                replacementPosition = 1;
            }
            if (chkPCard.Checked) {
                pCard = 1;
            }
            if (chkCellPhone.Checked) {
                CCSIssuedCellPhone = 1;
            }
            if (chkAdditionalAccess.Checked) {
                ctcLinkAccess = 1;
            }
            if (chkTimeSheetApprover.Checked) {
                timeSheetApprover = 1;
            }

            if (hidTodo.Value == "add") {
                positionRequestID = positionRequest.AddPositionRequest(lblOriginatorID.Text, lblOriginatorName.Text, txtOfficialTitle.Text, txtWorkingTitle.Text, txtJobCode.Text, cboCollegeUnit.SelectedValue, txtBudgetNumber.Text, txtPositionNumber.Text, hidDepartmentID.Value, hidDepartment.Value, txtPhoneNumber.Text, txtMailStop.Text, txtSupervisorName.Text, hidSupervisorID.Value, cboPositionType.Text, permanentPosition, nonPermanentPosition, txtEndDate.Value, cboRecruitmentType.SelectedValue, newPosition, replacementPosition, txtEmployeeReplaced.Text, pCard, CCSIssuedCellPhone, ctcLinkAccess, timeSheetApprover, txtHoursPerDay.Text, txtHoursPerWeek.Text, txtMonthsPerYear.Text, txtCyclicCalendarCode.Text, "Incomplete", DateTime.Now);
                if (positionRequestID > 0) {
                    blnSuccess = true;
                }
            } else if (hidTodo.Value == "edit") {
                positionRequestID = Convert.ToInt32(hidID.Value);
                blnSuccess = positionRequest.EditPositionRequest(positionRequestID, lblOriginatorName.Text, txtOfficialTitle.Text, txtWorkingTitle.Text, txtJobCode.Text, cboCollegeUnit.SelectedValue, txtBudgetNumber.Text, txtPositionNumber.Text, hidDepartmentID.Value, hidDepartment.Value, txtPhoneNumber.Text, txtMailStop.Text, txtSupervisorName.Text, hidSupervisorID.Value, cboPositionType.Text, permanentPosition, nonPermanentPosition, txtEndDate.Value, cboRecruitmentType.SelectedValue, newPosition, replacementPosition, txtEmployeeReplaced.Text, pCard, CCSIssuedCellPhone, ctcLinkAccess, timeSheetApprover, txtHoursPerDay.Text, txtHoursPerWeek.Text, txtMonthsPerYear.Text, txtCyclicCalendarCode.Text, "Incomplete");
                if (blnSuccess) {
                    positionRequest.EditCreationDate(positionRequestID, DateTime.Now);
                }
            }

            if (blnSuccess) {

                //update tracking number
                Int32 trackingNumber = Convert.ToInt32(DateTime.Now.Year.ToString() + positionRequestID);
                positionRequest.EditTrackingNumber(positionRequestID, trackingNumber);

                //add work schedule
                if (cboPositionType.SelectedValue == "Classified" || cboPositionType.SelectedValue == "Part-Time Hourly") {
                    if (hidTodo.Value == "add") {
                        blnSuccess = positionRequest.AddWorkSchedule(positionRequestID, txtMondayStart.Value, txtMondayEnd.Value, txtTuesdayStart.Value, txtTuesdayEnd.Value, txtWednesdayStart.Value, txtWednesdayEnd.Value, txtThursdayStart.Value, txtThursdayEnd.Value, txtFridayStart.Value, txtFridayEnd.Value, txtSaturdayStart.Value, txtSaturdayEnd.Value, txtSundayStart.Value, txtSundayEnd.Value);
                    }else if(hidTodo.Value == "edit") {
                        blnSuccess = positionRequest.EditWorkSchedule(positionRequestID, txtMondayStart.Value, txtMondayEnd.Value, txtTuesdayStart.Value, txtTuesdayEnd.Value, txtWednesdayStart.Value, txtWednesdayEnd.Value, txtThursdayStart.Value, txtThursdayEnd.Value, txtFridayStart.Value, txtFridayEnd.Value, txtSaturdayStart.Value, txtSaturdayEnd.Value, txtSundayStart.Value, txtSundayEnd.Value);
                    }
                } else {
                    //delete the work schedule
                    positionRequest.DeleteWorkSchedule(positionRequestID);
                }

                if (blnSuccess) {
                    //upload file attachments
                    blnSuccess = true;
                    HttpFileCollection uploads = HttpContext.Current.Request.Files;
                    for (int i = 0; i < uploads.Count; i++) {
                        HttpPostedFile upload = uploads[i];
                        if (upload.ContentLength == 0) {
                            continue;
                        }
                        String c = System.IO.Path.GetFileName(upload.FileName);
                        try {
                            String strFileName = positionRequestID + "_" + i + System.IO.Path.GetExtension(c);
                            upload.SaveAs(Server.MapPath("Attachments\\") + strFileName);
                            positionRequest.AddAttachment(positionRequestID, strFileName);
                        } catch {
                            blnSuccess = false;
                        }
                    }

                    if (blnSuccess) {
                        try {
                            //add submitted routing record
                            blnSuccess = positionRequest.AddRouting(positionRequestID, "Originator", "Submitted", "", lblOriginatorName.Text, lblOriginatorID.Text, DateTime.Now.ToString("MM/dd/yy HH:mm:ss.fff"));

                            if (blnSuccess) {
                                //add pending approval routing record
                                blnSuccess = positionRequest.AddRouting(positionRequestID, status, "Pending Approval", "", supervisorName, supervisorID, DateTime.Now.AddMilliseconds(100).ToString("MM/dd/yy HH:mm:ss.fff"));

                                if (blnSuccess) {
                                    //update position request status
                                    blnSuccess = positionRequest.EditStatus(positionRequestID, status); //may need to check boolean return value to make sure status is update and notify HR if not

                                    if (blnSuccess) {
                                        if (status == "Budget") {
                                            supervisorName = "the Budget Admin";
                                        }

                                        //send email for approval
                                        SmtpClient client = new SmtpClient();
                                        MailMessage objEmail = new MailMessage();
                                        String emailBody = "<p><strong>Tracking #:</strong> " + trackingNumber +
                                                              "<br /><strong>Originator:</strong> " + lblOriginatorName.Text +
                                                              "<br /><strong>Created:</strong> " + DateTime.Now.ToString("MM/dd/yy %h:mm tt") +
                                                              "<br /><strong>College/Unit:</strong> " + cboCollegeUnit.Text +
                                                              "<br /><strong>Department:</strong> " + hidDepartment.Value +
                                                              "<br /><strong>Official Title:</strong> " + txtOfficialTitle.Text +
                                                              "<br /><strong>Supervisor Name:</strong> " + txtSupervisorName.Text +
                                                              "<br /><strong>Position Type:</strong> " + cboPositionType.Text +
                                                              "<br /><strong>Status:</strong> " + status + "</p>";

                                        if (supervisorEmail != "") {
                                            objEmail.To.Add(supervisorEmail); 
                                            objEmail.To.Add("vu.nguyen@ccs.spokane.edu");
                                            //objEmail.From = new MailAddress(strAdminEmailAddress);
                                            objEmail.IsBodyHtml = true;
                                            objEmail.Priority = MailPriority.High;
                                            objEmail.Subject = "Position Request Approval Notification";
                                            objEmail.Body = "<p>Please review the Position Request below for approval.</p>";
                                            objEmail.Body += "<p><a href=\"https://internal.spokane.edu/PositionRequest/\">https://internal.spokane.edu/PositionRequest/</a>";
                                            objEmail.Body += "<br />Log in with your Active Directory account and click on the PR Approval link in the " + menu + " menu.</p>";
                                            objEmail.Body += emailBody;

                                            try {
                                                client.Send(objEmail);
                                                confirmMessage += "|Approval notification has been sent to " + supervisorName + ".|";
                                            } catch {
                                                objEmail = new MailMessage();
                                                objEmail.To.Add(strAdminEmailAddress);
                                                objEmail.From = new MailAddress(strAdminEmailAddress);
                                                objEmail.IsBodyHtml = true;
                                                objEmail.Priority = MailPriority.High;
                                                objEmail.Subject = "Position Request Approval Notification Could Not Be Sent";
                                                objEmail.Body = "<p>The following approval notification email could not be sent to " + supervisorName + " at the email address " + supervisorEmail + ".</p>";
                                                objEmail.Body += "<hr style=\"width:100%\" />";
                                                objEmail.Body += emailBody;

                                                try {
                                                    client.Send(objEmail);
                                                    confirmMessage += "|The approval notification email could not be sent to " + supervisorName + ". HRO has been notified.|";
                                                } catch {
                                                    confirmMessage += "|The approval notification email could not be sent to " + supervisorName + " or HRO. Please contact HRO at " + strAdminEmailAddress + ".|";
                                                }
                                            }
                                        } else {
                                            objEmail = new MailMessage();
                                            objEmail.To.Add(strAdminEmailAddress);
                                            objEmail.From = new MailAddress(strAdminEmailAddress);
                                            objEmail.IsBodyHtml = true;
                                            objEmail.Priority = MailPriority.High;
                                            objEmail.Subject = "Position Request Approval Notification Could Not Be Sent";
                                            objEmail.Body = "<p>The following approval notification email could not be sent to " + supervisorName + " because their email address does not exist.</p>";
                                            objEmail.Body += "<hr style=\"width:100%\" />";
                                            objEmail.Body += emailBody;

                                            try {
                                                client.Send(objEmail);
                                                confirmMessage += "|The approval notification email could not be sent to " + supervisorName + ". HRO has been notified.|";
                                            } catch {
                                                confirmMessage += "|The approval notification email could not be sent to " + supervisorName + " or HRO. Please contact HRO at " + strAdminEmailAddress + ".|";
                                            }
                                        }
                                    } else {
                                        lblError.Text = "<p><strong>Error: The Position Request status could not be updated to " + status + ".</strong></p>" + strErrorContact + "<br />";
                                    }
                                } else {
                                    lblError.Text = "<p><strong>Error: The pending approval routing record cound not be recorded.</strong></p>" + strErrorContact + "<br />";
                                }
                            } else {
                                lblError.Text = "<p><strong>Error: The submitted routing record could not be recorded.</strong></p>" + strErrorContact + "<br />";
                            }
                        } catch {
                            lblError.Text = "<p><strong>Error: The routing process could not be submitted.</strong></p>" + strErrorContact + "<br />";
                            blnSuccess = false;
                        }
                    } else {
                        lblError.Text = "<p><strong>Error: The file attachment(s) could not be uploaded.</strong></p>" + strErrorContact + "<br />";
                    }
                } else {
                    lblError.Text = "<p><strong>Error: The work schedule could not be submitted.</strong></p>" + strErrorContact + "<br />";
                }
            } else {
                lblError.Text = "<p><strong>Error: The form failed to submit.</strong></p>" + strErrorContact + "<br />";
            }
        } else {
            lblError.Text = "<p><strong>Error: The supervisor for approval could not be found.</strong></p>" + strErrorContact + "<br />";
        }

        if (blnSuccess) {
            Response.Redirect("form.aspx?todo=confirm&id=" + positionRequestID + "&task=submitted&msg=" + confirmMessage);
        } else {
            lblError.Visible = true;
            cmdBack.Visible = false;
            cmdSave.Visible = false;
            cmdSubmit.Visible = false;
            cmdOK.Visible = true;
        }
    }

    protected void lnkDeleteAttachment_Click(object sender, System.EventArgs e) {
        //get clicked linkbutton
        LinkButton lnkDelete = (LinkButton)sender;
        //get the file name
        String strFileName = lnkDelete.CommandArgument.ToString();
        //delete the file from the server
        System.IO.File.Delete(Server.MapPath("Attachments\\") + strFileName);
        //delete the file from the database
        positionRequest.DeleteAttachment(strFileName);
        //delete the file name from the hidden form element
        hidFileAttachments.Value = hidFileAttachments.Value.Replace("|" + strFileName, "");
        //populate file attachments
        populateFileAttachments();
    }

    private void SearchPositionRequests(String status, String officialTitle, String positionType) {

        DataSet dsPositionRequests = positionRequest.GetPositionRequests(Request.Cookies["phatt2"]["userctclinkid"], "", status, "", officialTitle, "", "", positionType, "", "", "", "", "", "");
        Int32 rowCount = dsPositionRequests.Tables[0].Rows.Count;

        if (rowCount > 0) {
            for (Int32 i = 0; i < rowCount; i++) {
                Int32 positionRequestID = Convert.ToInt32(dsPositionRequests.Tables[0].Rows[i]["PositionRequestID"]);
                String created = "";
                try { //temporary until created date is required and added to the db when form is saved and updated
                    created = Convert.ToDateTime(dsPositionRequests.Tables[0].Rows[i]["Created"]).ToShortDateString();
                } catch {
                    //do nothing
                }
                String department = dsPositionRequests.Tables[0].Rows[i]["Department"].ToString();
                status = dsPositionRequests.Tables[0].Rows[i]["Status"].ToString();
                officialTitle = dsPositionRequests.Tables[0].Rows[i]["OfficialTitle"].ToString();
                positionType = dsPositionRequests.Tables[0].Rows[i]["PositionType"].ToString();

                TableRow tr = new TableRow();
                TableCell td = new TableCell();
                td.CssClass = "solid";
                td.Controls.Add(new LiteralControl(status));
                tr.Cells.Add(td);

                td = new TableCell();
                td.CssClass = "solid";
                td.Controls.Add(new LiteralControl(created));
                tr.Cells.Add(td);

                td = new TableCell();
                td.CssClass = "solid";
                td.Controls.Add(new LiteralControl(officialTitle));
                tr.Cells.Add(td);

                td = new TableCell();
                td.CssClass = "solid";
                td.Controls.Add(new LiteralControl(positionType));
                tr.Cells.Add(td);

                td = new TableCell();
                td.CssClass = "action";
                td.Attributes["style"] = "vertical-align: top;width: 16px; background: #eee none";
                if (status == "Incomplete" || status == "Rejected") {
                    td.Controls.Add(new LiteralControl("<a href=\"#\" class=\"edit\" title=\"Edit Form\" onclick=\"document.getElementById('" + hidTodo.ClientID + "').value='edit';document.getElementById('" + hidStatus.ClientID + "').value='" + status + "';document.getElementById('" + hidID.ClientID + "').value='" + positionRequestID + "';document.getElementById('" + frmPR.ClientID + "').submit();\"></a>"));
                } else {
                    td.Controls.Add(new LiteralControl("<a href=\"#\" class=\"view\" title=\"View Form\" onclick=\"document.getElementById('" + hidTodo.ClientID + "').value='view';document.getElementById('" + hidStatus.ClientID + "').value='" + status + "';document.getElementById('" + hidID.ClientID + "').value='" + positionRequestID + "';document.getElementById('" + frmPR.ClientID + "').submit();\"></a>"));
                }
                tr.Cells.Add(td);

                td = new TableCell();
                td.CssClass = "action";
                td.Attributes["style"] = "vertical-align: top;width: 16px; background: #eee none";
                if (status == "Incomplete") {
                    td.Controls.Add(new LiteralControl("<a href=\"#\" class=\"delete\" title=\"Delete Form\" onclick=\"if(confirm('Are you sure you want to delete the following position request form?\\n\\nStatus: " + status + "\\nCreated: " + created + "\\nOfficial Title: " + officialTitle + "\\nPosition Type: " + positionType + "\\nDepartment: " + department.Replace("'", "\\'") + "')){document.getElementById('" + hidTodo.ClientID + "').value='delete';document.getElementById('" + hidID.ClientID + "').value='" + positionRequestID + "';document.getElementById('" + frmPR.ClientID + "').submit();}\"></a>"));
                } else if (status != "HRO" && status != "Recruiting" && status != "Hired" && status != "Void" && status != "Cancelled" && status != "Complete") {
                    td.Controls.Add(new LiteralControl("<a href=\"#\" class=\"delete\" title=\"Cancel Form\" onclick=\"if(confirm('Cancelling the form will stop the approval process and submittion to HRO. Are you sure you want to cancel the following position request form?\\n\\nStatus: " + status + "\\nCreated: " + created + "\\nOfficial Title: " + officialTitle + "\\nPosition Type: " + positionType + "\\nDepartment: " + department.Replace("'", "\\'") + "')){document.getElementById('" + hidTodo.ClientID + "').value='cancel';document.getElementById('" + hidID.ClientID + "').value='" + positionRequestID + "';document.getElementById('" + frmPR.ClientID + "').submit();}\"></a>"));
                }
                tr.Cells.Add(td);
                tblList.Rows.Add(tr);
            }
        } else {
            TableRow tr = new TableRow();
            TableCell td = new TableCell();
            td.ColumnSpan = 6;
            if (status == "" && officialTitle == "" && positionType == "") {
                td.Controls.Add(new LiteralControl("No forms currently exist."));
            } else {
                td.Controls.Add(new LiteralControl("Your search returned 0 results."));
            }
            tr.Cells.Add(td);
            tblList.Rows.Add(tr);
        }

    }

    private void populateFileAttachments() {
        tblAttachments.Rows.Clear();
        if (hidFileAttachments.Value != "") {
            String[] strFileName = hidFileAttachments.Value.Substring(1).Split('|');
            for (Int32 i = 0; i < strFileName.Length; i++) {
                TableRow tr = new TableRow();
                TableCell td = new TableCell();
                tr = new TableRow();
                td = new TableCell();
                td.Attributes["style"] = "background:none;border:none;padding-left:10px;padding-top:10px;";
                if (i < (strFileName.Length - 1)) {
                    td.Attributes["style"] += "border-bottom:solid 1px #aaa";
                }
                td.Controls.Add(new LiteralControl("<a href=\"Attachments/" + strFileName[i] + "\" target=\"self\" class=\"smallText\" title=\"View File Attachment " + (i + 1) + "\">Attachment " + (i + 1) + "</a>"));
                tr.Cells.Add(td);
                td = new TableCell();
                td.CssClass = "action";
                td.Attributes["style"] = "background:none;border:none;vertical-align:top;padding-top:10px;width:16px;";
                if (i < (strFileName.Length - 1)) {
                    td.Attributes["style"] += "border-bottom:solid 1px #aaa";
                }
                LinkButton lnkDeleteAttachment = new LinkButton();
                lnkDeleteAttachment.ID = "lnkDeleteAttachment" + i;
                lnkDeleteAttachment.CssClass = "delete";
                lnkDeleteAttachment.Text = "Delete File Attachment " + (i + 1);
                lnkDeleteAttachment.OnClientClick = "return confirm('Are you sure you want to delete file attachment " + (i + 1) + "?\\nNote: Any existing file attachments will be renumbered.');";
                lnkDeleteAttachment.CommandArgument = strFileName[i];
                lnkDeleteAttachment.Click += new EventHandler(lnkDeleteAttachment_Click);
                td.Controls.Add(lnkDeleteAttachment);
                tr.Cells.Add(td);
                tblAttachments.Rows.Add(tr);
            }
            divAttachment1.Visible = true;
            divAttachment2.Visible = true;
        } else {
            divAttachment1.Visible = false;
            divAttachment2.Visible = false;
        }
    }
}




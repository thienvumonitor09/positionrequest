﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Net.Mail;
using System.Web.UI.HtmlControls;

public partial class Budget_Approval : System.Web.UI.Page {
    PositionRequest positionRequest = new PositionRequest();
    String strErrorContact = "<p>To report this error, please contact the IT Support Center by <a href=\"mailto:ITSupportCenter@ccs.spokane.edu\">email</a> or phone: 533-HELP.<br />Include a screen shot of this page and as much information as possible.</p>";
    String strAdminEmailAddress = ConfigurationManager.AppSettings["AdminEmailAddress"].ToString();
    protected override void OnInit(EventArgs e) {
        String strTodo = Request.Form["ctl00$maintextHolder$hidTodo"];
        if (strTodo == "view" || strTodo == "edit") {
            //get Routing/Approvals
            DataSet dsRouting = positionRequest.GetRouting(Convert.ToInt32(Request.Form["ctl00$maintextHolder$hidID"]));
            Int32 routingCount = dsRouting.Tables[0].Rows.Count;
            if (routingCount > 0) {
                for (Int32 i = 0; i < routingCount; i++) {
                    String note = dsRouting.Tables[0].Rows[i]["Note"].ToString();
                    String description = dsRouting.Tables[0].Rows[i]["Description"].ToString();
                    TableRow tr = new TableRow();
                    TableCell td = new TableCell();
                    td.CssClass = "solid";
                    td.Attributes["style"] = "vertical-align:top;padding-left:10px;width:75px";
                    td.Controls.Add(new LiteralControl(dsRouting.Tables[0].Rows[i]["UserType"].ToString()));
                    tr.Cells.Add(td);
                    td = new TableCell();
                    td.CssClass = "solid";
                    td.Attributes["style"] = "vertical-align:top;";
                    if (description != "Pending Approval") {
                        td.Attributes["style"] += "width:190px";
                        td.Controls.Add(new LiteralControl(dsRouting.Tables[0].Rows[i]["Name"].ToString()));
                        tr.Cells.Add(td);
                        td = new TableCell();
                        td.CssClass = "solid";
                        td.Attributes["style"] = "vertical-align:top";
                        td.Controls.Add(new LiteralControl(description + "&nbsp;" + Convert.ToDateTime(dsRouting.Tables[0].Rows[i]["Date"]).ToShortDateString()));
                        if (note != "") {
                            td.Controls.Add(new LiteralControl("<br />" + note));
                        }
                    } else {
                        td.ColumnSpan = 2;
                        td.Attributes["style"] += "wite-space:nowrap";
                        HtmlInputRadioButton optApprove = new HtmlInputRadioButton();
                        optApprove.Attributes["runat"] = "server";
                        optApprove.ID = "optApprove";
                        optApprove.Name = "optApproval";
                        td.Controls.Add(optApprove);
                        td.Controls.Add(new LiteralControl("<strong>Approve</strong>&nbsp;"));
                        if (strTodo == "view") {
                            HtmlInputRadioButton optReject = new HtmlInputRadioButton();
                            optReject.Attributes["runat"] = "server";
                            optReject.ID = "optReject";
                            optReject.Name = "optApproval";
                            td.Controls.Add(optReject);
                            td.Controls.Add(new LiteralControl("<strong>Reject</strong>&nbsp;"));
                            TextBox txtReject = new TextBox();
                            txtReject.ID = "txtReject";
                            txtReject.Attributes["runat"] = "server";
                            txtReject.Attributes["style"] = "width:310px";
                            td.Controls.Add(txtReject);
                        } else if (strTodo == "edit") {
                            HtmlInputRadioButton optRestart = new HtmlInputRadioButton();
                            optRestart.Attributes["runat"] = "server";
                            optRestart.ID = "optRestart";
                            optRestart.Name = "optApproval";
                            td.Controls.Add(optRestart);
                            td.Controls.Add(new LiteralControl("<strong>Restart the Approval Process</strong>&nbsp;"));
                        }
                    }
                    tr.Cells.Add(td);
                    tblApprovals.Rows.Add(tr);
                }
                panApprovals.Visible = true;
            } else {
                panApprovals.Visible = false;
            }
        }
        base.OnInit(e);
    }

    protected void Page_Load(object sender, EventArgs e) {
        //check if the user is logged in
        HttpCookie cookie = Request.Cookies.Get("phatt2");
        if (cookie == null) {
            users user = new users(HttpContext.Current.User.Identity.Name);
        }

        panList.Visible = false;
        panForm.Visible = false;
        panInput.Visible = false;
        panOutput.Visible = false;
        panCheckbox.Visible = false;
        panModificationReason.Visible = false;
        panEditFileAttachments.Visible = false;
        lblError.Visible = false;
        lblConfirm.Visible = false;
        cmdOK.Visible = false;
        cmdPrint.Visible = false;
        cmdSubmit.Visible = false;
        cmdOK.Visible = false;

        //check if user has the right permissions
        if (positionRequest.GetUserPermission(Request.Cookies["phatt2"]["userctclinkid"]) != "Budget Admin") {
            lblError.Text = "<p><strong>Error: Access Denied</strong><br />You do not have permission to view this page.</p>";
            lblError.Visible = true;

        } else {
            if (!IsPostBack) {
                hidPopulateFromDB.Value = "true";
            }

            String todo = Request.Form["ctl00$maintextHolder$hidTodo"];
            if (todo == null || todo == "") {
                todo = Request.QueryString["todo"];
            }

            if (todo == "view") {

                //uncheck approval selections
                if (hidChangeViews.Value == "true") {
                    HtmlInputRadioButton optApprove = (HtmlInputRadioButton)this.Master.FindControl("maintextHolder").FindControl("optApprove");
                    HtmlInputRadioButton optReject = (HtmlInputRadioButton)this.Master.FindControl("maintextHolder").FindControl("optReject");
                    optApprove.Checked = false;
                    optReject.Checked = false;
                    hidChangeViews.Value = "";
                }

                Int32 positionRequestID = Convert.ToInt32(hidID.Value);

                DataSet dsPositionRequest = positionRequest.GetPositionRequest(positionRequestID);
                if (dsPositionRequest.Tables[0].Rows.Count > 0) {
                    lblOriginatorName.Text = dsPositionRequest.Tables[0].Rows[0]["OriginatorName"].ToString();
                    lblOriginatorID.Text = dsPositionRequest.Tables[0].Rows[0]["OriginatorID"].ToString();
                    lblTrackingNumber.Text = dsPositionRequest.Tables[0].Rows[0]["TrackingNumber"].ToString();
                    lblStatus.Text = dsPositionRequest.Tables[0].Rows[0]["Status"].ToString();
                    lblOfficialTitle.Text = dsPositionRequest.Tables[0].Rows[0]["OfficialTitle"].ToString();
                    lblWorkingTitle.Text = dsPositionRequest.Tables[0].Rows[0]["WorkingTitle"].ToString();
                    lblJobCode.Text = dsPositionRequest.Tables[0].Rows[0]["JobCode"].ToString();
                    lblBudgetNumber.Text = dsPositionRequest.Tables[0].Rows[0]["BudgetNumber"].ToString();
                    lblPositionNumber.Text = dsPositionRequest.Tables[0].Rows[0]["PositionNumber"].ToString();
                    lblCollegeUnit.Text = dsPositionRequest.Tables[0].Rows[0]["CollegeUnit"].ToString();
                    lblDepartmentName.Text = dsPositionRequest.Tables[0].Rows[0]["Department"].ToString();
                    lblDepartmentID2.Text = dsPositionRequest.Tables[0].Rows[0]["DepartmentID"].ToString();
                    lblPhoneNumber.Text = dsPositionRequest.Tables[0].Rows[0]["PhoneNumber"].ToString();
                    lblMailStop.Text = dsPositionRequest.Tables[0].Rows[0]["MailStop"].ToString();
                    lblSupervisorName.Text = dsPositionRequest.Tables[0].Rows[0]["SupervisorName"].ToString();
                    lblSupervisorID2.Text = dsPositionRequest.Tables[0].Rows[0]["SupervisorID"].ToString();
                    if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["NewPosition"]) == 1) {
                        lblNewReplacement.Text = "New Position";
                        employeeReplaced2.Visible = false;
                    } else if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["ReplacementPosition"]) == 1) {
                        lblNewReplacement.Text = "Replacement Position";
                        lblEmployeeReplaced.Text = dsPositionRequest.Tables[0].Rows[0]["ReplacedEmployeeName"].ToString();
                        employeeReplaced2.Visible = true;
                    }
                    lblPositionType.Text = dsPositionRequest.Tables[0].Rows[0]["PositionType"].ToString();
                    if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["PermanentPosition"]) == 1) {
                        lblClassified.Text = "Permanent";
                        lblClassified.Visible = true;
                    }
                    if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["NonPermanentPosition"]) == 1) {
                        lblClassified.Text = "Non Permanent";
                        lblClassified.Visible = true;
                        try {
                            lblEndDate.Text = Convert.ToDateTime(dsPositionRequest.Tables[0].Rows[0]["PositionEndDate"]).ToShortDateString();
                            lblEndDate.Visible = true;
                        } catch {
                            //do nothing - no date exists
                        }
                    } else {
                        endDate2.Visible = false;
                    }
                    lblRecruitmentType.Text = dsPositionRequest.Tables[0].Rows[0]["RecruitmentType"].ToString();
                    if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["PCard"]) == 1) {
                        lblOnboarding.Text += "<div style=\"padding-bottom:5px;\">P-Card</div>";
                    }
                    if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["CCSIssuedCellPhone"]) == 1) {
                        lblOnboarding.Text += "<div style=\"padding-bottom:5px;\">CCS-issued Cell Phone</div>";
                    }
                    if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["CTCLinkAccess"]) == 1) {
                        lblOnboarding.Text += "<div style=\"padding-bottom:5px;\">Additional ctcLink Security Access</div>";
                    }
                    if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["TimeSheetApprover"]) == 1) {
                        lblOnboarding.Text += "<div style=\"padding-bottom:5px;\">Manager/Timesheet Approver</div>";
                    }
                    if (lblPositionType.Text == "Classified" || lblPositionType.Text == "Part-Time Hourly") {
                        lblHoursPerDay.Text = dsPositionRequest.Tables[0].Rows[0]["HoursPerDay"].ToString();
                        lblHoursPerWeek.Text = dsPositionRequest.Tables[0].Rows[0]["HoursPerWeek"].ToString();
                        lblMonthsPerYear.Text = dsPositionRequest.Tables[0].Rows[0]["MonthsPerYear"].ToString();
                        lblCyclicCalendarCode.Text = dsPositionRequest.Tables[0].Rows[0]["CyclicCalendarCode"].ToString();
                        if (lblCyclicCalendarCode.Text != "") {
                            cyclicCalendarCode2.Attributes["style"] = cyclicCalendarCode2.Attributes["style"].Replace("display: none;", "");
                        }

                        DataSet dsWorkSchedule = positionRequest.GetWorkSchedule(positionRequestID);
                        if (dsWorkSchedule.Tables[0].Rows.Count > 0) {
                            String mondayStart = dsWorkSchedule.Tables[0].Rows[0]["MondayStart"].ToString();
                            String mondayEnd = dsWorkSchedule.Tables[0].Rows[0]["MondayEnd"].ToString();
                            String tuesdayStart = dsWorkSchedule.Tables[0].Rows[0]["TuesdayStart"].ToString();
                            String tuesdayEnd = dsWorkSchedule.Tables[0].Rows[0]["TuesdayEnd"].ToString();
                            String wednesdayStart = dsWorkSchedule.Tables[0].Rows[0]["WednesdayStart"].ToString();
                            String wednesdayEnd = dsWorkSchedule.Tables[0].Rows[0]["WednesdayEnd"].ToString();
                            String thursdayStart = dsWorkSchedule.Tables[0].Rows[0]["ThursdayStart"].ToString();
                            String thursdayEnd = dsWorkSchedule.Tables[0].Rows[0]["ThursdayEnd"].ToString();
                            String fridayStart = dsWorkSchedule.Tables[0].Rows[0]["FridayStart"].ToString();
                            String fridayEnd = dsWorkSchedule.Tables[0].Rows[0]["FridayEnd"].ToString();
                            String saturdayStart = dsWorkSchedule.Tables[0].Rows[0]["SaturdayStart"].ToString();
                            String saturdayEnd = dsWorkSchedule.Tables[0].Rows[0]["SaturdayEnd"].ToString();
                            String sundayStart = dsWorkSchedule.Tables[0].Rows[0]["SundayStart"].ToString();
                            String sundayEnd = dsWorkSchedule.Tables[0].Rows[0]["SundayEnd"].ToString();
                            if (mondayStart != "" || mondayEnd != "") {
                                if (mondayStart != "") {
                                    mondayStart = Convert.ToDateTime(mondayStart).ToString("%h:mm tt");
                                }
                                if (mondayEnd != "") {
                                    mondayEnd = Convert.ToDateTime(mondayEnd).ToString("%h:mm tt");
                                }
                                lblMonday.Text = mondayStart + "<br />to<br />" + mondayEnd;
                            }
                            if (tuesdayStart != "" || tuesdayEnd != "") {
                                if (tuesdayStart != "") {
                                    tuesdayStart = Convert.ToDateTime(tuesdayStart).ToString("%h:mm tt");
                                }
                                if (tuesdayEnd != "") {
                                    tuesdayEnd = Convert.ToDateTime(tuesdayEnd).ToString("%h:mm tt");
                                }
                                lblTuesday.Text = tuesdayStart + "<br />to<br />" + tuesdayEnd;
                            }
                            if (wednesdayStart != "" || wednesdayEnd != "") {
                                if (wednesdayStart != "") {
                                    wednesdayStart = Convert.ToDateTime(wednesdayStart).ToString("%h:mm tt");
                                }
                                if (wednesdayEnd != "") {
                                    wednesdayEnd = Convert.ToDateTime(wednesdayEnd).ToString("%h:mm tt");
                                }
                                lblWednesday.Text = wednesdayStart + "<br />to<br />" + wednesdayEnd;
                            }
                            if (thursdayStart != "" || thursdayEnd != "") {
                                if (thursdayStart != "") {
                                    thursdayStart = Convert.ToDateTime(thursdayStart).ToString("%h:mm tt");
                                }
                                if (thursdayEnd != "") {
                                    thursdayEnd = Convert.ToDateTime(thursdayEnd).ToString("%h:mm tt");
                                }
                                lblThursday.Text = thursdayStart + "<br />to<br />" + thursdayEnd;
                            }
                            if (fridayStart != "" || fridayEnd != "") {
                                if (fridayStart != "") {
                                    fridayStart = Convert.ToDateTime(fridayStart).ToString("%h:mm tt");
                                }
                                if (fridayEnd != "") {
                                    fridayEnd = Convert.ToDateTime(fridayEnd).ToString("%h:mm tt");
                                }
                                lblFriday.Text = fridayStart + "<br />to<br />" + fridayEnd;
                            }
                            if (saturdayStart != "" || saturdayEnd != "") {
                                if (saturdayStart != "") {
                                    saturdayStart = Convert.ToDateTime(saturdayStart).ToString("%h:mm tt");
                                }
                                if (saturdayEnd != "") {
                                    saturdayEnd = Convert.ToDateTime(saturdayEnd).ToString("%h:mm tt");
                                }
                                lblSaturday.Text = saturdayStart + "<br />to<br />" + saturdayEnd;
                            }
                            if (sundayStart != "" || sundayEnd != "") {
                                if (sundayStart != "") {
                                    sundayStart = Convert.ToDateTime(sundayStart).ToString("%h:mm tt");
                                }
                                if (sundayEnd != "") {
                                    sundayEnd = Convert.ToDateTime(sundayEnd).ToString("%h:mm tt");
                                }
                                lblSunday.Text = sundayStart + "<br />to<br />" + sundayEnd;
                            }
                            schedule2.Visible = true;
                        }
                    } else {
                        schedule2.Visible = false;
                    }
                }

                //show uploaded file attachments
                lblAttachments.Text = "";
                DataSet dsAttachments = positionRequest.GetAttachments(positionRequestID);
                Int32 attachmentCount = dsAttachments.Tables[0].Rows.Count;
                if (attachmentCount > 0) {
                    for (Int32 i = 0; i < attachmentCount; i++) {
                        lblAttachments.Text += "<div style=\"padding-bottom:5px;\"><a href=\"..\\Attachments/" + dsAttachments.Tables[0].Rows[i]["FileName"].ToString() + "\" target=\"self\" class=\"smallText\" title=\"View File Attachment " + (i + 1) + "\">Attachment " + (i + 1) + "</a></div>";
                    }
                    panViewFileAttachments.Visible = true;
                } else {
                    panViewFileAttachments.Visible = false;
                }

                panCheckbox.Visible = true;
                panForm.Visible = true;
                panOutput.Visible = true;
                cmdSubmit.Visible = true;
                cmdBack.Visible = true;
                cmdPrint.Visible = true;

            } else if (todo == "edit") {

                panModificationReason.Visible = true;

                //uncheck approval selections
                if (hidChangeViews.Value == "true") {
                    HtmlInputRadioButton optApprove = (HtmlInputRadioButton)this.Master.FindControl("maintextHolder").FindControl("optApprove");
                    HtmlInputRadioButton optRestart = (HtmlInputRadioButton)this.Master.FindControl("maintextHolder").FindControl("optRestart");
                    optApprove.Checked = false;
                    optRestart.Checked = false;
                    hidChangeViews.Value = "";
                }

                Int32 positionRequestID = Convert.ToInt32(hidID.Value);

                if (hidPopulateFromDB.Value == "true") {
                    
                    //show edit panel and populate position request form
                    DataSet dsPositionRequest = positionRequest.GetPositionRequest(positionRequestID);
                    if (dsPositionRequest.Tables[0].Rows.Count > 0) {
                        lblOriginatorID.Text = dsPositionRequest.Tables[0].Rows[0]["OriginatorID"].ToString();
                        lblOriginatorName.Text = dsPositionRequest.Tables[0].Rows[0]["OriginatorName"].ToString();
                        txtOfficialTitle.Text = dsPositionRequest.Tables[0].Rows[0]["OfficialTitle"].ToString();
                        txtWorkingTitle.Text = dsPositionRequest.Tables[0].Rows[0]["WorkingTitle"].ToString();
                        txtJobCode.Text = dsPositionRequest.Tables[0].Rows[0]["JobCode"].ToString();
                        txtBudgetNumber.Text = dsPositionRequest.Tables[0].Rows[0]["BudgetNumber"].ToString();
                        txtPositionNumber.Text = dsPositionRequest.Tables[0].Rows[0]["PositionNumber"].ToString();
                        cboCollegeUnit.SelectedValue = dsPositionRequest.Tables[0].Rows[0]["CollegeUnit"].ToString();
                        hidDepartment.Value = dsPositionRequest.Tables[0].Rows[0]["Department"].ToString();
                        hidDepartmentID.Value = dsPositionRequest.Tables[0].Rows[0]["DepartmentID"].ToString();
                        txtPhoneNumber.Text = dsPositionRequest.Tables[0].Rows[0]["PhoneNumber"].ToString();
                        txtMailStop.Text = dsPositionRequest.Tables[0].Rows[0]["MailStop"].ToString();
                        txtSupervisorName.Text = dsPositionRequest.Tables[0].Rows[0]["SupervisorName"].ToString();
                        hidSupervisorID.Value = dsPositionRequest.Tables[0].Rows[0]["SupervisorID"].ToString();
                        lblStatus.Text = dsPositionRequest.Tables[0].Rows[0]["Status"].ToString();
                        lblTrackingNumber.Text = dsPositionRequest.Tables[0].Rows[0]["TrackingNumber"].ToString();
                        if (lblTrackingNumber.Text == "") {
                            panTrackingNumber.Visible = false;
                        } else {
                            panTrackingNumber.Visible = true;
                        }
                        if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["NewPosition"]) == 1) {
                            optNew.Checked = true;
                        } else if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["ReplacementPosition"]) == 1) {
                            optReplacement.Checked = true;
                            txtEmployeeReplaced.Text = dsPositionRequest.Tables[0].Rows[0]["ReplacedEmployeeName"].ToString();
                        }
                        cboPositionType.SelectedValue = dsPositionRequest.Tables[0].Rows[0]["PositionType"].ToString();
                        if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["PermanentPosition"]) == 1) {
                            optPermanent.Checked = true;
                        } else if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["NonPermanentPosition"]) == 1) {
                            optNonPermanent.Checked = true;
                            try {
                                txtEndDate.Value = Convert.ToDateTime(dsPositionRequest.Tables[0].Rows[0]["PositionEndDate"]).ToShortDateString();
                            } catch {
                                //do nothing - no date exists
                            }
                        }
                        cboRecruitmentType.SelectedValue = dsPositionRequest.Tables[0].Rows[0]["RecruitmentType"].ToString();
                        if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["PCard"]) == 1) {
                            chkPCard.Checked = true;
                        }
                        if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["CCSIssuedCellPhone"]) == 1) {
                            chkCellPhone.Checked = true;
                        }
                        if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["CTCLinkAccess"]) == 1) {
                            chkAdditionalAccess.Checked = true;
                        }
                        if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["TimeSheetApprover"]) == 1) {
                            chkTimeSheetApprover.Checked = true;
                        }
                        txtHoursPerDay.Text = dsPositionRequest.Tables[0].Rows[0]["HoursPerDay"].ToString();
                        txtHoursPerWeek.Text = dsPositionRequest.Tables[0].Rows[0]["HoursPerWeek"].ToString();
                        txtMonthsPerYear.Text = dsPositionRequest.Tables[0].Rows[0]["MonthsPerYear"].ToString();
                        txtCyclicCalendarCode.Text = dsPositionRequest.Tables[0].Rows[0]["CyclicCalendarCode"].ToString();
                    }

                    DataSet dsWorkSchedule = positionRequest.GetWorkSchedule(positionRequestID);
                    if (dsWorkSchedule.Tables[0].Rows.Count > 0) {
                        String mondayStart = dsWorkSchedule.Tables[0].Rows[0]["MondayStart"].ToString();
                        String mondayEnd = dsWorkSchedule.Tables[0].Rows[0]["MondayEnd"].ToString();
                        String tuesdayStart = dsWorkSchedule.Tables[0].Rows[0]["TuesdayStart"].ToString();
                        String tuesdayEnd = dsWorkSchedule.Tables[0].Rows[0]["TuesdayEnd"].ToString();
                        String wednesdayStart = dsWorkSchedule.Tables[0].Rows[0]["WednesdayStart"].ToString();
                        String wednesdayEnd = dsWorkSchedule.Tables[0].Rows[0]["WednesdayEnd"].ToString();
                        String thursdayStart = dsWorkSchedule.Tables[0].Rows[0]["ThursdayStart"].ToString();
                        String thursdayEnd = dsWorkSchedule.Tables[0].Rows[0]["ThursdayEnd"].ToString();
                        String fridayStart = dsWorkSchedule.Tables[0].Rows[0]["FridayStart"].ToString();
                        String fridayEnd = dsWorkSchedule.Tables[0].Rows[0]["FridayEnd"].ToString();
                        String saturdayStart = dsWorkSchedule.Tables[0].Rows[0]["SaturdayStart"].ToString();
                        String saturdayEnd = dsWorkSchedule.Tables[0].Rows[0]["SaturdayEnd"].ToString();
                        String sundayStart = dsWorkSchedule.Tables[0].Rows[0]["SundayStart"].ToString();
                        String sundayEnd = dsWorkSchedule.Tables[0].Rows[0]["SundayEnd"].ToString();
                        if (mondayStart != "") {
                            txtMondayStart.Value = Convert.ToDateTime(mondayStart).ToString("%h:mm tt");
                        }
                        if (mondayEnd != "") {
                            txtMondayEnd.Value = Convert.ToDateTime(mondayEnd).ToString("%h:mm tt");
                        }
                        if (tuesdayStart != "") {
                            txtTuesdayStart.Value = Convert.ToDateTime(tuesdayStart).ToString("%h:mm tt");
                        }
                        if (tuesdayEnd != "") {
                            txtTuesdayEnd.Value = Convert.ToDateTime(tuesdayEnd).ToString("%h:mm tt");
                        }
                        if (wednesdayStart != "") {
                            txtWednesdayStart.Value = Convert.ToDateTime(wednesdayStart).ToString("%h:mm tt");
                        }
                        if (wednesdayEnd != "") {
                            txtWednesdayEnd.Value = Convert.ToDateTime(wednesdayEnd).ToString("%h:mm tt");
                        }
                        if (thursdayStart != "") {
                            txtThursdayStart.Value = Convert.ToDateTime(thursdayStart).ToString("%h:mm tt");
                        }
                        if (thursdayEnd != "") {
                            txtThursdayEnd.Value = Convert.ToDateTime(thursdayEnd).ToString("%h:mm tt");
                        }
                        if (fridayStart != "") {
                            txtFridayStart.Value = Convert.ToDateTime(fridayStart).ToString("%h:mm tt");
                        }
                        if (fridayEnd != "") {
                            txtFridayEnd.Value = Convert.ToDateTime(fridayEnd).ToString("%h:mm tt");
                        }
                        if (saturdayStart != "") {
                            txtSaturdayStart.Value = Convert.ToDateTime(saturdayStart).ToString("%h:mm tt");
                        }
                        if (saturdayEnd != "") {
                            txtSaturdayEnd.Value = Convert.ToDateTime(saturdayEnd).ToString("%h:mm tt");
                        }
                        if (sundayStart != "") {
                            txtSundayStart.Value = Convert.ToDateTime(sundayStart).ToString("%h:mm tt");
                        }
                        if (sundayEnd != "") {
                            txtSundayEnd.Value = Convert.ToDateTime(sundayEnd).ToString("%h:mm tt");
                        }
                    }

                    hidPopulateFromDB.Value = "false";
                }

                //get uploaded file attachments
                hidFileAttachments.Value = "";
                DataSet dsAttachments = positionRequest.GetAttachments(positionRequestID);
                Int32 attachmentCount = dsAttachments.Tables[0].Rows.Count;
                if (attachmentCount > 0) {
                    for (Int32 i = 0; i < attachmentCount; i++) {
                        hidFileAttachments.Value += "|" + dsAttachments.Tables[0].Rows[i]["FileName"].ToString();
                    }
                }
                populateFileAttachments();

                panCheckbox.Visible = true;
                panInput.Visible = true;
                panForm.Visible = true;
                cmdBack.Visible = true;
                cmdSubmit.Visible = true;
                cmdPrint.Visible = true;

            } else if (todo == "confirm") {

                Int32 positionRequestID = Convert.ToInt32(Request.QueryString["id"]);
                hidID.Value = positionRequestID.ToString();

                DataSet dsPositionRequest = positionRequest.GetPositionRequest(positionRequestID);
                if (dsPositionRequest.Tables[0].Rows.Count > 0) {
                    String task = Request.QueryString["task"];
                    String notified = Request.QueryString["notified"];
                    String notNotified = Request.QueryString["notNotified"];
                    lblConfirm.Text = "<p><strong>The following form has been " + task + ".</strong><br /></p>";
                    if (notified != "") {
                        lblConfirm.Text += "<p><strong>Notification has been sent to:</strong>" + notified.Replace("|", "<br />") + "</p>";
                    }
                    if (notNotified != "") {
                        lblConfirm.Text += "<p><strong>Notification could not be sent to:</strong>" + notNotified.Replace("|", "<br />") + "</p>";
                    }
                    lblConfirm.Text += "<p>To view previous approvals, click on the PR Report link in the menu to the left.<br /></p>"; lblOriginatorID.Text = dsPositionRequest.Tables[0].Rows[0]["OriginatorID"].ToString();
                    lblOriginatorName.Text = dsPositionRequest.Tables[0].Rows[0]["OriginatorName"].ToString();
                    lblTrackingNumber.Text = dsPositionRequest.Tables[0].Rows[0]["TrackingNumber"].ToString();
                    lblStatus.Text = dsPositionRequest.Tables[0].Rows[0]["Status"].ToString();
                    lblOfficialTitle.Text = dsPositionRequest.Tables[0].Rows[0]["OfficialTitle"].ToString();
                    lblWorkingTitle.Text = dsPositionRequest.Tables[0].Rows[0]["WorkingTitle"].ToString();
                    lblJobCode.Text = dsPositionRequest.Tables[0].Rows[0]["JobCode"].ToString();
                    lblBudgetNumber.Text = dsPositionRequest.Tables[0].Rows[0]["BudgetNumber"].ToString();
                    lblPositionNumber.Text = dsPositionRequest.Tables[0].Rows[0]["PositionNumber"].ToString();
                    lblCollegeUnit.Text = dsPositionRequest.Tables[0].Rows[0]["CollegeUnit"].ToString(); //may need to add conditional logic to show scc, sfcc, or dist
                    lblDepartmentName.Text = dsPositionRequest.Tables[0].Rows[0]["Department"].ToString();
                    lblDepartmentID2.Text = dsPositionRequest.Tables[0].Rows[0]["DepartmentID"].ToString();
                    lblPhoneNumber.Text = dsPositionRequest.Tables[0].Rows[0]["PhoneNumber"].ToString();
                    lblMailStop.Text = dsPositionRequest.Tables[0].Rows[0]["MailStop"].ToString();
                    lblSupervisorName.Text = dsPositionRequest.Tables[0].Rows[0]["SupervisorName"].ToString();
                    lblSupervisorID2.Text = dsPositionRequest.Tables[0].Rows[0]["SupervisorID"].ToString();
                    if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["NewPosition"]) == 1) {
                        lblNewReplacement.Text = "New Position";
                        employeeReplaced2.Visible = false;
                    } else if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["ReplacementPosition"]) == 1) {
                        lblNewReplacement.Text = "Replacement Position";
                        lblEmployeeReplaced.Text = dsPositionRequest.Tables[0].Rows[0]["ReplacedEmployeeName"].ToString();
                        employeeReplaced2.Visible = true;
                    }
                    lblPositionType.Text = dsPositionRequest.Tables[0].Rows[0]["PositionType"].ToString();
                    if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["PermanentPosition"]) == 1) {
                        lblClassified.Text = "Permanent";
                        lblClassified.Visible = true;
                    }
                    if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["NonPermanentPosition"]) == 1) {
                        lblClassified.Text = "Non Permanent";
                        lblClassified.Visible = true;
                        try {
                            lblEndDate.Text = Convert.ToDateTime(dsPositionRequest.Tables[0].Rows[0]["PositionEndDate"]).ToShortDateString();
                            lblEndDate.Visible = true;
                        } catch {
                            //do nothing - no date exists
                        }
                    } else {
                        endDate2.Visible = false;
                    }
                    lblRecruitmentType.Text = dsPositionRequest.Tables[0].Rows[0]["RecruitmentType"].ToString();
                    if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["PCard"]) == 1) {
                        lblOnboarding.Text += "<div style=\"padding-bottom:5px;\">P-Card</div>";
                    }
                    if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["CCSIssuedCellPhone"]) == 1) {
                        lblOnboarding.Text += "<div style=\"padding-bottom:5px;\">CCS-issued Cell Phone</div>";
                    }
                    if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["CTCLinkAccess"]) == 1) {
                        lblOnboarding.Text += "<div style=\"padding-bottom:5px;\">Additional ctcLink Security Access</div>";
                    }
                    if (Convert.ToByte(dsPositionRequest.Tables[0].Rows[0]["TimeSheetApprover"]) == 1) {
                        lblOnboarding.Text += "<div style=\"padding-bottom:5px;\">Manager/Timesheet Approver</div>";
                    }
                    if (lblPositionType.Text == "Classified" || lblPositionType.Text == "Part-Time Hourly") {
                        lblHoursPerDay.Text = dsPositionRequest.Tables[0].Rows[0]["HoursPerDay"].ToString();
                        lblHoursPerWeek.Text = dsPositionRequest.Tables[0].Rows[0]["HoursPerWeek"].ToString();
                        lblMonthsPerYear.Text = dsPositionRequest.Tables[0].Rows[0]["MonthsPerYear"].ToString();
                        lblCyclicCalendarCode.Text = dsPositionRequest.Tables[0].Rows[0]["CyclicCalendarCode"].ToString();
                        if (lblCyclicCalendarCode.Text != "") {
                            cyclicCalendarCode2.Attributes["style"] = cyclicCalendarCode2.Attributes["style"].Replace("display: none;", "");
                        }

                        DataSet dsWorkSchedule = positionRequest.GetWorkSchedule(positionRequestID);
                        if (dsWorkSchedule.Tables[0].Rows.Count > 0) {
                            String mondayStart = dsWorkSchedule.Tables[0].Rows[0]["MondayStart"].ToString();
                            String mondayEnd = dsWorkSchedule.Tables[0].Rows[0]["MondayEnd"].ToString();
                            String tuesdayStart = dsWorkSchedule.Tables[0].Rows[0]["TuesdayStart"].ToString();
                            String tuesdayEnd = dsWorkSchedule.Tables[0].Rows[0]["TuesdayEnd"].ToString();
                            String wednesdayStart = dsWorkSchedule.Tables[0].Rows[0]["WednesdayStart"].ToString();
                            String wednesdayEnd = dsWorkSchedule.Tables[0].Rows[0]["WednesdayEnd"].ToString();
                            String thursdayStart = dsWorkSchedule.Tables[0].Rows[0]["ThursdayStart"].ToString();
                            String thursdayEnd = dsWorkSchedule.Tables[0].Rows[0]["ThursdayEnd"].ToString();
                            String fridayStart = dsWorkSchedule.Tables[0].Rows[0]["FridayStart"].ToString();
                            String fridayEnd = dsWorkSchedule.Tables[0].Rows[0]["FridayEnd"].ToString();
                            String saturdayStart = dsWorkSchedule.Tables[0].Rows[0]["SaturdayStart"].ToString();
                            String saturdayEnd = dsWorkSchedule.Tables[0].Rows[0]["SaturdayEnd"].ToString();
                            String sundayStart = dsWorkSchedule.Tables[0].Rows[0]["SundayStart"].ToString();
                            String sundayEnd = dsWorkSchedule.Tables[0].Rows[0]["SundayEnd"].ToString();
                            if (mondayStart != "" || mondayEnd != "") {
                                if (mondayStart != "") {
                                    mondayStart = Convert.ToDateTime(mondayStart).ToString("%h:mm tt");
                                }
                                if (mondayEnd != "") {
                                    mondayEnd = Convert.ToDateTime(mondayEnd).ToString("%h:mm tt");
                                }
                                lblMonday.Text = mondayStart + "<br />to<br />" + mondayEnd;
                            }
                            if (tuesdayStart != "" || tuesdayEnd != "") {
                                if (tuesdayStart != "") {
                                    tuesdayStart = Convert.ToDateTime(tuesdayStart).ToString("%h:mm tt");
                                }
                                if (tuesdayEnd != "") {
                                    tuesdayEnd = Convert.ToDateTime(tuesdayEnd).ToString("%h:mm tt");
                                }
                                lblTuesday.Text = tuesdayStart + "<br />to<br />" + tuesdayEnd;
                            }
                            if (wednesdayStart != "" || wednesdayEnd != "") {
                                if (wednesdayStart != "") {
                                    wednesdayStart = Convert.ToDateTime(wednesdayStart).ToString("%h:mm tt");
                                }
                                if (wednesdayEnd != "") {
                                    wednesdayEnd = Convert.ToDateTime(wednesdayEnd).ToString("%h:mm tt");
                                }
                                lblWednesday.Text = wednesdayStart + "<br />to<br />" + wednesdayEnd;
                            }
                            if (thursdayStart != "" || thursdayEnd != "") {
                                if (thursdayStart != "") {
                                    thursdayStart = Convert.ToDateTime(thursdayStart).ToString("%h:mm tt");
                                }
                                if (thursdayEnd != "") {
                                    thursdayEnd = Convert.ToDateTime(thursdayEnd).ToString("%h:mm tt");
                                }
                                lblThursday.Text = thursdayStart + "<br />to<br />" + thursdayEnd;
                            }
                            if (fridayStart != "" || fridayEnd != "") {
                                if (fridayStart != "") {
                                    fridayStart = Convert.ToDateTime(fridayStart).ToString("%h:mm tt");
                                }
                                if (fridayEnd != "") {
                                    fridayEnd = Convert.ToDateTime(fridayEnd).ToString("%h:mm tt");
                                }
                                lblFriday.Text = fridayStart + "<br />to<br />" + fridayEnd;
                            }
                            if (saturdayStart != "" || saturdayEnd != "") {
                                if (saturdayStart != "") {
                                    saturdayStart = Convert.ToDateTime(saturdayStart).ToString("%h:mm tt");
                                }
                                if (saturdayEnd != "") {
                                    saturdayEnd = Convert.ToDateTime(saturdayEnd).ToString("%h:mm tt");
                                }
                                lblSaturday.Text = saturdayStart + "<br />to<br />" + saturdayEnd;
                            }
                            if (sundayStart != "" || sundayEnd != "") {
                                if (sundayStart != "") {
                                    sundayStart = Convert.ToDateTime(sundayStart).ToString("%h:mm tt");
                                }
                                if (sundayEnd != "") {
                                    sundayEnd = Convert.ToDateTime(sundayEnd).ToString("%h:mm tt");
                                }
                                lblSunday.Text = sundayStart + "<br />to<br />" + sundayEnd;
                            }
                            schedule2.Visible = true;
                        }
                    } else {
                        schedule2.Visible = false;
                    }
                }

                //show uploaded file attachments
                DataSet dsAttachments = positionRequest.GetAttachments(positionRequestID);
                Int32 attachmentCount = dsAttachments.Tables[0].Rows.Count;
                if (attachmentCount > 0) {
                    for (Int32 i = 0; i < attachmentCount; i++) {
                        lblAttachments.Text += "<div style=\"padding-bottom:5px;\"><a href=\"..\\Attachments/" + dsAttachments.Tables[0].Rows[i]["FileName"].ToString() + "\" target=\"self\" class=\"smallText\" title=\"View File Attachment " + (i + 1) + "\">Attachment " + (i + 1) + "</a></div>";
                    }
                    panViewFileAttachments.Visible = true;
                } else {
                    panViewFileAttachments.Visible = false;
                }

                //get Routing/Approvals
                DataSet dsRouting = positionRequest.GetRouting(positionRequestID);
                Int32 routingCount = dsRouting.Tables[0].Rows.Count;
                if (routingCount > 0) {
                    for (Int32 i = 0; i < routingCount; i++) {
                        String note = dsRouting.Tables[0].Rows[i]["Note"].ToString();
                        String description = dsRouting.Tables[0].Rows[i]["Description"].ToString();
                        TableRow tr = new TableRow();
                        TableCell td = new TableCell();
                        td.CssClass = "solid";
                        td.Attributes["style"] = "vertical-align:top;padding-left:10px;width:75px";
                        td.Controls.Add(new LiteralControl(dsRouting.Tables[0].Rows[i]["UserType"].ToString()));
                        tr.Cells.Add(td);
                        td = new TableCell();
                        td.CssClass = "solid";
                        td.Attributes["style"] = "vertical-align:top;width:190px";
                        td.Controls.Add(new LiteralControl(dsRouting.Tables[0].Rows[i]["Name"].ToString()));
                        tr.Cells.Add(td);
                        td = new TableCell();
                        td.CssClass = "solid";
                        td.Attributes["style"] = "vertical-align:top";
                        td.Controls.Add(new LiteralControl(description));
                        if (description != "Pending Approval") {
                            td.Controls.Add(new LiteralControl("&nbsp;" + Convert.ToDateTime(dsRouting.Tables[0].Rows[i]["Date"]).ToString("MM/dd/yy %h:mm tt")));
                        }
                        if (note != "") {
                            td.Controls.Add(new LiteralControl("<br />" + note));
                        }
                        tr.Cells.Add(td);
                        tblApprovals.Rows.Add(tr);
                    }
                    panApprovals.Visible = true;
                } else {
                    panApprovals.Visible = false;
                }

                lblConfirm.Visible = true;
                cmdBack.Visible = false;
                cmdSubmit.Visible = false;
                panAddAttachments.Visible = false; //set this in page load? then show for view and edit?
                panForm.Visible = true;
                panOutput.Visible = true;
                cmdPrint.Visible = true;
                cmdOK.Visible = true;

            } else {

                DataSet dsPositionRequests = positionRequest.GetPositionRequestsForBudgetApproval();
                Int32 rowCount = dsPositionRequests.Tables[0].Rows.Count;
                if (rowCount > 0) {
                    for (Int32 i = 0; i < rowCount; i++) {
                        Int32 positionRequestID = Convert.ToInt32(dsPositionRequests.Tables[0].Rows[i]["PositionRequestID"]);
                        String created = "";
                        try { //temporary until created date is required and added to the db when form is saved and updated
                            created = Convert.ToDateTime(dsPositionRequests.Tables[0].Rows[i]["Created"]).ToShortDateString();
                        } catch {
                            //do nothing
                        }

                        TableRow tr = new TableRow();
                        TableCell td = new TableCell();
                        td.CssClass = "solid";
                        td.Controls.Add(new LiteralControl(dsPositionRequests.Tables[0].Rows[i]["OriginatorName"].ToString()));
                        tr.Cells.Add(td);

                        td = new TableCell();
                        td.CssClass = "solid";
                        td.Controls.Add(new LiteralControl(created));
                        tr.Cells.Add(td);

                        td = new TableCell();
                        td.CssClass = "solid";
                        td.Controls.Add(new LiteralControl(dsPositionRequests.Tables[0].Rows[i]["OfficialTitle"].ToString()));
                        tr.Cells.Add(td);

                        td = new TableCell();
                        td.CssClass = "solid";
                        td.Controls.Add(new LiteralControl(dsPositionRequests.Tables[0].Rows[i]["PositionType"].ToString()));
                        tr.Cells.Add(td);

                        td = new TableCell();
                        td.CssClass = "action";
                        td.Attributes["style"] = "vertical-align:top;width:16px;background:#eee none";
                        td.Controls.Add(new LiteralControl("<a href=\"#\" class=\"view\" title=\"View Form for Approval\" onclick=\"document.getElementById('" + hidTodo.ClientID + "').value='view';document.getElementById('" + hidID.ClientID + "').value='" + positionRequestID + "';document.getElementById('" + hidCreated.ClientID + "').value='" + created + "';document.getElementById('" + frmPR.ClientID + "').submit();\"></a>"));
                        tr.Cells.Add(td);
                        tblList.Rows.Add(tr);
                    }
                } else {
                    TableRow tr = new TableRow();
                    TableCell td = new TableCell();
                    td.ColumnSpan = 5;
                    td.Controls.Add(new LiteralControl("No forms currently require your approval."));
                    tr.Cells.Add(td);
                    tblList.Rows.Add(tr);
                }
                panList.Visible = true;
            }
        }
    }


    private void populateFileAttachments() {
        tblAttachments.Rows.Clear();
        if (hidFileAttachments.Value != "") {
            String[] strFileName = hidFileAttachments.Value.Substring(1).Split('|');
            for (Int32 i = 0; i < strFileName.Length; i++) {
                TableRow tr = new TableRow();
                TableCell td = new TableCell();
                tr = new TableRow();
                td = new TableCell();
                td.Attributes["style"] = "background:none;border:none;padding-left:10px;padding-top:10px;";
                if (i < (strFileName.Length - 1)) {
                    td.Attributes["style"] += "border-bottom:solid 1px #aaa";
                }
                td.Controls.Add(new LiteralControl("<a href=\"..\\Attachments/" + strFileName[i] + "\" target=\"self\" class=\"smallText\" title=\"View File Attachment " + (i + 1) + "\">Attachment " + (i + 1) + "</a>"));
                tr.Cells.Add(td);
                td = new TableCell();
                td.CssClass = "action";
                td.Attributes["style"] = "background:none;border:none;vertical-align:top;padding-top:10px;width:16px;";
                if (i < (strFileName.Length - 1)) {
                    td.Attributes["style"] += "border-bottom:solid 1px #aaa";
                }
                LinkButton lnkDeleteAttachment = new LinkButton();
                lnkDeleteAttachment.ID = "lnkDeleteAttachment" + i;
                lnkDeleteAttachment.CssClass = "delete";
                lnkDeleteAttachment.Text = "Delete File Attachment " + (i + 1);
                lnkDeleteAttachment.OnClientClick = "return confirm('Are you sure you want to delete file attachment " + (i + 1) + "?\\nNote: Any existing file attachments will be renumbered.');";
                lnkDeleteAttachment.CommandArgument = strFileName[i];
                lnkDeleteAttachment.Click += new EventHandler(lnkDeleteAttachment_Click);
                td.Controls.Add(lnkDeleteAttachment);
                tr.Cells.Add(td);
                tblAttachments.Rows.Add(tr);
            }
            panEditFileAttachments.Visible = true;
        } else {
            panEditFileAttachments.Visible = false;
        }
    }

    protected void lnkDeleteAttachment_Click(object sender, System.EventArgs e) {
        //get clicked linkbutton
        LinkButton lnkDelete = (LinkButton)sender;
        //get the file name
        String strFileName = lnkDelete.CommandArgument.ToString();
        //delete the file from the server
        System.IO.File.Delete(Server.MapPath("..\\Attachments\\") + strFileName);
        //delete the file from the database
        positionRequest.DeleteAttachment(strFileName);
        //delete the file name from the hidden form element
        hidFileAttachments.Value = hidFileAttachments.Value.Replace("|" + strFileName, "");
        //populate file attachments
        populateFileAttachments();
    }

    protected void cmdSubmit_Click(object sender, System.EventArgs e) {
        String employeeID = Request.Cookies["phatt2"]["userctclinkid"], selectedOption = Request.Form["ctl00$maintextHolder$optApproval"], rejectReason = Request.Form["ctl00$maintextHolder$txtReject"];
        String name = positionRequest.GetEmployeeName(employeeID), notified = "", notNotified = "", task = "", status = lblStatus.Text;
        Int32 positionRequestID = Convert.ToInt32(hidID.Value);
        bool blnSuccess = true;

        //upload file attachments
        HttpFileCollection uploads = HttpContext.Current.Request.Files;
        for (int i = 0; i < uploads.Count; i++) {
            HttpPostedFile upload = uploads[i];
            if (upload.ContentLength == 0) {
                continue;
            }
            String c = System.IO.Path.GetFileName(upload.FileName);
            try {
                String strFileName = positionRequestID + "_" + i + System.IO.Path.GetExtension(c);
                upload.SaveAs(Server.MapPath("..\\Attachments\\") + strFileName); //test this
                positionRequest.AddAttachment(positionRequestID, strFileName);
            } catch {
                blnSuccess = false;
            }
        }

        if (blnSuccess) {
            if (chkModify.Checked) {
                //store bit field values
                Int16 permanentPosition = 0, nonPermanentPosition = 0, newPosition = 0, replacementPosition = 0, pCard = 0, CCSIssuedCellPhone = 0, ctcLinkAccess = 0, timeSheetApprover = 0;

                if (optPermanent.Checked) {
                    permanentPosition = 1;
                }
                if (optNonPermanent.Checked) {
                    nonPermanentPosition = 1;
                }
                if (optNew.Checked) {
                    newPosition = 1;
                }
                if (optReplacement.Checked) {
                    replacementPosition = 1;
                }
                if (chkPCard.Checked) {
                    pCard = 1;
                }
                if (chkCellPhone.Checked) {
                    CCSIssuedCellPhone = 1;
                }
                if (chkAdditionalAccess.Checked) {
                    ctcLinkAccess = 1;
                }
                if (chkTimeSheetApprover.Checked) {
                    timeSheetApprover = 1;
                }

                positionRequestID = Convert.ToInt32(hidID.Value);
                blnSuccess = positionRequest.EditPositionRequest(positionRequestID, lblOriginatorName.Text, txtOfficialTitle.Text, txtWorkingTitle.Text, txtJobCode.Text, cboCollegeUnit.SelectedValue, txtBudgetNumber.Text, txtPositionNumber.Text, hidDepartmentID.Value, hidDepartment.Value, txtPhoneNumber.Text, txtMailStop.Text, txtSupervisorName.Text, hidSupervisorID.Value, cboPositionType.Text, permanentPosition, nonPermanentPosition, txtEndDate.Value, cboRecruitmentType.SelectedValue, newPosition, replacementPosition, txtEmployeeReplaced.Text, pCard, CCSIssuedCellPhone, ctcLinkAccess, timeSheetApprover, txtHoursPerDay.Text, txtHoursPerWeek.Text, txtMonthsPerYear.Text, txtCyclicCalendarCode.Text, status);

                if (blnSuccess) {
                    task = "modified";

                    //add work schedule
                    if (cboPositionType.SelectedValue == "Classified" || cboPositionType.SelectedValue == "Part-Time Hourly") {
                        blnSuccess = positionRequest.EditWorkSchedule(positionRequestID, txtMondayStart.Value, txtMondayEnd.Value, txtTuesdayStart.Value, txtTuesdayEnd.Value, txtWednesdayStart.Value, txtWednesdayEnd.Value, txtThursdayStart.Value, txtThursdayEnd.Value, txtFridayStart.Value, txtFridayEnd.Value, txtSaturdayStart.Value, txtSaturdayEnd.Value, txtSundayStart.Value, txtSundayEnd.Value);
                        if (blnSuccess == false) {
                            lblError.Text = "<p><strong>Error: Submitting the work schedule failed.</strong></p>" + strErrorContact + "<br />";
                        }
                    } else {
                        //delete the work schedule
                        positionRequest.DeleteWorkSchedule(positionRequestID);
                    }

                    //store when budget modifed the form
                    positionRequest.AddRouting(positionRequestID, "Budget", "Modified", txtModReason.Text, name, employeeID, DateTime.Now.ToString("MM/dd/yy HH:mm:ss.fff"));

                    SmtpClient client = new SmtpClient();
                    MailMessage objEmail = new MailMessage();
                    String emailBody = "";

                    if (selectedOption == "optRestart") {//restart the approval process
                        //get the originator's supervisor
                        DataSet dsSupervisor = positionRequest.GetSupervisor(lblOriginatorID.Text);
                        String supervisorJobCode = "", supervisorEmail = "", supervisorID = "", supervisorName = "", menu = "Supervisor";
                        if (dsSupervisor.Tables[0].Rows.Count > 0) {
                            supervisorJobCode = dsSupervisor.Tables[0].Rows[0]["JOBCODE"].ToString();
                            supervisorEmail = dsSupervisor.Tables[0].Rows[0]["EMAIL_ADDR"].ToString();
                            supervisorID = dsSupervisor.Tables[0].Rows[0]["EMPLID"].ToString();
                            supervisorName = dsSupervisor.Tables[0].Rows[0]["NAME"].ToString();
                            status = "Approver 1";

                            if (cboPositionType.SelectedValue != "Adjunct Faculty" && cboPositionType.SelectedValue != "Part-Time Hourly" && (String.Compare(hidDepartmentID.Value, "99001") < 0 || String.Compare(hidDepartmentID.Value, "99021") > 0)) { 
                                if (supervisorJobCode == "111113" || supervisorJobCode == "111133") {
                                    status = "Executive";
                                }
                            } else if (cboPositionType.SelectedValue == "Adjunct Faculty") {
                                if (supervisorJobCode == "111274" || supervisorJobCode == "111577" || supervisorJobCode == "111465" || supervisorJobCode == "111439" || supervisorJobCode == "111120") {
                                    status = "Budget";
                                    supervisorEmail = "vu.nguyen@ccs.spokane.edu"; //for testing
                                    //supervisorEmail = "BudgetPR@ccs.spokane.edu"; 
                                    supervisorName = "";
                                    supervisorID = "";
                                    menu = "Budget Admin";
                                }
                            } else if (cboPositionType.SelectedValue == "Part-Time Hourly" || (String.Compare(hidDepartmentID.Value, "99001") >= 0 && String.Compare(hidDepartmentID.Value, "99021") <= 0)) {
                                if (supervisorJobCode == "111272" || supervisorJobCode == "111165" || supervisorJobCode == "111319" || supervisorJobCode == "111433" || supervisorJobCode == "111274" || supervisorJobCode == "111577" || supervisorJobCode == "111465" || supervisorJobCode == "111439" || supervisorJobCode == "111120") {
                                    status = "Executive";
                                    if (supervisorJobCode == "111433") { //if Greg Stevens is the Executive approver
                                        //change the Executive approver to Melody Matthews
                                        supervisorJobCode = "111490";
                                        dsSupervisor = positionRequest.GetEmployeeByJobCode(supervisorJobCode);
                                        if (dsSupervisor.Tables[0].Rows.Count > 0) {
                                            supervisorEmail = dsSupervisor.Tables[0].Rows[0]["EMAIL_ADDR"].ToString();
                                            supervisorID = dsSupervisor.Tables[0].Rows[0]["EMPLID"].ToString();
                                            supervisorName = dsSupervisor.Tables[0].Rows[0]["NAME"].ToString();
                                        } else {
                                            lblError.Text = "<p><strong>Error: The approval process could not be restarted. The originator's supervisor data could not be found.</strong></p>" + strErrorContact + "<br />";
                                            blnSuccess = false;
                                        }
                                    }
                                }
                            }

                            if (blnSuccess) {
                                //stop budget approval
                                blnSuccess = positionRequest.EditApproval(positionRequestID, employeeID, name, "Budget", "Pending Approval", "Restarted Approval Process", "", DateTime.Now.AddMilliseconds(100).ToString("MM/dd/yy HH:mm:ss.fff"));
                                if (blnSuccess) {
                                    //restart approval process
                                    blnSuccess = positionRequest.AddRouting(positionRequestID, status, "Pending Approval", "", supervisorName, supervisorID, DateTime.Now.AddMilliseconds(200).ToString("MM/dd/yy HH:mm:ss.fff"));
                                    if (blnSuccess) {
                                        //change position request status
                                        blnSuccess = positionRequest.EditStatus(positionRequestID, status);

                                        if (blnSuccess) {
                                            if (status == "Budget") {
                                                supervisorName = "the Budget Admin";
                                            }

                                            //send email for approval
                                            client = new SmtpClient();
                                            objEmail = new MailMessage();
                                            emailBody = "<p><strong>Tracking #:</strong> " + lblTrackingNumber.Text +
                                                        "<br /><strong>Originator:</strong> " + lblOriginatorName.Text +
                                                        "<br /><strong>Created:</strong> " + hidCreated.Value +
                                                        "<br /><strong>College/Unit:</strong> " + cboCollegeUnit.Text +
                                                        "<br /><strong>Department:</strong> " + hidDepartment.Value +
                                                        "<br /><strong>Official Title:</strong> " + txtOfficialTitle.Text +
                                                        "<br /><strong>Supervisor Name:</strong> " + txtSupervisorName.Text +
                                                        "<br /><strong>Position Type:</strong> " + cboPositionType.Text +
                                                        "<br /><strong>Status:</strong> " + status + "</p>";

                                            if (supervisorEmail != "") {
                                                //objEmail.To.Add(supervisorEmail); 
                                                objEmail.To.Add("vu.nguyen@ccs.spokane.edu");
                                                objEmail.From = new MailAddress(strAdminEmailAddress);
                                                objEmail.IsBodyHtml = true;
                                                objEmail.Priority = MailPriority.High;
                                                objEmail.Subject = "Position Request Approval Notification";
                                                objEmail.Body = "<p>Please review the Position Request below for approval.</p>";
                                                objEmail.Body += "<p><a href=\"https://internal.spokane.edu/PositionRequest/\">https://internal.spokane.edu/PositionRequest/</a>";
                                                objEmail.Body += "<br />Log in with your Active Directory account and click on the PR Approval link in the " + menu + " menu.</p>";
                                                objEmail.Body += emailBody;

                                                try {
                                                    client.Send(objEmail);
                                                    notified += "|" + status + ": " + supervisorName;
                                                } catch {
                                                    objEmail = new MailMessage();
                                                    objEmail.To.Add(strAdminEmailAddress);
                                                    objEmail.From = new MailAddress(strAdminEmailAddress);
                                                    objEmail.IsBodyHtml = true;
                                                    objEmail.Priority = MailPriority.High;
                                                    objEmail.Subject = "Position Request Approval Notification Could Not Be Sent";
                                                    objEmail.Body = "<p>The following approval notification email could not be sent to " + supervisorName + " at the email address " + supervisorEmail + ".</p>";
                                                    objEmail.Body += "<hr style=\"width:100%\" />";
                                                    objEmail.Body += emailBody;

                                                    try {
                                                        client.Send(objEmail);
                                                        notNotified += "|" + status + ": " + supervisorName + ".|HRO has been notified.";
                                                    } catch {
                                                        notNotified += "|" + status + ": " + supervisorName + " or HRO.|Please contact HRO at " + strAdminEmailAddress + ".";
                                                    }
                                                }
                                            } else {
                                                objEmail = new MailMessage();
                                                objEmail.To.Add(strAdminEmailAddress);
                                                objEmail.From = new MailAddress(strAdminEmailAddress);
                                                objEmail.IsBodyHtml = true;
                                                objEmail.Priority = MailPriority.High;
                                                objEmail.Subject = "Position Request Approval Notification Could Not Be Sent";
                                                objEmail.Body = "<p>The following approval notification email could not be sent to " + supervisorName + " because their email address does not exist.</p>";
                                                objEmail.Body += "<hr style=\"width:100%\" />";
                                                objEmail.Body += emailBody;

                                                try {
                                                    client.Send(objEmail);
                                                    notNotified += "|" + status + ": " + supervisorName + ".|HRO has been notified.";
                                                } catch {
                                                    notNotified += "|" + status + ": " + supervisorName + " or HRO.|Please contact HRO at " + strAdminEmailAddress + ".";
                                                }
                                            }

                                            task += " and the approval process restarted";

                                        } else {
                                            lblError.Text = "<p><strong>Error: The Position Request status could not be updated to " + status + ".";
                                            //set routing record back to Pending Budget Approval
                                            blnSuccess = positionRequest.EditApproval(positionRequestID, employeeID, name, "Budget", "Restarted Approval Process", "Pending Approval", "", DateTime.Now.AddMilliseconds(100).ToString("MM/dd/yy HH:mm:ss.fff"));
                                            if (blnSuccess) {
                                                lblError.Text += " The position request has been set back to pending Budget approval.";
                                            } else {
                                                lblError.Text += " The position request could not be set back to pending Budget approval.";
                                            }
                                            lblError.Text += "</strong></p>" + strErrorContact + "<br />";
                                            blnSuccess = false;
                                        }
                                    } else {
                                        lblError.Text = "<p><strong>Error: The approval process could not be restarted. Unable to send to " + status + ".";
                                        //set routing record back to Pending Budget Approval
                                        blnSuccess = positionRequest.EditApproval(positionRequestID, employeeID, name, "Budget", "Restarted Approval Process", "Pending Approval", "", DateTime.Now.AddMilliseconds(100).ToString("MM/dd/yy HH:mm:ss.fff"));
                                        if (blnSuccess) {
                                            lblError.Text += " The position request has been set back to pending Budget approval.";
                                        } else {
                                            lblError.Text += " The position request could not be set back to pending Budget approval.";
                                        }
                                        lblError.Text += "</strong></p>" + strErrorContact + "<br />";
                                        blnSuccess = false;
                                    }
                                } else {
                                    lblError.Text = "<p><strong>Error: The approval process could not be restarted. Unable to record Budget Restart Approval record.</strong></p>" + strErrorContact + "<br />";
                                }
                            }
                        } else { 
                            lblError.Text = "<p><strong>Error: The approval process could not be restarted. The originator's supervisor data could not be found.</strong></p>" + strErrorContact + "<br />";
                            blnSuccess = false;
                        }
                    }

                    //notify originator of modification
                    client = new SmtpClient();
                    objEmail = new MailMessage();
                    String originatorEmail = positionRequest.GetEmployeeEmail(lblOriginatorID.Text);
                    emailBody = "<p><strong>Modified by:</strong> Budget: " + name +
                                "<br /><strong>Reason:</strong> " + txtModReason.Text +
                                "<br /><br /><strong>Tracking #:</strong> " + lblTrackingNumber.Text +
                                "<br /><strong>Originator:</strong> " + lblOriginatorName.Text +
                                "<br /><strong>Created:</strong> " + hidCreated.Value +
                                "<br /><strong>College/Unit:</strong> " + cboCollegeUnit.Text +
                                "<br /><strong>Department:</strong> " + hidDepartment.Value +
                                "<br /><strong>Official Title:</strong> " + txtOfficialTitle.Text +
                                "<br /><strong>Supervisor Name:</strong> " + txtSupervisorName.Text +
                                "<br /><strong>Position Type:</strong> " + cboPositionType.Text +
                                "<br /><strong>Status:</strong> " + status + "</p>";

                    objEmail.To.Add(originatorEmail); 
                    objEmail.To.Add("vu.nguyen@ccs.spokane.edu");
                    //objEmail.From = new MailAddress(strAdminEmailAddress);
                    objEmail.IsBodyHtml = true;
                    objEmail.Priority = MailPriority.High;
                    objEmail.Subject = "Position Request has been Modified by Budget";
                    objEmail.Body = "<p>Please review the Position Request below to view the modifications.";
                    if (selectedOption == "optRestart") {
                        objEmail.Body += "<br />The approval process has been restarted.";
                    }
                    objEmail.Body += "</p>";
                    objEmail.Body += "<p><a href=\"https://internal.spokane.edu/PositionRequest/\">https://internal.spokane.edu/PositionRequest/</a>";
                    objEmail.Body += "<br />Log in with your Active Directory account and click on the Employee Action Notice link in the Main Menu.</p>";
                    objEmail.Body += emailBody;

                    try {
                        client.Send(objEmail);
                        notified += "|Originator: " + lblOriginatorName.Text;
                    } catch {
                        objEmail = new MailMessage();
                        objEmail.To.Add(strAdminEmailAddress);
                        objEmail.From = new MailAddress(strAdminEmailAddress);
                        objEmail.IsBodyHtml = true;
                        objEmail.Priority = MailPriority.High;
                        objEmail.Subject = "Position Request Modification Notification Could Not Be Sent";
                        objEmail.Body = "<p>The following modification email could not be sent to the originator: " + lblOriginatorName.Text + " at the email address " + originatorEmail + ".</p>";
                        objEmail.Body += "<hr style=\"width:100%\" />";
                        objEmail.Body += emailBody;

                        try {
                            client.Send(objEmail);
                            notNotified += "|Originator: " + lblOriginatorName.Text + ". HRO has been notified.";
                        } catch {
                            notNotified += "|Originator: " + lblOriginatorName.Text + ". HRO could not be notified. Please contact HRO at " + strAdminEmailAddress + ".";
                        }
                    }
                } else {
                    lblError.Text = "<p><strong>Error: Submitting the modifications failed.</strong></p>" + strErrorContact + "<br />";
                }
            }

            if (blnSuccess && selectedOption == "optApprove") {

                //update Approval Record
                blnSuccess = positionRequest.EditApproval(positionRequestID, employeeID, name, lblStatus.Text, "Pending Approval", "Approved", "", DateTime.Now.ToString("MM/dd/yy HH:mm:ss.fff"));
                String approverEmail = "", approverName = "", approverID = "", menu = "Supervisor", error = "";

                if (blnSuccess) {
                    String positionType = lblPositionType.Text, collegeUnit = lblCollegeUnit.Text, officialTitle = lblOfficialTitle.Text, supervisorName = lblSupervisorName.Text;
                    if (chkModify.Checked) {
                        positionType = cboPositionType.SelectedValue;
                        collegeUnit = cboCollegeUnit.SelectedValue; 
                        officialTitle = txtOfficialTitle.Text;
                        supervisorName = txtSupervisorName.Text;
                    } else {
                        hidDepartment.Value = lblDepartmentName.Text;
                        hidDepartmentID.Value = lblDepartmentID2.Text;
                    }

                    if (positionType != "Adjunct Faculty" && positionType != "Part-Time Hourly" && (String.Compare(hidDepartmentID.Value, "99001") < 0 || String.Compare(hidDepartmentID.Value, "99021") > 0)) { //(Convert.ToInt32(lblDepartmentID.Text) < 99001 && Convert.ToInt32(lblDepartmentID.Text) > 99021)) {
                        DataSet dsApprover = positionRequest.GetEmployeeByJobCode("111133");
                        if (dsApprover.Tables[0].Rows.Count > 0) {
                            status = "Chancellor";
                            approverEmail = "vu.nguyen@ccs.spokane.edu"; //for testing
                            //approverEmail = dsApprover.Tables[0].Rows[0]["EMAIL_ADDR"].ToString();
                            approverName = dsApprover.Tables[0].Rows[0]["Name"].ToString();
                            approverID = dsApprover.Tables[0].Rows[0]["EMPLID"].ToString();
                        } else {
                            error = "Error: The Chancellor's contact information could not be found in the database.";
                        }
                    } else if (positionType == "Adjunct Faculty") {
                        status = "HRO";
                        approverEmail = "vu.nguyen@ccs.spokane.edu"; //for testing
                        //approverEmail = "HROPR@ccs.spokane.edu"; 
                        approverName = "";
                        approverID = "";
                        menu = "HRO Admin";
                    } else if (positionType == "Part-Time Hourly" || (String.Compare(hidDepartmentID.Value, "99001") >= 0 && String.Compare(hidDepartmentID.Value, "99021") <= 0)) {
                        //Set Executive approver as Melody Matthews 
                        DataSet dsApprover = positionRequest.GetEmployeeByJobCode("111490"); //positionRequest.GetEmployeeByJobCode("111433"); //Greg Stevens job code
                        if (dsApprover.Tables[0].Rows.Count > 0) {
                            status = "Executive";
                            approverEmail = "vu.nguyen@ccs.spokane.edu"; //for testing
                            //approverEmail = dsApprover.Tables[0].Rows[0]["EMAIL_ADDR"].ToString();
                            approverName = dsApprover.Tables[0].Rows[0]["Name"].ToString();
                            approverID = dsApprover.Tables[0].Rows[0]["EMPLID"].ToString();
                        } else {
                            error = "Error: The Executive Approver's contact information could not be found in the database.";
                        }
                    }

                    if (error == "") {
                        try {
                            //add pending approval routing record
                            blnSuccess = positionRequest.AddRouting(positionRequestID, status, "Pending Approval", "", approverName, approverID, DateTime.Now.AddMilliseconds(100).ToString("MM/dd/yy HH:mm:ss.fff"));

                            if (blnSuccess) { 
                                //update position request status
                                blnSuccess = positionRequest.EditStatus(positionRequestID, status); //may need to check boolean return value to make sure status is updated and notify HR if not

                                if (blnSuccess) {
                                    if (status == "HRO") {
                                        approverName = "the HRO Admin";
                                    }

                                    //send email for approval
                                    SmtpClient client = new SmtpClient();
                                    MailMessage objEmail = new MailMessage();
                                    String emailBody = "<p><strong>Tracking #:</strong> " + lblTrackingNumber.Text +
                                                          "<br /><strong>Originator:</strong> " + lblOriginatorName.Text +
                                                          "<br /><strong>Created:</strong> " + hidCreated.Value +
                                                          "<br /><strong>College/Unit:</strong> " + collegeUnit +
                                                          "<br /><strong>Department:</strong> " + hidDepartment.Value +
                                                          "<br /><strong>Official Title:</strong> " + officialTitle +
                                                          "<br /><strong>Supervisor Name:</strong> " + supervisorName +
                                                          "<br /><strong>Position Type:</strong> " + positionType +
                                                          "<br /><strong>Status:</strong> " + status + "</p>";

                                    if (approverEmail != "") {
                                        objEmail.To.Add(approverEmail); 
                                        objEmail.To.Add("vu.nguyen@ccs.spokane.edu");
                                        //objEmail.From = new MailAddress(strAdminEmailAddress);
                                        objEmail.IsBodyHtml = true;
                                        objEmail.Priority = MailPriority.High;
                                        objEmail.Subject = "Position Request Approval Notification";
                                        objEmail.Body = "<p>Please review the Position Request below for approval.</p>";
                                        objEmail.Body += "<p><a href=\"https://internal.spokane.edu/PositionRequest/\">https://internal.spokane.edu/PositionRequest/</a>";
                                        objEmail.Body += "<br />Log in with your Active Directory account and click on the PR Approval link in the " + menu + " menu.</p>";
                                        objEmail.Body += emailBody;

                                        try {
                                            client.Send(objEmail);
                                            notified += "|" + status + ": " + approverName;
                                        } catch {
                                            objEmail = new MailMessage();
                                            objEmail.To.Add(strAdminEmailAddress);
                                            objEmail.From = new MailAddress(strAdminEmailAddress);
                                            objEmail.IsBodyHtml = true;
                                            objEmail.Priority = MailPriority.High;
                                            objEmail.Subject = "Position Request Approval Notification Could Not Be Sent";
                                            objEmail.Body = "<p>The following approval notification email could not be sent to " + approverName + " at the email address " + approverEmail + ".</p>";
                                            objEmail.Body += "<hr style=\"width:100%\" />";
                                            objEmail.Body += emailBody;

                                            try {
                                                client.Send(objEmail);
                                                notNotified += "|" + status + ": " + approverName + ".|HRO has been notified.";
                                            } catch {
                                                notNotified += "|" + status + ": " + approverName + " or HRO.|Please contact HRO at " + strAdminEmailAddress + ".";
                                            }
                                        }
                                    } else {
                                        objEmail = new MailMessage();
                                        objEmail.To.Add(strAdminEmailAddress);
                                        objEmail.From = new MailAddress(strAdminEmailAddress);
                                        objEmail.IsBodyHtml = true;
                                        objEmail.Priority = MailPriority.High;
                                        objEmail.Subject = "Position Request Approval Notification Could Not Be Sent";
                                        objEmail.Body = "<p>The following approval notification email could not be sent to " + approverName + " because their email address does not exist.</p>";
                                        objEmail.Body += "<hr style=\"width:100%\" />";
                                        objEmail.Body += emailBody;

                                        try {
                                            client.Send(objEmail);
                                            notNotified += "|" + status + ": " + approverName + ".|HRO has been notified.";
                                        } catch {
                                            notNotified += "|" + status + ": " + approverName + " or HRO.|Please contact HRO at " + strAdminEmailAddress + ".";
                                        }
                                    }

                                    if (task == "modified") {
                                        task += " and approved";
                                    } else {
                                        task = "approved";
                                    }
                                } else {
                                    lblError.Text = "<p><strong>Error: The Position Request status could not be updated to " + status + ".";
                                    //set routing record back to Pending Budget Approval
                                    blnSuccess = positionRequest.EditApproval(positionRequestID, employeeID, name, "Budget", "Approved", "Pending Approval", "", DateTime.Now.AddMilliseconds(100).ToString("MM/dd/yy HH:mm:ss.fff"));
                                    if (blnSuccess) {
                                        lblError.Text += " The position request has been set back to pending Budget approval.";
                                    } else {
                                        lblError.Text += " The position request could not be set back to pending Budget approval.";
                                    }
                                    lblError.Text += "</strong></p>" + strErrorContact + "<br />";
                                    blnSuccess = false;
                                }
                            } else {
                                lblError.Text = "<p><strong>Error: The " + status + " pending approval record could not be added.";
                                //set routing record back to Pending Budget Approval
                                blnSuccess = positionRequest.EditApproval(positionRequestID, employeeID, name, "Budget", "Approved", "Pending Approval", "", DateTime.Now.AddMilliseconds(100).ToString("MM/dd/yy HH:mm:ss.fff"));
                                if (blnSuccess) {
                                    lblError.Text += " The position request has been set back to pending Budget approval.";
                                } else {
                                    lblError.Text += " The position request could not be set back to pending Budget approval.";
                                }
                                lblError.Text += "</strong></p>" + strErrorContact + "<br />";
                                blnSuccess = false;
                            }

                        } catch {
                            lblError.Text = "<p><strong>Error: An error occurred while submitting the " + status + " pending approval record.";
                            //set routing record back to Pending Budget Approval
                            blnSuccess = positionRequest.EditApproval(positionRequestID, employeeID, name, "Budget", "Approved", "Pending Approval", "", DateTime.Now.AddMilliseconds(100).ToString("MM/dd/yy HH:mm:ss.fff"));
                            if (blnSuccess) {
                                lblError.Text += " The position request has been set back to pending Budget approval.";
                            } else {
                                lblError.Text += " The position request could not be set back to pending Budget approval.";
                            }
                            lblError.Text += "</strong></p>" + strErrorContact + "<br />";
                            blnSuccess = false;
                        }
                    } else {
                        lblError.Text = "<p><strong>" + error;
                        //set routing record back to Pending Budget Approval
                        blnSuccess = positionRequest.EditApproval(positionRequestID, employeeID, name, "Budget", "Approved", "Pending Approval", "", DateTime.Now.AddMilliseconds(100).ToString("MM/dd/yy HH:mm:ss.fff"));
                        if (blnSuccess) {
                            lblError.Text += " The position request has been set back to pending Budget approval.";
                        } else {
                            lblError.Text += " The position request could not be set back to pending Budget approval.";
                        }
                        lblError.Text += "</strong></p>" + strErrorContact + "<br />";
                        blnSuccess = false;
                    }

                } else {
                    //display error message
                    lblError.Text = "<p><strong>Error: Your approval could not be recorded.</strong></p>" + strErrorContact + "<br />";
                }

            } else if (blnSuccess && selectedOption == "optReject") { //FIGURE OUT WHY REJECT PROCESS IS SO SLOW!!!

                blnSuccess = positionRequest.EditApproval(positionRequestID, employeeID, name, lblStatus.Text, "Pending Approval", "Rejected", rejectReason, DateTime.Now.ToString("MM/dd/yy HH:mm:ss.fff"));

                if (blnSuccess) {
                    //update the status
                    blnSuccess = positionRequest.EditStatus(positionRequestID, "Rejected");

                    if (blnSuccess) {
                        SmtpClient client = new SmtpClient();
                        MailMessage objEmail = new MailMessage();
                        String strEmailBody = "<p><strong>Rejected by:</strong> " + lblStatus.Text + ": " + name +
                                              "<br /><strong>Reason:</strong> " + rejectReason +
                                              "<br /><br /><strong>Tracking #:</strong> " + lblTrackingNumber.Text +
                                              "<br /><strong>Originator:</strong> " + lblOriginatorName.Text +
                                              "<br /><strong>Created:</strong> " + hidCreated.Value +
                                              "<br /><strong>College/Unit:</strong> " + lblCollegeUnit.Text +
                                              "<br /><strong>Department:</strong> " + lblDepartmentName.Text +
                                              "<br /><strong>Official Title:</strong> " + lblOfficialTitle.Text +
                                              "<br /><strong>Supervisor Name:</strong> " + lblSupervisorName.Text +
                                              "<br /><strong>Position Type:</strong> " + lblPositionType.Text + "</p>";

                        //notify the originator
                        String originatorEmail = positionRequest.GetEmployeeEmail(lblOriginatorID.Text);
                        String originatorName = lblOriginatorName.Text;
                        objEmail.From = new MailAddress(strAdminEmailAddress);
                        //objEmail.To.Add(originatorEmail); 
                        objEmail.To.Add("vu.nguyen@ccs.spokane.edu");
                        objEmail.IsBodyHtml = true;
                        objEmail.Priority = MailPriority.High;
                        objEmail.Subject = "Position Request Rejected Notification";
                        objEmail.Body = "<p>The following Position Request has been rejected.<p>";
                        objEmail.Body += "<p><a href=\"https://internal.spokane.edu/PositionRequest/\">https://internal.spokane.edu/PositionRequest/</a>";
                        objEmail.Body += "<br />To modify and resubmit this form, log in with your Active Directory account and click on the Position Request link in the Main Menu.</p>";
                        objEmail.Body += strEmailBody;

                        try {
                            client.Send(objEmail);
                            notified += "|Originator: " + originatorName;
                        } catch {
                            objEmail = new MailMessage();
                            objEmail.From = new MailAddress(strAdminEmailAddress);
                            objEmail.To.Add(strAdminEmailAddress);
                            objEmail.IsBodyHtml = true;
                            objEmail.Priority = MailPriority.High;
                            objEmail.Subject = "Position Request Rejection Notification Could Not Be Sent";
                            objEmail.Body = "<p>The following rejection notification email could not be sent to:<br />Originator: " + originatorName + "</p>";
                            objEmail.Body += "<hr style=\"width:100%\" />";
                            objEmail.Body += strEmailBody;
                            try {
                                client.Send(objEmail);
                                notNotified += "|Originator: " + originatorName + ". HRO has been notified.";
                            } catch {
                                notNotified += "|Originator: " + originatorName + ". HRO could not be notified. Please contact HRO at " + strAdminEmailAddress + ".";
                            }
                        }

                        //notify past approvers
                        String previousApproverList = "", notificationList = "";
                        DataSet dsPreviousApprovers = positionRequest.GetPreviousApprovers(positionRequestID);
                        Int32 approverCount = dsPreviousApprovers.Tables[0].Rows.Count;
                        objEmail = new MailMessage();
                        if (approverCount > 0) {
                            for (Int32 i = 0; i < approverCount; i++) {
                                String userType = dsPreviousApprovers.Tables[0].Rows[i]["UserType"].ToString();
                                String approverName = dsPreviousApprovers.Tables[0].Rows[i]["Name"].ToString();
                                if (!previousApproverList.Contains(approverName)) {
                                    previousApproverList += "<br />" + userType + ": " + approverName;
                                    notificationList += "|" + userType + ": " + approverName;
                                    //objEmail.To.Add(dsPreviousApprovers.Tables[0].Rows[i]["EMAIL_ADDR"].ToString());
                                    objEmail.To.Add("vu.nguyen@ccs.spokane.edu");
                                }
                            }
                            //objEmail.CC.Add(positionRequest.GetEmployeeEmail(employeeID)); 
                            objEmail.CC.Add("vu.nguyen@ccs.spokane.edu");
                            objEmail.From = new MailAddress(strAdminEmailAddress);
                            objEmail.IsBodyHtml = true;
                            objEmail.Priority = MailPriority.High;
                            objEmail.Subject = "Position Request Rejected Notification";
                            objEmail.Body = "<p>The following Position Request has been rejected.</p>";
                            objEmail.Body += strEmailBody;

                            try {
                                client.Send(objEmail);
                                notified += notificationList;
                            } catch {
                                objEmail = new MailMessage();
                                objEmail.From = new MailAddress(strAdminEmailAddress);
                                objEmail.To.Add(strAdminEmailAddress);
                                objEmail.IsBodyHtml = true;
                                objEmail.Priority = MailPriority.High;
                                objEmail.Subject = "Position Request Rejection Notification Could Not Be Sent";
                                objEmail.Body = "<p>The following rejection notification email could not be sent to:" + previousApproverList + ".</p>";
                                objEmail.Body += "<hr style=\"width:100%\" />";
                                objEmail.Body += strEmailBody;
                                try {
                                    client.Send(objEmail);
                                    notNotified += notificationList + "|HRO has been notified.";
                                } catch {
                                    notNotified += notificationList + "|HRO could not be notified. Please contact HRO at " + strAdminEmailAddress + ".";
                                }
                            }
                        }

                        task = "rejected";

                    } else {
                        lblError.Text = "<p><strong>Error: The Position Request status could not be updated to Rejected.";
                        //set routing record back to Pending Budget Approval
                        blnSuccess = positionRequest.EditApproval(positionRequestID, employeeID, name, "Budget", "Rejected", "Pending Approval", "", DateTime.Now.AddMilliseconds(100).ToString("MM/dd/yy HH:mm:ss.fff"));
                        if (blnSuccess) {
                            lblError.Text += " The position request has been set back to pending Budget approval.";
                        } else {
                            lblError.Text += " The position request could not be set back to pending Budget approval.";
                        }
                        lblError.Text += "</strong></p>" + strErrorContact + "<br />";                       
                        blnSuccess = false;
                    }
                } else {
                    //display error message
                    lblError.Text = "<p><strong>Error: Your rejection could not be recorded.</strong></p>" + strErrorContact + "<br />";
                }

            }

        } else {
            lblError.Text = "<p><strong>Error: The file attachment(s) could not be uploaded.</strong></p>" + strErrorContact + "<br />";
        }

        if (blnSuccess) {
            Response.Redirect("Approval.aspx?todo=confirm&id=" + positionRequestID + "&task=" + task + "&notified=" + notified + "&notNotified=" + notNotified);
        } else {
            lblError.Visible = true;
            cmdBack.Visible = false;
            cmdPrint.Visible = false;
            cmdSubmit.Visible = false;
            cmdOK.Visible = true;
        }
    }
}
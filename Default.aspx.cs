﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page {
    protected void Page_Load(object sender, EventArgs e) {
        HttpCookie cookie = Request.Cookies.Get("phatt2");
        if (cookie == null) {
            users user = new users(HttpContext.Current.User.Identity.Name);
        }

        String userctclinkid = Request.Cookies["phatt2"]["userctclinkid"].ToString();
        //if the user is not logged in with a valid AD account
        if (userctclinkid == null || userctclinkid == "000000000" || userctclinkid == "") {
            //redirect them to an error page
            Response.Redirect("error.htm");
        } else {
            PositionRequest positionRequest = new PositionRequest();
            welcomeText.InnerHtml = positionRequest.GetWelcomeText();
        }
    }
}
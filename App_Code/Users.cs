﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.DirectoryServices;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for users
/// </summary>
public class users {
    public string sid;
    public string deptname = "";
    public string ssn = "";
    public string name = "";
    public string emptype = "";
    public string excepted = "";
    public string theme = "";
    public string ctcLinkID = "";

    public users(string username)
    {
        if (HttpContext.Current.User.Identity.Name.Contains("student\\".ToUpper())) {
            System.Web.HttpContext.Current.Response.Redirect("/domainmessage.aspx");
        } else {
            double myNum = 0;
            if (username.Length == 9 && Double.TryParse(username, out myNum)) //if a nine-digit number was sent as the username
            {
                this.sid = username;
            } else if (username == "0") {
                this.sid = username;
            } else {
                this.sid = this.findUserInAD(username);

                //Test for adjunct
                //this.sid = this.findUserInAD("ANNA.GONZALES@sfcc.spokane.edu");
                //this.sid = this.findUserInAD("linda.beane-boose@sfcc.spokane.edu"); 


                //this.sid = this.findUserInAD("isabel.evans@scc.spokane.edu");
                //this.sid = this.findUserInAD("JEFF.BROWN@SCC.SPOKANE.EDU");

                //Test for PT Hourly
                //this.sid = this.findUserInAD("JAN.CARPENTER@SFCC.SPOKANE.EDU");
                //this.sid = this.findUserInAD(" Kimberlee.Messina@sfcc.spokane.edu");
                //this.sid = this.findUserInAD("Christine.Johnson@ccs.spokane.edu");
                //this.sid = this.findUserInAD("MELODY.MATTHEWS@CCS.SPOKANE.EDU");

                //Test for PT Hourly
                //this.sid = this.findUserInAD("BOB.NELSON@ccs.spokane.edu");
                //this.sid = this.findUserInAD("Rick.Sparks@ccs.spokane.edu");

                //Test Head start : Patty.Allen is District Director for HS, report to Chief Compliance Office Amy McCoy, and Amy report to Chancellor, Chancellor to Stevens, Melody will approve for Stevens Jobcode 111433
                //this.sid = this.findUserInAD("TERESA.BEEMAN@CCS.SPOKANE.EDU");
                //this.sid = this.findUserInAD("crystal.connolly@ccs.spokane.edu");
                //this.sid = this.findUserInAD("BOBBI.WOODRAL@CCS.SPOKANE.EDU");
                //this.sid = this.findUserInAD("Patty.Allen@ccs.spokane.edu");
                //this.sid = this.findUserInAD("AMY.MCCOY@CCS.SPOKANE.EDU");
                //this.sid = this.findUserInAD("Christine.Johnson@ccs.spokane.edu");

                
                //this.sid = this.findUserInAD("Laura.Padden@ccs.spokane.edu"); //Budget admin
                //this.sid = this.findUserInAD("Greg.Stevens@ccs.spokane.edu");
                //this.sid = this.findUserInAD("Christine.Johnson@ccs.spokane.edu");
                //this.sid = this.findUserInAD("Rick.Sparks@ccs.spokane.edu");
                //this.sid = this.findUserInAD("BOB.NELSON@ccs.spokane.edu");//this.findUserInAD(username); //this.findUserInAD("Kamori.Cattadoris@ccs.spokane.edu");
                setSIDCookie();
            }
        }
    }

    private string findUserInAD(string domainuser) 
    {
        string usersid = "0";
        string searchuser;
        string searchdomain;
        string updomain;
        if (domainuser.Contains("@")) {
            string[] temp = domainuser.Split("@".ToCharArray());
            searchuser = temp[0];
            searchdomain = temp[1];
            updomain = searchdomain;
            if (searchdomain.ToLower() == "ccs") updomain = "ccs.spokane.edu";
            if (searchdomain.ToLower().Equals("scc.spokane.edu")) updomain = "scc.spokane.edu";
            if (searchdomain.ToLower().Equals("sfcc.spokane.edu")) updomain = "sfcc.spokane.edu";
        } else {
            try {
                string[] temp = domainuser.Split("\\".ToCharArray());
                searchuser = temp[1];
                searchdomain = temp[0];
            } catch {
                searchuser = domainuser;
                searchdomain = "ccs";
            }
            updomain = searchdomain;
            if (searchdomain.ToLower() == "district17_adm") updomain = "dist";
            if (searchdomain.ToLower() == "ccs") updomain = "ccs.spokane.edu";
        }
        if (searchdomain.ToLower() == "district17_adm" || searchdomain.ToLower() == "dist") searchdomain = "dist.spokane.cc.wa.us";
        if (searchdomain.ToLower() == "scc") searchdomain = "scc.spokane.cc.wa.us";
        if (searchdomain.ToLower() == "sfcc") searchdomain = "sfcc.spokane.cc.wa.us";
        if (searchdomain.ToLower() == "iel") searchdomain = "iel.spokane.cc.wa.us";
        if (searchdomain.ToLower() == "ccs.spokane.edu" || searchdomain.ToLower() == "scc.spokane.edu" || searchdomain.ToLower() == "sfcc.spokane.edu" || searchdomain.ToLower() == "ccs") searchdomain = "ccs.spokane.cc.wa.us";

        DirectoryEntry entry = new DirectoryEntry("LDAP://" + searchdomain);
        DirectorySearcher mySearcher = new DirectorySearcher(entry);

        string filter = string.Format("(userPrincipalName= {0})", searchuser + "@" + updomain);
        mySearcher.Filter = (filter);
        SearchResult result = mySearcher.FindOne();
        string testValue = "";

        if ((result == null) && (updomain == "ccs.spokane.edu")) {

            updomain = updomain.Replace(".spokane.edu", "");
            filter = "(&((&(objectCategory=Person)(objectClass=User)))(samaccountname=" + searchuser + "))";
            mySearcher.Filter = (filter);
            result = mySearcher.FindOne();

            DirectoryEntry directoryEntry = result.GetDirectoryEntry();
            try {
                testValue = directoryEntry.Properties["employeeNumber"].Value.ToString();
            } catch {
                testValue = "0";
            }

            if (testValue != "0")
                usersid = directoryEntry.Properties["employeeNumber"][0].ToString();
            else
                usersid = directoryEntry.Properties["employeeID"][0].ToString();
            if (directoryEntry.Properties["EmployeeID"].Value != null)
                ctcLinkID = directoryEntry.Properties["employeeID"][0].ToString();
            else
                ctcLinkID = "";

            directoryEntry.Dispose();
            directoryEntry = null;
            mySearcher.Dispose();

        } else {

            DirectoryEntry directoryEntry = result.GetDirectoryEntry();
            try {
                testValue = directoryEntry.Properties["employeeNumber"].Value.ToString();
            } catch {
                testValue = "0";
            }

            if (testValue != "0")
                usersid = directoryEntry.Properties["employeeNumber"][0].ToString();
            else
                usersid = directoryEntry.Properties["employeeID"][0].ToString();
            if (directoryEntry.Properties["EmployeeID"].Value != null)
                ctcLinkID = directoryEntry.Properties["employeeID"][0].ToString();
            else
                ctcLinkID = "";
            
            directoryEntry.Dispose();
            directoryEntry = null;
            mySearcher.Dispose();

        }
        mySearcher = null;

        return usersid;
    }

    private void setSIDCookie() 
    {
        HttpCookie objCookie = new HttpCookie("phatt2");
        System.Web.HttpContext.Current.Response.Cookies.Clear();
        System.Web.HttpContext.Current.Response.Cookies.Add(objCookie);
        objCookie.Values.Add("userid", this.sid);
        if ((this.sid.Trim().Length == 9) || (this.ctcLinkID.Trim().Length == 9)) {
            getCTCLinkData(this.ctcLinkID);
            objCookie.Values.Add("userName", this.name);
            objCookie.Values.Add("emptype", this.emptype);
            objCookie.Values.Add("deptName", this.deptname);
            objCookie.Values.Add("theme", this.theme);
            objCookie.Values.Add("userctclinkid", this.ctcLinkID);
        }
        DateTime dtExpiry = DateTime.Now.AddDays(35);
        System.Web.HttpContext.Current.Response.Cookies["phatt2"].Path = "/";
        System.Web.HttpContext.Current.Response.Cookies["phatt2"].Domain = System.Configuration.ConfigurationManager.AppSettings["cookiePath"];
    }

    public void getCTCLinkData(string inemplid) 
    {
        // Setup
        this.sid = inemplid;
        string connection = System.Configuration.ConfigurationManager.AppSettings["ctcLinkODSConnection"];
        SqlConnection dbcon = new SqlConnection(connection);
        SqlCommand getuserinfo = new SqlCommand("usp_GetEmpInfoByEmplID", dbcon);
        getuserinfo.CommandType = CommandType.StoredProcedure;

        // Set parameters
        getuserinfo.Parameters.Add("@pEmplID", SqlDbType.VarChar, 9);
        getuserinfo.Parameters["@pEmplID"].Value = this.ctcLinkID;
        getuserinfo.Parameters.Add("@pEmpName", SqlDbType.VarChar, 30); getuserinfo.Parameters["@pEmpName"].Direction = ParameterDirection.Output;
        getuserinfo.Parameters.Add("@pEmpType", SqlDbType.VarChar, 2); getuserinfo.Parameters["@pEmpType"].Direction = ParameterDirection.Output;
        getuserinfo.Parameters.Add("@pDeptName", SqlDbType.VarChar, 30); getuserinfo.Parameters["@pDeptName"].Direction = ParameterDirection.Output;

        // Execute SP
        dbcon.Open();
        getuserinfo.ExecuteNonQuery();
        this.deptname = getuserinfo.Parameters["@pDeptName"].Value.ToString();
        this.name = getuserinfo.Parameters["@pEmpName"].Value.ToString();
        this.emptype = getuserinfo.Parameters["@pEmpType"].Value.ToString();

        // Clean up
        getuserinfo.Dispose();
        getuserinfo = null;
        dbcon.Close();
        dbcon = null;
    }
}

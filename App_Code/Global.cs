﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Security.Principal;
using System.Net.Mail;


/// <summary>
/// Summary description for Global
/// </summary>
public class Global : System.Web.HttpApplication {
    void Application_Error(object sender, EventArgs e) {
        bool blSuppressEmail = false;
        // Code that runs when an unhandled error occurs
        HttpContext ctx = HttpContext.Current;

        Exception exception = ctx.Server.GetLastError();
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        // Get the path of the page
        sb.Append("More information may be available in the system log on the target server.<br /><br /><b>Error in Path:</b> " + Request.Path);
        // Get the QueryString along with the Virtual Path
        sb.Append("<br /><br /><b>Error Raw Url: </b>" + Request.RawUrl);
        // Create an Exception object from the Last error that occurred on the server
        Exception myError = Server.GetLastError();
        // Get the error message
        sb.Append("<br /><br /><b>Error Message:</b> " + myError.Message);
        // Suppress 404 errors to eliminate noise
        if (myError.Message.Contains("does not exist")) { blSuppressEmail = true; }
        // Source of the message
        sb.Append("<br /><br /><b>Error Source:</b> " + myError.Source);
        // Stack Trace of the error
        sb.Append("<br /><br /><b>Error Stack Trace:</b> " + myError.StackTrace);
        // Method where the error occurred
        sb.Append("<br /><br /><b>Error TargetSite:</b> " + Request.ServerVariables["SERVER_NAME"]);
        sb.Append("<br /><br /><b>IP:</b> " + Request.ServerVariables["REMOTE_ADDR"] + "<br /><br />" + DateTime.Now.ToString() +
            "<br /><br />" + myError.InnerException + "<br /><br />" + myError.Data + "<br /><br />" +
            "<b>Client Information:</b> " + HttpContext.Current.Request.Browser.Browser + "<br /><br />");
        String temp = "<b>Header Key Value Pairs</b><br />";
        foreach (string st in HttpContext.Current.Request.Headers.AllKeys) {
            foreach (string st2 in HttpContext.Current.Request.Headers.GetValues(st)) {
                temp += "  " + st + " = " + st2 + "<br />";
            }
        }
        sb.Append(temp + "<br /><br />");
        temp = "<b>Form Key Value Pairs:</b><br />";
        foreach (string st in HttpContext.Current.Request.Form.AllKeys) {
            temp += "  " + st + " = " + HttpContext.Current.Request.Form[st] + "<br />";
        }
        sb.Append(temp + "<br /><br />");

        if (!blSuppressEmail) {
            //send error details via email
            SmtpClient client = new SmtpClient();
            MailMessage objEmail = new MailMessage();
            //objEmail.To.Add("brandy.vaughn@ccs.spokane.edu");
            objEmail.To.Add("vu.nguyen@ccs.spokane.edu");
            objEmail.To.Add("laura.padden@ccs.spokane.edu");
            objEmail.To.Add("bob.nelson@ccs.spokane.edu");
            objEmail.From = new MailAddress("CCSWebApp@ccs.spokane.edu");
            objEmail.IsBodyHtml = true;
            objEmail.Subject = "Position Request Application Error";
            objEmail.Body = sb.ToString();
            try {
                client.Send(objEmail);
            } catch {
                //do nothing
            }
        }
        // Attempt to redirect the user
        //Server.Transfer("https://ccsnet.ccs.spokane.edu/error.aspx"); //comment out to debug
    }

}
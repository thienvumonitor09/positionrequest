﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for WebService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]
public class WebService : System.Web.Services.WebService
{
    public class Employee
    {
        public string empId;
        public string empName;
        public string deptName;

        public Employee() { }
        public Employee(string empId, string empName, string deptName)
        {
            this.empId = empId;
            this.empName = empName;
            this.deptName = deptName;
        }

    }

    public class Department
    {
        public string departmentName;
        public string departmentNo;
        public string unit;
        public Department(string departmentName, string departmentNo, string unit)
        {
            this.departmentName = departmentName;
            this.departmentNo = departmentNo;
            this.unit = unit;
        }
    }
    public WebService()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }
    
    
    [WebMethod()]
    public List<Employee> GetEmployeeList(string lastName)
    {
        PositionRequest positionRequest = new PositionRequest();
        DataSet objDS = positionRequest.GetEmployeesByLastName(lastName);
        List<Employee> employees = new List<Employee>();
        if (objDS != null && objDS.Tables.Count > 0)
        {
            foreach (DataRow dr in objDS.Tables[0].Rows)
            {
                employees.Add(new Employee(dr["EMPLID"].ToString(), dr["NAME"].ToString(), dr["DESCR"].ToString()));
            }
        }
        return employees;
    }

    //Reaturn a list of all employees whoes names match keyword
    [WebMethod()]
    public List<Employee> GetEmployeeListByKeyword(string lastName)
    {
        string keyword = lastName;
        PositionRequest positionRequest = new PositionRequest();
        DataSet objDS = positionRequest.GetEmployeeListByKeyword(keyword);
        DataTable dtEmployees = objDS.Tables[0];
        // Split on all non - word characters.
        // ... This returns an array of all the words.
        string[] words = Regex.Split(keyword, @"\W+");
        words = Array.ConvertAll(words, e => e.ToLower());
        
        List<Employee> employees = new List<Employee>();
        if (dtEmployees.Rows.Count > 0)
        {
            foreach (DataRow drEmployee in dtEmployees.Rows)
            {
                if (words.All(drEmployee["NAME"].ToString().ToLower().Contains))
                {
                    employees.Add(new Employee(drEmployee["EMPLID"].ToString(), drEmployee["NAME"].ToString(), drEmployee["DESCR"].ToString()));
                }
               // employees.Add(new Employee(dr["EMPLID"].ToString(), dr["NAME"].ToString(), dr["DESCR"].ToString()));
            }
        }
        return employees;
    }

    [WebMethod]
    public ArrayList PopulateDepartments()
    {
        PositionRequest positionRequest = new PositionRequest();
        ArrayList list = new ArrayList();
        DataSet dsDeptNames = positionRequest.GetDepartments();
        for (Int32 i = 0; i < dsDeptNames.Tables[0].Rows.Count; i++)
        {
            list.Add(new ListItem(dsDeptNames.Tables[0].Rows[i]["DESCR"].ToString(), dsDeptNames.Tables[0].Rows[i]["DEPTID"].ToString()));
        }
        return list;
    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

/// <summary>
/// Summary description for PositionRequest
/// </summary>
public class PositionRequest {

    private SqlConnection objCon;
    private String strConnection;
    private SqlCommand objCmd;
    private SqlDataAdapter objDA;
    private DataSet objDS;

    public string OriginatorID { get; set; }
    public PositionRequest() {
        //Production connection string
        //strConnection = "Data Source=CCSSQL2\\CCSSQLInt2;Workstation ID=internalPositionRequest;Initial Catalog=CCSPositionRequest;Integrated Security=true;";

        //Development connection string
        strConnection = "Data Source=CCS-SQL-Dev;Workstation ID=internalPositionRequest;Initial Catalog=CCSPositionRequest;Integrated Security=true;";
    }

    public String GetWelcomeText() {
        objCon = new SqlConnection(strConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_GetWelcomeText", objCon);
            objCmd.CommandType = CommandType.StoredProcedure;
            try {
                return objCmd.ExecuteScalar().ToString();
            } catch (Exception) {
                return "";
            }
        } finally {
            objCon.Close();
        }
    }

    public bool EditWelcomeText(String welcomeText) {
        objCon = new SqlConnection(strConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_EditWelcomeText", objCon);
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@WelcomeText", SqlDbType.VarChar).Value = welcomeText;
            Int32 intCtr = objCmd.ExecuteNonQuery();
            if (intCtr == 0) {
                return false;
            } else {
                return true;
            }
        } finally {
            objCon.Close();
        }
    }

    public String GetEmployeeBusinessUnit(String employeeID) {
        objCon = new SqlConnection(ConfigurationManager.AppSettings["ctcLinkODSConnection"]);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_GetEmployeeBusinessUnit", objCon);
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@EMPLID", SqlDbType.VarChar).Value = employeeID;
            try {
                return objCmd.ExecuteScalar().ToString();
            } catch {
                return "";
            }
        } finally {
            objCon.Close();
        }
    }

    public String GetEmployeeName(String employeeID) {
        objCon = new SqlConnection(ConfigurationManager.AppSettings["ctcLinkODSConnection"]);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_GetEmployeeNameByID", objCon);
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@EMPLID", SqlDbType.VarChar).Value = employeeID;
            try {
                return objCmd.ExecuteScalar().ToString();
            } catch {
                return "";
            }
        } finally {
            objCon.Close();
        }
    }

    public DataSet GetEmployeeNames(String employeeID) {
        objCon = new SqlConnection(ConfigurationManager.AppSettings["ctcLinkODSConnection"]);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_GetEmployeeNameByID", objCon);
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@EMPLID", SqlDbType.VarChar).Value = employeeID;
            objDA = new SqlDataAdapter(objCmd);
            objDS = new DataSet();
            objDA.Fill(objDS);
            return objDS;
        } finally {
            objCon.Close();
        }
    }

    public DataSet GetEmployeesByLastName(String lastName) {
        objCon = new SqlConnection(ConfigurationManager.AppSettings["ctcLinkODSConnection"]);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_GetEmployeesByLastName", objCon);
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.AddWithValue("@LastName", String.IsNullOrEmpty(lastName) ? (object)DBNull.Value : lastName); //null cannot be saved to db, instead DBNull.Value
            objDA = new SqlDataAdapter(objCmd);
            objDS = new DataSet();
            objDA.Fill(objDS);
            return objDS;
        } finally {
            objCon.Close();
        }
    }

    public DataSet GetEmployeeListByKeyword(String lastName)
    {
        try
        {
            using (objCon = new SqlConnection(ConfigurationManager.AppSettings["ctcLinkODSConnection"]))
            {
                objCon.Open();
                objCmd = new SqlCommand("usp_GetEmployees", objCon);
                objCmd.CommandType = CommandType.StoredProcedure;
                objDA = new SqlDataAdapter(objCmd);
                objDS = new DataSet();
                objDA.Fill(objDS);
                return objDS;
            } 
        }
        catch 
        {
            return null;
        }
        
    }

    public String GetEmployeeEmail(String employeeID) {
        objCon = new SqlConnection(ConfigurationManager.AppSettings["ctcLinkODSConnection"]);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_GetEmployeeEmail", objCon);
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@EMPLID", SqlDbType.VarChar).Value = employeeID;
            try {
                return objCmd.ExecuteScalar().ToString();
            } catch {
                return "";
            }
        } finally {
            objCon.Close();
        }
    }

    public String GetEmployeeJobCode(String employeeID) {
        objCon = new SqlConnection(ConfigurationManager.AppSettings["ctcLinkODSConnection"]);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_GetEmployeeJobCode", objCon);
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@EMPLID", SqlDbType.VarChar).Value = employeeID;
            try {
                return objCmd.ExecuteScalar().ToString();
            } catch {
                return "";
            }
        } finally {
            objCon.Close();
        }
    }

    public DataSet GetEmployeeByJobCode(String jobCode) {
        objCon = new SqlConnection(ConfigurationManager.AppSettings["ctcLinkODSConnection"]);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_GetEmployeeByJobCode", objCon);
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@JOBCODE", SqlDbType.VarChar).Value = jobCode;
            objDA = new SqlDataAdapter(objCmd);
            objDS = new DataSet();
            objDA.Fill(objDS);
            return objDS;
        } finally {
            objCon.Close();
        }
    }

    public DataSet GetDepartments() {
        objCon = new SqlConnection(ConfigurationManager.AppSettings["ctcLinkODSConnection"]);
        try { 
            objCon.Open();
            objCmd = new SqlCommand("usp_GetDepartments", objCon);
            objCmd.CommandType = CommandType.StoredProcedure;
            objDA = new SqlDataAdapter(objCmd);
            objDS = new DataSet();
            objDA.Fill(objDS);
            return objDS;
        } finally {
            objCon.Close();
        }
    }

    public DataSet GetSupervisor(String employeeID) {
        objCon = new SqlConnection(ConfigurationManager.AppSettings["ctcLinkODSConnection"]);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_GetSupervisor", objCon);
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@EMPLID", SqlDbType.VarChar).Value = employeeID;
            objDA = new SqlDataAdapter(objCmd);
            objDS = new DataSet();
            objDA.Fill(objDS);
            return objDS;
        } finally {
            objCon.Close();
        }
    }

    public String GetUserPermission(String employeeID) {
        objCon = new SqlConnection(strConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_GetUserPermission", objCon);
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@EmployeeID", SqlDbType.VarChar).Value = employeeID;
            try {
                return objCmd.ExecuteScalar().ToString();
            } catch {
                return "";
            }
        } finally {
            objCon.Close();
        }
    }

    public DataSet GetPermissions() {
        objCon = new SqlConnection(strConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_GetPermissions", objCon);
            objCmd.CommandType = CommandType.StoredProcedure;
            objDA = new SqlDataAdapter(objCmd);
            objDS = new DataSet();
            objDA.Fill(objDS);
            return objDS;
        } finally {
            objCon.Close();
        }
    }

    public DataSet GetPermission(Int32 permissionID) {
        objCon = new SqlConnection(strConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_GetPermission", objCon);
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@PermissionID", SqlDbType.Int).Value = permissionID;
            objDA = new SqlDataAdapter(objCmd);
            objDS = new DataSet();
            objDA.Fill(objDS);
            return objDS;
        } finally {
            objCon.Close();
        }
    }

    public bool AddPermission(String employeeID, String name, String permission) {
        objCon = new SqlConnection(strConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_AddPermission", objCon);
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@EmployeeID", SqlDbType.VarChar).Value = employeeID;
            objCmd.Parameters.Add("@Name", SqlDbType.VarChar).Value = name;
            objCmd.Parameters.Add("@Permission", SqlDbType.VarChar).Value = permission;
            if (objCmd.ExecuteNonQuery() > 0) {
                return true;
            } else {
                return false;
            }
        } finally {
            objCon.Close();
        }
    }

    public bool EditPermission(Int32 permissionID, String permission) {
        objCon = new SqlConnection(strConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_EditPermission", objCon);
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@PermissionID", SqlDbType.Int).Value = permissionID;
            objCmd.Parameters.Add("@Permission", SqlDbType.VarChar).Value = permission;
            if (objCmd.ExecuteNonQuery() > 0) {
                return true;
            } else {
                return false;
            }
        } finally {
            objCon.Close();
        }
    }

    public bool DeletePermission(Int32 permissionID) {
        objCon = new SqlConnection(strConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_DeletePermission", objCon);
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@PermissionID", SqlDbType.Int).Value = permissionID;
            if (objCmd.ExecuteNonQuery() > 0) {
                return true;
            } else {
                return false;
            }
        } finally {
            objCon.Close();
        }
    }

    public Int32 AddPositionRequest(String originatorID, String originatorName, String officialTitle, String workingTitle, String jobCode, String collegeUnit, String budgetNumber, 
                                   String positionNumber, String departmentID, String department, String phoneNumber, String mailStop, String supervisorName, String supervisorID, 
                                   String positionType, Int16 permanentPosition, Int16 nonPermanentPosition, String positionEndDate, String recruitmentType, Int16 newPosition,
                                   Int16 replacementPosition, String replacedEmployeeName, Int16 pCard, Int16 CCSIssuedCellPhone, Int16 ctcLinkAccess, Int16 timeSheetApprover, 
                                   String hoursPerDay, String hoursPerWeek, String monthsPerYear, String cyclicCalendarCode
                                    , String status, DateTime created, string comments
                                    ,string buildingNumber, string roomNumber)
    {
        using (SqlConnection objCon = new SqlConnection(strConnection))
        {
            try
            {
                objCon.Open();
                objCmd = new SqlCommand("usp_AddPositionRequest", objCon);
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.Parameters.Add("@OriginatorID", SqlDbType.VarChar).Value = originatorID;
                objCmd.Parameters.Add("@OriginatorName", SqlDbType.VarChar).Value = originatorName;
                objCmd.Parameters.Add("@OfficialTitle", SqlDbType.VarChar).Value = officialTitle;
                if (workingTitle != null && workingTitle != "")
                {
                    objCmd.Parameters.Add("@WorkingTitle", SqlDbType.VarChar).Value = workingTitle;
                }
                if (jobCode != null && jobCode != "")
                {
                    objCmd.Parameters.Add("@JobCode", SqlDbType.VarChar).Value = jobCode;
                }
                objCmd.Parameters.Add("@CollegeUnit", SqlDbType.VarChar).Value = collegeUnit;
                objCmd.Parameters.Add("@BudgetNumber", SqlDbType.VarChar).Value = budgetNumber;
                if (positionNumber != null && positionNumber != "")
                {
                    objCmd.Parameters.Add("@PositionNumber", SqlDbType.VarChar).Value = positionNumber;
                }
                objCmd.Parameters.Add("@DepartmentID", SqlDbType.VarChar).Value = departmentID;
                objCmd.Parameters.Add("@Department", SqlDbType.VarChar).Value = department;
                objCmd.Parameters.Add("@PhoneNumber", SqlDbType.VarChar).Value = phoneNumber;
                objCmd.Parameters.Add("@MailStop", SqlDbType.VarChar).Value = mailStop;
                objCmd.Parameters.Add("@SupervisorName", SqlDbType.VarChar).Value = supervisorName;
                objCmd.Parameters.Add("@SupervisorID", SqlDbType.VarChar).Value = supervisorID;
                objCmd.Parameters.Add("@PositionType", SqlDbType.VarChar).Value = positionType;
                objCmd.Parameters.Add("@PermanentPosition", SqlDbType.Bit).Value = Convert.ToByte(permanentPosition);
                objCmd.Parameters.Add("@NonPermanentPosition", SqlDbType.Bit).Value = Convert.ToByte(nonPermanentPosition);
                if (positionEndDate != null && positionEndDate != "")
                {
                    objCmd.Parameters.Add("@PositionEndDate", SqlDbType.Date).Value = positionEndDate;
                }
                if (recruitmentType != null && recruitmentType != "")
                {
                    objCmd.Parameters.Add("@RecruitmentType", SqlDbType.VarChar).Value = recruitmentType;
                }
                objCmd.Parameters.Add("@NewPosition", SqlDbType.Bit).Value = Convert.ToByte(newPosition);
                objCmd.Parameters.Add("@ReplacementPosition", SqlDbType.Bit).Value = Convert.ToByte(replacementPosition);
                if (replacedEmployeeName != null && replacedEmployeeName != "")
                {
                    objCmd.Parameters.Add("@ReplacedEmployeeName", SqlDbType.VarChar).Value = replacedEmployeeName;
                }
                objCmd.Parameters.Add("@PCard", SqlDbType.Bit).Value = pCard;
                objCmd.Parameters.Add("@CCSIssuedCellPhone", SqlDbType.Bit).Value = Convert.ToByte(CCSIssuedCellPhone);
                objCmd.Parameters.Add("@CTCLinkAccess", SqlDbType.Bit).Value = ctcLinkAccess;
                objCmd.Parameters.Add("@TimeSheetApprover", SqlDbType.Bit).Value = Convert.ToByte(timeSheetApprover);
                if (!String.IsNullOrEmpty(hoursPerDay))
                {
                    //objCmd.Parameters.Add("@HoursPerDay", SqlDbType.TinyInt).Value = hoursPerDay;
                    objCmd.Parameters.AddWithValue("@HoursPerDay", hoursPerDay);
                }
                if (!String.IsNullOrEmpty(hoursPerWeek))
                {
                    //objCmd.Parameters.Add("@HoursPerWeek", SqlDbType.TinyInt).Value = hoursPerWeek;
                    objCmd.Parameters.AddWithValue("@HoursPerWeek", hoursPerWeek);
                }
                if (monthsPerYear != null && monthsPerYear != "")
                {
                    objCmd.Parameters.Add("@MonthsPerYear", SqlDbType.Int).Value = monthsPerYear;
                }
                if (cyclicCalendarCode != null && cyclicCalendarCode != "")
                {
                    objCmd.Parameters.Add("@CyclicCalendarCode", SqlDbType.VarChar).Value = cyclicCalendarCode;
                }
                objCmd.Parameters.Add("@Status", SqlDbType.VarChar).Value = status;
                objCmd.Parameters.Add("@Created", SqlDbType.DateTime).Value = created;
                objCmd.Parameters.AddWithValue("@Comments", comments);
                objCmd.Parameters.AddWithValue("@BuildingNumber", buildingNumber);
                objCmd.Parameters.AddWithValue("@RoomNumber", roomNumber);
                objCmd.Parameters.Add("@PositionRequestID", SqlDbType.Int).Direction = ParameterDirection.Output;
                objCmd.ExecuteNonQuery();
                return Convert.ToInt32(objCmd.Parameters["@PositionRequestID"].Value);

            }
            catch
            {
                return 0;
            }   
        }        
    }

    public bool EditPositionRequest(Int32 positionRequestID, String originatorName, String officialTitle, String workingTitle, String jobCode, String collegeUnit, String budgetNumber,
                                   String positionNumber, String departmentID, String department, String phoneNumber, String mailStop, String supervisorName, String supervisorID,
                                   String positionType, Int16 permanentPosition, Int16 nonPermanentPosition, String positionEndDate, String recruitmentType, Int16 newPosition,
                                   Int16 replacementPosition, String replacedEmployeeName, Int16 pCard, Int16 CCSIssuedCellPhone, Int16 ctcLinkAccess, Int16 timeSheetApprover,
                                   String hoursPerDay, String hoursPerWeek, String monthsPerYear, String cyclicCalendarCode, String status
                                    ,string comments
                                    , string buildingNumber, string roomNumber)
    {
        using (SqlConnection objCon = new SqlConnection(strConnection))
        {
            try
            {
                objCon.Open();
                objCmd = new SqlCommand("usp_EditPositionRequest", objCon);
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.Parameters.Add("@PositionRequestID", SqlDbType.Int).Value = positionRequestID;
                objCmd.Parameters.Add("@OriginatorName", SqlDbType.VarChar).Value = originatorName;
                objCmd.Parameters.Add("@OfficialTitle", SqlDbType.VarChar).Value = officialTitle;
                if (workingTitle != null && workingTitle != "")
                {
                    objCmd.Parameters.Add("@WorkingTitle", SqlDbType.VarChar).Value = workingTitle;
                }
                if (jobCode != null && jobCode != "")
                {
                    objCmd.Parameters.Add("@JobCode", SqlDbType.VarChar).Value = jobCode;
                }
                objCmd.Parameters.Add("@CollegeUnit", SqlDbType.VarChar).Value = collegeUnit;
                objCmd.Parameters.Add("@BudgetNumber", SqlDbType.VarChar).Value = budgetNumber;
                if (positionNumber != null && positionNumber != "")
                {
                    objCmd.Parameters.Add("@PositionNumber", SqlDbType.VarChar).Value = positionNumber;
                }
                objCmd.Parameters.Add("@DepartmentID", SqlDbType.VarChar).Value = departmentID;
                objCmd.Parameters.Add("@Department", SqlDbType.VarChar).Value = department;
                objCmd.Parameters.Add("@PhoneNumber", SqlDbType.VarChar).Value = phoneNumber;
                objCmd.Parameters.Add("@MailStop", SqlDbType.VarChar).Value = mailStop;
                objCmd.Parameters.Add("@SupervisorName", SqlDbType.VarChar).Value = supervisorName;
                objCmd.Parameters.Add("@SupervisorID", SqlDbType.VarChar).Value = supervisorID;
                objCmd.Parameters.Add("@PositionType", SqlDbType.VarChar).Value = positionType;
                objCmd.Parameters.Add("@PermanentPosition", SqlDbType.Bit).Value = Convert.ToByte(permanentPosition);
                objCmd.Parameters.Add("@NonPermanentPosition", SqlDbType.Bit).Value = Convert.ToByte(nonPermanentPosition);
                if (positionEndDate != null && positionEndDate != "")
                {
                    objCmd.Parameters.Add("@PositionEndDate", SqlDbType.Date).Value = positionEndDate;
                }
                if (recruitmentType != null && recruitmentType != "")
                {
                    objCmd.Parameters.Add("@RecruitmentType", SqlDbType.VarChar).Value = recruitmentType;
                }
                objCmd.Parameters.Add("@NewPosition", SqlDbType.Bit).Value = Convert.ToByte(newPosition);
                objCmd.Parameters.Add("@ReplacementPosition", SqlDbType.Bit).Value = Convert.ToByte(replacementPosition);
                if (replacedEmployeeName != null && replacedEmployeeName != "")
                {
                    objCmd.Parameters.Add("@ReplacedEmployeeName", SqlDbType.VarChar).Value = replacedEmployeeName;
                }
                objCmd.Parameters.Add("@PCard", SqlDbType.Bit).Value = pCard;
                objCmd.Parameters.Add("@CCSIssuedCellPhone", SqlDbType.Bit).Value = Convert.ToByte(CCSIssuedCellPhone);
                objCmd.Parameters.Add("@CTCLinkAccess", SqlDbType.Bit).Value = ctcLinkAccess;
                objCmd.Parameters.Add("@TimeSheetApprover", SqlDbType.Bit).Value = Convert.ToByte(timeSheetApprover);
                if (!String.IsNullOrEmpty(hoursPerDay))
                {
                    //objCmd.Parameters.Add("@HoursPerDay", SqlDbType.TinyInt).Value = hoursPerDay;
                    objCmd.Parameters.AddWithValue("@HoursPerDay", hoursPerDay);
                }
                if (!String.IsNullOrEmpty(hoursPerWeek))
                {
                    //objCmd.Parameters.Add("@HoursPerWeek", SqlDbType.TinyInt).Value = hoursPerWeek;
                    objCmd.Parameters.AddWithValue("@HoursPerWeek", hoursPerWeek);
                }
                if (monthsPerYear != null && monthsPerYear != "")
                {
                    objCmd.Parameters.Add("@MonthsPerYear", SqlDbType.Int).Value = monthsPerYear;
                }
                if (cyclicCalendarCode != null && cyclicCalendarCode != "")
                {
                    objCmd.Parameters.Add("@CyclicCalendarCode", SqlDbType.VarChar).Value = cyclicCalendarCode;
                }
                objCmd.Parameters.Add("@Status", SqlDbType.VarChar).Value = status;
                objCmd.Parameters.AddWithValue("@Comments", comments);
                objCmd.Parameters.AddWithValue("@BuildingNumber", buildingNumber);
                objCmd.Parameters.AddWithValue("@RoomNumber", roomNumber);
                if (objCmd.ExecuteNonQuery() > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        } 
    }

    public bool UpdateComboCode(Int32 positionRequestID, string comboCode)
    {
        using (SqlConnection objCon = new SqlConnection(strConnection))
        {
            try
            {
                objCon.Open();
                objCmd = new SqlCommand("usp_UpdateComboCode", objCon);
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.Parameters.AddWithValue("@PositionRequestID", positionRequestID);
                objCmd.Parameters.AddWithValue("@ComboCode", comboCode);
                if (objCmd.ExecuteNonQuery() > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch { return false; }
        }
    }
    public DataSet GetPositionRequest(Int32 positionRequestID) {
        //objCon = new SqlConnection(strConnection);
        using (objCon = new SqlConnection(strConnection))
        {
            objCon.Open();
            objDS = new DataSet();
            try
            {
                objCmd = new SqlCommand("usp_GetPositionRequest", objCon);
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.Parameters.Add("@PositionRequestID", SqlDbType.Int).Value = positionRequestID;
                objDA = new SqlDataAdapter(objCmd);
                objDA.Fill(objDS);
                
            }
            catch { }
            return objDS;
        }    
    }

    public DataSet GetPositionRequests(String originatorID, String originatorName, String status, String trackingNumber, String officialTitle, String positionNumber, String departmentID, String positionType, String recruitmentType, String collegeUnit, String creationStartDate, String creationEndDate, String completionStartDate, String completionEndDate) {
        objCon = new SqlConnection(strConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_GetPositionRequests", objCon);
            objCmd.CommandType = CommandType.StoredProcedure;
            if(originatorID != null && originatorID != "") {
                objCmd.Parameters.Add("@OriginatorID", SqlDbType.VarChar).Value = originatorID;
            }
            if (originatorName != null && originatorName != "") {
                objCmd.Parameters.Add("@OriginatorName", SqlDbType.VarChar).Value = originatorName;
            }
            if (status != null && status != "") {
                objCmd.Parameters.Add("@Status", SqlDbType.VarChar).Value = status;
            }
            if (trackingNumber != null && trackingNumber != "") {
                objCmd.Parameters.Add("@TrackingNumber", SqlDbType.Int).Value = Convert.ToInt32(trackingNumber);
            }
            if (officialTitle != null && officialTitle != "") {
                objCmd.Parameters.Add("@OfficialTitle", SqlDbType.VarChar).Value = officialTitle;
            }
            if (positionNumber != null && positionNumber != "") {
                objCmd.Parameters.Add("@PositionNumber", SqlDbType.VarChar).Value = positionNumber;
            }
            if (departmentID != null && departmentID != "") {
                objCmd.Parameters.Add("@DepartmentID", SqlDbType.VarChar).Value = departmentID;
            }
            if (positionType != null && positionType != "") {
                objCmd.Parameters.Add("@PositionType", SqlDbType.VarChar).Value = positionType;
            }
            if(recruitmentType != null && recruitmentType != "") {
                objCmd.Parameters.Add("@RecruitmentType", SqlDbType.VarChar).Value = recruitmentType;
            }
            if(collegeUnit != null && collegeUnit != "") {
                objCmd.Parameters.Add("@CollegeUnit", SqlDbType.VarChar).Value = collegeUnit;
            }
            if(creationStartDate != null && creationStartDate != "" && creationEndDate != null && creationEndDate != "") {
                try {
                    objCmd.Parameters.Add("@CreationStartDate", SqlDbType.DateTime).Value = Convert.ToDateTime(creationStartDate).ToShortDateString();
                    objCmd.Parameters.Add("@CreationEndDate", SqlDbType.DateTime).Value = Convert.ToDateTime(creationEndDate).ToShortDateString();
                } catch {
                    //do nothing
                }
            }
            if (completionStartDate != null && completionStartDate != "" && completionEndDate != null && completionEndDate != "") {
                try {
                    objCmd.Parameters.Add("@CompletionStartDate", SqlDbType.DateTime).Value = Convert.ToDateTime(completionStartDate).ToShortDateString();
                    objCmd.Parameters.Add("@CompletionEndDate", SqlDbType.DateTime).Value = Convert.ToDateTime(completionEndDate).ToShortDateString();
                } catch {
                    //do nothing
                }
            }
            objDA = new SqlDataAdapter(objCmd);
            objDS = new DataSet();
            objDA.Fill(objDS);
            return objDS;
        } finally {
            objCon.Close();
        }
    }

    public DataSet GetReport(String originatorName, String status, String trackingNumber, String officialTitle, String positionNumber, String departmentID, String positionType, String recruitmentType, String collegeUnit, String creationStartDate, String creationEndDate, String completionStartDate, String completionEndDate) {
        objCon = new SqlConnection(strConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_GetReport", objCon);
            objCmd.CommandType = CommandType.StoredProcedure;
            if (originatorName != null && originatorName != "") {
                objCmd.Parameters.Add("@OriginatorName", SqlDbType.VarChar).Value = originatorName;
            }
            if (status != null && status != "") {
                objCmd.Parameters.Add("@Status", SqlDbType.VarChar).Value = status;
            }
            if (trackingNumber != null && trackingNumber != "") {
                objCmd.Parameters.Add("@TrackingNumber", SqlDbType.Int).Value = Convert.ToInt32(trackingNumber);
            }
            if (officialTitle != null && officialTitle != "") {
                objCmd.Parameters.Add("@OfficialTitle", SqlDbType.VarChar).Value = officialTitle;
            }
            if (positionNumber != null && positionNumber != "") {
                objCmd.Parameters.Add("@PositionNumber", SqlDbType.VarChar).Value = positionNumber;
            }
            if (departmentID != null && departmentID != "") {
                objCmd.Parameters.Add("@DepartmentID", SqlDbType.VarChar).Value = departmentID;
            }
            if (positionType != null && positionType != "") {
                objCmd.Parameters.Add("@PositionType", SqlDbType.VarChar).Value = positionType;
            }
            if (recruitmentType != null && recruitmentType != "") {
                objCmd.Parameters.Add("@RecruitmentType", SqlDbType.VarChar).Value = recruitmentType;
            }
            if (collegeUnit != null && collegeUnit != "") {
                objCmd.Parameters.Add("@CollegeUnit", SqlDbType.VarChar).Value = collegeUnit;
            }
            if (creationStartDate != null && creationStartDate != "" && creationEndDate != null && creationEndDate != "") {
                try {
                    objCmd.Parameters.Add("@CreationStartDate", SqlDbType.DateTime).Value = Convert.ToDateTime(creationStartDate).ToShortDateString();
                    objCmd.Parameters.Add("@CreationEndDate", SqlDbType.DateTime).Value = Convert.ToDateTime(creationEndDate).ToShortDateString();
                } catch {
                    //do nothing
                }
            }
            if (completionStartDate != null && completionStartDate != "" && completionEndDate != null && completionEndDate != "") {
                try {
                    objCmd.Parameters.Add("@CompletionStartDate", SqlDbType.DateTime).Value = Convert.ToDateTime(completionStartDate).ToShortDateString();
                    objCmd.Parameters.Add("@CompletionEndDate", SqlDbType.DateTime).Value = Convert.ToDateTime(completionEndDate).ToShortDateString();
                } catch {
                    //do nothing
                }
            }
            objDA = new SqlDataAdapter(objCmd);
            objDS = new DataSet();
            objDA.Fill(objDS);
            return objDS;
        } finally {
            objCon.Close();
        }
    }

    public DataSet GetSupervisorReport(String supervisorID, String originatorName, String status, String trackingNumber, String officialTitle, String positionNumber, String departmentID, String positionType, String recruitmentType, String collegeUnit, String creationStartDate, String creationEndDate, String completionStartDate, String completionEndDate) {
        objCon = new SqlConnection(strConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_GetSupervisorReport", objCon);
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@SupervisorID", SqlDbType.VarChar).Value = supervisorID;
            if (originatorName != null && originatorName != "") {
                objCmd.Parameters.Add("@OriginatorName", SqlDbType.VarChar).Value = originatorName;
            }
            if (status != null && status != "") {
                objCmd.Parameters.Add("@Status", SqlDbType.VarChar).Value = status;
            }
            if (trackingNumber != null && trackingNumber != "") {
                objCmd.Parameters.Add("@TrackingNumber", SqlDbType.Int).Value = Convert.ToInt32(trackingNumber);
            }
            if (officialTitle != null && officialTitle != "") {
                objCmd.Parameters.Add("@OfficialTitle", SqlDbType.VarChar).Value = officialTitle;
            }
            if (positionNumber != null && positionNumber != "") {
                objCmd.Parameters.Add("@PositionNumber", SqlDbType.VarChar).Value = positionNumber;
            }
            if (departmentID != null && departmentID != "") {
                objCmd.Parameters.Add("@DepartmentID", SqlDbType.VarChar).Value = departmentID;
            }
            if (positionType != null && positionType != "") {
                objCmd.Parameters.Add("@PositionType", SqlDbType.VarChar).Value = positionType;
            }
            if (recruitmentType != null && recruitmentType != "") {
                objCmd.Parameters.Add("@RecruitmentType", SqlDbType.VarChar).Value = recruitmentType;
            }
            if (collegeUnit != null && collegeUnit != "") {
                objCmd.Parameters.Add("@CollegeUnit", SqlDbType.VarChar).Value = collegeUnit;
            }
            if (creationStartDate != null && creationStartDate != "" && creationEndDate != null && creationEndDate != "") {
                try {
                    objCmd.Parameters.Add("@CreationStartDate", SqlDbType.DateTime).Value = Convert.ToDateTime(creationStartDate).ToShortDateString();
                    objCmd.Parameters.Add("@CreationEndDate", SqlDbType.DateTime).Value = Convert.ToDateTime(creationEndDate).ToShortDateString();
                } catch {
                    //do nothing
                }
            }
            if (completionStartDate != null && completionStartDate != "" && completionEndDate != null && completionEndDate != "") {
                try {
                    objCmd.Parameters.Add("@CompletionStartDate", SqlDbType.DateTime).Value = Convert.ToDateTime(completionStartDate).ToShortDateString();
                    objCmd.Parameters.Add("@CompletionEndDate", SqlDbType.DateTime).Value = Convert.ToDateTime(completionEndDate).ToShortDateString();
                } catch {
                    //do nothing
                }
            }
            objDA = new SqlDataAdapter(objCmd);
            objDS = new DataSet();
            objDA.Fill(objDS);
            return objDS;
        } finally {
            objCon.Close();
        }
    }

    public DataSet GetExecutiveAssistantReport(String employeeID, String originatorName, String status, String trackingNumber, String officialTitle, String positionNumber, String departmentID, String positionType, String recruitmentType, String collegeUnit, String creationStartDate, String creationEndDate, String completionStartDate, String completionEndDate) {
        objCon = new SqlConnection(strConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_GetExecutiveAssistantReport", objCon);
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@EMPLID", SqlDbType.VarChar).Value = employeeID;
            if (originatorName != null && originatorName != "") {
                objCmd.Parameters.Add("@OriginatorName", SqlDbType.VarChar).Value = originatorName;
            }
            if (status != null && status != "") {
                objCmd.Parameters.Add("@Status", SqlDbType.VarChar).Value = status;
            }
            if (trackingNumber != null && trackingNumber != "") {
                objCmd.Parameters.Add("@TrackingNumber", SqlDbType.Int).Value = Convert.ToInt32(trackingNumber);
            }
            if (officialTitle != null && officialTitle != "") {
                objCmd.Parameters.Add("@OfficialTitle", SqlDbType.VarChar).Value = officialTitle;
            }
            if (positionNumber != null && positionNumber != "") {
                objCmd.Parameters.Add("@PositionNumber", SqlDbType.VarChar).Value = positionNumber;
            }
            if (departmentID != null && departmentID != "") {
                objCmd.Parameters.Add("@DepartmentID", SqlDbType.VarChar).Value = departmentID;
            }
            if (positionType != null && positionType != "") {
                objCmd.Parameters.Add("@PositionType", SqlDbType.VarChar).Value = positionType;
            }
            if (recruitmentType != null && recruitmentType != "") {
                objCmd.Parameters.Add("@RecruitmentType", SqlDbType.VarChar).Value = recruitmentType;
            }
            if (collegeUnit != null && collegeUnit != "") {
                objCmd.Parameters.Add("@CollegeUnit", SqlDbType.VarChar).Value = collegeUnit;
            }
            if (creationStartDate != null && creationStartDate != "" && creationEndDate != null && creationEndDate != "") {
                try {
                    objCmd.Parameters.Add("@CreationStartDate", SqlDbType.DateTime).Value = Convert.ToDateTime(creationStartDate).ToShortDateString();
                    objCmd.Parameters.Add("@CreationEndDate", SqlDbType.DateTime).Value = Convert.ToDateTime(creationEndDate).ToShortDateString();
                } catch {
                    //do nothing
                }
            }
            if (completionStartDate != null && completionStartDate != "" && completionEndDate != null && completionEndDate != "") {
                try {
                    objCmd.Parameters.Add("@CompletionStartDate", SqlDbType.DateTime).Value = Convert.ToDateTime(completionStartDate).ToShortDateString();
                    objCmd.Parameters.Add("@CompletionEndDate", SqlDbType.DateTime).Value = Convert.ToDateTime(completionEndDate).ToShortDateString();
                } catch {
                    //do nothing
                }
            }
            objDA = new SqlDataAdapter(objCmd);
            objDS = new DataSet();
            objDA.Fill(objDS);
            return objDS;
        } finally {
            objCon.Close();
        }
    }

    public bool EditTrackingNumber(Int32 positionRequestID, Int32 trackingNumber) {
        objCon = new SqlConnection(strConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_EditTrackingNumber", objCon);
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@PositionRequestID", SqlDbType.Int).Value = positionRequestID;
            objCmd.Parameters.Add("@TrackingNumber", SqlDbType.Int).Value = trackingNumber;
            if (objCmd.ExecuteNonQuery() > 0) {
                return true;
            } else {
                return false;
            }
        } finally {
            objCon.Close();
        }
    }

    public bool AddWorkSchedule(Int32 positionRequestID, String mondayStart, String mondayEnd, String tuesdayStart, String tuesdayEnd, String wednesdayStart, String wednesdayEnd, String thursdayStart, String thursdayEnd, String fridayStart, String fridayEnd, String saturdayStart, String saturdayEnd, String sundayStart, String sundayEnd) {
        objCon = new SqlConnection(strConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_AddWorkSchedule", objCon);
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@PositionRequestID", SqlDbType.Int).Value = positionRequestID;
            if(mondayStart != null && mondayStart != "") {
                objCmd.Parameters.Add("@MondayStart", SqlDbType.DateTime).Value = mondayStart;
            }
            if (mondayEnd != null && mondayEnd != "") {
                objCmd.Parameters.Add("@MondayEnd", SqlDbType.DateTime).Value = mondayEnd;
            }
            if (tuesdayStart != null && tuesdayStart != "") {
                objCmd.Parameters.Add("@TuesdayStart", SqlDbType.DateTime).Value = tuesdayStart;
            }
            if (tuesdayEnd != null && tuesdayEnd != "") {
                objCmd.Parameters.Add("@TuesdayEnd", SqlDbType.DateTime).Value = tuesdayEnd;
            }
            if (wednesdayStart != null && wednesdayStart != "") {
                objCmd.Parameters.Add("@WednesdayStart", SqlDbType.DateTime).Value = wednesdayStart;
            }
            if (wednesdayEnd != null && wednesdayEnd != "") {
                objCmd.Parameters.Add("@WednesdayEnd", SqlDbType.DateTime).Value = wednesdayEnd;
            }
            if (thursdayStart != null && thursdayStart != "") {
                objCmd.Parameters.Add("@ThursdayStart", SqlDbType.DateTime).Value = thursdayStart;
            }
            if (thursdayEnd != null && thursdayEnd != "") {
                objCmd.Parameters.Add("@ThursdayEnd", SqlDbType.DateTime).Value = thursdayEnd;
            }
            if (fridayStart != null && fridayStart != "") {
                objCmd.Parameters.Add("@FridayStart", SqlDbType.DateTime).Value = fridayStart;
            }
            if (fridayEnd != null && fridayEnd != "") {
                objCmd.Parameters.Add("@FridayEnd", SqlDbType.DateTime).Value = fridayEnd;
            }
            if (saturdayStart != null && saturdayStart != "") {
                objCmd.Parameters.Add("@SaturdayStart", SqlDbType.DateTime).Value = saturdayStart;
            }
            if (saturdayEnd != null && saturdayEnd != "") {
                objCmd.Parameters.Add("@SaturdayEnd", SqlDbType.DateTime).Value = saturdayEnd;
            }
            if (sundayStart != null && sundayStart != "") {
                objCmd.Parameters.Add("@SundayStart", SqlDbType.DateTime).Value = sundayStart;
            }
            if (sundayEnd != null && sundayEnd != "") {
                objCmd.Parameters.Add("@SundayEnd", SqlDbType.DateTime).Value = sundayEnd;
            }
            if (objCmd.ExecuteNonQuery() > 0) {
                return true;
            } else {
                return false;
            }
        } finally {
            objCon.Close();
        }
    }

    public bool EditWorkSchedule(Int32 positionRequestID, String mondayStart, String mondayEnd, String tuesdayStart, String tuesdayEnd, String wednesdayStart, String wednesdayEnd, String thursdayStart, String thursdayEnd, String fridayStart, String fridayEnd, String saturdayStart, String saturdayEnd, String sundayStart, String sundayEnd) {
        objCon = new SqlConnection(strConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_EditWorkSchedule", objCon);
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@PositionRequestID", SqlDbType.Int).Value = positionRequestID;
            if (mondayStart != null && mondayStart != "") {
                objCmd.Parameters.Add("@MondayStart", SqlDbType.DateTime).Value = mondayStart;
            }
            if (mondayEnd != null && mondayEnd != "") {
                objCmd.Parameters.Add("@MondayEnd", SqlDbType.DateTime).Value = mondayEnd;
            }
            if (tuesdayStart != null && tuesdayStart != "") {
                objCmd.Parameters.Add("@TuesdayStart", SqlDbType.DateTime).Value = tuesdayStart;
            }
            if (tuesdayEnd != null && tuesdayEnd != "") {
                objCmd.Parameters.Add("@TuesdayEnd", SqlDbType.DateTime).Value = tuesdayEnd;
            }
            if (wednesdayStart != null && wednesdayStart != "") {
                objCmd.Parameters.Add("@WednesdayStart", SqlDbType.DateTime).Value = wednesdayStart;
            }
            if (wednesdayEnd != null && wednesdayEnd != "") {
                objCmd.Parameters.Add("@WednesdayEnd", SqlDbType.DateTime).Value = wednesdayEnd;
            }
            if (thursdayStart != null && thursdayStart != "") {
                objCmd.Parameters.Add("@ThursdayStart", SqlDbType.DateTime).Value = thursdayStart;
            }
            if (thursdayEnd != null && thursdayEnd != "") {
                objCmd.Parameters.Add("@ThursdayEnd", SqlDbType.DateTime).Value = thursdayEnd;
            }
            if (fridayStart != null && fridayStart != "") {
                objCmd.Parameters.Add("@FridayStart", SqlDbType.DateTime).Value = fridayStart;
            }
            if (fridayEnd != null && fridayEnd != "") {
                objCmd.Parameters.Add("@FridayEnd", SqlDbType.DateTime).Value = fridayEnd;
            }
            if (saturdayStart != null && saturdayStart != "") {
                objCmd.Parameters.Add("@SaturdayStart", SqlDbType.DateTime).Value = saturdayStart;
            }
            if (saturdayEnd != null && saturdayEnd != "") {
                objCmd.Parameters.Add("@SaturdayEnd", SqlDbType.DateTime).Value = saturdayEnd;
            }
            if (sundayStart != null && sundayStart != "") {
                objCmd.Parameters.Add("@SundayStart", SqlDbType.DateTime).Value = sundayStart;
            }
            if (sundayEnd != null && sundayEnd != "") {
                objCmd.Parameters.Add("@SundayEnd", SqlDbType.DateTime).Value = sundayEnd;
            }
            if (objCmd.ExecuteNonQuery() > 0) {
                return true;
            } else {
                return false;
            }
        } finally {
            objCon.Close();
        }
    }

    public DataSet GetWorkSchedule(Int32 positionRequestID) {
        objCon = new SqlConnection(strConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_GetWorkSchedule", objCon);
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@PositionRequestID", SqlDbType.Int).Value = positionRequestID;
            objDA = new SqlDataAdapter(objCmd);
            objDS = new DataSet();
            objDA.Fill(objDS);
            return objDS;
        } finally {
            objCon.Close();
        }
    }

    public bool DeleteWorkSchedule(Int32 positionRequestID) {
        objCon = new SqlConnection(strConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_DeleteWorkSchedule", objCon);
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@PositionRequestID", SqlDbType.Int).Value = positionRequestID;
            if (objCmd.ExecuteNonQuery() > 0) {
                return true;
            } else {
                return false;
            }
        } finally {
            objCon.Close();
        }
    }

    public bool AddAttachment(Int32 positionRequestID, String fileName) {
        objCon = new SqlConnection(strConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_AddAttachment", objCon);
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@PositionRequestID", SqlDbType.Int).Value = positionRequestID;
            objCmd.Parameters.Add("@FileName", SqlDbType.VarChar).Value = fileName;
            if (objCmd.ExecuteNonQuery() > 0) {
                return true;
            } else {
                return false;
            }
        } finally {
            objCon.Close();
        }
    }

    public DataSet GetAttachments(Int32 positionRequestID) {
        objCon = new SqlConnection(strConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_GetAttachments", objCon);
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@PositionRequestID", SqlDbType.Int).Value = positionRequestID;
            objDA = new SqlDataAdapter(objCmd);
            objDS = new DataSet();
            objDA.Fill(objDS);
            return objDS;
        } finally {
            objCon.Close();
        }
    }

    public bool DeleteAttachment(String fileName) {
        objCon = new SqlConnection(strConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_DeleteAttachment", objCon);
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@FileName", SqlDbType.VarChar).Value = fileName;
            if(objCmd.ExecuteNonQuery() > 0) {
                return true;
            } else {
                return false;
            }
        } finally {
            objCon.Close();
        }
    }

    public bool AddRouting(Int32 positionRequestID, String userType, String description, String note, String name, String employeeID, String date) {
        objCon = new SqlConnection(strConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_AddRouting", objCon);
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@PositionRequestID", SqlDbType.Int).Value = positionRequestID;
            objCmd.Parameters.Add("@UserType", SqlDbType.VarChar).Value = userType;
            objCmd.Parameters.Add("@Description", SqlDbType.VarChar).Value = description;
            objCmd.Parameters.Add("@Note", SqlDbType.VarChar).Value = note;
            objCmd.Parameters.Add("@Name", SqlDbType.VarChar).Value = name;
            objCmd.Parameters.Add("@EmployeeID", SqlDbType.VarChar).Value = employeeID;
            objCmd.Parameters.Add("@Date", SqlDbType.DateTime).Value = date;
            if (objCmd.ExecuteNonQuery() > 0) {
                return true;
            } else {
                return false;
            }
        } finally {
            objCon.Close();
        }
    }

    public DataSet GetRouting(Int32 positionRequestID) {
        objCon = new SqlConnection(strConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_GetRouting", objCon);
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@PositionRequestID", SqlDbType.Int).Value = positionRequestID;
            objDA = new SqlDataAdapter(objCmd);
            objDS = new DataSet();
            objDA.Fill(objDS);
            return objDS;
        } finally {
            objCon.Close();
        }
    }

    public DataSet GetPendingApproval(Int32 positionRequestID) {
        objCon = new SqlConnection(strConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_GetPendingApproval", objCon);
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@PositionRequestID", SqlDbType.Int).Value = positionRequestID;
            objDA = new SqlDataAdapter(objCmd);
            objDS = new DataSet();
            objDA.Fill(objDS);
            return objDS;
        } finally {
            objCon.Close();
        }
    }

    public DataSet GetCurrentRoutingRecord(Int32 positionRequestID, String status) {
        objCon = new SqlConnection(strConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_GetCurrentRoutingRecord", objCon);
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@PositionRequestID", SqlDbType.Int).Value = positionRequestID;
            objCmd.Parameters.Add("@Status", SqlDbType.VarChar).Value = status;
            objDA = new SqlDataAdapter(objCmd);
            objDS = new DataSet();
            objDA.Fill(objDS);
            return objDS;
        } finally {
            objCon.Close();
        }
    }

    public Int16 GetBudgetApprovalCount(Int32 positionRequestID) {
        objCon = new SqlConnection(strConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_GetBudgetApprovalCount", objCon);
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@PositionRequestID", SqlDbType.Int).Value = positionRequestID;
            try {
                return Convert.ToInt16(objCmd.ExecuteScalar());
            } catch {
                return 0;
            }
        } finally {
            objCon.Close();
        }
    }

    public Int32 GetSupervisorPositionRequestCount(String supervisorID, String description) {
        objCon = new SqlConnection(strConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_GetSupervisorPositionRequestCount", objCon);
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@SupervisorID", SqlDbType.VarChar).Value = supervisorID;
            if(description != null && description != "") {
                objCmd.Parameters.Add("@Description", SqlDbType.VarChar).Value = description;
            }
            try {
                return Convert.ToInt32(objCmd.ExecuteScalar());
            } catch {
                return 0;
            }
        } finally {
            objCon.Close();
        }
    }

    public DataSet GetPositionRequestsForSupervisorApproval(String supervisorID) {
        objCon = new SqlConnection(strConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_GetPositionRequestsForSupervisorApproval", objCon);
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@SupervisorID", SqlDbType.VarChar).Value = supervisorID;
            objDA = new SqlDataAdapter(objCmd);
            objDS = new DataSet();
            objDA.Fill(objDS);
            return objDS;
        } finally {
            objCon.Close();
        }
    }

    public DataSet GetPositionRequestsForBudgetApproval() {
        objCon = new SqlConnection(strConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_GetPositionRequestsForBudgetApproval", objCon);
            objCmd.CommandType = CommandType.StoredProcedure;
            objDA = new SqlDataAdapter(objCmd);
            objDS = new DataSet();
            objDA.Fill(objDS);
            return objDS;
        } finally {
            objCon.Close();
        }
    }

    public DataSet GetPositionRequestsForHROAction() {
        objCon = new SqlConnection(strConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_GetPositionRequestsForHROAction", objCon);
            objCmd.CommandType = CommandType.StoredProcedure;
            objDA = new SqlDataAdapter(objCmd);
            objDS = new DataSet();
            objDA.Fill(objDS);
            return objDS;
        } finally {
            objCon.Close();
        }
    }


    public bool EditApproval(Int32 positionRequestID, String employeeID, String name, String userType, String oldDescription, String newDescription, String note, String date) {
        objCon = new SqlConnection(strConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_EditApproval", objCon);
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@PositionRequestID", SqlDbType.Int).Value = positionRequestID;
            objCmd.Parameters.Add("@EmployeeID", SqlDbType.VarChar).Value = employeeID;
            objCmd.Parameters.Add("@Name", SqlDbType.VarChar).Value = name;
            objCmd.Parameters.Add("@UserType", SqlDbType.VarChar).Value = userType;
            objCmd.Parameters.Add("@OldDescription", SqlDbType.VarChar).Value = oldDescription;
            objCmd.Parameters.Add("@NewDescription", SqlDbType.VarChar).Value = newDescription;
            objCmd.Parameters.Add("@Note", SqlDbType.VarChar).Value = note;
            objCmd.Parameters.Add("@Date", SqlDbType.DateTime).Value = date;
            if (objCmd.ExecuteNonQuery() > 0) {
                return true;
            } else {
                return false;
            }
        } finally {
            objCon.Close();
        }
    }

    public DataSet GetPreviousApprovers(Int32 positionRequestID) {
        objCon = new SqlConnection(strConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_GetPreviousApprovers", objCon);
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@PositionRequestID", SqlDbType.Int).Value = positionRequestID;
            objDA = new SqlDataAdapter(objCmd);
            objDS = new DataSet();
            objDA.Fill(objDS);
            return objDS;
        } finally {
            objCon.Close();
        }
    }

    public bool EditStatus(Int32 positionRequestID, String status) {
        objCon = new SqlConnection(strConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_EditStatus", objCon);
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@PositionRequestID", SqlDbType.Int).Value = positionRequestID;
            objCmd.Parameters.Add("@Status", SqlDbType.VarChar).Value = status;
            if (objCmd.ExecuteNonQuery() > 0) {
                return true;
            } else {
                return false;
            }
        } finally {
            objCon.Close();
        }
    }

    public bool EditCreationDate(Int32 positionRequestID, DateTime created) {
        objCon = new SqlConnection(strConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_EditCreationDate", objCon);
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@PositionRequestID", SqlDbType.Int).Value = positionRequestID;
            objCmd.Parameters.Add("@Created", SqlDbType.DateTime).Value = created;
            if (objCmd.ExecuteNonQuery() > 0) {
                return true;
            } else {
                return false;
            }
        } finally {
            objCon.Close();
        }
    }

    public bool EditRoutingDate(Int32 positionRequestID, String userType, String description, String date) {
        objCon = new SqlConnection(strConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_EditRoutingDate", objCon);
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@PositionRequestID", SqlDbType.Int).Value = positionRequestID;
            objCmd.Parameters.Add("@UserType", SqlDbType.VarChar).Value = userType;
            objCmd.Parameters.Add("@Description", SqlDbType.VarChar).Value = description;
            objCmd.Parameters.Add("@Date", SqlDbType.DateTime).Value = date;
            if (objCmd.ExecuteNonQuery() > 0) {
                return true;
            } else {
                return false;
            }
        } finally {
            objCon.Close();
        }
    }

    public bool DeleteRouting(Int32 positionRequestID, String userType, String description, String name, String employeeID) {
        objCon = new SqlConnection(strConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_DeleteRouting", objCon);
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@PositionRequestID", SqlDbType.Int).Value = positionRequestID;
            objCmd.Parameters.Add("@UserType", SqlDbType.VarChar).Value = userType;
            objCmd.Parameters.Add("@Description", SqlDbType.VarChar).Value = description;
            objCmd.Parameters.Add("@Name", SqlDbType.VarChar).Value = name;
            objCmd.Parameters.Add("@EmployeeID", SqlDbType.VarChar).Value = employeeID;
            if (objCmd.ExecuteNonQuery() > 0) {
                return true;
            } else {
                return false;
            }
        } finally {
            objCon.Close();
        }
    }

    public bool DeletePositionRequest(Int32 positionRequestID) {
        objCon = new SqlConnection(strConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_DeletePositionRequest", objCon);
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@PositionRequestID", SqlDbType.Int).Value = positionRequestID;
            if (objCmd.ExecuteNonQuery() > 0) {
                return true;
            } else {
                return false;
            }
        } finally {
            objCon.Close();
        }
    }

    public bool EditPositionEmployee(Int32 positionRequestID, String employeeName, String employeeID, String salaryRangeStep, Decimal salaryAmount
                                    , String employeeType, String employeeEmailAddress, String completionDate, string effectiveDate) {
        objCon = new SqlConnection(strConnection);
        try {
            objCon.Open();
            objCmd = new SqlCommand("usp_EditPositionEmployee", objCon);
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.Parameters.Add("@PositionRequestID", SqlDbType.Int).Value = positionRequestID;
            objCmd.Parameters.Add("@EmployeeName", SqlDbType.VarChar).Value = employeeName;
            objCmd.Parameters.Add("@EmployeeID", SqlDbType.VarChar).Value = employeeID;
            objCmd.Parameters.Add("@SalaryRangeStep", SqlDbType.VarChar).Value = salaryRangeStep;
            objCmd.Parameters.Add("@SalaryAmount", SqlDbType.Money).Value = salaryAmount;
            objCmd.Parameters.Add("@EmployeeType", SqlDbType.VarChar).Value = employeeType;
            objCmd.Parameters.Add("@EmployeeEmailAddress", SqlDbType.VarChar).Value = employeeEmailAddress;
            if(completionDate != null && completionDate != "") { 
                objCmd.Parameters.Add("@Completed", SqlDbType.DateTime).Value = completionDate;
            }
            objCmd.Parameters.AddWithValue("@Effective", effectiveDate);
            if (objCmd.ExecuteNonQuery() > 0) {
                return true;
            } else {
                return false;
            }
        } finally {
            objCon.Close();
        }
    }

    public DataTable GetColumnNames(string tableName)
    {
        using (SqlConnection objCon = new SqlConnection(strConnection))
        {
            objCon.Open();
            using (SqlCommand objCmd = new SqlCommand("SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '" + tableName + "'", objCon))
            {
                objDA = new SqlDataAdapter(objCmd);
                objDS = new DataSet();
                objDA.Fill(objDS);
                return objDS.Tables[0];
            }
        }
    }
}
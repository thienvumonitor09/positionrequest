﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for Utility
/// </summary>
public class Utility
{
    public Utility()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static List<string> GetExecutiveApprovalJobCode()
    {
        List<string> excutiveApprovalJC = new List<string> { "111272", "111165", "111499", "111319", "111433", "111274", "111577", "111465", "111439", "111120" };
        return excutiveApprovalJC;
    }

    public static List<string> GetAdjunctApprovalJobCode()
    {
        return  new List<string> { "111274" , "111577", "111465", "111439", "111120" };
    }

    public static string GetSendEmail(string liveEmail)
    {
        string emailTest = "vu.nguyen@ccs.spokane.edu";
        string appHost = HttpContext.Current.Request.Url.Host.ToLower();
        return (appHost.Contains("internal")) ? liveEmail : emailTest;
    }

    public static string ConvertToDateStr(string dateS)
    {
        string format = "%h:mm tt";
        DateTime dateValue;
        return (DateTime.TryParse(dateS, out dateValue)) ? dateValue.ToString(format) : "";
    }

    public static string ConvertDateTimeToStr(string dateS, string format)
    {
        DateTime dateValue;
        return (DateTime.TryParse(dateS, out dateValue)) ? dateValue.ToString(format) : "";
    }

    public static string[] GetDayWeek()
    {
        return new string[]{ "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" };
    }


    
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PositionRequestEntity
/// </summary>
public class PositionRequestEntity
{
    public int PositionRequestID { get; set; }
    public int TrackingNumber { get; set; }
    public string OriginatorID { get; set; }
    public string OriginatorName { get; set; }
    public string OfficialTitle { get; set; }
    public string WorkingTitle { get; set; }
    public string JobCode { get; set; }
    public string CollegeUnit { get; set; }
    public string BudgetNumber { get; set; }
    public string PositionNumber { get; set; }
    public string DepartmentID { get; set; }
    public string Department { get; set; }
    public string PhoneNumber { get; set; }
    public string MailStop { get; set; }
    public string SupervisorName { get; set; }
    public string SupervisorID { get; set; }
    public string PositionType { get; set; }
    public int PermanentPosition { get; set; }
    public int NonPermanentPosition { get; set; }
    public DateTime PositionEndDate { get; set; }
    public string RecruitmentType { get; set; }
    public int NewPosition { get; set; }
    public int ReplacementPosition { get; set; }
    public string ReplacedEmployeeName { get; set; }
    public int PCard { get; set; }
    public int CCSIssuedCellPhone { get; set; }
    public int CTCLinkAccess { get; set; }
    public int TimeSheetApprover { get; set; }
    public string HoursPerDay { get; set; }


    public PositionRequestEntity()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}
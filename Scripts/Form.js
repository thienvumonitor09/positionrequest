﻿$(function () {
    
    var hidTodo = $("input[id$='hidTodo']");
    if (hidTodo.val() == "add" || hidTodo.val() == "edit" || hidTodo.val() == "view") {
        var lblStatus = $("span[id$='lblStatus']");
        //alert("hidTodo.val() = " + hidTodo.val() + " && lblStatus.text() = " + lblStatus.text());
        if (hidTodo.val() == "add" || hidTodo.val() == "edit" || (hidTodo.val() == "view" && (lblStatus.text() == "Budget" || lblStatus.text() == "HRO"))) {
            var uploadArea = $("div[id$='uploadArea']");
            var fileExtension = ['pdf', 'doc', 'docx', 'xls', 'xlsx', 'txt', 'jpg', 'jpeg', 'gif', 'png', 'tif', 'tiff'];

            //Add file field on page load
            addFileField();

            //Check uploaded file extension
            uploadArea.change(function () {
                uploadArea.children().each(function (index, item) {
                    var typeID = $('#' + item.id).find("input").val();
                    console.log(typeID);
                    if (typeID.trim()) {
                        if ($.inArray(typeID.split('.').pop().toLowerCase(), fileExtension) == -1) {
                            alert("Only the following formats are allowed : " + fileExtension.join(', '));
                            //removeCurrentFile(item.id);
                            var uploadArea = document.getElementById($("div[id$='uploadArea']").attr('id'));
                            uploadArea.removeChild(document.getElementById(item.id));
                            fileUploadBox();
                            //return false;
                        }
                    }

                });
            });

        }

        if (hidTodo.val() == "add" || hidTodo.val() == "edit") {
            //declare form field variables
            var permissionType = getParameterValues("type");
            var hidSupervisorID = $("input[id$='hidSupervisorID']");
            var lblSupervisorID = $("span[id$='lblSupervisorID']");
            var txtSupervisorName = $("input[id$='txtSupervisorName']");
            var cboCollegeUnit = $("select[id$='cboCollegeUnit']");
            var hidDepartmentID = $("input[id$='hidDepartmentID']");
            var lblDepartmentID = $("span[id$='lblDepartmentID']");
            var cboDepartment = $("select[id$='cboDepartment']");
            var hidDepartment = $("input[id$='hidDepartment']");
            var optReplacement = $("input[id$='optReplacement']");
            var txtEmployeeReplaced = $("input[id$='txtEmployeeReplaced']");
            var cboPositionType = $("select[id$='cboPositionType']");
            var optNonPermanent = $("input[id$='optNonPermanent']");
            var txtEndDate = $("input[id$='txtEndDate']");
            var cboRecruitmentType = $("select[id$='cboRecruitmentType']");
            var txtMonthsPerYear = $("input[id$='txtMonthsPerYear']");
            var txtCyclicCalendarCode = $("input[id$='txtCyclicCalendarCode']");
            var txtMondayStart = $("input[id$='txtMondayStart']");
            var txtTuesdayStart = $("input[id$='txtTuesdayStart']");
            var txtWednesdayStart = $("input[id$='txtWednesdayStart']");
            var txtThursdayStart = $("input[id$='txtThursdayStart']");
            var txtFridayStart = $("input[id$='txtFridayStart']");
            var txtSaturdayStart = $("input[id$='txtSaturdayStart']");
            var txtSundayStart = $("input[id$='txtSundayStart']");
            var txtMondayEnd = $("input[id$='txtMondayEnd']");
            var txtTuesdayEnd = $("input[id$='txtTuesdayEnd']");
            var txtWednesdayEnd = $("input[id$='txtWednesdayEnd']");
            var txtThursdayEnd = $("input[id$='txtThursdayEnd']");
            var txtFridayEnd = $("input[id$='txtFridayEnd']");
            var txtSaturdayEnd = $("input[id$='txtSaturdayEnd']");
            var txtSundayEnd = $("input[id$='txtSundayEnd']");
            
            if (hidSupervisorID.val() != "") {
                lblSupervisorID.html(hidSupervisorID.val());
            } else {
                lblSupervisorID.html("Auto Populated");
            }

            if (cboCollegeUnit.val() != "") {
                getDepartments();
            }

            if (hidDepartmentID.val() != "") {
                lblDepartmentID.html(hidDepartmentID.val());
            } else {
                lblDepartmentID.html("Auto Populated");
            }

            if (optReplacement.is(":checked")) {
                $("#employeeReplaced").show();
            } else {
                $("#employeeReplaced").hide();
            }

            if (cboPositionType.val() == "Classified" || cboPositionType.val() == "Part-Time Hourly") {
                $("#schedule").show();
            } else {
                $("#schedule").hide();
            }

            if (cboPositionType.val() == "Classified") {
                $("#classified").show();
            } else {
                $("#classified").hide();
            }

            if (cboPositionType.val() == "Part-Time Hourly") {
                $("#worksheet").show();
            } else {
                $("#worksheet").hide();
            }

            if (cboPositionType.val() == "Adjunct Faculty") {
                $("#attachmentDescription").hide();
            } else {
                $("#attachmentDescription").show();
            }

            /* 07-30
            if (cboPositionType.val() == "Part-Time Hourly" || cboPositionType.val() == "Adjunct Faculty") {
                cboRecruitmentType.append("<option value='No Recruitment'>No Recruitment</option>");
            }
            */
            if (optNonPermanent.is(":checked")) {
                $("#endDate").show();
            } else {
                $("#endDate").hide();
            }

            if (parseInt(txtMonthsPerYear.val()) < 12 && cboPositionType.val() == "Classified") {
                $("#cyclicCalendarCode").show();
            } else {
                $("#cyclicCalendarCode").hide();
            }

            cboCollegeUnit.change(getDepartments);

            cboPositionType.change(function () {

                if (cboPositionType.val() == "Classified" || cboPositionType.val() == "Part-Time Hourly") {
                    $("#schedule").show();
                } else {
                    $("#schedule").hide();
                }

                if (cboPositionType.val() == "Classified") {
                    $("#classified").show();
                } else {
                    $("#classified").hide();
                }

                if (cboPositionType.val() == "Part-Time Hourly") {
                    $("#worksheet").show();
                } else {
                    $("#worksheet").hide();
                }

                if (cboPositionType.val() == "Adjunct Faculty") {
                    $("#attachmentDescription").hide();
                } else {
                    $("#attachmentDescription").show();
                }

                //check type of recruitment
                //$(cboRecruitmentType.id + " option[value='No Recruitment']").remove(); //test this
                //Add "No Recruitment" when permisionType <> HRO"
                /* 07-30
                if (typeof permissionType === 'undefined' || permissionType.indexOf("HRO") == -1) {
                    $(cboRecruitmentType).find("[value='No Recruitment']").remove();
                    if (cboPositionType.val() == "Part-Time Hourly"
                        || cboPositionType.val() == "Adjunct Faculty"
                        || (cboPositionType.val() == "Classified" && optNonPermanent.is(":checked"))) {
                        cboRecruitmentType.append("<option value='No Recruitment'>No Recruitment</option>");
                    }
                }
                */
                if (parseInt(txtMonthsPerYear.val()) < 12 && cboPositionType.val() == "Classified") {
                    $("#cyclicCalendarCode").show();
                } else {
                    $("#cyclicCalendarCode").hide();
                }
            });

            //immediate supervisor last name autocomplete
            txtSupervisorName.autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        url: '/PositionRequest/WebService.asmx/GetEmployeeListByKeyword',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        data: "{ 'lastName': '" + request.term + "' }", //lastName param to be passed to GetEmployeeList function in code-behind
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    value: item.empId,
                                    label: item.empName,
                                    dept: item.deptName
                                } //must map return value because automplete only works with object array in format (value, label)
                            }));
                        },
                        error: function (error) {
                            console.log("error");
                        }
                    });
                },
                minLength: 1, //The minimum number of characters a user must type before a search is performed.
                select: function (event, ui) {
                    txtSupervisorName.val(ui.item.label);
                    lblSupervisorID.html(ui.item.value);
                    hidSupervisorID.val(ui.item.value);
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li>").append("<a>" + item.label + " | " + item.dept + "</a>").appendTo(ul);
            };

             
            $("#classified").change(function () {
                //$(cboRecruitmentType.id + " option[value='No Recruitment']").remove(); //test this
                //$(cboRecruitmentType).find("[value='No Recruitment']").remove();
                if (optNonPermanent.is(":checked")) {
                    $("#endDate").show();
                    txtEndDate.focus();

                    //add non-recruitment to type of recruitment
                    //cboRecruitmentType.append("<option value='No Recruitment'>No Recruitment</option>");
                } else {
                    $("#endDate").hide();
                }

                //Allow HR to override "No Recruitment"
                if (typeof permissionType !== 'undefined' && permissionType.indexOf("HRO") > -1) {
                    //$(cboRecruitmentType).find("[value='No Recruitment']").remove();
                    //cboRecruitmentType.append("<option value='No Recruitment'>No Recruitment</option>");
                }
            });
            
            $("#newReplacement").change(function () {
                if (optReplacement.is(":checked")) {
                    $("#employeeReplaced").show();
                    txtEmployeeReplaced.focus();
                } else {
                    $("#employeeReplaced").hide();
                }
            });

            txtMonthsPerYear.change(function () {
                if (parseInt(txtMonthsPerYear.val()) < 12 && cboPositionType.val() == "Classified") {
                    $("#cyclicCalendarCode").show();
                    txtCyclicCalendarCode.focus();
                } else {
                    $("#cyclicCalendarCode").hide();
                }
            });

            $("#cmdApplyAll").click(function () {
                var mondayStart = txtMondayStart.val();
                var mondayEnd = txtMondayEnd.val();
                txtTuesdayStart.val(mondayStart);
                txtWednesdayStart.val(mondayStart);
                txtThursdayStart.val(mondayStart);
                txtFridayStart.val(mondayStart);
                txtSaturdayStart.val(mondayStart);
                txtSundayStart.val(mondayStart);

                txtTuesdayEnd.val(mondayEnd);
                txtWednesdayEnd.val(mondayEnd);
                txtThursdayEnd.val(mondayEnd);
                txtFridayEnd.val(mondayEnd);
                txtSaturdayEnd.val(mondayEnd);
                txtSundayEnd.val(mondayEnd);
            });

            $("#cmdReset").click(function () {
                var mondayStart = txtMondayStart.val();
                var mondayEnd = txtMondayEnd.val();
                txtMondayStart.val("");
                txtTuesdayStart.val("");
                txtWednesdayStart.val("");
                txtThursdayStart.val("");
                txtFridayStart.val("");
                txtSaturdayStart.val("");
                txtSundayStart.val("");

                txtMondayEnd.val("");
                txtTuesdayEnd.val("");
                txtWednesdayEnd.val("");
                txtThursdayEnd.val("");
                txtFridayEnd.val("");
                txtSaturdayEnd.val("");
                txtSundayEnd.val("");
            });

            //onchange of the department dropdownlist populate the department number label
            cboDepartment.change(function () {
                lblDepartmentID.html($(this).val() == "" ? "Auto Populated" : $(this).val());
                hidDepartmentID.val($(this).val());
                hidDepartment.val($("option:selected", cboDepartment).text());
            });
        }
    }
});

function getDepartments() {
    $.ajax({
        type: "POST",
        url: "/PositionRequest/WebService.asmx/PopulateDepartments",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var cboDepartment = $("select[id$='cboDepartment']");
            var cboCollegeUnit = $("select[id$='cboCollegeUnit']");
            var hidDepartmentID = $("input[id$='hidDepartmentID']");
            //var hidDepartment = $("input[id$='hidDepartment']");
            var lblDepartmentID = $("span[id$='lblDepartmentID']");

            //remove all options from the department dropdownlist
            cboDepartment.removeOption(/./);

            //if a college unit is not selected
            if (cboCollegeUnit.val() == "") {
                cboDepartment.addOption("", "-- Select One --");
                lblDepartmentID.html("Auto Populated");
            } else {
                //populate the department dropdownlist
                $.each(response.d, function () {
                    if (hidDepartmentID.val() != "") {
                        if (hidDepartmentID.val() == this["Value"]) {
                            cboDepartment.addOption(this["Value"], this["Text"], true);
                        } else {
                            cboDepartment.addOption(this["Value"], this["Text"], false);
                        }
                    } else {
                        cboDepartment.addOption(this["Value"], this["Text"], false);
                    }
                });
            }
        }
    });
}

function addFileField() {
    if ($("div[id$='uploadArea']").has("div").length == 0) {
        fileUploadBox();
    }
}

function fileUploadBox() {
    if (!document.getElementById || !document.createElement)
        return false;

    var uploadArea = document.getElementById($("div[id$='uploadArea']").attr('id')); 

    if (!uploadArea)
        return;

    // The new box needs a name and an ID
    if (!fileUploadBox.lastAssignedId)
        fileUploadBox.lastAssignedId = 100;

    //Create div container for newUploadBox input and delete image
    var fileDiv = document.createElement("div");
    fileDiv.style.padding = "3px";
    fileDiv.setAttribute("id", "fileDiv" + fileUploadBox.lastAssignedId);
    var currentID = "fileDiv" + fileUploadBox.lastAssignedId;
    
    //Create newUploadBox input
    var newUploadBox = document.createElement("input");

    // Set up the new input for file uploads
    newUploadBox.type = "file";
    newUploadBox.size = "72";
    newUploadBox.style.width = "93%";
    newUploadBox.style.marginTop = "-3px";
    newUploadBox.style.fontSize = "9pt";
    newUploadBox.setAttribute("id", "dynamic" + fileUploadBox.lastAssignedId);
    newUploadBox.setAttribute("name", "dynamic:" + fileUploadBox.lastAssignedId);
    fileDiv.appendChild(newUploadBox);
    fileUploadBox.lastAssignedId++;

    //Create div container for <a>
    var actionDiv = document.createElement("div");
    actionDiv.style.cssFloat = "right";
    actionDiv.style.width = "16px";
    actionDiv.classList.add('action');

    //Create <a> for delete image
    var deleteLink = document.createElement('a');
    deleteLink.setAttribute('href', 'javascript:void(0);'); // use javascript:void(0); instead of # to prevent returning back to top of the page
    deleteLink.setAttribute('class', 'delete');
    deleteLink.onclick = function () {
        removeCurrentFile(currentID);
    };
    actionDiv.appendChild(deleteLink);
    fileDiv.appendChild(actionDiv);
    uploadArea.appendChild(fileDiv);
}

//Function to remove link for file upload
function removeCurrentFile(currentID) {
    var uploadArea = document.getElementById($("div[id$='uploadArea']").attr('id')); 
    uploadArea.removeChild(document.getElementById(currentID));
    addFileField();
}

//Function to validate add panel when submit or save form
function validate(user) {
    var isValid = true;
    var hidTodo = $("input[id$='hidTodo']");

    if (hidTodo.val() == "add" || hidTodo.val() == "edit") {
        var txtOfficialTitle = $("input[id$='txtOfficialTitle']");
        var txtWorkingTitle = $("input[id$='txtWorkingTitle']");
        var txtJobCode = $("input[id$='txtJobCode']");
        var txtBudgetNumber = $("input[id$='txtBudgetNumber']");
        var cboCollegeUnit = $("select[id$='cboCollegeUnit']");
        var cboDepartment = $("select[id$='cboDepartment']");
        var txtPhoneNumber = $("input[id$='txtPhoneNumber']");
        var txtMailStop = $("input[id$='txtMailStop']");
        var txtSupervisorName = $("input[id$='txtSupervisorName']");
        var lblSupervisorID = $("span[id$='lblSupervisorID']");
        var optNew = $("input[id$='optNew']");
        var optReplacement = $("input[id$='optReplacement']");
        var txtEmployeeReplaced = $("input[id$='txtEmployeeReplaced']");
        var cboPositionType = $("select[id$='cboPositionType']");
        var optPermanent = $("input[id$='optPermanent']");
        var optNonPermanent = $("input[id$='optNonPermanent']");
        var txtEndDate = $("input[id$='txtEndDate']");
        var cboRecruitmentType = $("select[id$='cboRecruitmentType']");

        if (txtOfficialTitle.val() == "") {
            alert("Please enter the Official Position Title before submitting the form.");
            txtOfficialTitle.focus();
            isValid = false;
        } else if (txtWorkingTitle.val() == "" && ( cboPositionType.val() != "Part-Time Hourly" && cboPositionType.val() != "Adjunct Faculty") ) { //Working Title is required for all positions except PT and adjunct
            alert("Please enter the Working Title before submitting the form.");
            txtWorkingTitle.focus();
            isValid = false;
        }else if (txtJobCode.val() == "") {
            alert("Please enter the Job Code before submitting the form.");
            txtJobCode.focus();
            isValid = false;
        } else if (txtBudgetNumber.val() == "") {
            alert("Please enter the Budget # before submitting the form.");
            txtBudgetNumber.focus();
            isValid = false;
        } else if (cboCollegeUnit.val() == "") {
            alert("Please select the College/Unit before submitting the form.");
            cboCollegeUnit.focus();
            isValid = false;
        } else if (cboDepartment.val() == "") {
            alert("Please select the Department Name before submitting the form.");
            cboDepartment.focus();
            isValid = false;
        } else if (txtPhoneNumber.val() == "") {
            alert("Please enter the Position Phone # before submitting the form.");
            txtPhoneNumber.focus();
            isValid = false;
        } else if (txtMailStop.val() == "") {
            alert("Please enter the Mail Stop before submitting the form.");
            txtMailStop.focus();
            isValid = false;
        } else if (txtSupervisorName.val() == "") {
            alert("Please enter the Immediate Supervisor before submitting the form.");
            txtSupervisorName.focus();
            isValid = false;
        } else if (lblSupervisorID.html() == "Auto Populated") {
            alert("Please search and select the Immediate Supervisor by last name before submitting the form.");
            txtSupervisorName.focus();
            txtSupervisorName.select();
            isValid = false;
        } else if (!optNew.is(":checked") && !optReplacement.is(":checked")) {
            alert("Please select if this is a New or Replacement position before submitting the form.");
            optNew.focus();
            isValid = false;
        } else if (optReplacement.is(":checked") && txtEmployeeReplaced.val() == "") {
            alert("Please enter the Name of Employee Replaced before submitting the form.");
            txtEmployeeReplaced.focus();
            isValid = false;
        } else if (cboPositionType.val() == "") {
            alert("Please select the Type of Position before submitting the form.");
            cboPositionType.focus();
            isValid = false;
        } else if (cboPositionType.val() == "Classified" && (!optPermanent.is(":checked") && !optNonPermanent.is(":checked"))) {
            alert("Please select if this is a Permanent or Non-permanent position before submitting the form.");
            optPermanent.focus();
            isValid = false;
        } else if (optNonPermanent.is(":checked") && txtEndDate.val() == "") {
            alert("Please enter the End Date before submitting the form.");
            txtEndDate.focus();
            isValid = false;
        } else if (cboRecruitmentType.val() == "") {
            alert("Please select the Type of Recruitment before submitting the form.");
            cboRecruitmentType.focus();
            isValid = false;
        } else if (cboPositionType.val() == "Classified" || cboPositionType.val() == "Part-Time Hourly") {
            var txtHoursPerDay = $("input[id$='txtHoursPerDay']");
            var txtHoursPerWeek = $("input[id$='txtHoursPerWeek']");
            var txtMonthsPerYear = $("input[id$='txtMonthsPerYear']");
            var txtCyclicCalendarCode = $("input[id$='txtCyclicCalendarCode']");
            var txtMondayStart = $("input[id$='txtMondayStart']");
            var txtTuesdayStart = $("input[id$='txtTuesdayStart']");
            var txtWednesdayStart = $("input[id$='txtWednesdayStart']");
            var txtThursdayStart = $("input[id$='txtThursdayStart']");
            var txtFridayStart = $("input[id$='txtFridayStart']");
            var txtSaturdayStart = $("input[id$='txtSaturdayStart']");
            var txtSundayStart = $("input[id$='txtSundayStart']");
            var txtMondayEnd = $("input[id$='txtMondayEnd']");
            var txtTuesdayEnd = $("input[id$='txtTuesdayEnd']");
            var txtWednesdayEnd = $("input[id$='txtWednesdayEnd']");
            var txtThursdayEnd = $("input[id$='txtThursdayEnd']");
            var txtFridayEnd = $("input[id$='txtFridayEnd']");
            var txtSaturdayEnd = $("input[id$='txtSaturdayEnd']");
            var txtSundayEnd = $("input[id$='txtSundayEnd']");

            if (cboPositionType.val() == "Classified" && txtHoursPerDay.val() == "") {
                alert("Please enter how many Hours Per Day this position will work before submitting the form.");
                txtHoursPerDay.focus();
                isValid = false;
            } else if (cboPositionType.val() == "Classified" & txtHoursPerWeek.val() == "") {
                alert("Please enter how many Hours Per Week this position will work before submitting the form.");
                txtHoursPerWeek.focus();
                isValid = false;
            } else if (cboPositionType.val() == "Classified" & txtMonthsPerYear.val() == "") {
                alert("Please enter how many Months Per Year this position will work before submitting the form.");
                txtMonthsPerYear.focus();
                isValid = false;
            } else {
                /*
                if (isNaN(txtHoursPerDay.val())) {
                    alert("Please enter a valid number for Hours per Day.");
                    txtHoursPerDay.focus();
                    isValid = false;
                }else if(isNaN(txtHoursPerWeek.val())) {
                    alert("Please enter a valid number for Hours Per Week.");
                    txtHoursPerWeek.focus();
                    isValid = false;
                } else if (isNaN(txtMonthsPerYear.val())) {
                    alert("Please enter a valid number for Months Per Year.");
                    txtMonthsPerYear.focus();
                    isValid = false;
                }
                */
                if (cboPositionType.val() == "Classified" && txtMonthsPerYear.val() < 12 && txtCyclicCalendarCode.val() == "") {
                    alert("Please enter Cyclic Calendar Code before submitting the form.");
                    txtCyclicCalendarCode.focus();
                    isValid = false;
                } else {
                    if ((txtMondayStart.val() != "" && txtMondayEnd.val() != "") || (txtTuesdayStart.val() != "" && txtTuesdayEnd.val() != "")
                        || (txtWednesdayStart.val() != "" && txtWednesdayEnd.val() != "") || (txtThursdayStart.val() != "" && txtThursdayEnd.val() != "")
                        || (txtFridayStart.val() != "" && txtFridayEnd.val() != "") || (txtSaturdayStart.val() != "" && txtSaturdayEnd.val() != "")
                        || (txtSundayStart.val() != "" && txtSundayEnd.val() != "")) {
                    } else {
                        if (cboPositionType.val() != "Part-Time Hourly") {
                            alert("Please complete the Daily Work Schedule before submitting the form.");
                            txtMondayStart.focus();
                            isValid = false;
                        }

                    }
                }
            }
        }

        

        if (isValid && cboPositionType.val() != "Adjunct Faculty") {
            var uploadArea = $("div[id$='uploadArea']");
            var hidFileAttachments = $("input[id$='hidFileAttachments']");
            if (uploadArea.first().find("input").val() == "" && hidFileAttachments.val() == "") {
                if (cboPositionType.val() == "Part-Time Hourly") {
                    alert("Please attach a part-time hourly eligibility worksheet and an approved position description before submitting the form.");
                } else {
                    alert("Please attach an approved position description before submitting the form.");
                }
                uploadArea.focus();
                isValid = false;
            }
        }

        if (isValid && (user == "HRO" || user == "Budget")) {
            var txtModReason = $("textarea[id$='txtModReason']");
            var txtComboCode = $("input[id$='txtComboCode']");
            if (txtModReason.val() == "") {
                alert("Please enter a reason for the modifications.");
                txtModReason.focus();
                isValid = false;
            }//else check HRO approval checkboxes and input fields and budget checkboxes and input fields (which may be different) 
        }
    } else if (hidTodo.val() == "view") {

        var approvalOption = $("input:radio[name='" + $("input:radio[name$='optApproval']").attr('name') + "']:checked").val();

        if (user == "HRO") {
            var hidStatus = $("input[id$='hidStatus']"); 
            var cboStatus = $("select[id$='cboStatus']");
            var txtEmployeeName = $("input[id$='txtEmployeeName']");
            var txtEmployeeID = $("input[id$='txtEmployeeID']");
            var txtEmployeeEmailAddress = $("input[id$='txtEmployeeEmailAddress']");
            var txtSalaryRangeStep = $("input[id$='txtSalaryRangeStep']");
            var txtSalaryAmount = $("input[id$='txtSalaryAmount']");
            var txtEffectiveDate = $("input[id$='txtEffectiveDate']");
            var cboEmployeeType = $("select[id$='cboEmployeeType']");
            var txtCompletionDate = $("input[id$='txtCompletionDate']");

            //make sure approve, reject, or void are selected
            if (hidStatus.val() == "HRO") {
                if (approvalOption == undefined || approvalOption == "") {
                    alert("Please select Approve, Reject or Void.");
                    isValid = false;
                } else if (approvalOption == "optApprove") {
                    if (cboStatus.val() == "") {
                        alert("Please select the Status before submitting your approval.");
                        isValid = false;
                    } else if (cboStatus.val() == "Complete") {
                        //check that all employee information fields are filled out
                        if (txtEmployeeName.val() == "") {
                            alert("Please enter the Employee Name.");
                            txtEmployeeName.select();
                            isValid = false;
                        } else if (txtEmployeeID.val() == "") {
                            alert("Please enter the Employee ID.");
                            txtEmployeeID.select();
                            isValid = false;
                        } else if (txtEmployeeEmailAddress.val() == "") {
                            alert("Please enter the Employee Email Address.");
                            txtEmployeeEmailAddress.select();
                            isValid = false;
                        } else if (txtSalaryRangeStep.val() == "") {
                            alert("Please enter the Salary Range/Step.");
                            txtSalaryRangeStep.select();
                            isValid = false;
                        } else if (txtSalaryAmount.val() == "") {
                            alert("Please enter the Salary Amount.");
                            txtSalaryAmount.select();
                            isValid = false;
                        } else if (txtEffectiveDate.val() == "") {
                            alert("Please select an effective date.");
                            txtEffectiveDate.select();
                            isValid = false;
                        }
                        else if (cboEmployeeType.val() == "") {
                            alert("Please select an Employee Type.");
                            cboEmployeeType.focus();
                            isValid = false;
                        } else if (txtCompletionDate.val() == "") {
                            alert("Please select a completion date.");
                            txtCompletionDate.select();
                            isValid = false;
                        }
                    }
                } else if (approvalOption == "optReject") {
                    var txtReject = $("input[id$='txtReject']");
                    if (txtReject.val() == "") {
                        alert("Please enter a reason for rejecting the position request.");
                        txtReject.select();
                        isValid = false;
                    }
                } else if (approvalOption == "optVoid") {
                    var txtVoid = $("input[id$='txtVoid']");
                    if (txtVoid.val() == "") {
                        alert("Please enter a reason for marking the position request as void.");
                        txtVoid.select();
                        isValid = false;
                    }
                }
            } else if (hidStatus.val() == "Recruiting" || hidStatus.val() == "Hired") {
                if (cboStatus.val() == "") {
                    alert("Please select the Status before submitting your approval.");
                    isValid = false;
                } else if (cboStatus.val() == hidStatus.val()) {
                    alert("Please change the status before submitting the form.");
                    isValid = false;
                } else if (cboStatus.val() == "Complete") {
                    //check that all employee information fields are filled out
                    if (txtEmployeeName.val() == "") {
                        alert("Please enter the Employee Name.");
                        txtEmployeeName.select();
                        isValid = false;
                    } else if (txtEmployeeID.val() == "") {
                        alert("Please enter the Employee ID.");
                        txtEmployeeID.select();
                        isValid = false;
                    } else if (txtEmployeeEmailAddress.val() == "") {
                        alert("Please enter the Employee Email Address.");
                        txtEmployeeEmailAddress.select();
                        isValid = false;
                    } else if (txtSalaryRangeStep.val() == "") {
                        alert("Please enter the Salary Range/Step.");
                        txtSalaryRangeStep.select();
                        isValid = false;
                    } else if (txtSalaryAmount.val() == "") {
                        alert("Please enter the Salary Amount.");
                        txtSalaryAmount.select();
                        isValid = false;
                    } else if (txtEffectiveDate.val() == "") {
                        alert("Please select an effective date.");
                        txtEffectiveDate.select();
                        isValid = false;
                    } else if (cboEmployeeType.val() == "") {
                        alert("Please select an Employee Type.");
                        cboEmployeeType.select();
                        isValid = false;
                    } else if (txtCompletionDate.val() == "") {
                        alert("Please select a completion date.");
                        txtCompletionDate.select();
                        isValid = false;
                    }
                }
            }
        } else if (user == "Budget") {
            //make sure approve or reject are selected
            if (approvalOption == undefined || approvalOption == "") {
                alert("Please select Approve or Reject.");
                isValid = false;
            } else if (approvalOption == "optReject") {
                var txtReject = $("input[id$='txtReject']");
                if (txtReject.val() == "") {
                    alert("Please enter a reason for rejecting the position request.");
                    txtReject.select();
                    isValid = false;
                }
            }
        } else if (user == "Supervisor") { 
            //make sure approve or reject are selected
            if (approvalOption == undefined || approvalOption == "") {
                alert("Please select Approve or Reject.");
                isValid = false;
            } else if (approvalOption == "optReject") {
                var txtReject = $("input[id$='txtReject']");
                if (txtReject.val() == "") {
                    alert("Please enter a reason for rejecting the position request.");
                    txtReject.select();
                    isValid = false;
                }
            }
        }
    }

    return isValid;
}

//Function to get parameters from URL 
function getParameterValues(param) {
    var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < url.length; i++) {
        var urlparam = url[i].split('=');
        if (urlparam[0] == param) {
            return urlparam[1];
        }
    }
}  
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/container/container.master" AutoEventWireup="true" CodeFile="Form.aspx.cs" Inherits="Form"  EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContent" Runat="Server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="menuHolder" Runat="Server"></asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="maintextHolder" Runat="Server">

    <form id="frmPR" runat="server" enctype="multipart/form-data">
        
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
        </asp:ScriptManager>

        <input type="hidden" id="hidTodo" runat="server" />
        <input type="hidden" id="hidDepartment" runat="server" />
        <input type="hidden" id="hidDepartmentID" runat="server" />
        <input type="hidden" id="hidSupervisorID" runat="server" />
        <input type="hidden" id="hidStatus" runat="server" />
        <input type="hidden" id="hidID" runat="server" />
        <input type="hidden" id="hidPopulateFromDB" runat="server" />
        <input type="hidden" id="hidFileAttachments" runat="server" />

        <h3 class="pageTitle">Position Request (PR)</h3>
        <!-- SEARCH AND LIST POSITION REQUEST FORMS -->
        <asp:Panel ID="panList" runat="server">
            <div class="title">
                <div style="float: left; width: 50%"><strong>SEARCH</strong></div>
                <div style="float: right; width: 50%; text-align: right; font-weight: normal">Note: All the search fields work together.&nbsp;&nbsp;</div>
                <div class="clearer"></div>
            </div>
            <div class="section" style="margin-bottom: 20px">
                <div class="row">
                    <div class="col" style="width: 17%">
                        <strong>Status</strong><br />
                        <asp:DropDownList ID="cboSearchStatus" runat="server" Style="width: 100px">
                            <asp:ListItem Value="">ALL</asp:ListItem>
                            <asp:ListItem Value="Approver">Approver</asp:ListItem>
                            <asp:ListItem Value="Budget">Budget</asp:ListItem>
                            <asp:ListItem Value="Cancelled">Cancelled</asp:ListItem>
                            <asp:ListItem Value="Chancellor">Chancellor</asp:ListItem>
                            <asp:ListItem Value="Complete">Complete</asp:ListItem>
                            <asp:ListItem Value="Executive">Executive</asp:ListItem>
                            <asp:ListItem Value="Hired">Hired</asp:ListItem>
                            <asp:ListItem Value="HRO">HRO</asp:ListItem>
                            <asp:ListItem Value="Incomplete">Incomplete</asp:ListItem>
                            <asp:ListItem Value="Recruiting">Recruiting</asp:ListItem>
                            <asp:ListItem Value="Rejected">Rejected</asp:ListItem>
                            <asp:ListItem Value="Void">Void</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col" style="width: 35%">
                        <strong>Official Position Title</strong> <br />
                        <asp:TextBox ID="txtSearchOfficialTitle" runat="server" Style="width: 100%"></asp:TextBox> 
                    </div>
                    <div class="col" style="width: 35%">
                        <strong>Type of Position</strong><br />
                        <asp:DropDownList ID="cboSearchPositionType" runat="server" Style="width: 210px">
                            <asp:ListItem Value="">ALL</asp:ListItem>
                            <asp:ListItem Value="Classified">Classified</asp:ListItem>
                            <asp:ListItem Value="Part-Time Hourly">Part-Time Hourly</asp:ListItem>
                            <asp:ListItem Value="Tenure-Track Faculty">Tenure-Track Faculty</asp:ListItem>
                            <asp:ListItem Value="Non Tenure-Track Faculty">Non Tenure-Track Faculty</asp:ListItem>
                            <asp:ListItem Value="Adjunct Faculty">Adjunct Faculty</asp:ListItem>
                            <asp:ListItem Value="Professional/Confidential Exempt">Professional/Confidential Exempt</asp:ListItem>
                            <asp:ListItem Value="Administrator/Executive">Administrator/Executive</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="row" style="padding-top: 10px;">
                    <div class="col" style="width: 48%; text-align: right; margin-right: 0; padding-right: 0;">
                        <asp:Button ID="cmdSearch" runat="server" Text="Search" Style="width: 100px" OnClick="cmdSearch_Click" />
                    </div>
                    <div class="col" style="width: 45%; text-align: left; padding-left: 0;">
                        <input type="reset" value="Reset" style="width: 100px; margin-left: 10px;" onclick="location.href = 'Form.aspx'" />
                    </div>
                </div>
            </div>
            <asp:Table ID="tblList" runat="server">
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell CssClass="sortHeader" onclick="sortTable(0,'maintextHolder_tblList')">Status</asp:TableHeaderCell>
                    <asp:TableHeaderCell CssClass="sortHeader" onclick="sortTable(1,'maintextHolder_tblList')">Created</asp:TableHeaderCell>
                    <asp:TableHeaderCell CssClass="sortHeader" onclick="sortTable(2,'maintextHolder_tblList')">Official Title</asp:TableHeaderCell>
                    <asp:TableHeaderCell CssClass="sortHeader" onclick="sortTable(3,'maintextHolder_tblList')">Position Type</asp:TableHeaderCell>
                    <asp:TableHeaderCell CssClass="sortHeader" onclick="sortTable(3,'maintextHolder_tblList')">Budget #</asp:TableHeaderCell>
                    <asp:TableHeaderCell ColumnSpan="2" style="background: url('App_Themes/Moxie/images/defaults/blue.gif');width:75px"><a href="#" style="color:#fff" class="add" title="Add a New Form" onclick="document.getElementById('<%= hidTodo.ClientID %>').value='add';document.getElementById('<%= frmPR.ClientID %>').submit();">ADD NEW</a></asp:TableHeaderCell>
                </asp:TableHeaderRow>
            </asp:Table>
        </asp:Panel>

        <!-- POSITION REQUEST FORM -->
        <asp:Panel ID="panForm" runat="server">
            <asp:Label ID="lblError" runat="server" Style="color: red;"></asp:Label>
            <asp:Label ID="lblConfirm" runat="server"></asp:Label>
            <div class="title">
                <div style="float: left; width: 68%;">
                    ORIGINATOR:&nbsp;<span class="normal"><asp:Label ID="lblOriginatorName" runat="Server"></asp:Label></span>
                    &nbsp;ctcLink ID:&nbsp;<span class="normal"><asp:Label ID="lblOriginatorID" runat="Server"></asp:Label></span>
                </div>
                <div style="float: right; text-align: right; width: 31%; margin-right: 5px;">
                    <asp:Panel ID="panTrackingNumber" runat="server">TRACKING #:&nbsp;<span class="normal"><asp:Label ID="lblTrackingNumber" runat="server"></asp:Label></span></asp:Panel>
                </div>
                <div class="clearer"></div>
            </div>
            <asp:Panel ID="panOutput" runat="server">
                <div class="section" style="border-bottom: none;">
                    <div class="row">
                        <div class="col" style="width: 46%">
                            <strong>Official Position Title</strong> <br />
                            <asp:Label ID="lblOfficialTitle" runat="Server" Style="width: 100%;"></asp:Label>
                        </div>
                        <div class="col" style="width: 46%">
                            <strong>Working Title</strong><br />
                            <asp:Label ID="lblWorkingTitle" runat="Server" Style="width: 100%;"></asp:Label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col" style="width: 46%;">
                            <strong>Job Code</strong><br />
                            <asp:Label ID="lblJobCode" runat="Server" Style="width: 100%;"></asp:Label>
                        </div>
                        <div class="col" style="width: 46%;">
                            <strong>Budget #</strong><br />
                            <asp:Label ID="lblBudgetNumber" runat="Server" Style="width: 100%;"></asp:Label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col" style="width: 46%;">
                            <strong>Position Control #</strong><br />
                            <asp:Label ID="lblPositionNumber" runat="Server" Style="width: 100%;"></asp:Label>
                        </div>
                        <div class="col" style="width: 46%;">
                            <strong>College/Unit</strong><br />
                            <asp:Label ID="lblCollegeUnit" runat="server" Style="width: 100%;"></asp:Label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col" style="width: 46%;">
                            <strong>Department Name</strong><br />
                            <asp:Label ID="lblDepartmentName" runat="server" Style="width: 100%;"></asp:Label>
                        </div>
                        <div class="col">
                            <strong>Department #</strong><br />
                            <asp:Label ID="lblDepartmentID2" runat="server" Style="width: 100%;"></asp:Label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col" style="width: 46%;">
                            <strong>Building #</strong><br />
                            <asp:Label ID="lblBuildingNumber" runat="Server" Style="width: 100%"></asp:Label>
                        </div>
                        <div class="col" style="width: 46%">
                            <strong>Room #</strong><br />
                            <asp:Label ID="lblRoomNumber" runat="server" Style="width: 100%"></asp:Label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col" style="width: 46%;">
                            <strong>Position Phone #</strong><br />
                            <asp:Label ID="lblPhoneNumber" runat="Server" Style="width: 100%"></asp:Label>
                        </div>
                        <div class="col" style="width: 46%">
                            <strong>Mail Stop</strong><br />
                            <asp:Label ID="lblMailStop" runat="server" Style="width: 100%"></asp:Label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col" style="width: 46%">
                            <strong>Immediate Supervisor</strong><br />
                            <asp:Label ID="lblSupervisorName" runat="Server" Style="width: 100%"></asp:Label>
                        </div>
                        <div class="col" style="width: 46%">
                            <strong>Supervisor's ctcLink ID</strong><br />
                            <asp:Label ID="lblSupervisorID2" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
                <div style="display: flex; overflow: hidden; border-bottom: 1px solid #aaa;">
                    <div style="width: 50%;">
                        <div class="title" style="width: 100%">TYPE OF POSITION</div>
                        <div class="section" style="width: 100%; height: 100%;">
                            <div class="row">
                                <div id="newReplacement2" class="col" style="padding-bottom: 5px;">
                                    <asp:Label ID="lblNewReplacement" runat="server"><strong></strong></asp:Label>
                                </div>
                                <div id="employeeReplaced2" runat="server" style="margin-left: 15px; padding-top: 10px;">
                                    <strong>Name of Employee Replaced</strong><br />
                                    <asp:Label ID="lblEmployeeReplaced" runat="server" Style="width: 70%"></asp:Label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col" style="width: 100%;">
                                    <div class="col" style="text-align: left;">
                                        <strong>Type of Position</strong><br />
                                        <asp:Label ID="lblClassified" runat="server"></asp:Label>
                                        <asp:Label ID="lblPositionType" runat="server"></asp:Label>
                                    </div>
                                    <div class="col" id="endDate2" runat="server" style="margin-left: 10px; padding-bottom: 10px;">
                                        <strong>End Date</strong><br />
                                        <asp:Label ID="lblEndDate" runat="server"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col" style="width: 100%;" id="recruitmentType2">
                                    <strong>Type of Recruitment</strong>
                                    <div>
                                        <asp:Label ID="lblRecruitmentType" runat="server"></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="padding-right: 5px; width: 48%">
                        <div class="title" style="width: 100%; padding-right: 5px;">ONBOARDING</div>
                        <div class="section" style="width: 100%; padding-right: 0; padding-bottom: 15px; height: 100%;">
                            <div class="row">
                                <div class="col">
                                    <strong>This position will require:</strong>
                                    <div style="padding-top: 10px; margin-left: 10px;">
                                        <asp:Label ID="lblOnboarding" runat="server"></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="schedule2" runat="server">
                    <div class="title" style="border-top: none;">POSITION WORK SCHEDULE</div>
                    <div class="section">
                        <div class="row">
                            <div class="col" style="width: 20%">
                                <strong>Hours Per Day</strong><br />
                                <asp:Label ID="lblHoursPerDay" runat="server" Style="width: 100%"></asp:Label>
                            </div>
                            <div class="col" style="width: 19%">
                                <strong>Hours Per Week</strong><br />
                                <asp:Label ID="lblHoursPerWeek" runat="server" Style="width: 100%"></asp:Label>
                            </div>
                            <div class="col" style="width: 19%">
                                <strong>Months Per Year</strong><br />
                                <asp:Label ID="lblMonthsPerYear" runat="server" Style="width: 100%"></asp:Label>
                            </div>
                            <div id="cyclicCalendarCode2" runat="server" class="col" style="width: 25%; vertical-align: top; display: none;">
                                <strong>Cyclic Calendar Code</strong><br />
                                <asp:Label ID="lblCyclicCalendarCode" runat="server" Style="width: 100%"></asp:Label>
                            </div>
                        </div>
                        <div class="row" style="white-space: nowrap; padding-left: 0;padding-top:10px;">
                            <div class="timeCol" style="width: 12%; text-align: center; padding-left: 0;">
                                <div style="padding-bottom: 3px;"><strong>Monday</strong></div>
                                <asp:Label ID="lblMonday" runat="server"></asp:Label>
                            </div>
                            <div class="timeCol" style="width: 13%; text-align: center;">
                                <div style="padding-bottom: 3px;"><strong>Tuesday</strong></div>
                                <asp:Label ID="lblTuesday" runat="server"></asp:Label>
                            </div>
                            <div class="timeCol" style="width: 14%; text-align: center;">
                                <div style="padding-bottom: 3px;"><strong>Wednesday</strong></div>
                                <asp:Label ID="lblWednesday" runat="server"></asp:Label>
                            </div>
                            <div class="timeCol" style="width: 13%; text-align: center;">
                                <div style="padding-bottom: 3px;"><strong>Thursday</strong></div>
                                <asp:Label ID="lblThursday" runat="server"></asp:Label>
                            </div>
                            <div class="timeCol" style="width: 13%; text-align: center;">
                                <div style="padding-bottom: 3px;"><strong>Friday</strong></div>
                                <asp:Label ID="lblFriday" runat="server"></asp:Label>
                            </div>
                            <div class="timeCol" style="width: 12%; text-align: center;">
                                <div style="padding-bottom: 3px;"><strong>Saturday</strong></div>
                                <asp:Label ID="lblSaturday" runat="server"></asp:Label>
                            </div>
                            <div class="timeCol" style="width: 12%; text-align: center;">
                                <div style="padding-bottom: 3px;"><strong>Sunday</strong></div>
                                <asp:Label ID="lblSunday" runat="server"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
                <asp:Panel ID="panFileAttachments" runat="server">
                    <div class="title" style="border-top:none;">FILE ATTACHMENTS</div>
                    <div class="section">
                        <div style="padding: 0px 5px 5px 5px; text-align: left">
                            <asp:Label ID="lblAttachments" runat="server"></asp:Label>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="panEmployeeInfo" runat="server">
                    <div class="title" style="border-top: none;">EMPLOYEE INFORMATION</div>
                    <div class="section">
                        <div class="row">
                            <div class="col" style="width: 25%">
                                <strong>Employee Name</strong><br />
                                <asp:Label ID="lblEmployeeName" runat="Server" Style="width: 100%;"></asp:Label>
                            </div>
                            <div class="col" style="width: 25%">
                                <strong>Employee ctcLink ID</strong><br />
                                <asp:Label ID="lblEmployeeID" runat="Server" Style="width: 100%;"></asp:Label>
                            </div>
                            <div class="col" style="width: 37%">
                                <strong>Employee Email Address</strong><br />
                                <asp:Label ID="lblEmployeeEmailAddress" runat="Server" Style="width: 100%;"></asp:Label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col" style="width: 25%">
                                <strong>Salary Range/Step</strong><br />
                                <asp:Label ID="lblSalaryRangeStep" runat="Server" Style="width: 100%;"></asp:Label>
                            </div>
                            <div class="col" style="width: 25%">
                                <strong>Salary Amount</strong><br />
                                <asp:Label ID="lblSalaryAmount" runat="Server" Style="width: 100%;"></asp:Label>
                            </div>
                            <div class="col" style="width: 37%">
                                <strong>Employee Type</strong><br />
                                <asp:Label ID="lblEmployeeType" runat="Server" Style="width: 100%;"></asp:Label>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="panComments" runat="server">
                    <div class="title" style="border-top:none;">Originator Comments</div>
                    <div class="section">
                        <div style="padding: 0px 5px 5px 5px; text-align: left">
                            <asp:Label ID="lblOrigComments" runat="server"></asp:Label>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="panApprovals2" runat="server">
                    <div style="padding:10px"></div>
                    <div class="title" style="padding-right:5px;border-bottom:none">
                        <div style="float:left;width:50%">ROUTING/APPROVALS</div>
                        <div style="float:right;text-align:right;width:50%;">STATUS:&nbsp;<span class="normal"><asp:label id="lblStatus2" runat="server"></asp:label></span></div>
                        <div class="clearer"></div>
                    </div>
                    <asp:Table ID="tblApprovals2" runat="Server" style="width:100%" CellPadding="0"></asp:Table>
                </asp:Panel>
            </asp:Panel>
            <asp:Panel ID="panInput" runat="server">
                <div class="section" style="border-bottom: none;">
                    <div class="row">
                        <div class="col" style="width: 46%;">
                            <strong>Official Position Title</strong> <span class="required">*</span><br />
                            <asp:TextBox ID="txtOfficialTitle" runat="Server" MaxLength="30" Style="width: 100%;"></asp:TextBox>
                        </div>
                        <div class="col" style="width: 46%;">
                            <strong>Working Title</strong><br />
                            <asp:TextBox ID="txtWorkingTitle" runat="Server" MaxLength="30" Style="width: 100%;"></asp:TextBox>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col" style="width: 29%;">
                            <strong>Job Code</strong> <span class="required">*</span><br />
                            <asp:TextBox ID="txtJobCode" runat="Server" MaxLength="6" Style="width: 100%;" ></asp:TextBox>
                        </div>
                        <div class="col" style="width: 29%;">
                            <strong>Budget #</strong> <span class="required">*</span><br />
                            <asp:TextBox ID="txtBudgetNumber" runat="Server" MaxLength="75" Style="width: 100%;" ></asp:TextBox>
                        </div>
                        <div class="col" style="width: 29%;">
                            <strong>Position Control #</strong><br />
                            <asp:TextBox ID="txtPositionNumber" runat="Server" MaxLength="8" Style="width: 100%;"></asp:TextBox>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col" style="width: 29%;">
                            <strong>College/Unit</strong> <span class="required">*</span><br />
                            <asp:DropDownList ID="cboCollegeUnit" runat="server" Style="width: 100%">
                                <asp:ListItem Value="">-- Select One --</asp:ListItem>
                                <asp:ListItem Value="District">District</asp:ListItem>
                                <asp:ListItem Value="SCC">SCC</asp:ListItem>
                                <asp:ListItem Value="SFCC">SFCC</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="col" style="width: 42%;">
                            <strong>Department Name</strong> <span class="required">*</span><br />
                            <asp:DropDownList ID="cboDepartment" runat="Server" Style="width: 100%;">
                                <asp:ListItem Value="">-- Select One --</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="col">
                            <strong>Department #</strong><br />
                            <div style="padding-top:3px"><asp:Label ID="lblDepartmentID" runat="server">Auto Populated</asp:Label></div>
                        </div>
                    </div>
                     <div class="row">
                        <div class="col" style="width: 46%;">
                            <strong>Building #</strong> <span class="required">*</span><br />
                            <asp:TextBox ID="txtBuildingNumber" runat="Server" MaxLength="30" Style="width: 100%"></asp:TextBox>
                        </div>
                        <div class="col" style="width: 46%;">
                            <strong>Room #</strong> <span class="required">*</span><br />
                            <asp:TextBox ID="txtRoomNumber" runat="server" MaxLength="30" Style="width: 100%"></asp:TextBox>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col" style="width: 46%;">
                            <strong>Position Phone #</strong> <span class="required">*</span><br />
                            <asp:TextBox ID="txtPhoneNumber" runat="Server" MaxLength="30" Style="width: 100%"></asp:TextBox>
                        </div>
                        <div class="col" style="width: 46%;">
                            <strong>Mail Stop</strong> <span class="required">*</span><br />
                            <asp:TextBox ID="txtMailStop" runat="server" MaxLength="30" Style="width: 100%"></asp:TextBox>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col" style="width: 46%;">
                            <strong>Immediate Supervisor</strong> <span class="required">*</span><br />
                            <asp:TextBox ID="txtSupervisorName" runat="Server" Style="width: 100%" placeholder="Search by Name"></asp:TextBox>
                        </div>
                        <div class="col" style="width: 46%;">
                            <strong>Supervisor's ctcLink ID</strong><br />
                            <div style="padding-top:3px"><asp:Label ID="lblSupervisorID" runat="server"></asp:Label></div>
                        </div>
                    </div>
                </div>
                <div style="display: flex; overflow: hidden">
                    <div style="width: 50%;">
                        <div class="title" style="width: 100%">TYPE OF POSITION</div>
                        <div class="section" style="width: 100%; height: 100%;">
                            <div class="row"> <strong>Select one: </strong> 
                                <div id="newReplacement" class="col" style="margin-left: -5px; padding-bottom: 5px;"> <span class="required">*</span>
                                    <input type="radio" id="optNew" name="optNewReplacement" runat="server" /><strong>New</strong> &nbsp;
                                    <input type="radio" id="optReplacement" name="optNewReplacement" runat="server" /><strong>Replacement</strong>
                                </div>
                                <div id="employeeReplaced" style="margin-left: 15px; padding-top: 10px;">
                                    Name of Employee Replaced <span class="required">*</span><br />
                                    <asp:TextBox ID="txtEmployeeReplaced" runat="server" MaxLength="30" Style="width: 70%"></asp:TextBox>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col" style="width: 100%;">
                                    <div class="col" style="text-align: left;">
                                        <strong>Type of Position</strong> <span class="required">*</span>
                                        <asp:DropDownList ID="cboPositionType" runat="server" Style="width: 93%;" AutoPostBack = "true" OnSelectedIndexChanged="cboPositionType_SelectedIndexChanged">
                                            <asp:ListItem Value="">-- Select One --</asp:ListItem>
                                            <asp:ListItem Value="Classified">Classified</asp:ListItem>
                                            <asp:ListItem Value="Part-Time Hourly">Part-Time Hourly</asp:ListItem>
                                            <asp:ListItem Value="Tenure-Track Faculty">Tenure-Track Faculty</asp:ListItem>
                                            <asp:ListItem Value="Non Tenure-Track Faculty">Non Tenure-Track Faculty</asp:ListItem>
                                            <asp:ListItem Value="Adjunct Faculty">Adjunct Faculty</asp:ListItem>
                                            <asp:ListItem Value="Professional/Confidential Exempt">Professional/Confidential Exempt</asp:ListItem>
                                            <asp:ListItem Value="Administrator/Executive">Administrator/Executive</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col" id="worksheet">
                                        <p>Please complete, save, and attach the <br /><a href="https://shared.spokane.edu/ccsglobal/media/Global/FormsA-Z/ccs-1693.pdf" target="_blank">Part-Time Hourly Eligibility Worksheet</a>.</p>
                                    </div>
                                    <div class="col" id="classified">
                                        <div class="col" style="margin-left: 10px; padding-top: 15px; padding-bottom: 10px;">
                                            <%-- <input type="radio" id="optPermanent" name="optClassified" runat="server" />Permanent &nbsp;
                                            <input type="radio" id="optNonPermanent" name="optClassified" runat="server" />Non-Permanent--%>
                                            <asp:RadioButton ID="optPermanent"  runat="server" GroupName="Controls" OnCheckedChanged="RadioButton_CheckedChanged" AutoPostBack="true"/> Permanent &nbsp;
                                            <asp:RadioButton ID="optNonPermanent"  runat="server" GroupName="Controls" OnCheckedChanged="RadioButton_CheckedChanged" AutoPostBack="true"/> Non-Permanent
                                        </div>
                                        <div class="col" id="endDate" style="margin-left: 124px; padding-bottom: 10px;">
                                            End Date<br />
                                            <input type="text" id="txtEndDate" name="txtEndDate" class="datepicker" runat="server" maxlength="10" style="width: 90%;" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col" style="width: 100%;" id="recruitmentType">
                                    <strong>Type of Recruitment</strong> <span class="required">*</span>
                                    <div>
                                        <asp:DropDownList ID="cboRecruitmentType" runat="server" style="width: 90%;">
                                            <asp:ListItem Value="" Text="">-- Select One --</asp:ListItem>
                                            <asp:ListItem Value="Promotional" Text="Promotional"></asp:ListItem>
                                            <asp:ListItem Value="Internal" Text="Internal"></asp:ListItem>
                                            <asp:ListItem Value="Open Competitive" Text="Open Competitive"></asp:ListItem>
                                            <asp:ListItem Value="No Recruitment" Text="No Recruitment"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div >
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="padding-right: 5px; width: 48%;">
                        <div class="title" style="width: 100%; padding-right: 5px;">ONBOARDING</div>
                        <div class="section" style="width: 100%; padding-right: 0; padding-bottom:15px; height: 100%;">
                            <p><strong>This position will require:</strong></p>
                            <div style="padding-top: 5px; margin-left: 10px;">
                                <asp:CheckBox ID="chkPCard" runat="server" />P-Card
                            </div>
                            <div style="padding-top: 5px; margin-left: 10px;">
                                <asp:CheckBox ID="chkCellPhone" runat="server" />CCS-issued Cell Phone
                            </div>
                            <div style="padding-top: 5px; margin-left: 10px;">
                                <asp:CheckBox ID="chkAdditionalAccess" runat="server" />Additional ctcLink Security Access
                            </div>
                            <div style="padding-top: 5px; margin-left: 10px;">
                                <asp:CheckBox ID="chkTimeSheetApprover" runat="server" />Manager/Timesheet Approver
                            </div>
                        </div>
                    </div>
                </div>
                <div id="schedule">
                    <div class="title">POSITION WORK SCHEDULE</div>
                    <div class="section" style="border-bottom: none;">
                        <div class="row">
                            <div class="col" style="width: 20%;">
                                <strong>Hours Per Day</strong><br />
                                <asp:TextBox ID="txtHoursPerDay" runat="server" MaxLength="30" Style="width: 100%;"></asp:TextBox>
                            </div>
                            <div class="col" style="width: 19%;">
                                <strong>Hours Per Week</strong><br />
                                <asp:TextBox ID="txtHoursPerWeek" runat="server" MaxLength="30" Style="width: 100%;"></asp:TextBox>
                            </div>
                            <div class="col" style="width: 19%;">
                                <strong>Months Per Year</strong><br />
                                <asp:TextBox ID="txtMonthsPerYear" runat="server" MaxLength="30" Style="width: 100%;"></asp:TextBox>
                            </div>
                            <div id="cyclicCalendarCode" class="col" style="width: 25%;">
                                <strong>Cyclic Calendar Code</strong><br />
                                <asp:TextBox ID="txtCyclicCalendarCode" runat="server" MaxLength="30" Style="width: 100%;"></asp:TextBox>
                            </div>
                        </div>
                        <div class="row" style="white-space:nowrap;padding-top:10px;">
                            <div class="timeCol">
                                <strong>Monday</strong><br />
                                <input type="text" id="txtMondayStart" class="timepicker text-center" runat="server" maxlength="8" placeholder="From" /><br />
                                <input type="text" id="txtMondayEnd" class="timepicker text-center" runat="server" maxlength="8" placeholder="To" />
                            </div>
                            <div class="timeCol">
                                <strong>Tuesday</strong><br />
                                <input type="text" id="txtTuesdayStart" class="timepicker text-center" runat="server" maxlength="8" placeholder="From" /><br />
                                <input type="text" id="txtTuesdayEnd" class="timepicker text-center" runat="server" maxlength="8" placeholder="To" />
                            </div>
                            <div class="timeCol">
                                <strong>Wednesday</strong><br />
                                <input type="text" id="txtWednesdayStart" class="timepicker text-center" runat="server" maxlength="8" placeholder="From" /><br />
                                <input type="text" id="txtWednesdayEnd" class="timepicker text-center" runat="server" maxlength="8" placeholder="To" />
                            </div>
                            <div class="timeCol">
                                <strong>Thursday</strong><br />
                                <input type="text" id="txtThursdayStart" class="timepicker text-center" runat="server" maxlength="8" placeholder="From" /><br />
                                <input type="text" id="txtThursdayEnd" class="timepicker text-center" runat="server" maxlength="8" placeholder="To" />
                            </div>
                            <div class="timeCol">
                                <strong>Friday</strong><br />
                                <input type="text" id="txtFridayStart" class="timepicker text-center" runat="server" maxlength="8" placeholder="From" /><br />
                                <input type="text" id="txtFridayEnd" class="timepicker text-center" runat="server" maxlength="8" placeholder="To" />
                            </div>
                            <div class="timeCol">
                                <strong>Saturday</strong><br />
                                <input type="text" id="txtSaturdayStart" class="timepicker text-center" runat="server" maxlength="8" placeholder="From" /><br />
                                <input type="text" id="txtSaturdayEnd" class="timepicker text-center" runat="server" maxlength="8" placeholder="To" />
                            </div>
                            <div class="timeCol">
                                <strong>Sunday</strong><br />
                                <input type="text" id="txtSundayStart" class="timepicker text-center" runat="server" maxlength="8" placeholder="From" /><br />
                                <input type="text" id="txtSundayEnd" class="timepicker text-center" runat="server" maxlength="8" placeholder="To" />
                            </div>
                        </div>
                        <div style="width: 100%; text-align: center; padding-top: 15px;padding-bottom:15px;">
                            <button id="cmdApplyAll" type="button">Apply All</button>&nbsp;&nbsp;
                            <button id="cmdReset" type="button">Reset</button>
                        </div>
                    </div>
                </div>
                <div id="divAttachment1" runat="server" class="title">FILE ATTACHMENTS</div>
                <div id="divAttachment2" runat="server" class="section" style="border-bottom:none;">
                    <div style="padding: 0px 5px 5px 5px; text-align: left">
                        <asp:Table ID="tblAttachments" style="width:100%" CellPadding="0" CellSpacing="0" runat="server"></asp:Table>
                    </div>
                </div>
                <div class="title"><strong>Originator Comments</strong></div>
                <div class="section" style="border-top:none;border-bottom:none">
                    <div style="padding: 0px 5px 5px 5px; text-align: left">
                        <asp:TextBox ID="txtOrigComments" runat="server" style="width:100%" Rows="4" TextMode="multiLine" MaxLength="4000"></asp:TextBox>
                    </div>
                </div>
                <div class="title">ADD FILE ATTACHMENTS</div>
                <div class="section" style="border-top:none">
                    <div style="padding: 0px 5px 5px 5px; text-align: left">
                        <div id="attachmentDescription"><p>An approved position description must be attached. If you have questions, please contact the Human Resources Office at (509) 434-5040.</p></div>
                        <div style="margin-top: 3px; padding-top: 3px" id="uploadArea" runat="server"></div>
                        <div style="width: 100%; text-align: right">
                            <input id="cmdAddFile" type="button" value="Add Another File" onclick="fileUploadBox()" />
                        </div>
                    </div>
                </div>
                <asp:Panel ID="panApprovals" runat="server">
                    <div style="padding:10px"></div>
                    <div class="title" style="padding-right:5px;border-bottom:none">
                        <div style="float:left;width:50%">ROUTING/APPROVALS</div>
                        <div style="float:right;text-align:right;width:50%;">STATUS:&nbsp;<span class="normal"><asp:label id="lblStatus" runat="server"></asp:label></span></div>
                        <div class="clearer"></div>
                    </div>
                    <asp:Table ID="tblApprovals" runat="Server" style="width:100%" CellPadding="0"></asp:Table>
                </asp:Panel>
            </asp:Panel>
            <div style="width: 100%; text-align: center; padding-top: 20px">
                <input type="button" runat="server" value="Back" id="cmdBack" style="width: 70px;margin-right:10px;" onclick="history.back(1)" />
                <asp:Button ID="cmdSave" runat="server" Text="Save" Style="width: 70px;margin-right:10px;" OnClick="cmdSave_Click" />
                <asp:Button ID="cmdSubmit" runat="server" Text="Submit" Style="width: 70px" OnClientClick="return validate('Originator');" OnClick="cmdSubmit_Click" />
                <input type="button" runat="server" id="cmdPrint" value="Print" style="width: 70px;margin-right:10px;" onclick="openPage('print.aspx?id=' + document.getElementById('maintextHolder_hidID').value, 'PositionRequest', '1000', '800');" />
                <input type="button" runat="server" value="OK" id="cmdOK" style="width: 70px;" onclick="location.href = 'Form.aspx'" />
            </div>
        </asp:Panel>
    </form>
    <script type="text/javascript" src="Scripts/Form.js"></script>
    <script type="text/javascript" src="Scripts/SortTable.js"></script>
</asp:Content>


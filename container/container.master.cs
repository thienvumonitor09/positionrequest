﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Text;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Globalization;

public partial class container_container : System.Web.UI.MasterPage {
    protected void Page_Load(object sender, EventArgs e) {
        HttpCookie objCookie = Request.Cookies.Get("phatt2");
        if (objCookie != null) {
            support.Visible = true;
            PositionRequest positionRequest = new PositionRequest();

            Label lblWelcome = (Label)maintextHolder.FindControl("lblWelcome");
            if (lblWelcome != null) {
                lblWelcome.Text = "<h3 class=\"pageTitle\">Position Request (PR)</h3>";
                System.Globalization.CultureInfo cultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture;
                System.Globalization.TextInfo textInfo = cultureInfo.TextInfo;
                DataSet dsEmployeeName = positionRequest.GetEmployeeNames(Request.Cookies["phatt2"]["userctclinkid"]);

                try {
                    lblWelcome.Text += "<h5 style=\"padding-bottom:0px;margin-bottom:0px;\">Welcome " + textInfo.ToTitleCase(dsEmployeeName.Tables[0].Rows[0]["FIRST_NAME"].ToString().ToLower() + " " + dsEmployeeName.Tables[0].Rows[0]["LAST_NAME"].ToString().ToLower()) + ".</h5>";
                } catch {
                    lblWelcome.Text += "<h5 style=\"color:red\">Your employee record could not be found.<br />Please contact the IT Support Center at 533-HELP (4357). id=" + Request.Cookies["phatt2"]["userctclinkid"] + "</h5>";
                }
            }

            if (!IsPostBack) {

                support.InnerHtml = "<strong>Need help?</strong><br /><a href=\"mailto:" + ConfigurationManager.AppSettings["AdminEmailAddress"].ToString() + "\">Contact HRO</a>";

                String strUserPermission = positionRequest.GetUserPermission(objCookie["userctclinkid"]);

                if (strUserPermission == "HRO Admin") {
                    //HRO Admin Menu
                    sidemenu.InnerHtml += "<ul class=\"menu\"><li class=\"header\"><h2>HRO Admin</h2></li>";
                    sidemenu.InnerHtml += "<li><a href=\"/PositionRequest/HRO/CreateWelcome.aspx\" title=\"Create Welcome\">Create Welcome</a></li>";
                    sidemenu.InnerHtml += "<li><a href=\"/PositionRequest/HRO/Permissions.aspx\" title=\"Permissions\">Grant Permissions</a></li>";
                    sidemenu.InnerHtml += "<li><a href=\"/PositionRequest/HRO/Approval.aspx?type=HROAdmin\" title=\"Position Request Approval\">PR Approval</a></li>";
                    sidemenu.InnerHtml += "<li><a href=\"/PositionRequest/Report.aspx?type=HROAdmin\" title=\"Position Request Report\">PR Report</a></li>";
                    sidemenu.InnerHtml += "</ul>";
                } else if (strUserPermission == "HRO Coordinator") {
                    //HRO Coordinator Menu
                    sidemenu.InnerHtml += "<ul class=\"menu\"><li class=\"header\"><h2>HRO Coordinator</h2></li>";
                    sidemenu.InnerHtml += "<li><a href=\"/PositionRequest/HRO/Approval.aspx?type=HROCoordinator\" title=\"Position Request Action\">PR Action</a></li>";
                    sidemenu.InnerHtml += "<li><a href=\"/PositionRequest/Report.aspx?type=HROCoordinator\" title=\"Position Request Report\">PR Report</a></li>";
                    sidemenu.InnerHtml += "</ul>";
                } else if (strUserPermission == "HRO Recruiter") {
                    //HRO Recruiter Menu
                    sidemenu.InnerHtml += "<ul class=\"menu\"><li class=\"header\"><h2>HRO Recruiter</h2></li>";
                    sidemenu.InnerHtml += "<li><a href=\"/PositionRequest/HRO/Approval.aspx?type=HRORecruiter\" title=\"Position Request Action\">PR Action</a></li>";
                    sidemenu.InnerHtml += "<li><a href=\"/PositionRequest/Report.aspx?type=HRORecruiter\" title=\"Position Request Report\">PR Report</a></li>";
                    sidemenu.InnerHtml += "</ul>";
                } else if (strUserPermission == "HRO Assistant") {
                    //HRO Assistant Menu
                    sidemenu.InnerHtml += "<ul class=\"menu\"><li class=\"header\"><h2>HRO Assistant</h2></li>";
                    sidemenu.InnerHtml += "<li><a href=\"/PositionRequest/Report.aspx?type=HROAssistant\" title=\"Position Request Report\">PR Report</a></li>";
                    sidemenu.InnerHtml += "</ul>";
                } else if (strUserPermission == "Budget Admin") {
                    //Budget Admin Menu
                    sidemenu.InnerHtml += "<ul class=\"menu\"><li class=\"header\"><h2>Budget Admin</h2></li>";
                    sidemenu.InnerHtml += "<li><a href=\"/PositionRequest/Budget/Approval.aspx\" title=\"Position Request Approval\">PR Approval</a></li>";
                    sidemenu.InnerHtml += "<li><a href=\"/PositionRequest/Report.aspx?type=BudgetAdmin\" title=\"Position Request Report\">PR Report</a></li>";
                    sidemenu.InnerHtml += "</ul>";
                } else if (strUserPermission == "Executive Assistant") {
                    //Executive Assistant Menu
                    sidemenu.InnerHtml += "<ul class=\"menu\"><li class=\"header\"><h2>Executive Assistant</h2></li>";
                    sidemenu.InnerHtml += "<li><a href=\"/PositionRequest/Report.aspx?type=ExecutiveAssistant\" title=\"Position Request Report\">PR Report</a></li>";
                    sidemenu.InnerHtml += "</ul>";
                }

                if (positionRequest.GetSupervisorPositionRequestCount(objCookie["userctclinkid"], "") > 0) {
                    //Supervisor Menu
                    sidemenu.InnerHtml += "<ul class=\"menu\"><li class=\"header\"><h2>Supervisor</h2></li>";
                    if (positionRequest.GetSupervisorPositionRequestCount(objCookie["userctclinkid"], "Pending Approval") > 0) {
                        sidemenu.InnerHtml += "<li><a href=\"/PositionRequest/Supervisor/Approval.aspx\" title=\"Position Request Approval\">PR Approval</a></li>";
                    }
                    sidemenu.InnerHtml += "<li><a href=\"/PositionRequest/Report.aspx?type=Supervisor\" title=\"Position Request Report\">PR Report</a></li>";
                    sidemenu.InnerHtml += "</ul>";
                    //Response.Write("positionRequest.GetSupervisorPositionRequestCount(" + objCookie["userctclinkid"] + ", Pending Approval)");
                }

                //Main Menu
                sidemenu.InnerHtml += "<ul class=\"menu\"><li class=\"header\"><h2>Main Menu</h2></li>";
                sidemenu.InnerHtml += "<li><a href=\"/PositionRequest/Default.aspx\" title=\"Main\">Main Page</a></li>";
                sidemenu.InnerHtml += "<li><a href=\"/PositionRequest/Form.aspx\" title=\"Create Form\">Position Request</a></li>";
                sidemenu.InnerHtml += "<li><a href=\"http://ccsnet.ccs.spokane.edu/\">CCSNet Home</a></li>";
                sidemenu.InnerHtml += "<li><a href=\"http://ccsnet.ccs.spokane.edu/lo.aspx\" title=\"Log out from CCSNet\">Switch CCSNet User</a></li>";
                sidemenu.InnerHtml += "</ul>";
            }

        } else {
            support.Visible = false;
        }
    }
}


﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Print.aspx.cs" Inherits="Print" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Position Request</title>
    <link href="App_Themes/Moxie/print.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        @media print {
            input#cmdPrint, input#cmdClose {
                display: none;
            }
        }
    </style>
</head>
<body>
    <form id="frmPR" runat="server">
        <asp:Panel ID="panPrint" runat="server">
            <div style="font-size: large; padding-bottom: 10px">
                <div style="float: left; padding: 0px 0px 5px 10px">
                    <img alt="Community Colleges of Spokane" src="App_Themes/Moxie/images/newlogo2.jpg" /></div>
                <div style="float: left; padding: 30px 0px 5px 20px">Human Resources Office (HRO)<br />
                    <strong>Position Request</strong></div>
                <div class="clearer"></div>
            </div>
            <div class="title">
                <div style="float: left; width: 68%;">
                    ORIGINATOR:&nbsp;<span class="normal"><asp:Label ID="lblOriginatorName" runat="Server"></asp:Label></span>
                    &nbsp;ctcLink ID:&nbsp;<span class="normal"><asp:Label ID="lblOriginatorID" runat="Server"></asp:Label></span>
                </div>
                <div style="float: right; text-align: right; width: 31%; margin-right: 5px;">
                    <asp:Panel ID="panTrackingNumber" runat="server">TRACKING #:&nbsp;<span class="normal"><asp:Label ID="lblTrackingNumber" runat="server"></asp:Label></span></asp:Panel>
                </div>
                <div class="clearer"></div>
            </div>
            <div class="section" style="border-bottom: none;">
                <div class="row">
                    <div class="col" style="width: 46%">
                        <strong>Official Position Title</strong><br />
                        <asp:Label ID="lblOfficialTitle" runat="Server" Style="width: 100%;"></asp:Label>
                    </div>
                    <div class="col" style="width: 46%">
                        <strong>Working Title</strong><br />
                        <asp:Label ID="lblWorkingTitle" runat="Server" Style="width: 100%;"></asp:Label>
                    </div>
                </div>
                <div class="row">
                    <div class="col" style="width: 46%;">
                        <strong>Job Code</strong><br />
                        <asp:Label ID="lblJobCode" runat="Server" Style="width: 100%;"></asp:Label>
                    </div>
                    <div class="col" style="width: 46%;">
                        <strong>Budget #</strong><br />
                        <asp:Label ID="lblBudgetNumber" runat="Server" Style="width: 100%;"></asp:Label>
                    </div>
                </div>
                <div class="row">
                    <div class="col" style="width: 46%;">
                        <strong>Position Control #</strong><br />
                        <asp:Label ID="lblPositionNumber" runat="Server" Style="width: 100%;"></asp:Label>
                    </div>
                    <div class="col" style="width: 46%;">
                        <strong>College/Unit</strong><br />
                        <asp:Label ID="lblCollegeUnit" runat="server" Style="width: 100%;"></asp:Label>
                    </div>
                </div>
                <div class="row">
                    <div class="col" style="width: 46%;">
                        <strong>Department Name</strong><br />
                        <asp:Label ID="lblDepartmentName" runat="server" Style="width: 100%;"></asp:Label>
                    </div>
                    <div class="col">
                        <strong>Department #</strong><br />
                        <asp:Label ID="lblDepartmentID2" runat="server" Style="width: 100%;"></asp:Label>
                    </div>
                </div>
                <div class="row">
                    <div class="col" style="width: 46%;">
                        <strong>Building #</strong><br />
                        <asp:Label ID="lblBuildingNumber" runat="Server" Style="width: 100%"></asp:Label>
                    </div>
                    <div class="col" style="width: 46%">
                        <strong>Room #</strong><br />
                        <asp:Label ID="lblRoomNumber" runat="server" Style="width: 100%"></asp:Label>
                    </div>
                </div>
                <div class="row">
                    <div class="col" style="width: 46%;">
                        <strong>Position Phone #</strong><br />
                        <asp:Label ID="lblPhoneNumber" runat="Server" Style="width: 100%"></asp:Label>
                    </div>
                    <div class="col" style="width: 46%">
                        <strong>Mail Stop</strong><br />
                        <asp:Label ID="lblMailStop" runat="server" Style="width: 100%"></asp:Label>
                    </div>
                </div>
                <div class="row">
                    <div class="col" style="width: 46%">
                        <strong>Immediate Supervisor</strong><br />
                        <asp:Label ID="lblSupervisorName" runat="Server" Style="width: 100%"></asp:Label>
                    </div>
                    <div class="col" style="width: 46%">
                        <strong>Supervisor's ctcLink ID</strong><br />
                        <asp:Label ID="lblSupervisorID2" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div style="display: flex; overflow: hidden; border-bottom: 1px solid #000;"">
                <div style="width: 50%;">
                    <div class="title" style="width: 100%;border-right:none;">TYPE OF POSITION</div>
                    <div class="section" style="width: 100%; height: 100%;border-right:none;">
                        <div class="row">
                            <div id="newReplacement2" class="col" style="padding-bottom: 5px;">
                                <asp:Label ID="lblNewReplacement" runat="server"><strong></strong></asp:Label>
                            </div>
                            <div id="employeeReplaced2" runat="server" style="margin-left: 15px; padding-top: 10px;">
                                <strong>Name of Employee Replaced</strong><br />
                                <asp:Label ID="lblEmployeeReplaced" runat="server" Style="width: 70%"></asp:Label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col" style="width: 100%;">
                                <div class="col" style="text-align: left;">
                                    <strong>Type of Position</strong><br />
                                    <asp:Label ID="lblClassified" runat="server"></asp:Label>
                                    <asp:Label ID="lblPositionType" runat="server"></asp:Label>
                                </div>
                                <div class="col" id="endDate2" runat="server" style="margin-left: 10px; padding-bottom: 10px;">
                                    <strong>End Date</strong><br />
                                    <asp:Label ID="lblEndDate" runat="server"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col" style="width: 100%;" id="recruitmentType2">
                                <strong>Type of Recruitment</strong>
                                <div>
                                    <asp:Label ID="lblRecruitmentType" runat="server"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="width: 48%">
                    <div class="title" style="width: 100%; padding-right: 9px;">ONBOARDING</div>
                    <div class="section" style="width: 100%; padding-right: 2px; padding-bottom: 15px; height: 100%;">
                        <div class="row">
                            <div class="col">
                                <strong>This position will require:</strong>
                                <div style="padding-top: 10px; margin-left: 10px;">
                                    <asp:Label ID="lblOnboarding" runat="server"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="schedule2" runat="server">
                <div class="title" style="border-top: none;">POSITION WORK SCHEDULE</div>
                <div class="section">
                    <div class="row">
                        <div class="col" style="width: 20%">
                            <strong>Hours Per Day</strong><br />
                            <asp:Label ID="lblHoursPerDay" runat="server" Style="width: 100%"></asp:Label>
                        </div>
                        <div class="col" style="width: 19%">
                            <strong>Hours Per Week</strong><br />
                            <asp:Label ID="lblHoursPerWeek" runat="server" Style="width: 100%"></asp:Label>
                        </div>
                        <div class="col" style="width: 19%">
                            <strong>Months Per Year</strong><br />
                            <asp:Label ID="lblMonthsPerYear" runat="server" Style="width: 100%"></asp:Label>
                        </div>
                        <div id="cyclicCalendarCode2" runat="server" class="col" style="width: 25%; vertical-align: top; display: none;">
                            <strong>Cyclic Calendar Code</strong><br />
                            <asp:Label ID="lblCyclicCalendarCode" runat="server" Style="width: 100%"></asp:Label>
                        </div>
                    </div>
                    <div class="row" style="white-space: nowrap; padding-left: 0; padding-top: 10px;">
                        <div class="timeCol" style="width: 12%; text-align: center; padding-left: 0;">
                            <div style="padding-bottom: 3px;"><strong>Monday</strong></div>
                            <asp:Label ID="lblMonday" runat="server"></asp:Label>
                        </div>
                        <div class="timeCol" style="width: 13%; text-align: center;">
                            <div style="padding-bottom: 3px;"><strong>Tuesday</strong></div>
                            <asp:Label ID="lblTuesday" runat="server"></asp:Label>
                        </div>
                        <div class="timeCol" style="width: 14%; text-align: center;">
                            <div style="padding-bottom: 3px;"><strong>Wednesday</strong></div>
                            <asp:Label ID="lblWednesday" runat="server"></asp:Label>
                        </div>
                        <div class="timeCol" style="width: 13%; text-align: center;">
                            <div style="padding-bottom: 3px;"><strong>Thursday</strong></div>
                            <asp:Label ID="lblThursday" runat="server"></asp:Label>
                        </div>
                        <div class="timeCol" style="width: 13%; text-align: center;">
                            <div style="padding-bottom: 3px;"><strong>Friday</strong></div>
                            <asp:Label ID="lblFriday" runat="server"></asp:Label>
                        </div>
                        <div class="timeCol" style="width: 12%; text-align: center;">
                            <div style="padding-bottom: 3px;"><strong>Saturday</strong></div>
                            <asp:Label ID="lblSaturday" runat="server"></asp:Label>
                        </div>
                        <div class="timeCol" style="width: 12%; text-align: center;">
                            <div style="padding-bottom: 3px;"><strong>Sunday</strong></div>
                            <asp:Label ID="lblSunday" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
            <asp:Panel ID="panAttachments" runat="server">
                <div class="title" id="fileAttachments" runat="server" style="border-top:none;">FILE ATTACHMENTS</div>
                <div class="section">
                    <div style=" text-align: left">
                        <asp:Label ID="lblAttachments" runat="server"></asp:Label>
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="panEmployeeInfo" runat="server">
                <div class="title" style="border-top: none;">EMPLOYEE INFORMATION</div>
                <div class="section">
                    <div class="row">
                        <div class="col" style="width: 27%">
                            <strong>Employee Name</strong><br />
                            <asp:Label ID="lblEmployeeName" runat="Server" Style="width: 100%;"></asp:Label>
                        </div>
                        <div class="col" style="width: 27%">
                            <strong>Employee ctcLink ID</strong><br />
                            <asp:Label ID="lblEmployeeID" runat="Server" Style="width: 100%;"></asp:Label>
                        </div>
                        <div class="col" style="width: 39%">
                            <strong>Employee Email Address</strong><br />
                            <asp:Label ID="lblEmployeeEmailAddress" runat="Server" Style="width: 100%;"></asp:Label>
                        </div>
                    </div>
                    <div class="row">
                         <div class="col" style="width: 25%">
                            <strong>Salary Range/Step</strong><br />
                            <asp:Label ID="lblSalaryRangeStep" runat="Server" Style="width: 80%;"></asp:Label>
                        </div>
                        <div class="col" style="width: 15%">
                            <strong>Salary</strong><br />
                            <asp:Label ID="lblSalaryAmount" runat="Server" Style="width: 80%;"></asp:Label>
                        </div>
                        <div class="col" style="width: 20%">
                            <strong>Effective Date</strong><br />
                            <asp:Label ID="lblEffectiveDate" runat="Server" Style="width: 80%;"></asp:Label>
                        </div>
                        <div class="col" style="width: 20%">
                            <strong>Employee Type</strong><br />
                            <asp:Label ID="lblEmployeeType" runat="Server" Style="width: 80%;"></asp:Label>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="panComments" runat="server">
                <div class="title" style="border-top:none;">ORIGINATOR COMMENTS</div>
                    <div class="section">
                        <div style="padding: 0px 5px 5px 5px; text-align: left">
                            <asp:Label ID="lblOrigComments" runat="server"></asp:Label>
                        </div>
                    </div>
            </asp:Panel>
            
            <asp:Panel ID="panApprovals" runat="server">
                <div class="title" style="padding-right: 5px; border-top:none; border-bottom: none">
                    <div style="float: left; width: 50%">ROUTING/APPROVALS</div>
                    <div style="float: right; text-align: right; width: 50%;">STATUS:&nbsp;<span class="normal"><asp:Label ID="lblStatus" runat="server"></asp:Label></span></div>
                    <div class="clearer"></div>
                </div>
                <asp:Table ID="tblApprovals" runat="Server" Style="width: 100%" CellPadding="0"></asp:Table>
            </asp:Panel>
            <div style="width: 100%; text-align: center; padding-top: 10px" id="divButtons">
                <input type="button" id="cmdPrint" value="Print" style="width: 70px" onclick="window.print();" />&nbsp;
                <input type="button" id="cmdClose" value="Close" style="width: 70px" onclick="self.close();" />
            </div>
        </asp:Panel>
        <asp:Panel ID="panError" runat="server">
            <div style="width: 100%; text-align: center; padding-top: 10px">
                <div style="padding-bottom: 10px;">
                    <asp:Label Style="color: red" ID="lblError" runat="server"></asp:Label>
                </div>
                <input type="button" id="cmdClose2" value="Close" style="width: 70px" onclick="self.close();" />
            </div>
        </asp:Panel>
    </form>
</body>
</html>

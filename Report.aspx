﻿<%@ Page Title="" Language="C#" MasterPageFile="~/container/container.master" AutoEventWireup="true" CodeFile="Report.aspx.cs" Inherits="Report" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContent" runat="Server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="menuHolder" runat="Server"></asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="maintextHolder" runat="Server">
    <form id="frmPR" runat="server">
        <input type="hidden" id="hidOrderBy" runat="server" />
        <input type="hidden" id="hidDirection" runat="server" />
        <input type="hidden" id="hidUserType" runat="server" />
        <input type="hidden" id="hidTodo" runat="server" />
        <input type="hidden" id="hidStatus" runat="server" />
        <input type="hidden" id="hidCreated" runat="server" />
        <input type="hidden" id="hidID" runat="server" />
        <input type="hidden" id="hidSearchDepartment" runat="server" />
        <h3 class="pageTitle">Position Request
            <asp:Label ID="lblTitle" Style="font-weight: bold" runat="server"></asp:Label>
            Report</h3>
        <asp:Panel ID="panList" runat="Server">
            <asp:Panel ID="panSearch" runat="server">
                <div class="title">
                    <div style="float: left; width: 50%"><strong>SEARCH</strong></div>
                    <div style="float: right; width: 50%; text-align: right; font-weight: normal">Note: All the search fields work together.&nbsp;&nbsp;</div>
                    <div class="clearer"></div>
                </div>
                <div class="section" style="margin-bottom: 20px">
                    <div class="row">
                        <div class="col" style="width: 20%">
                            <strong>Status</strong><br />
                            <asp:DropDownList ID="cboSearchStatus" runat="server" Style="width: 117px">
                                <asp:ListItem Value="">ALL</asp:ListItem>
                                <asp:ListItem Value="Approver">Approver</asp:ListItem>
                                <asp:ListItem Value="Budget">Budget</asp:ListItem>
                                <asp:ListItem Value="Cancelled">Cancelled</asp:ListItem>
                                <asp:ListItem Value="Chancellor">Chancellor</asp:ListItem>
                                <asp:ListItem Value="Complete">Complete</asp:ListItem>
                                <asp:ListItem Value="Executive">Executive</asp:ListItem>
                                <asp:ListItem Value="Hired">Hired</asp:ListItem>
                                <asp:ListItem Value="HRO">HRO</asp:ListItem>
                                <asp:ListItem Value="Incomplete">Incomplete</asp:ListItem>
                                <asp:ListItem Value="Recruiting">Recruiting</asp:ListItem>
                                <asp:ListItem Value="Rejected">Rejected</asp:ListItem>
                                <asp:ListItem Value="Void">Void</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="col" style="width: 37%">
                            <strong>Tracking #</strong><br />
                            <asp:TextBox ID="txtSearchTrackingNumber" runat="server" Style="width: 100%"></asp:TextBox>
                        </div>
                        <div class="col" style="width: 30%">
                            <strong>Originator Name</strong><br />
                            <asp:TextBox ID="txtSearchOriginatorName" runat="server" Style="width: 100%"></asp:TextBox>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col" style="width: 20%; text-align: left; white-space: nowrap">
                            <strong>Position Control #</strong><br />
                            <asp:TextBox ID="txtSearchPositionNumber" runat="server" Style="width: 109px"></asp:TextBox>
                        </div>
                        <div class="col" style="width: 37%">
                            <strong>Type of Position</strong><br />
                            <asp:DropDownList ID="cboSearchPositionType" runat="server" Style="width: 214px">
                                <asp:ListItem Value="">ALL</asp:ListItem>
                                <asp:ListItem Value="Classified">Classified</asp:ListItem>
                                <asp:ListItem Value="Part-Time Hourly">Part-Time Hourly</asp:ListItem>
                                <asp:ListItem Value="Tenure-Track Faculty">Tenure-Track Faculty</asp:ListItem>
                                <asp:ListItem Value="Non Tenure-Track Faculty">Non Tenure-Track Faculty</asp:ListItem>
                                <asp:ListItem Value="Adjunct Faculty">Adjunct Faculty</asp:ListItem>
                                <asp:ListItem Value="Professional/Confidential Exempt">Professional/Confidential Exempt</asp:ListItem>
                                <asp:ListItem Value="Administrator/Executive">Administrator/Executive</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="col" style="width: 30%">
                            <strong>Official Position Title</strong><br />
                            <asp:TextBox ID="txtSearchOfficialTitle" runat="server" Style="width: 100%"></asp:TextBox>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col" style="width: 20%">
                            <strong>College/Unit</strong><br />
                            <asp:DropDownList ID="cboSearchCollegeUnit" runat="server" Style="width: 117px"></asp:DropDownList>
                        </div>
                        <div class="col" style="width: 37%">
                            <strong>Department</strong><br />
                            <asp:DropDownList ID="cboSearchDepartment" runat="server" Style="width: 214px"></asp:DropDownList>
                        </div>
                        <div class="col" style="width: 30%">
                            <strong>Type of Recruitment</strong><br />
                            <asp:DropDownList ID="cboSearchRecruitmentType" runat="server" Style="width: 175px">
                                <asp:ListItem Value="">ALL</asp:ListItem>
                                <asp:ListItem Value="Promotional">Promotional</asp:ListItem>
                                <asp:ListItem Value="Internal">Internal</asp:ListItem>
                                <asp:ListItem Value="Open Competitive">Open Competitive</asp:ListItem>
                                <asp:ListItem Value="No Recruitment">No Recruitment</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col" style="width: 46%">
                            <strong>Creation Date</strong><br />
                            <input type="text" id="txtCreationStartDate" name="txtCreationStartDate" class="datepicker" runat="server" maxlength="10" style="width: 107px" placeholder="Start Date" />
                            <strong>&mdash;</strong>
                            <input type="text" id="txtCreationEndDate" name="txtCreationEndDate" class="datepicker" runat="server" maxlength="10" style="width: 107px" placeholder="End Date" />
                        </div>
                        <div class="col" style="width: 46%">
                            <strong>Completion Date</strong><br />
                            <input type="text" id="txtCompletionStartDate" name="txtCompletionStartDate" class="datepicker" runat="server" maxlength="10" style="width: 107px" placeholder="Start Date" />
                            <strong>&mdash;</strong>
                            <input type="text" id="txtCompletionEndDate" name="txtCompletionEndDate" class="datepicker" runat="server" maxlength="10" style="width: 107px" placeholder="End Date" />
                        </div>
                    </div>
                    <div class="row" style="padding-top: 10px;">
                        <div class="col" style="width: 100%; text-align: center; margin-right: 0; padding-right: 0;">
                            <asp:Button ID="cmdSearch" runat="server" Text="Search" Style="width: 100px" OnClick="cmdSearch_Click" />
                            &nbsp;<input type="reset" value="Reset" style="width: 100px; margin-left: 10px;" onclick="location.href = 'Report.aspx?type=' + document.getElementById('maintextHolder_hidUserType').value.replace(' ', '')" />
                            &nbsp;<asp:Button ID="cmdExport" runat="server" Text="Export to Excel" Style="width: 100px; margin-left: 10px;" OnClick="cmdExport_Click" />
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <asp:Table ID="tblList" runat="server"></asp:Table>
        </asp:Panel>
        <asp:Panel ID="panView" runat="server">
            <div class="title">
                <div style="float: left; width: 68%;">
                    ORIGINATOR:&nbsp;<span class="normal"><asp:Label ID="lblOriginatorName" runat="Server"></asp:Label></span>
                    &nbsp;ctcLink ID:&nbsp;<span class="normal"><asp:Label ID="lblOriginatorID" runat="Server"></asp:Label></span>
                </div>
                <div style="float: right; text-align: right; width: 31%; margin-right: 5px;">
                    <asp:Panel ID="panTrackingNumber" runat="server">TRACKING #:&nbsp;<span class="normal"><asp:Label ID="lblTrackingNumber" runat="server"></asp:Label></span></asp:Panel>
                </div>
                <div class="clearer"></div>
            </div>
            <div class="section" style="border-bottom: none;">
                <div class="row">
                    <div class="col" style="width: 46%">
                        <strong>Official Position Title</strong><br />
                        <asp:Label ID="lblOfficialTitle" runat="Server" Style="width: 100%;"></asp:Label>
                    </div>
                    <div class="col" style="width: 46%">
                        <strong>Working Title</strong><br />
                        <asp:Label ID="lblWorkingTitle" runat="Server" Style="width: 100%;"></asp:Label>
                    </div>
                </div>
                <div class="row">
                    <div class="col" style="width: 46%;">
                        <strong>Job Code</strong><br />
                        <asp:Label ID="lblJobCode" runat="Server" Style="width: 100%;"></asp:Label>
                    </div>
                    <div class="col" style="width: 46%;">
                        <strong>Budget #</strong><br />
                        <asp:Label ID="lblBudgetNumber" runat="Server" Style="width: 100%;"></asp:Label>
                    </div>
                </div>
                <div class="row">
                    <div class="col" style="width: 46%;">
                        <strong>Position Control #</strong><br />
                        <asp:Label ID="lblPositionNumber" runat="Server" Style="width: 100%;"></asp:Label>
                    </div>
                    <div class="col" style="width: 46%;">
                        <strong>College/Unit</strong><br />
                        <asp:Label ID="lblCollegeUnit" runat="server" Style="width: 100%;"></asp:Label>
                    </div>
                </div>
                <div class="row">
                    <div class="col" style="width: 46%;">
                        <strong>Department Name</strong><br />
                        <asp:Label ID="lblDepartmentName" runat="server" Style="width: 100%;"></asp:Label>
                    </div>
                    <div class="col">
                        <strong>Department #</strong><br />
                        <asp:Label ID="lblDepartmentID" runat="server" Style="width: 100%;"></asp:Label>
                    </div>
                </div>
                <div class="row">
                    <div class="col" style="width: 46%;">
                        <strong>Building #</strong><br />
                        <asp:Label ID="lblBuildingNumber" runat="Server" Style="width: 100%"></asp:Label>
                    </div>
                    <div class="col" style="width: 46%">
                        <strong>Room #</strong><br />
                        <asp:Label ID="lblRoomNumber" runat="server" Style="width: 100%"></asp:Label>
                    </div>
                </div>
                <div class="row">
                    <div class="col" style="width: 46%;">
                        <strong>Position Phone #</strong><br />
                        <asp:Label ID="lblPhoneNumber" runat="Server" Style="width: 100%"></asp:Label>
                    </div>
                    <div class="col" style="width: 46%">
                        <strong>Mail Stop</strong><br />
                        <asp:Label ID="lblMailStop" runat="server" Style="width: 100%"></asp:Label>
                    </div>
                </div>
                <div class="row">
                    <div class="col" style="width: 46%">
                        <strong>Immediate Supervisor</strong><br />
                        <asp:Label ID="lblSupervisorName" runat="Server" Style="width: 100%"></asp:Label>
                    </div>
                    <div class="col" style="width: 46%">
                        <strong>Supervisor's ctcLink ID</strong><br />
                        <asp:Label ID="lblSupervisorID" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div style="display: flex; overflow: hidden; border-bottom: 1px solid #aaa;">
                <div style="width: 50%;">
                    <div class="title" style="width: 100%">TYPE OF POSITION</div>
                    <div class="section" style="width: 100%; height: 100%;">
                        <div class="row">
                            <div id="newReplacement" class="col" style="padding-bottom: 5px;">
                                <asp:Label ID="lblNewReplacement" runat="server"><strong></strong></asp:Label>
                            </div>
                            <div id="employeeReplaced" runat="server" style="margin-left: 15px; padding-top: 10px;">
                                <strong>Name of Employee Replaced</strong><br />
                                <asp:Label ID="lblEmployeeReplaced" runat="server" Style="width: 70%"></asp:Label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col" style="width: 100%;">
                                <div class="col" style="text-align: left;">
                                    <strong>Type of Position</strong><br />
                                    <asp:Label ID="lblClassified" runat="server"></asp:Label>
                                    <asp:Label ID="lblPositionType" runat="server"></asp:Label>
                                </div>
                                <div class="col" id="endDate" runat="server" style="margin-left: 10px; padding-bottom: 10px;">
                                    <strong>End Date</strong><br />
                                    <asp:Label ID="lblEndDate" runat="server"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col" style="width: 100%;" id="recruitmentType">
                                <strong>Type of Recruitment</strong>
                                <div>
                                    <asp:Label ID="lblRecruitmentType" runat="server"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="padding-right: 5px; width: 48%">
                    <div class="title" style="width: 100%; padding-right: 5px;">ONBOARDING</div>
                    <div class="section" style="width: 100%; padding-right: 0; padding-bottom: 15px; height: 100%;">
                        <div class="row">
                            <div class="col">
                                <strong>This position will require:</strong>
                                <div style="padding-top: 10px; margin-left: 10px;">
                                    <asp:Label ID="lblOnboarding" runat="server"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="schedule" runat="server">
                <div class="title" style="border-top: none;">POSITION WORK SCHEDULE</div>
                <div class="section">
                    <div class="row">
                        <div class="col" style="width: 20%">
                            <strong>Hours Per Day</strong><br />
                            <asp:Label ID="lblHoursPerDay" runat="server" Style="width: 100%"></asp:Label>
                        </div>
                        <div class="col" style="width: 19%">
                            <strong>Hours Per Week</strong><br />
                            <asp:Label ID="lblHoursPerWeek" runat="server" Style="width: 100%"></asp:Label>
                        </div>
                        <div class="col" style="width: 19%">
                            <strong>Months Per Year</strong><br />
                            <asp:Label ID="lblMonthsPerYear" runat="server" Style="width: 100%"></asp:Label>
                        </div>
                        <div id="cyclicCalendarCode" runat="server" class="col" style="width: 25%; vertical-align: top; display: none;">
                            <strong>Cyclic Calendar Code</strong><br />
                            <asp:Label ID="lblCyclicCalendarCode" runat="server" Style="width: 100%"></asp:Label>
                        </div>
                    </div>
                    <div class="row" style="white-space: nowrap; padding-left: 0; padding-top: 10px;">
                        <div class="timeCol" style="width: 12%; text-align: center; padding-left: 0;">
                            <div style="padding-bottom: 3px;"><strong>Monday</strong></div>
                            <asp:Label ID="lblMonday" runat="server"></asp:Label>
                        </div>
                        <div class="timeCol" style="width: 13%; text-align: center;">
                            <div style="padding-bottom: 3px;"><strong>Tuesday</strong></div>
                            <asp:Label ID="lblTuesday" runat="server"></asp:Label>
                        </div>
                        <div class="timeCol" style="width: 14%; text-align: center;">
                            <div style="padding-bottom: 3px;"><strong>Wednesday</strong></div>
                            <asp:Label ID="lblWednesday" runat="server"></asp:Label>
                        </div>
                        <div class="timeCol" style="width: 13%; text-align: center;">
                            <div style="padding-bottom: 3px;"><strong>Thursday</strong></div>
                            <asp:Label ID="lblThursday" runat="server"></asp:Label>
                        </div>
                        <div class="timeCol" style="width: 13%; text-align: center;">
                            <div style="padding-bottom: 3px;"><strong>Friday</strong></div>
                            <asp:Label ID="lblFriday" runat="server"></asp:Label>
                        </div>
                        <div class="timeCol" style="width: 12%; text-align: center;">
                            <div style="padding-bottom: 3px;"><strong>Saturday</strong></div>
                            <asp:Label ID="lblSaturday" runat="server"></asp:Label>
                        </div>
                        <div class="timeCol" style="width: 12%; text-align: center;">
                            <div style="padding-bottom: 3px;"><strong>Sunday</strong></div>
                            <asp:Label ID="lblSunday" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
            <asp:Panel ID="panComments" runat="server">
                    <div class="title" style="border-top:none;">ORIGINATOR COMMENTS</div>
                    <div class="section">
                        <div style="padding: 0px 5px 5px 5px; text-align: left">
                            <asp:Label ID="lblOrigComments" runat="server"></asp:Label>
                        </div>
                    </div>
            </asp:Panel>
            <asp:Panel ID="panFileAttachments" runat="server">
                <div class="title" style="border-top: none;">FILE ATTACHMENTS</div>
                <div class="section">
                    <div style="padding: 0px 5px 5px 5px; text-align: left">
                        <asp:Label ID="lblAttachments" runat="server"></asp:Label>
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="panEmployeeInfo" runat="server">
                <div class="title" style="border-top: none;">EMPLOYEE INFORMATION</div>
                <div class="section">
                    <div class="row">
                        <div class="col" style="width: 25%">
                            <strong>Employee Name</strong><br />
                            <asp:Label ID="lblEmployeeName" runat="Server" Style="width: 100%;"></asp:Label>
                        </div>
                        <div class="col" style="width: 25%">
                            <strong>Employee ctcLink ID</strong><br />
                            <asp:Label ID="lblEmployeeID" runat="Server" Style="width: 100%;"></asp:Label>
                        </div>
                        <div class="col" style="width: 37%">
                            <strong>Employee Email Address</strong><br />
                            <asp:Label ID="lblEmployeeEmailAddress" runat="Server" Style="width: 100%;"></asp:Label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col" style="width: 25%">
                            <strong>Salary Range/Step</strong><br />
                            <asp:Label ID="lblSalaryRangeStep" runat="Server" Style="width: 100%;"></asp:Label>
                        </div>
                        <div class="col" style="width: 25%">
                            <strong>Salary Amount</strong><br />
                            <asp:Label ID="lblSalaryAmount" runat="Server" Style="width: 100%;"></asp:Label>
                        </div>
                        <div class="col" style="width: 37%">
                            <strong>Employee Type</strong><br />
                            <asp:Label ID="lblEmployeeType" runat="Server" Style="width: 100%;"></asp:Label>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="panApprovals" runat="server">
                <div style="padding: 10px"></div>
                <div class="title" style="padding-right: 5px; border-bottom: none">
                    <div style="float: left; width: 50%">ROUTING/APPROVALS</div>
                    <div style="float: right; text-align: right; width: 50%;">STATUS:&nbsp;<span class="normal"><asp:Label ID="lblStatus" runat="server">Recruiting</asp:Label></span></div>
                    <div class="clearer"></div>
                </div>
                <asp:Table ID="tblApprovals" runat="Server" Style="width: 100%" CellPadding="0"></asp:Table>
            </asp:Panel>
            <div style="width: 100%; text-align: center; padding-top: 20px">
                <input type="button" runat="server" value="Back" id="cmdBack" style="width: 70px; margin-right: 10px;" onclick="history.back(1)" />
                <input type="button" runat="server" id="cmdPrint" value="Print" style="width: 70px; margin-right: 10px;" onclick="openPage('print.aspx?id=' + document.getElementById('maintextHolder_hidID').value, 'PositionRequest', '1000', '800');" />
            </div>
        </asp:Panel>
        <asp:Panel ID="panError" runat="server">
            <asp:Label Style="color: red" ID="lblError" runat="server"></asp:Label>
        </asp:Panel>
        <asp:Panel ID="panMsg" runat="server">Please enter your search criteria and click "Search" to view the results.</asp:Panel>
    </form>
    <script type="text/javascript" src="Scripts/SortTable.js"></script>
    <script type="text/javascript">      
        var cboSearchDepartment = $("select[id$='cboSearchDepartment']");
        var hidSearchDepartment = $("input[id$='hidSearchDepartment']");

        cboSearchDepartment.change(function () {
            hidSearchDepartment.val($(this).val());
        });

        $.ajax({
            type: "POST",
            url: "/PositionRequest/WebService.asmx/PopulateDepartments",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {

                //remove all options from the department dropdownlist
                cboSearchDepartment.removeOption(/./);

                cboSearchDepartment.addOption("", "ALL");
                //populate the department dropdownlist
                $.each(response.d, function () {
                    if (hidSearchDepartment.val() == this["Value"]) {
                        cboSearchDepartment.addOption(this["Value"], this["Text"], true);
                    } else {
                        cboSearchDepartment.addOption(this["Value"], this["Text"], false);
                    }
                });
            }
        });
    </script>
</asp:Content>

